<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
    <?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
    <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/backup.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="location = '<?php echo $cancel; ?>';" class="n_button"><?php echo $button_cancel; ?></a></div>
    </div>
    
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-export"><?php echo $tab_export; ?><a href="#tab-import"><?php echo $tab_import; ?></a><a href="#tab-option"><?php echo $tab_setting; ?></a><a href="#tab-tools"><?php echo $tab_tools; ?></a><a href="#tab-help"><?php echo $tab_help; ?></a></div>

<div id="tab-export">
<fieldset>
<form action="<?php echo $action_export; ?>" method="post" id="form_export" enctype="multipart/form-data" class="form-horizontal">
	
<!-- BEGIN Export Detail Data -->	

<table border="0"><tr><td style="vertical-align: top;">

<legend><?php echo $text_export; ?></legend>

<input type="hidden" name="csv_export[file_format]" value="csv"
<!-- Languages -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_languages; ?></label>
    <div class="controls">
      <select class="span2" name="csv_export[language_id]">
        <?php foreach ($languages as $language) { ?>
        	<?php if ( $csv_export['language_id'] == $language['language_id'] ) { ?>
        	<option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
        	<?php } else { ?>
        	<option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
	    	<?php } ?>
	    <?php } ?>
      </select>
    </div>
  </div>

 <!-- Category -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_category; ?><br /><span class="help-inline"><?php echo $text_help_category; ?></span></label>
    <div class="controls">
	<div class="scrollbox" style="height: 160px;">
	    <?php $class = 'odd'; ?>
	    <?php foreach ($categories as $category) { ?>
		<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
		<div class="<?php echo $class; ?>">
		<input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" />
		<?php echo $category['name']; ?>
		</div>
	    <?php } ?>
	</div>
	<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
    
    </div>
</div>

<!-- Manufacturer -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_manufacturer; ?><br /><span class="help-inline"><?php echo $text_help_manufacturer; ?></span></label>
    <div class="controls">
	<div class="scrollbox" style="height: 160px;">
	    <?php $class = 'odd'; ?>
	    <?php foreach ($manufacturers as $manufacturer) { ?>
		<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
		<div class="<?php echo $class; ?>">
		<input type="checkbox" name="product_manufacturer[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
		<?php echo $manufacturer['name']; ?>
		</div>
	    <?php } ?>
	</div>
	<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
    </div>
</div>

<div class="control-group">
    <label class="control-label"><?php echo $entry_export_category; ?></label>
    <div class="controls">
      <select class="span3" name="csv_export[export_category]">
	        <option value="0"<?php if (!isset($csv_export['export_category']) || $csv_export['export_category'] == 0) { echo '  selected="selected"'; }?>><?php echo $text_no; ?></option>
	        <option value="1"<?php if ( isset($csv_export['export_category']) && $csv_export['export_category'] == 1 ) { echo '  selected="selected"'; }?>><?php echo $text_export_category_id; ?></option>
	        <option value="2"<?php if ( isset($csv_export['export_category']) && $csv_export['export_category'] == 2 ) { echo '  selected="selected"'; }?>><?php echo $text_export_category; ?></option>
	</select>
    </div>
  </div>
<!-- Delimiter Category -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_import_delimiter_category; ?></label>
    <div class="controls">
	<select class="span1" name="csv_export[delimiter_category]">
		<option value="|"<?php if($csv_export['delimiter_category'] == '|'){echo ' selected="selected"';}?>> | </option>
		<option value=","<?php if($csv_export['delimiter_category'] == ','){echo ' selected="selected"';}?>> , </option>
	</select>
    </div>
</div>

<!-- QTY -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_last_qty; ?></label>
    <div class="controls">
      <input type="text" class="span1" name="product_qty" value="0">
      <span class="help-inline"><?php echo $text_help_export_qty; ?></span>
    </div>
  </div>
 
 <!-- Limit -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_expot_limit; ?></label>
    <div class="controls">
      <input type="text" class="span1" name="csv_export[limit_start]" value="<?php echo $csv_export['limit_start']; ?>"> - <input type="text" class="span1" name="csv_export[limit_end]" value="<?php echo $csv_export['limit_end']; ?>">
      <span class="help-inline"><?php echo $text_help_export_limit; ?></span>
    </div>
  </div>
 
<div class="control-group">
    <label class="control-label"><?php echo $entry_export_gzcompress; ?></label>
    <div class="controls">
      <select class="span1" name="csv_export[gzcompress]">
		<?php if ($csv_export['gzcompress']) { ?>
	        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
	        <option value="0"><?php echo $text_no; ?></option>
	        <?php } else { ?>
	        <option value="1"><?php echo $text_yes; ?></option>
	        <option value="0" selected="selected"><?php echo $text_no; ?></option>
		<?php } ?>
	</select>
    </div>
  </div>
<!-- END Export Detail Data -->

</td>
<td style="padding-left: 20px; vertical-align: top;">
<legend><?php echo $entry_fields_set; ?></legend>

<div class="control-group">
	<table class="list table_fields_set">
<?php foreach( $csv_export['fields_set_data'] as $field ) { ?>
	<tr>
	<td>
		<label class="checkbox inline" title="<?php echo $field['uid']; ?>"><input class="fields_set" <?php	if (array_key_exists($field['uid'], $csv_export['fields_set']) || $field['uid'] == '_ID_') echo 'checked id="field_product_id"';	?> <?php	if ($field['uid'] == '_ID_') echo 'disabled="disabled"';	?> type="checkbox" name="csv_export[fields_set][<?php echo $field['uid']; ?>]" value="1"><?php echo $field['name']; ?></label>
	</td>
	<td><span class="help-inline"><?php echo $fields_set_help[$field['uid']]; ?></span></td>
	<td><span style="color: #003A88;"><?php echo $field['uid']; ?></span></td>
	</tr>
<?php } ?>
</table>
	<input type="hidden" name="csv_export[fields_set][_ID_]" value="1">
    <a onclick="$(this).parent().find(':checkbox').attr('checked', true);initFieldsSet();"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);initFieldsSet();"><?php echo $text_unselect_all; ?></a>
</div>	
	
</td>
</tr></table>

<hr>
    <div align="left"><a class="n_button" onclick="$('#form_export').submit();"><?php echo $button_export; ?></a></div>


</form>
</fieldset>
        </div>

<div id="tab-import">
<fieldset>
<form action="<?php echo $action_import; ?>" method="post" id="form_import" enctype="multipart/form-data" class="form-horizontal">

<table border="0"><tr><td style="vertical-align: top;">

<legend><?php echo $text_import; ?></legend>

<input type="hidden" name="csv_import[file_format]" value="csv"

<!-- IMPORT MODE -->
<div class="control-group">
	<label class="control-label"><?php echo $entry_import_mode; ?></label>
	<div class="controls">
		<select name="csv_import[mode]">
        	<option value="1" <?php if ( $csv_import['mode'] == 1 ) echo 'selected'; ?>><?php echo $text_import_mode_both; ?></option>
        	<option value="2" <?php if ( $csv_import['mode'] == 2 ) echo 'selected'; ?>><?php echo $text_import_mode_update; ?></option>
        	<option value="3" <?php if ( $csv_import['mode'] == 3 ) echo 'selected'; ?>><?php echo $text_import_mode_insert; ?></option>
      </select>
	</div>
</div>

<!-- Languages -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_languages; ?></label>
    <div class="controls">
      <select class="span2" name="csv_import[language_id]">
        <?php foreach ($languages as $language) { ?>
        	<?php if ( $csv_import['language_id'] == $language['language_id'] ) { ?>
        	<option value="<?php echo $language['language_id']; ?>" selected="selected"><?php echo $language['name']; ?></option>
        	<?php } else { ?>
        	<option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
	    	<?php } ?>
	    <?php } ?>
      </select>
    </div>
  </div>

<div class="control-group">
    <label class="control-label"><?php echo $entry_key_field; ?></label>
    <div class="controls">
      <select class="span2" name="csv_import[key_field]">
        <option value="_ID_"<?php if ($csv_import['key_field'] == '_ID_')echo ' selected="selected"';?>>Product ID</option>
        <option value="_MODEL_"<?php if ($csv_import['key_field'] == '_MODEL_')echo ' selected="selected"';?>>Product Model</option>
        <option value="_SKU_"<?php if ($csv_import['key_field'] == '_SKU_')echo ' selected="selected"';?>>Product SKU</option>
        <option value="_NAME_"<?php if ($csv_import['key_field'] == '_NAME_')echo ' selected="selected"';?>>Product Name</option>
      </select>
    </div>
  </div>

<!-- Delimiter Category -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_import_delimiter_category; ?></label>
    <div class="controls">
	<select class="span1" name="csv_import[delimiter_category]">
		<option value="|"<?php if($csv_import['delimiter_category'] == '|'){echo ' selected="selected"';}?>> | </option>
		<option value=","<?php if($csv_import['delimiter_category'] == ','){echo ' selected="selected"';}?>> , </option>
	</select>
    </div>
</div>

<!-- Fill Parent Category -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_import_fill_category; ?></label>
    <div class="controls">
	<select class="span1" name="csv_import[fill_category]">
		<?php if (isset($csv_import['fill_category']) && $csv_import['fill_category'] == 1) { ?>
	        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
	        <option value="0"><?php echo $text_no; ?></option>
	        <?php } else { ?>
	        <option value="1"><?php echo $text_yes; ?></option>
	        <option value="0" selected="selected"><?php echo $text_no; ?></option>
		<?php } ?>
	</select>
    </div>
</div>
<!-- Product OFF -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_import_product_disable; ?></label>
    <div class="controls">
		<select class="span1" name="csv_import[product_disable]">
		<?php if (isset($csv_import['product_disable']) && $csv_import['product_disable'] == 1) { ?>
	        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
	        <option value="0"><?php echo $text_no; ?></option>
	        <?php } else { ?>
	        <option value="1"><?php echo $text_yes; ?></option>
	        <option value="0" selected="selected"><?php echo $text_no; ?></option>
		<?php } ?>
		</select>
    </div>
</div>
<!-- Calc Mode -->
<div class="control-group">
	<label class="control-label"><?php echo $entry_import_calc_mode; ?></label>
	<div class="controls">
		<select class="span2" name="csv_import[calc_mode]">
			<option value="0" <?php if ( $csv_import['calc_mode'] == 0 ) echo 'selected'; ?>><?php echo $text_import_calc_mode_off; ?></option>
        	<option value="1" <?php if ( $csv_import['calc_mode'] == 1 ) echo 'selected'; ?>><?php echo $text_import_calc_mode_multiply; ?></option>
        	<option value="2" <?php if ( $csv_import['calc_mode'] == 2 ) echo 'selected'; ?>><?php echo $text_import_calc_mode_pluse; ?></option>
      </select>
	</div>
</div>  
<div class="control-group">
    <label class="control-label"><?php echo $entry_import_calc_value; ?></label>
    <div class="controls">
		<input class="span1" type="text" name="csv_import[calc_value][]" value="<?php if(isset($csv_import['calc_value'][0]))echo $csv_import['calc_value'][0];?>" size="2"/>
		<input class="span1" type="text" name="csv_import[calc_value][]" value="<?php if(isset($csv_import['calc_value'][1]))echo $csv_import['calc_value'][1];?>" size="2"/>
    </div>
</div>
 
 <!-- Iterator Mode -->
	<div class="control-group">
    <label class="control-label"><?php echo $entry_import_iter_limit; ?></label>
    <div class="controls">
  		<input class="span1" type="text" name="csv_import[iter_limit]" value="<?php echo $csv_import['iter_limit'];?>" size="2"/> <span class="help-inline"><?php echo $text_help_import_iter_limit; ?></span>
    </div>
  </div>

<!-- FILE -->
  <div class="control-group">
	<label class="control-label"><?php echo $entry_import; ?></label>
	<div class="controls">
    <input type="file" name="import" />
	</div>
    </div>
</td>

<td style="padding-left: 20px; vertical-align: top;">
<legend>&nbsp;</legend>
<!-- Manufacturer -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_import_manufacturer; ?></label>
    <div class="controls"><input type="hidden" value="0" name="csv_import[skip_manufacturer]">
    <label class="checkbox"><input type="checkbox" value="1" name="csv_import[skip_manufacturer]"<?php if($csv_import['skip_manufacturer']) echo ' checked';?> /> <?php echo $text_import_skip; ?></label>
	<select name="product_manufacturer">
		<option value="0" selected="selected"><?php echo $text_none; ?></option>
	    <?php foreach ($manufacturers as $manufacturer) { ?>
		<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
	    <?php } ?>
	</select>
    </div>
</div>
<?php if($core_type == 'ocstore') { ?>
<!-- Main Category -->
  <div class="control-group">
    <label class="control-label"><?php echo $entry_import_main_category; ?></label>
    <div class="controls"><input type="hidden" value="0" name="csv_import[skip_main_category]">
    <label class="checkbox"><input type="checkbox" value="1" name="csv_import[skip_main_category]"<?php if($csv_import['skip_main_category']) echo ' checked';?> /> <?php echo $text_import_skip; ?></label>
	<select name="main_category_id">
		<option value="0" selected="selected"><?php echo $text_none; ?></option>
	    <?php foreach ($categories as $category) { ?>
		<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
	    <?php } ?>
	</select>
    </div>
</div>
<?php } ?>
  <!-- Category -->
<div class="control-group">
    <label class="control-label"><?php echo $entry_import_category; ?></label>
    <div class="controls"><input type="hidden" value="0" name="csv_import[skip_category]">
    <label class="checkbox"><input type="checkbox" value="1" name="csv_import[skip_category]"<?php if($csv_import['skip_category']) echo ' checked';?> /> <?php echo $text_import_skip; ?></label>
	<div class="scrollbox" style="height: 400px;">
	    <?php $class = 'odd'; ?>
	    <?php foreach ($categories as $category) { ?>
		<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
		<div class="<?php echo $class; ?>">
		<input type="checkbox" name="product_category[]" value="<?php echo $category['category_id']; ?>" />
		<?php echo $category['name']; ?>
		</div>
	    <?php } ?>
	</div>
	<a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo $text_select_all; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo $text_unselect_all; ?></a>
    
    </div>
</div>

</td>
</tr>
</table>

</form>
</fieldset>
<hr>
<div align="left"><a class="n_button" onclick="$('#form_import').submit();"><?php echo $button_import; ?></a></div>
</div>

<div id="tab-option">

<form action="<?php echo $action_option; ?>" method="post" id="form_option" enctype="multipart/form-data" class="form-horizontal">
<fieldset>

<!-- BEGIN IMPORT SETTING -->
<legend><?php echo $text_import_setting; ?></legend>

<div class="control-group">
	<label class="control-label"><?php echo $entry_store; ?></label>
	<div class="controls">
              <div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array(0, $csv_setting['product_store'])) { ?>
                    <input type="checkbox" name="csv_setting[product_store][]" value="0" checked="checked" />
                    <?php echo $text_default; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="csv_setting[product_store][]" value="0" />
                    <?php echo $text_default; ?>
                    <?php } ?>
                  </div>
                  <?php foreach ($stores as $store) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($store['store_id'], $csv_setting['product_store'])) { ?>
                    <input type="checkbox" name="csv_setting[product_store][]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                    <?php echo $store['name']; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="csv_setting[product_store][]" value="<?php echo $store['store_id']; ?>" />
                    <?php echo $store['name']; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_tax_class; ?></label>
	<div class="controls">
              <select name="csv_setting[tax_class_id]">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $csv_setting['tax_class_id']) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
	</div>
</div>
<div class="control-group">
	<label class="control-label"><?php echo $entry_minimum; ?></label>
	<div class="controls">
		<input class="span1" type="text" name="csv_setting[minimum]" value="<?php echo $csv_setting['minimum']; ?>" size="2" />
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_subtract; ?></label>
	<div class="controls">
		<select class="span1" name="csv_setting[subtract]">
                  <?php if ($csv_setting['subtract']) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
		</select>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_stock_status; ?></label>
	<div class="controls">
		<select class="span2" name="csv_setting[stock_status_id]">
                  <?php foreach ($stock_statuses as $stock_status) { ?>
                  <?php if ($stock_status['stock_status_id'] == $csv_setting['stock_status_id']) { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
		</select>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_shipping; ?></label>
	<div class="controls">
		<select class="span1" name="csv_setting[shipping]">
                  <?php if ($csv_setting['shipping']) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <option value="0"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                  <?php } ?>
		</select>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_length; ?></label>
	<div class="controls">
		<select class="span2" name="csv_setting[length_class_id]">
                  <?php foreach ($length_classes as $length_class) { ?>
                  <?php if ($length_class['length_class_id'] == $csv_setting['length_class_id']) { ?>
                  <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_weight_class; ?></label>
	<div class="controls">
		<select class="span2" name="csv_setting[weight_class_id]">
                  <?php foreach ($weight_classes as $weight_class) { ?>
                  <?php if ($weight_class['weight_class_id'] == $csv_setting['weight_class_id']) { ?>
                  <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
	</div>
</div>

<div class="control-group">
	<label class="control-label"><?php echo $entry_status; ?></label>
	<div class="controls">
		<select class="span2" name="csv_setting[status]">
                  <?php if ($csv_setting['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
	</div>
</div>
<!-- END IMPORT SETTING -->

<hr>
<div align="left"><a class="n_button" onclick="$('#form_option').submit();"><?php echo $button_save; ?></a>

</fieldset>
</form>
        
    </div>
<div id="tab-tools">

<form action="<?php echo $action_tools; ?>" method="post" id="form_tools" enctype="multipart/form-data" class="form-horizontal">
<fieldset>
    <label><input type="checkbox" value="1" name="manufacturer" /> <?php echo $text_clear_manufacturer_cache; ?></label>
    <label><input type="checkbox" value="2" name="category" /> <?php echo $text_clear_category_cache; ?></label>
    <label><input type="checkbox" value="3" name="product" /> <?php echo $text_clear_product_cache; ?></label>
    <span class="help-inline"><?php echo $text_help_clear_manufacturer_cache; ?></span>
<hr>
<div align="left"><a class="n_button" onclick="$('#form_tools').submit();"><?php echo $button_apply; ?></a>	
</fieldset>
</form>
</div>


<div id="tab-help">
<?php echo $text_help; ?>
</div>
    </div>
<div style="padding:10px;text-align:center; color:#666666;"><strong>CSV Price Pro import/export v<?php echo $csvprice_pro_version; ?></strong></div>
  </div>
</div>
<div class="msg_error" style="display: none"></div>
<script type="text/javascript"><!--
function setBackgroundColor(obj) {
	if($(obj).attr('checked') == 'checked'){
		$(obj).parent().parent().parent().css('background-color', '#EEEEEE');
	} else {
		$(obj).parent().parent().parent().css('background-color', '');
	}
}

function initFieldsSet() {
	$('.fields_set').each(function() {
		setBackgroundColor(this);
	});
	$('#field_product_id').attr('checked', 'checked');
}

$(document).ready(initFieldsSet);

$('.fields_set').click(function(){
	setBackgroundColor(this);
});

$( ".msg_error" ).dialog({       
        modal: true,
        title: 'Error',
        resizable: false,
        autoOpen: false,
        buttons: {
            Close: function() {
                $(this).dialog('close');                                
            } 
        }
});

$('#tabs a').tabs();

$('#form_export').submit(function() {
	var status = 0;
	var msg = '';
	$('.fields_set:checked').each(function() {
			status ++;
	});
	
	if (status == 0) {
		msg = msg + "<br />" + '<?php echo $error_export_fields_set; ?>
		';
		}

		if (msg != '') {
		$('.msg_error div').empty();
		$('.msg_error').append('<div style="text-align:center;">'+msg+'<div>');
		$('.msg_error').dialog('open');
		return false;
		}

		return true;
		});
		//-->
</script>
<?php echo $footer; ?>