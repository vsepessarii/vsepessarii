<?php
	function getConfig($key) {
		$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM setting WHERE `key`="'.$key.'"'));
		return $tmp['value'];
	}

	function mb_ucfirst($str, $encoding='UTF-8')	{
		$str = mb_ereg_replace('^[\ ]+', '', $str);
		$str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding).
		mb_substr($str, 1, mb_strlen($str), $encoding);
		return $str;
	}	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru" xml:lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Счет на оплату</title>
<style type="text/css">
body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 10pt;
    width: 800px;
    max-width: 800px
}

h1 {
	font-size: 1.5em;	
}

.c1 {
    font-weight: bold;
    text-decoration: underline;
    padding-right: 15px
}
.goods {
    border-collapse: collapse;
    width: 100%
}
.goods td, .goods th {
    border: 1px solid #000;
}
.goods th {
    border: 1px solid #fff;
    border-bottom: 1px solid #000
}
.goods td {
    text-align: center;
    padding: 2px 5px
}

.left { text-align: left !important; }
.right { text-align: right !important; }

strong.summ_str::first-letter {
	text-transform: uppercase !important;
}
.ui-widget-overlay {
	opacity: 0.97 !important;
}

input.dialog {
	border: 1px solid #DDD;	
	margin: 3px 0;
}

.ui-dialog-titlebar-close {
  visibility: hidden;
}

.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix {
	background: none !important;
	background-color: #0081e5 !important;
	color: #FFF !important;
	-moz-border-radius: 0px !important;
	-webkit-border-radius: 0px !important;
	-khtml-border-radius: 0px !important;
	border-radius: 0px !important;	
}

.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only {
	background: none !important;
	background-color: #0081e5 !important;
	color: #FFF !important;
	margin: 4px !important;
	padding: 0 !important;
}

span.ui-button-text {
	line-height: 1.0 !important;
}

</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="/catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script>
  $(function() {
		$.mask.definitions['~']='[0-6,9]';
		$('input[name="phone"]').mask("+7 (999) 999-99-99");
	 
	  $( "#dialog-confirm" ).dialog({
			closeOnEscape: false,
   		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog || ui).hide(); },      
			resizable: false,
      width:500,
	height:340,
      modal: true,
      buttons: {
        "Показать счет": function() {
					var err = 0;
					var firm = $('input[name="firm"]').val(); if (firm == "") { $('input[name="firm"]').css('border' , '1px solid red'); err++; } else { $('input[name="firm"]').css('border' , '1px solid green'); }
					var inn = $('input[name="inn"]').val(); if (inn == "") { $('input[name="inn"]').css('border' , '1px solid red'); err++; } else { $('input[name="inn"]').css('border' , '1px solid green'); }
					var kpp = $('input[name="kpp"]').val(); if (kpp == "") { $('input[name="kpp"]').css('border' , '1px solid red'); err++; } else { $('input[name="kpp"]').css('border' , '1px solid green'); }
					var ogrn = $('input[name="ogrn"]').val(); if (ogrn == "") { $('input[name="ogrn"]').css('border' , '1px solid red'); err++; } else { $('input[name="ogrn"]').css('border' , '1px solid green'); }
					var address = $('input[name="address"]').val(); if (address == "") { $('input[name="address"]').css('border' , '1px solid red'); err++; } else { $('input[name="address"]').css('border' , '1px solid green'); }
					var face = $('input[name="face"]').val(); if (face == "") { $('input[name="face"]').css('border' , '1px solid red'); err++; } else { $('input[name="face"]').css('border' , '1px solid green'); }
					var rs = $('input[name="rs"]').val(); if (rs == "") { $('input[name="rs"]').css('border' , '1px solid red'); err++; } else { $('input[name="rs"]').css('border' , '1px solid green'); }
					var bank = $('input[name="bank"]').val(); if (bank == "") { $('input[name="bank"]').css('border' , '1px solid red'); err++; } else { $('input[name="bank"]').css('border' , '1px solid green'); }
					var bik = $('input[name="bik"]').val(); if (bik == "") { $('input[name="bik"]').css('border' , '1px solid red'); err++; } else { $('input[name="bik"]').css('border' , '1px solid green'); }
					
					/*
					var address = $('input[name="address"]').val();
					if (address == "") {
						$('input[name="address"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="address"]').css('border' , '1px solid green');
					}
					var phone = $('input[name="phone"]').val();
					if (phone == "") {
						$('input[name="phone"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="phone"]').css('border' , '1px solid green');
					}

					var inn = $('input[name="inn"]').val();
					if (inn == "") {
						$('input[name="inn"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="inn"]').css('border' , '1px solid green');
					}

					var rs = $('input[name="rs"]').val();
					if (rs == "") {
						$('input[name="rs"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="rs"]').css('border' , '1px solid green');
					}

					var bank = $('input[name="bank"]').val();
					if (bank == "") {
						$('input[name="bank"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="bank"]').css('border' , '1px solid green');
					}

					var bik = $('input[name="bik"]').val();
					if (bik == "") {
						$('input[name="bik"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="bik"]').css('border' , '1px solid green');
					}

					var okpo = $('input[name="okpo"]').val();
					if (okpo == "") {
						$('input[name="okpo"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="okpo"]').css('border' , '1px solid green');
					}

					var ogrn = $('input[name="ogrn"]').val();
					if (ogrn == "") {
						$('input[name="ogrn"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="ogrn"]').css('border' , '1px solid green');
					}

					var kpp = $('input[name="kpp"]').val();
					if (kpp == "") {
						$('input[name="kpp"]').css('border' , '1px solid red');
						err++;
					} else {
						$('input[name="kpp"]').css('border' , '1px solid green');
					}
					*/

					if (err > 0) return false;
					$('#client').html(firm + ', ИНН ' + inn + ', КПП ' + kpp + ', ' + address);
					$( this ).dialog( "close" );

					$.post('/ajax/save-rek-jur.php',{
						firm: firm,
						inn: inn,
						kpp: kpp,
						ogrn: ogrn,
						address: address,
						face: face,
						rs: rs,
						bank: bank,
						bik: bik
					});
 

					window.print();
        },
        "Отмена": function() {
          window.close();
        }
      }
    });
  });   
</script>
</head>
<body>
		<center style="font-size: 65%;">
			Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпускается
			по факту прихода денег на р/с Поставщика, самовывозом, при наличии доверенности и паспорта. 
		</center>
		<table class="goods">
			<tr>
				<td width="56%" colspan="2" rowspan="2" class="left">
					<?php echo getConfig('config_firm_bank_name');?><br />
					<em>Банк получателя</em>
				</td>
				<td width="10%"><em>БИК</em></td>
				<td width="34%"><?php echo getConfig('config_firm_bank_bik');?></td>
			</tr>
			<tr>
				<td><em>Сч. №</em></td>
				<td><?php echo getConfig('config_firm_bank_account');?></td>
			</tr>
			<tr>
				<td width="28%" class="left"><em>ИНН</em> <?php echo getConfig('config_firm_inn');?></td>
				<td width="28%" class="left"><?php echo (getConfig('config_firm_kpp') != '') ? '<em>КПП</em> '.getConfig('config_firm_kpp') : '<em>ОГРНИП</em> '.getConfig('config_firm_ogrnip'); ?></td>
				<td rowspan="2"><em>Сч. №</em></td>
				<td rowspan="2"><?php echo getConfig('config_firm_account');?></td>
			</tr>
			<tr>
				<td colspan="2" class="left">
					<?php echo getConfig('config_firm_name');?><br />
					<em>Получатель</em>
				</td>
			</tr>
		</table>
		<h1>Счет на оплату № <?php echo $order_id; ?> от <?php echo $date_added; ?> г.</h1>
		<hr />
		<table>
			<tr>
				<td width="14%" valign="top" class="left">Поставщик:</td>
				<td width="86%" valign="top" class="left"><?php echo getConfig('config_firm_name').', ИНН'.getConfig('config_firm_inn').((getConfig('config_firm_kpp') != '') ? ', КПП '.getConfig('config_firm_kpp') : '').((getConfig('config_firm_ogrnip') != '') ? ', ОРГНИП '.getConfig('config_firm_ogrnip') : '').', '.getConfig('config_firm_adres').', тел: '.getConfig('config_firm_tel')  ;?></td>
			</tr>
			<tr>
				<td valign="top" class="left">Покупатель:</td>
				<td valign="top" class="left" id="client"></td>
			</tr>
		</table>
		<br />
		
		
    <!--
    	$supplier_info - контакты Макса
    	$name.' '.$telehone - контакты клиента 
    -->
		
		<!--
		<table>
        <tbody>
            <tr>
                <td style="vertical-align:top"><span class="c1">Поставщик</span></td>
                <td><?php echo $supplier_info; ?></td>
            </tr>
            <tr>
                <td style="vertical-align:top"><span class="c1">Получатель</span></td>
                <td><?php echo $name; ?><br />тел. <?php echo $telephone; ?></td>
            </tr>
            <tr>
                <td style="vertical-align:top"><span class="c1">Плательщик</span></td>
                <td>тот самый</td>
            </tr>
            <tr>
                <td style="vertical-align:top"><span class="c1">Валюта</span></td>
                <td>Рубль</td>
            </tr>
        </tbody>
    </table>
		-->
    <table class="goods">
       	<!--
			  <thead>
            <tr>
                <th colspan="6">Счет-фактура № <?php echo $invoice; ?><br />от <?php echo $date_added; ?> г.</th>
            </tr>
        </thead>
        -->
        <tbody>
            <tr>
                <td width="6%">№</td>
                <td width="45%">Товары (работы, услуги)</td>
                <td width="9%">Кол-во</td>
                <td width="8%">Ед.</td>
                <td width="16%">Цена</td>
                <td width="16%">Сумма</td>
            </tr>
            <?php $i=0; foreach($products as $row) {
            	$manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT manufacturer.name FROM manufacturer, product WHERE product.manufacturer_id = manufacturer.manufacturer_id && product.product_id="'.$row['product_id'].'"'));
							$tmp = explode(':',$row['key']);
							unset($options);
							if ($tmp[1] != '') {
								$tmp = unserialize(base64_decode($tmp[1]));
								foreach ($tmp as $key => $value) {
									$name = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT option_description.name FROM option_description, product_option WHERE option_description.option_id = product_option.option_id && product_option.product_option_id='.$key));
									$val = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT option_value_description.name FROM option_value_description, product_option_value WHERE option_value_description.option_value_id = product_option_value.option_value_id && product_option_value.product_option_value_id='.$value));
									//echo 'SELECT option_description_value.name FROM option_value_description, product_option_value WHERE option_value_description.option_value_id = product_option_value.option_value_id && product_option_value.product_option_value_id='.$value.'<br>';
									//echo $name['name'].' '.$val['name'].'<br>';
									$options .= '<br>'.$name['name'].' '.$val['name'];
								}
							}
						?>
            <tr>
                <td><?php echo ++$i; ?></td>
                <td style="text-align:left"><?php echo $row['name'].' '.$manufacturer['name'].' '.$row['model'].'<span style="font-size:80%;">'.$options.'</span>'; ?></td>
                <td><?php echo $row['quantity']; ?></td>
                <td>шт.</td>
                <td><?php echo $this->currency->format($row['price'], 'RUR', '', FALSE); ?></td>
                <td><?php echo $this->currency->format($row['total'], 'RUR', '', FALSE); ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total): ?>
            <tr>
                <?php if (strcasecmp($total['code'], 'sub_total') == 0): ?>
                <td colspan="5" style="text-align:right;border-left:1px solid #fff;border-bottom:1px solid #fff">Итого без НДС: </td>
                <td><?php echo sprintf('%01.2f',$total['value']); $sub_total = $total['value']; ?></td>
                <?php elseif (strcasecmp($total['code'], 'total') == 0): ?>
                <td colspan="5" style="text-align:right;font-weight:bold;border:1px solid #fff;border-right:1px solid #000">Итого к оплате: </td>
                <td><strong><?php $total = $total['value']; echo sprintf('%01.2f',$total); ?></strong></td>
                <?php else: ?>
                <td colspan="5" style="text-align:right;border:1px solid #fff;border-right:1px solid #000"><?php echo $total['title']; ?>: </td>
                <td><?php echo $this->currency->format($total['value'], 'RUR', '', FALSE); if (strcasecmp($total['code'], 'shipping') == 0) $shipping = $total['value']; ?></td>
                <?php endif; ?>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table>
        <tbody>
        <tr>
            <td>
							Всего наименований <?php echo $i;?>, на сумму <?php echo sprintf('%01.2f',$sub_total); ?> руб.<br />
							Доставка на сумму <?php echo sprintf('%01.2f',$shipping); ?> руб.
							
						</td>
        </tr>
        <tr>
            <td><strong class="summ_str"><?php echo mb_ucfirst(num2str($total));?></strong></td>
        </tr>
        </tbody>
    </table>
    <hr />
    <div style="position: relative;z-index: 1;text-align: right;margin-top: -30px">
        <img src="/image/stamp.png" alt="печать" height="auto" width="auto" style="margin-right: 120px;" />
        <table style="position: absolute;z-index: 2;left: 0;top: 40%; width:100%;">
            <tbody>
            <tr>
                <td width="40%" style="text-align: left;"><!--Руководитель: __________________________________--></td>
                <td width="60%">_________________________________ /Васильев М.А./</td>
            </tr>
            <tr>
                <td style="text-align: center"><?php echo $who_wrote_bill; ?></td>
                <td style="text-align: center"><?php echo $who_wrote_bill; ?></td>
            </tr>
            <!--
						<tr>
                <td><strong>Счет действителен для оплаты до <?php echo $deadline; ?></strong></td>
            </tr>
            -->
            </tbody>
        </table>
    </div>
<!--strong>Счет действителен для оплаты до <?php echo $deadline; ?></strong-->        

<div id="dialog-confirm" title="Введите реквизиты для формирования счета" style="">
  <!--
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
	-->
	<span style="font-size: 80%;">
  	<span style="display: inline-block; width: 120px;">Наименование:</span><input class="dialog" name="firm" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">ИНН:</span><input class="dialog" name="inn" maxlength="12" style="width: 340px;" /><br />
  	<span style="display: inline-block; width: 120px;">КПП:</span><input class="dialog" name="kpp" maxlength="99" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">ОГРН:</span><input class="dialog" name="ogrn" maxlength="99" style="width: 340px;" /><br />
  	<span style="display: inline-block; width: 120px;">Полный адрес:</span><input class="dialog" name="address" style="width: 340px;" /><br />
  	<span style="display: inline-block; width: 120px;">В лице:</span><input class="dialog" name="face" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">Р/с:</span><input class="dialog" name="rs" maxlength="20" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">Банк:</span><input class="dialog" name="bank" maxlength="999" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">БИК:</span><input class="dialog" name="bik" maxlength="99" style="width: 340px;" /><br />
	<!--	
	<span style="display: inline-block; width: 120px;">Телефон:</span><input class="dialog" name="phone" placeholder="+7 (495) 123-45-67" style="width: 340px;" /><br />
	<span style="display: inline-block; width: 120px;">ОКПО:</span><input class="dialog" name="okpo" maxlength="99" style="width: 340px;" /><br />
	-->
  	Внимание! Все реквизиты обязательны для заполнения!
  </span>
</div>

</body>
</html>