					<aside id="cart" class="cart-wrapper">
						<a href="/index.php?route=checkout/simplecheckout"><div style="width:60px; height:60px; display: inline-block;"></div></a>
						<div class="heading" style="display: inline-block;">
							<p style="text-align:left !important" class="title"><?php echo $heading_title; ?></p>
							<a><span id="cart-total"><?php echo $text_items; ?></span></a>
						</div>
						<!--пустая корзина-->
						<!--<div class="content">
							<div class="empty">Your shopping cart is empty!</div>
						</div>-->
						<!--корзина с товаром-->
						<div class="content">
						 	<?php if ($products || $vouchers) { ?>
							<div class="mini-cart-info">
								<table>
									<tbody>
										<?php foreach ($products as $product) { ?>
										<tr>
											<td class="image">
												<?php if ($product['thumb']) { ?>
									            <a href="<?php echo $product['href']; ?>">
									            	<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
									            </a>
									            <?php } ?>
											</td>
								            <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
								            <div>
								              <?php foreach ($product['option'] as $option) { if ($option['option_id'] == 24 && $option['act_id'] == 0) continue; ?>
								              - <small><?php echo (($option['option_id'] == 24) ? 'Товар по акции: ' : $option['name']).' '.$option['value'].(($option['price'] != 0) ? ' ('.floatval($option['price']).' руб.)' : ''); ?></small><br />
								              <?php } ?>
								            </div></td>
								            <td class="quantity">x&nbsp;<?php echo $product['quantity']; ?></td>
								            <td class="total"><?php echo $product['total']; ?></td>
								            <td class="remove">
								            	<img src="catalog/view/theme/brief/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $product['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $product['key']; ?>' + ' #cart > *');" />
								            </td>
										</tr>
										<?php } ?>
        								<?php foreach ($vouchers as $voucher) { ?>
    								    <tr>
								          <td class="image"></td>
								          <td class="name"><?php echo $voucher['description']; ?></td>
								          <td class="quantity">x&nbsp;1</td>
								          <td class="total"><?php echo $voucher['amount']; ?></td>
								          <td class="remove">
								          	  <img src="catalog/view/theme/brief/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $voucher['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $voucher['key']; ?>' + ' #cart > *');" />
								          </td>
								        </tr>
        								<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="mini-cart-total">
								<table>
									<tbody>
										<?php foreach ($totals as $total) { ?>
										<tr>
										    <td class="right"><b><?php echo $total['title']; ?>:</b></td>
								            <td class="right"><?php echo $total['text']; ?></td>
								        </tr>
								        <?php } ?>
									</tbody>
								</table>
							</div>
							<div class="checkout">
								<!-- <a href="<?php echo $cart; ?>" class="button blue"><?php echo $text_cart; ?></a> -->
								<a href="<?php echo $checkout; ?>" class="button blue"><?php echo $text_checkout; ?></a>
							</div>
						 	<?php } else { ?>
						    <div class="empty"><?php echo $text_empty; ?></div>
						    <?php } ?>
						</div>
					</aside>

					<script language="javascript">
					    $('#clearcart_button').replaceWith('<a onclick="" id="clearcart_button" class="button">' + $('#clearcart_button').html() + '</a>'); 
    
    $('#clearcart_button').click(function () {
        $.ajax({
            type: 'post',
            url: 'index.php?route=module/cart/clear',
            dataType: 'html',
            data: $('#clearcart :input'),
            success: function (html) {
                $('#module_cart .middle').html(html);
            },
        });            
    });
					</script>
<?php /* ?>
<div id="cart">
  <div class="heading">
    <h4><?php echo $heading_title; ?></h4>
    <a><span id="cart-total"><?php echo $text_items; ?></span></a></div>
  <div class="content">
    <?php if ($products || $vouchers) { ?>
    <div class="mini-cart-info">
      <table>
        <?php foreach ($products as $product) { ?>
        <tr>
          <td class="image"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
          <td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <div>
              <?php foreach ($product['option'] as $option) { ?>
              - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small><br />
              <?php } ?>
            </div></td>
          <td class="quantity">x&nbsp;<?php echo $product['quantity']; ?></td>
          <td class="total"><?php echo $product['total']; ?></td>
          <td class="remove"><img src="catalog/view/theme/default/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $product['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $product['key']; ?>' + ' #cart > *');" /></td>
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr>
          <td class="image"></td>
          <td class="name"><?php echo $voucher['description']; ?></td>
          <td class="quantity">x&nbsp;1</td>
          <td class="total"><?php echo $voucher['amount']; ?></td>
          <td class="remove"><img src="catalog/view/theme/default/image/remove-small.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=<?php echo $voucher['key']; ?>' : $('#cart').load('index.php?route=module/cart&remove=<?php echo $voucher['key']; ?>' + ' #cart > *');" /></td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="mini-cart-total">
      <table>
        <?php foreach ($totals as $total) { ?>
        <tr>
          <td class="right"><b><?php echo $total['title']; ?>:</b></td>
          <td class="right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <div class="checkout"><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a> | <a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></div>
    <?php } else { ?>
    <div class="empty"><?php echo $text_empty; ?></div>
    <?php } ?>
  </div>
</div>
<?php */ ?>