<?php
$_['heading_title']      = 'Яндекс деньги ++';
$_['entry_login']        = 'Номер счета я.денег';
$_['entry_password']     = 'Секретное слово';
$_['entry_komis'] 		 = 'Комиссия с покупателя';
$_['entry_maxpay'] 		 = 'Максимальная сумма заказа(ели больше то метод не отображается)';
$_['entry_on_status']    = 'Статус заказа после неуспешной или ожидаемой оплаты';
$_['entry_order_status'] = 'Статус после удачной оплаты';
$_['entry_geo_zone']     = 'Регион доступности оплаты';
$_['entry_status']       = 'Статус';
$_['entry_sort_order']   = 'Сортировка';
$_['entry_style']        = 'Вид кнопки под стиль темы';
$_['text_payment']       = 'Оплата';
$_['text_order']         = 'Заказы';
$_['status_title']       = 'Оплаченные заказы';
$_['text_success']       = 'Настройки сохранены';
$_['text_my']       	 = 'Свой';
$_['text_default']       = 'По умолчанию';
$_['entry_yandexplusplus_instruction'] = 'Инструкция при оформлении заказа:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_yandexplusplus_instruction_tab']      = 'Использвать инструкцию при оформлении заказа?';
$_['entry_yandexplusplus_mail_instruction_tab']      = 'Использовать инструкцию в письме с заказом?';
$_['entry_yandexplusplus_mail_instruction']      = 'Инструкция в письме с заказом:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/> а также html теги.</small>';
$_['entry_yandexplusplus_success_comment_tab']      = 'Использовать комметарий покупателю в письме о успешной оплате?';
$_['entry_yandexplusplus_success_comment']      = 'Комметарий покупателю в письме о успешной оплате:<br/><small>Поддерживает переменные:<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа</small>';
$_['entry_yandexplusplus_name_tab']      = 'Текст в название метода оплаты?';
$_['entry_yandexplusplus_name']      = 'Название метода оплаты';
$_['entry_yandexplusplus_success_alert_admin_tab']      = 'Письмо администратору при успешной оплате';
$_['entry_yandexplusplus_success_alert_customer_tab']      = 'Письмо покупателю при успешной оплате';
$_['entry_yandexplusplus_success_page_tab']      = 'Свой текст на странице успешной оплаты?';
$_['entry_yandexplusplus_success_page_text']      = 'Текст на странице успешной оплаты:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_yandexplusplus_waiting_page_tab']      = 'Свой текст на странице ожидаемой оплаты?';
$_['entry_yandexplusplus_waiting_page_text']      = 'Текст на странице ожидаемой оплаты:<br/><small>Поддерживает переменные:<br/>$href$ - ссылка на оплату<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';
$_['entry_button_later']      = 'Кнопка оплатить позже в оформлении заказа';

$_['entry_yandexplusplus_hrefpage_tab']      = 'Свой текст на странице после перехода по ссылки из письма или из личного кабинета?';
$_['entry_yandexplusplus_hrefpage']      = 'Текст на странице после перехода по ссылки из письма или из личного кабинета:<br/><small>Поддерживает переменные:<br/>$orderid$ - номер заказа<br/>$itogo$ - сумма заказа<br/>$komis$ - комисcия в процентах<br/>$total-komis$ - посчитанная комиссия от суммы<br/>$plus-komis$ - сумма с комиссией<br/>а также html теги.</small>';

$_['entry_license'] = 'Лицензионный ключ';

$_['error_license']    = 'Введите Лицензионный ключ!<br/><small>Купить ключ можно отправив запрос на ashap08@ya.ru</small>';
$_['error_key_er']     = 'Неправильный Лицензионный ключ!<br/><small>Ключ действителен только для одного домена. Купить ключ можно отправив запрос на ashap08@ya.ru</small>';
$_['error_login']      = 'Введите номер счета';
$_['error_password']   = 'Введите секретное слово';

?>