<?php
require 'connect.php';
require 'functions.php';
use PhpOffice\PhpSpreadsheet\IOFactory;

error_reporting(0);

mb_internal_encoding('utf-8');
ini_set('display_errors', FALSE);
ini_set('display_startup_errors', FALSE);

if ($ro_user['user_group_id'] != 1) die('Access denied');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Europe/London');

//echo date('H:i:s') , " Load from Excel5 template" , EOL;

//$objReader = PHPExcel_IOFactory::createReader('Excel5');
$phpSpreadSheet = IOFactory::load("templates/torg12.xls");

//$phpSpreadSheet->setActiveSheetIndex(0);
//$phpSpreadSheet->getActiveSheet()->setCellValue('BC40', '77.20');

$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');

$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
$nu_product = @mysqli_num_rows(@mysqli_query($ddb, $qu_product));
$re_product = @mysqli_query($ddb, $qu_product);

$order_date = date('d').' '.$MonthRod[date('m')].' '.date('Y').' г.';
$client = $ro_order['firstname'].' '.$ro_order['lastname'].' '.$ro_order['telephone'].' '.$ro_order['shipping_city'].', '.$ro_order['shipping_address_1'];

$phpSpreadSheet->getActiveSheet()->setCellValue('AG28', 'P-'.$ro_order['order_id']);
$phpSpreadSheet->getActiveSheet()->setCellValue('AY21', 'P-'.$ro_order['order_id']);
$phpSpreadSheet->getActiveSheet()->setCellValue('AY22', $order_date);
$phpSpreadSheet->getActiveSheet()->setCellValue('AL28', $order_date);
$phpSpreadSheet->getActiveSheet()->setCellValue('I12', $client);
$phpSpreadSheet->getActiveSheet()->setCellValue('I18', 'тот же');

//увеличиваем количество товарных строк, если надо
if ($nu_product >1) {
	$phpSpreadSheet->getActiveSheet()->insertNewRowBefore(35,$nu_product-1);

	for ($i=35;$i<35+$nu_product;$i++) {
		$phpSpreadSheet->getActiveSheet()->mergeCells('BC'.$i.':BF'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AY'.$i.':BB'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AV'.$i.':AX'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AS'.$i.':AU'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AP'.$i.':AR'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AM'.$i.':AO'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AJ'.$i.':AL'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AH'.$i.':AI'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AE'.$i.':AG'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('AB'.$i.':AD'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('Y'.$i.':AA'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('V'.$i.':X'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('S'.$i.':U'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('E'.$i.':R'.$i);
		$phpSpreadSheet->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
		//$phpSpreadSheet->getActiveSheet()->duplicateStyle($phpSpreadSheet->getActiveSheet()->getStyle ('BC34'), 'BC'.$i);
		//$phpSpreadSheet->getActiveSheet()->duplicateStyle($phpSpreadSheet->getActiveSheet()->getStyle ('B34:BC34'), 'B'.$i.':BC'.$i);
	}

}
//заполняем товарные строки
$i = 34; $arr =[];
while ($ro_product = @mysqli_fetch_array($re_product)) {
	$ro_prod = Result($ddb, 'SELECT * FROM product WHERE product_id = "'.$ro_product['product_id'].'"');
	$phpSpreadSheet->getActiveSheet()->setCellValue('B'.$i, $i-33); //номер по порядку
	$phpSpreadSheet->getActiveSheet()->setCellValue('E'.$i, ProductTitle($ddb, $ro_product['product_id'])); //наименование товара
	//$phpSpreadSheet->getActiveSheet()->setCellValue('S'.$i, $ro_prod['sku']); //код товара
	$phpSpreadSheet->getActiveSheet()->setCellValue('S'.$i, '—'); //код товара
	$phpSpreadSheet->getActiveSheet()->setCellValue('V'.$i, 'шт'); //наименование ЕИ
	$phpSpreadSheet->getActiveSheet()->setCellValue('Y'.$i, '796'); //код ЕИ
	$phpSpreadSheet->getActiveSheet()->setCellValue('AB'.$i, '—'); //вид упаковки
	$phpSpreadSheet->getActiveSheet()->setCellValue('AE'.$i, '—'); //кол-во в одном месте
	$phpSpreadSheet->getActiveSheet()->setCellValue('AH'.$i, '—'); //кол-во мест
	$phpSpreadSheet->getActiveSheet()->setCellValue('AJ'.$i, '—'); //масса брутто
	$phpSpreadSheet->getActiveSheet()->setCellValue('AM'.$i, $ro_product['quantity']); //масса нетто
	$phpSpreadSheet->getActiveSheet()->setCellValue('AP'.$i, $ro_product['price']); //цена
	//$phpSpreadSheet->getActiveSheet()->setCellValue('AS'.$i, 2400.10); //сумма без НДС
	$phpSpreadSheet->getActiveSheet()->setCellValue('AS'.$i, '=AP'.$i.'*AM'.$i); //сумма без НДС
	$phpSpreadSheet->getActiveSheet()->setCellValue('AV'.$i, 'Без НДС'); //ставка НДС
	$phpSpreadSheet->getActiveSheet()->setCellValue('AY'.$i, 0); //сумма НДС
	$phpSpreadSheet->getActiveSheet()->setCellValue('BC'.$i, '=AS'.$i); //сумма с НДС
	$i++;
	array_push($arr, $ro_product);
}

//echo date('H:i:s') , " Add new data to the template" , EOL;
//$phpSpreadSheet->getActiveSheet()->duplicateStyle($phpSpreadSheet->getActiveSheet()->getStyle ('B34:BC34'), 'B35:BC39');

//считаем суммы
$phpSpreadSheet->getActiveSheet()->setCellValue('AH'.(34+$nu_product), '=SUM(AH34:AI'.(33+$nu_product).')'); //мест
$phpSpreadSheet->getActiveSheet()->setCellValue('AJ'.(34+$nu_product), '=SUM(AJ34:AL'.(33+$nu_product).')'); //масса брутто
$phpSpreadSheet->getActiveSheet()->setCellValue('AM'.(34+$nu_product), '=SUM(AM34:AO'.(33+$nu_product).')'); //масса нетто

$phpSpreadSheet->getActiveSheet()->setCellValue('AS'.(34+$nu_product), '=SUM(AS34:AU'.(33+$nu_product).')'); //сумма без НДС
$phpSpreadSheet->getActiveSheet()->setCellValue('AY'.(34+$nu_product), '=SUM(AY34:BB'.(33+$nu_product).')'); //сумма  НДС
$phpSpreadSheet->getActiveSheet()->setCellValue('BC'.(34+$nu_product), '=SUM(BC34:BF'.(33+$nu_product).')'); //сумма c НДС

//сумма прописью
$phpSpreadSheet->getActiveSheet()->setCellValue('K'.(46+$nu_product), mb_ucfirst(Summ2Str($phpSpreadSheet->getActiveSheet()->getCell('BC'.(34+$nu_product))->getCalculatedValue())).'. Без НДС.'); //сумма c НДС
$phpSpreadSheet->getActiveSheet()->setCellValue('AA'.(39+$nu_product), mb_ucfirst(Weight2Str($phpSpreadSheet->getActiveSheet()->getCell('AM'.(34+$nu_product))->getCalculatedValue()))); //масса нетто
$phpSpreadSheet->getActiveSheet()->setCellValue('AA'.(41+$nu_product), mb_ucfirst(Weight2Str($phpSpreadSheet->getActiveSheet()->getCell('AJ'.(34+$nu_product))->getCalculatedValue()))); //масса брутто
$phpSpreadSheet->getActiveSheet()->setCellValue('R'.(36+$nu_product), mb_ucfirst($NumPred['1'])); //количество листов
$phpSpreadSheet->getActiveSheet()->setCellValue('G'.(37+$nu_product), mb_ucfirst(Num2Str($nu_product))); //количество строк
$phpSpreadSheet->getActiveSheet()->setCellValue('G'.(41+$nu_product), mb_ucfirst(Num2Str($phpSpreadSheet->getActiveSheet()->getCell('AM'.(34+$nu_product))->getCalculatedValue()))); //количество мест

$phpSpreadSheet->getActiveSheet()->setCellValue('I'.(56+$nu_product), date('d/m/Y'));

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="torg12_'.$ro_order['order_id'].'.xlsx"');
header('Cache-Control: max-age=0');
//echo date('H:i:s') , " Write to Excel5 format" , EOL;

$objWriter = IOFactory::createWriter($phpSpreadSheet, 'Xlsx');
$objWriter->save('php://output');

