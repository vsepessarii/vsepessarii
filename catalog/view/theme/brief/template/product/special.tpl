<?php echo $header; ?>
<div class="content-container">
<section class="box">

    <div class="box-heading">
        <!--21:04:43--><div class="align-left"><?php echo $heading_title; ?></div>
        <div class="display">
            <span class="displayText"><?php echo $text_display; ?></span>
            <div class="gridListActive"></div>
            <a onclick="display('grid');">
                <div class="listView"></div>
            </a>
        </div>

        <div class="sort"><b><?php echo $text_sort; ?></b>
            <select onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>

    <nav class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a
            href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </nav>
    <?php echo $_SESSION['text_compare'];?>

    <?php if ($products) { /*?>
    <div class="product-filter">

        <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
        <div class="limit"><b><?php echo $text_limit; ?></b>
            <select onchange="location = this.value;">
                <?php foreach ($limits as $limits) { ?>
                <?php if ($limits['value'] == $limit) { ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>

        <div class="sort"><b><?php echo $text_sort; ?></b>
            <select onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>
    </div>

    <?php */ ?>

    <div class="product-list">
        <?php $i=0; foreach ($products as $product) { $i++; ?>
        <div class="one-third<?php echo ($i % 3 == 0)? " last":"";?>">

			<article class="box-content product-item<?php echo ($product['special'] || true)?" action":""; echo $product['sale'].$product['gift'];?>">

        <div class="left">
            <?php if ($product['thumb']) { ?>
            <div class="image">
                <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>"/>
                </a>
            </div>
            <?php } ?>
            <div class="name">
                <?php if (isset($product['category']) && false) { ?>
                <h6><a href="<?php echo $product['category_href']; ?>"><?php echo $product['category']; ?></a> &raquo; <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
                <?php } else { ?>
                <h6 style="height: 40px; margin: 0;"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
                <?php } ?>
                <?php if (isset($product['manufacturer']) && false) { ?>
                <h6>Производитель: <?php echo $product['manufacturer']; ?></h6>
                <?php } ?>
            </div>
            <div class="description"><?php echo $product['description']; ?></div>
        </div>

        <div class="right">
            <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
            <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>

            <?php if (!$product['special']) { ?>
            <div class="price"> <?php echo $product['price']; ?></div>
            <?php } else { ?>
            <div class="price">
							<div style="display: inline-block	; font-size: 14px; font-weight: bold; text-decoration: line-through; color: #000;"><?php echo intval($product['price']); ?></div>
							<?php echo $product['special']; ?>
						</div>
            <?php } ?>
            <?php /*if ($product['tax']) { ?>
            <br/>
            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
            <?php } */?>

            <?php /*if ($product['rating']) { ?>
            <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>"/></div>
            <?php } */?>

            <div class="cart">
               	<?php
															$incart = '';
															$tmp_cart_var_names  = array();
															foreach ($_SESSION['cart'] as $cart_name => $cart_value) {
																$tmp = explode(':',$cart_name);
																if ($product['product_id'] == $tmp[0]) $incart = 'incart';
															}

                              if($incart) { ?>
                              <a href="<?php echo $product['href']; ?>" class="button " onclick="addToCart('<?php echo $product['product_id']; ?>','1','prodspec'); return false;" id="addtocart_<?php echo $product['product_id']; ?>"><?php echo $button_cart; ?></a>
															<a href="/index.php?route=checkout/simplecheckout" class="button incart" id="incart_prodspec_<?php echo $product['product_id']; ?>" style="padding: 6px 12px 4px 16px;" title="В корзине"></a>
                              <?php } else { ?>
                              <a href="<?php echo $product['href']; ?>" class="button " onclick="addToCart('<?php echo $product['product_id']; ?>','1','prodspec'); return false;" id="addtocart_special_<?php echo $product['product_id']; ?>"><?php echo $button_cart; ?></a>
                              <a href="/index.php?route=checkout/simplecheckout" class="button incart" id="incart_prodspec_<?php echo $product['product_id']; ?>" style="padding: 6px 12px 4px 16px; display:none;" title="В корзине"></a>
                              <?php } ?>
					</div>
        </div>
        </article>
    </div>
    <?php } ?>
</div>

<div class="clear"></div>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } ?>
</section>

</div>

<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $footer; ?>