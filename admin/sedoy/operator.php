<?php
	//станции метро
	/*header('Content-Type: text/html; charset=utf-8');
	session_start();*/

	require 'connect.php';	
	
	if ($_GET['p'] == '') $_GET['p'] = 1;

	if ($ro_user['user_group_id'] == 1) {
		if (isset($_GET['act']) && $_GET['act'] == 'del') {
			$_GET['id'] = intval($_GET['id']);
			@mysqli_query($ddb, 'DELETE FROM operator WHERE operator_id="'.$_GET['id'].'"');
			$tpl['msg'] = 'Оператор удален.';
			$_GET['act'] = '';
			$_GET['id'] = '';
		}

		if(isset($_POST['act'])){
			switch ($_POST['act']) {
				case 'add':
					@mysqli_query($ddb, '
					INSERT INTO operator 
					SET	title="'.($_POST['title']).'"
				');
					//echo mysqli_error($ddb);
					$_GET['act'] = 'edit';
					$_GET['id'] = @mysqli_insert_id($ddb);
					$tpl['msg'] = 'Оператор " '.$_POST['title'].'" добавлен.';
					header('Location: ' . $_SERVER['REQUEST_URI']);
					exit();
					break;

				case 'edit':
					$_POST['id'] = intval($_POST['id']);
					@mysqli_query($ddb, '
					UPDATE operator
					SET	title="'.htmlspecialchars($_POST['title']).'"
					WHERE operator_id="'.$_POST['id'].'"
				');
					$_GET['act'] = 'edit';
					$_GET['id'] = $_POST['id'];
					$tpl['msg'] = 'Оператор "'.$_POST['title'].'" изменен.';
					break;
			}
		}
		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"><a href="'.$_SERVER['PHP_SELF'].'?act=add" class="button">Новый оператор</a></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br>
			'. ( isset($tpl) ? (($tpl['msg'] != '') ? '<div class="msg">'.$tpl['msg'].'</div>' : ''): '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="50%">
						<table class="list">
							<thead>
								<tr>
									<td width="5%"></td>
									<td width="95%">Оператор</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM operator ORDER BY title';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="javascript:conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.(isset($_GET['page']) ? $_GET['page']  : '' ).'&id='.$ro['operator_id'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['operator_id'].'" style="'.(isset($_GET['id']) ? (($ro['operator_id'] == $_GET['id']) ? 'color:red; ' : '') : '').'">'.$ro['title'].'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="50%">
		';
		if (isset($_GET['act']) && ($_GET['act'] == 'add' || $_GET['act'] == 'edit')) {
			if ($_GET['act'] == 'add') {
				//$tpl['meta_title'] = 'Добавить курьера';
				$tpl['body_title'] = 'Добавить оператора';
			} elseif ($_GET['act'] == 'edit') {
				//$tpl['meta_title'] = 'Обновить курьера';
				$tpl['body_title'] = 'Обновить оператора';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM operator WHERE operator_id="'.intval($_GET['id']).'"'));
			}
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.title.value == "") msg = msg + br + " - не заполнен оператор;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.(isset($_GET['act']) ? $_GET['act'] : '').'">
				<input type="hidden" name="id" value="'.(isset($_GET['act']) ? $_GET['act'] : '').'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Оператор:</td>
						<td width="85%" class="txt" align="left"><input name="title" id="title" title="Оператор" class="txt" style="width:350px;" value="'.$ro['title'].'"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="'.(($_GET['act'] == 'add') ? 'Добавить' : 'Обновить').' станцию метро"></td>
					</tr>
				</table>			
				</form>
			';
		} else {
			$tpl['body_title'] = 'Операторы';
		}
		$tpl['meta_title'] = 'Операторы';	
		require 'template.php';	
	} else {
		header('Location: /');
	}

