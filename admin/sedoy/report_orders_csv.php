<?php
	//отчет по заказам
	/*
	session_start();
	$ddb = @mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmed_brief');
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	*/

	require 'connect.php';

	if ($ro_user['user_group_id'] == 1) {
		$br = chr(13).chr(10);
		$ret = '';
		header('Content-Type: csv/plain; charset=cp-1251');
		header('Content-Disposition: attachment; filename="report_orders_'.date('Y-m-d H-i-s').'.csv"');
		$qu_order = '
			SELECT	*
			FROM 		tmp_report_order
			WHERE		1
							'.(($_SESSION['report_orders_id'] != '') ? ' && `ID` LIKE "%'.$_SESSION['report_orders_id'].'%"' : '').'
							'.(($_SESSION['report_orders_date_from'] != '') ? ' && `DATE`>="'.$_SESSION['report_orders_date_from'].'"' : '').'
							'.(($_SESSION['report_orders_date_to'] != '') ? ' && `DATE`<="'.$_SESSION['report_orders_date_to'].'"' : '').'
							'.(($_SESSION['report_orders_time_from'] != '') ? ' && `TIME`>="'.$_SESSION['report_orders_time_from'].'"' : '').'
							'.(($_SESSION['report_orders_time_to'] != '') ? ' && `TIME`<="'.$_SESSION['report_orders_time_to'].'"' : '').'
							'.(($_SESSION['report_orders_fio'] != '') ? ' && `FIO` LIKE "%'.$_SESSION['report_orders_fio'].'%"' : '').'
							'.(($_SESSION['report_orders_phone'] != '') ? ' && `PHONE` LIKE "%'.$_SESSION['report_orders_phone'].'%"' : '').'
							'.(($_SESSION['report_orders_email'] != '') ? ' && `EMAIL` LIKE "%'.$_SESSION['report_orders_email'].'%"' : '').'
							'.(($_SESSION['report_orders_price_in_from'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_orders_price_in_from'].'"' : '').'
							'.(($_SESSION['report_orders_price_in_to'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_orders_price_in_to'].'"' : '').'
							'.(($_SESSION['report_orders_price_out_from'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_orders_price_out_from'].'"' : '').'
							'.(($_SESSION['report_orders_price_out_to'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_orders_price_out_to'].'"' : '').'
							'.(($_SESSION['report_orders_price_delivery'] != '') ? ' && `PRICE_DELIVERY`="'.$_SESSION['report_orders_price_delivery'].'"' : '').'
							'.(($_SESSION['report_orders_total_from'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_orders_total_from'].'"' : '').'
							'.(($_SESSION['report_orders_total_to'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_orders_total_to'].'"' : '').'
							'.(($_SESSION['report_orders_margin_from'] != '') ? ' && `MARGIN`>="'.$_SESSION['report_orders_margin_from'].'"' : '').'
							'.(($_SESSION['report_orders_margin_to'] != '') ? ' && `MARGIN`<="'.$_SESSION['report_orders_margin_to'].'"' : '').'
							'.(($_SESSION['report_orders_profit_from'] != '') ? ' && `PROFIT`>="'.$_SESSION['report_orders_profit_from'].'"' : '').'
							'.(($_SESSION['report_orders_profit_to'] != '') ? ' && `PROFIT`<="'.$_SESSION['report_orders_profit_to'].'"' : '').'
							'.(($_SESSION['report_orders_profit_percent_from'] != '') ? ' && `PROFIT_PERCENT`>="'.$_SESSION['report_orders_profit_percent_from'].'"' : '').'
							'.(($_SESSION['report_orders_profit_percent_to'] != '') ? ' && `PROFIT_PERCENT`<="'.$_SESSION['report_orders_profit_percent_to'].'"' : '').'
							'.(($_SESSION['report_orders_delivery_method'] != '') ? ' && `DELIVERY_METHOD`="'.$_SESSION['report_orders_delivery_method'].'"' : '').'
							'.(($_SESSION['report_orders_payment_method'] != '') ? ' && `PAYMENT_METHOD`="'.$_SESSION['report_orders_payment_method'].'"' : '').'
							'.(($_SESSION['report_orders_status'] != '') ? ' && `STATUS`="'.$_SESSION['report_orders_status'].'"' : '').'
			ORDER	BY ID ASC
		';
		$re_order = @mysqli_query($ddb, $qu_order);
		$summ['price_in'] = 0;
		$summ['price_out'] = 0;
		$summ['price_delivery'] = 0;
		$summ['total'] = 0;
		$summ['margin'] = 0;
		$summ['tax'] = 0;
		$summ['courier_price'] = 0;
		$summ['profit'] = 0;
		$summ['profit_percent'] = 0;
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$summ['price_in'] += $ro_order['PRICE_IN'];
			$summ['price_out'] += $ro_order['PRICE_OUT'];
			$summ['price_delivery'] += $ro_order['PRICE_DELIVERY'];
			$summ['total'] += $ro_order['TOTAL'];
			$summ['margin'] += $ro_order['MARGIN'];
			$summ['tax'] += $ro_order['TAX'];
			$summ['courier_price'] += $ro_order['COURIER_PRICE'];
			$summ['profit'] += $ro_order['PROFIT'];
			$summ['profit_percent'] += $ro_order['PROFIT_PERCENT'];
			$ret .= '"'.$ro_order['ID'].'";"'.$ro_order['DATE'].'";"'.$ro_order['TIME'].'";"'.$ro_order['FIO'].'";"'.$ro_order['PHONE'].'";"'.
							$ro_order['EMAIL'].'";"'.$ro_order['PRICE_IN'].'";"'.$ro_order['PRICE_OUT'].'";"'.$ro_order['PRICE_DELIVERY'].'";"'.
							$ro_order['TOTAL'].'";"'.$ro_order['MARGIN'].'";"'.str_replace('.',',',$ro_order['TAX']).'";"'.$ro_order['COURIER'].'";"'.$ro_order['COURIER_PRICE'].'";"'.str_replace('.',',',$ro_order['PROFIT']).
							'";"'.str_replace('.',',',$ro_order['PROFIT_PERCENT']).'";"'.$ro_order['DELIVERY_METHOD'].'";"'.$ro_order['PAYMENT_METHOD'].'";"'.$ro_order['STATUS'].'"'.$br;			
		}
		//$summ['profit_percent'] = @round($summ['profit_percent']/$nu_order_total,2);
		echo iconv('utf-8','cp1251',$ret);
	} else {
		header('Location: /');
	}

