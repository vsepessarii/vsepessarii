CKEDITOR.plugins.add( 'info', {
    icons: 'info',
    init: function( editor ) {
        editor.addCommand( 'insertPHeadingH2', {
            exec: function( editor ) {
                editor.insertHtml( '<p class="h2">Heading</p><p>&nbsp;</p>' );
            }
        });

        editor.ui.addButton( 'info', {
            label: 'Insert Heading',
            command: 'insertPHeadingH2',
            toolbar: 'insert'
        });
    }
});
