<?php
	require 'connect.php';

	$option_id = 24;

	//чистим все упоминания о значениях опций и привязки типов и значений опций к товарам
	@mysqli_query($ddb, 'DELETE FROM option_value WHERE option_id='.$option_id);
	@mysqli_query($ddb, 'DELETE FROM option_value_description WHERE option_id='.$option_id);
	@mysqli_query($ddb, 'DELETE FROM product_option WHERE option_id='.$option_id);
	@mysqli_query($ddb, 'DELETE FROM product_option_value WHERE option_id='.$option_id);
	@mysqli_query($ddb, 'DELETE FROM action_sort');

	//заполняем пустое значение опции (чтобы клиент мог отказаться от подарка или акционного товара)
	@mysqli_query($ddb, '
			INSERT INTO	option_value
			SET			option_id='.$option_id.',
						image="data/no-action.png",
						sort_order="0"
	');
	$empty_option_value_id = @mysqli_insert_id($ddb);
	@mysqli_query($ddb, '
			INSERT INTO	option_value_description
			SET 		option_value_id='.$empty_option_value_id.',
						language_id=1,
						option_id='.$option_id.',
						name="Мне не нужен товар со скидкой!"
	');
	//заполняем значения опций (т.е. акционные и подарочные товары)
	$qu_action = '
		SELECT		*
		FROM		action
		GROUP BY	act_id
		ORDER BY	act_id
	';
	$re_action = @mysqli_query($ddb, $qu_action);
	while ($ro_action = @mysqli_fetch_array($re_action)) {
		$product_action = Result($ddb, '
			SELECT		product.product_id AS product_id,
						product_description.name AS name,
						(SELECT manufacturer.name FROM manufacturer WHERE manufacturer.manufacturer_id = product.manufacturer_id) AS manufacturer,
						product.model AS model,
						product.price AS price,
						product.image as image,
						(SELECT product_special.price FROM product_special WHERE product_special.product_id = product.product_id && (product_special.date_start <= NOW() || product_special.date_start = "0000-00-00") && (product_special.date_end >= NOW() || product_special.date_end = "0000-00-00") ORDER BY product_special.priority DESC) AS special
			FROM		product, product_description
			WHERE		product.product_id = product_description.product_id &&
						product.product_id = '.$ro_action['act_id'].'
		');

		@mysqli_query($ddb, '
			INSERT INTO	option_value
			SET			option_id='.$option_id.',
						image="'.$product_action['image'].'",
						sort_order="0"
		');
		$option_value_id = @mysqli_insert_id($ddb);
		@mysqli_query($ddb, '
			INSERT INTO	option_value_description
			SET 		option_value_id='.$option_value_id.',
						language_id=1,
						option_id='.$option_id.',
						name="'.mysqli_escape_string($product_action['name'].' '.$product_action['manufacturer'].' '.$product_action['model']).'",
						act_id='.$product_action['product_id'].',
						act_price='.floatval($product_action['price']).',
						act_special='.floatval($product_action['special']).'
		');
	}

	function ChildProducts($cat_id) {
		unset($products);
		$qu = 'SELECT product_id FROM product_to_category WHERE category_id IN('.implode(',',ChildCategories($cat_id)).') GROUP BY product_id';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_assoc($re)) $products[] = $ro['product_id'];
		return $products;
	}

	function InsertOptionAction($product_id, $action_id, $coeff, $delta, $sort) {
		global $option_id;
		global $empty_option_value_id;

		//смотрим, есть ли привязка товара к опции (вставляли ли ранее)
		$tmp = Result($ddb, 'SELECT * FROM product_option WHERE product_id='.$product_id.' && option_id='.$option_id);
		if ($tmp['product_id']) {
			$product_option_id = $tmp['product_option_id'];
		} else {
			@mysqli_query($ddb, 'INSERT INTO product_option SET product_id='.$product_id.', option_id='.$option_id.', required=1');
			$product_option_id = @mysqli_insert_id($ddb);
			//вставляем пустое значение (чтобы клиент мог отказаться
			@mysqli_query($ddb, '
						INSERT INTO	product_option_value
						SET 		product_option_id='.$product_option_id.',
									product_id='.$product_id.',
									option_id='.$option_id.',
									option_value_id='.$empty_option_value_id.',
									quantity=9999,
									subtract=1,
									price=0,
									price_prefix="+"
				');
			@mysqli_query($ddb, '
						INSERT INTO	action_sort
						SET 		option_id='.$option_id.',
									product_id='.$product_id.',
									option_value_id='.$empty_option_value_id.',
									sort_order="-1"
				');
		}
		//ищем опцию (акционный товар, который надо привязать)
		$product_action = Result($ddb, 'SELECT * FROM option_value_description WHERE option_id='.$option_id.' && act_id='.$action_id);
		$price = $product_action['act_price'];
		$special = $product_action['act_special'];
		//оригинальная цена акционного товара
		$price_orig = ($special) ? $special : $price;
		//цена акционного товара с учетом коэффициента и смещения
		$price_disc = $price_orig*$coeff+$delta;
		//echo $price_disc.'<br>';

		@mysqli_query($ddb, 'DELETE FROM product_option_value WHERE product_id='.$product_id.' && option_value_id='.$product_action['option_value_id']);
		@mysqli_query($ddb, 'DELETE FROM action_sort WHERE product_id='.$product_id.' && option_value_id='.$product_action['option_value_id']);

		@mysqli_query($ddb, '
						INSERT INTO	product_option_value
						SET 		product_option_id='.$product_option_id.',
									product_id='.$product_id.',
									option_id='.$option_id.',
									option_value_id='.$product_action['option_value_id'].',
									quantity=9999,
									subtract=1,
									price='.$price_disc.',
									price_prefix="+"
					');
		@mysqli_query($ddb, '
						INSERT INTO	action_sort
						SET 		option_id='.$option_id.',
									product_id='.$product_id.',
									option_value_id='.$product_action['option_value_id'].',
									sort_order='.$sort.'
					');
	}

	//2015-02-21 ПРОХОД ПО КАТЕГОРИЯМ
	$parent_id = 0;
	unset($actions);

	function ParseCategory($parent_id) {
		global $action;
		global $option_id;
		$qu_cat = 'SELECT category_id, parent_id FROM category WHERE status=1 && parent_id = '.$parent_id.' ORDER BY category_id';
		$re_cat = @mysqli_query($ddb, $qu_cat);
		while ($ro_cat = @mysqli_fetch_assoc($re_cat)) {
			//echo $ro_cat['category_id'];
			$qu_act = 'SELECT * FROM action WHERE category_id='.$ro_cat['category_id'];
			$re_act = @mysqli_query($ddb, $qu_act);
			while ($ro_act = @mysqli_fetch_assoc($re_act)) foreach (ChildProducts($ro_cat['category_id']) as $product_id)  InsertOptionAction($product_id, $ro_act['act_id'], $ro_act['coeff'], $ro_act['delta'], $ro_act['category_sort']);
			//$tmp = Result($ddb, 'SELECT * FROM action WHERE category_id='.$ro_cat['category_id']);
			//if ($tmp['category_id']) foreach (ChildProducts($ro_cat['category_id']) as $product_id)  InsertOptionAction($product_id, $tmp['act_id'], $tmp['coeff'], $tmp['delta'], $tmp['sort']);
			ParseCategory($ro_cat['category_id']);
		}
	}

	ParseCategory(0);

	//ПРОХОД ПО ТОВАРАМ
	$qu_prod = 'SELECT * FROM action WHERE product_id > 0';
	$re_prod = @mysqli_query($ddb, $qu_prod);
	while ($ro_prod = @mysqli_fetch_assoc($re_prod)) InsertOptionAction($ro_prod['product_id'], $ro_prod['act_id'], $ro_prod['coeff'], $ro_prod['delta'], $ro_prod['product_sort']);

	echo '<center>Пересчет акций (подарков) проведен.</center>';

