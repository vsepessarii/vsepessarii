// Mixin general functions
@import "mixin.less";

/* General */
body {
	font: 12px Arial, Helvetica, sans-serif;
	color: #000;
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: 100%;
	background-image: url(../image/bg.png);
}

p, dl, hr, h1, h2, h3, h4, h5, h6, ol, ul, pre, table, .h6, address, fieldset, figure, blockquote, .accordion, .tabs, .testimonial, .media-wrapper {
		margin-bottom: 20px;
}

p {
	margin-top: 10px;
	margin-bottom: 10px;
}

ul {
		list-style: none;
}
.fl {float: left;}
.fr {float: right;}

a {
	color: #000; cursor: pointer;
	&:hover {
		color: #0081e5;
	}
}

em {
	color: #0081e5;
	font-style: normal;
}

#notification {
  position: fixed;
  top: 0px;
  left: 0;
  width: 100%;
  text-align: center;
  z-index: 10000;
  opacity: 1;
}

input[type=text], input[type=password], textarea, select {
	border-width: 1px; 
	border-color: #bfbfbf #efefef #efefef #bfbfbf;
	border-style: solid;
	background-color: #f9f9f9;
	margin: 0;
}


/* Layout */
.layout-wrapper {
	width: 960px;
	margin: 0 auto;
}

.page-wrapper {
	width: 1000px;
	padding: 3px 10px 10px;
	margin: 0 auto;
	background-color: rgba(234, 234, 234, .5);
}

.page-container {
	border: 1px solid #ddd;
	background: #f9f9f9;
}

/* Top menu */
.top-line {
	height: 40px;
	background: #fff;
	border-bottom: 1px solid #f0f0f0;
}

.secondary-menu {
	margin: 0;
	padding-top: 10px;

	&.align-left {
		li {
			a {
				font-family: Arial, Helvetica, sans-serif;
				text-transform: none;
				font-size: 12px;
			}
		}
	}

	.director {
		margin-top: 3px;
		padding-right: 30px !important;
		/* font-weight: bold; */
		img {
			width: 26px;
		}
	}

	li {
		float: left;
		padding: 0px 4px 0 3px	;
		/* border-left: 1px solid #c2c2c2; */
		display: inline-block;
		height: 20px;
		vertical-align: middle;
		font-size:12px;
		&:first-child {
			border-left: none;
			padding-left: 0;
		}
		img {
			margin-top: -2px;
		}
		&:last-child {
			padding-right: 0;
		}
		a {
			/*
			font-family: 'Cuprum', sans-serif;
			font-size: 12px;
			*/
			/* text-transform: uppercase; */
			text-decoration: none;
			/* display: inline-block; */
		}
	}
}

/* Logo, Cart, Contact info */
.header {
	height: 118px;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	position: relative;
	.logo {
		margin: 15px 0 0;
	}
	.title {
		font: 24px/24px 'Cuprum', sans-serif;
		font-weight: bold;
		text-transform: uppercase;
		margin-bottom: 1px;
		text-align: right;
	}



	.cart-wrapper {
		position: absolute;
		top: 0;
		left: 400px;
		z-index: 100;
		background: url(../image/cart.png) left 15px no-repeat;
		padding-left: 0px; /* было 80 */
		padding-top: 22px;
		min-width: 300px;
		min-height: 72px;
		#cart-total {
			font-size: 14px;
			padding-right: 16px;
			background: url(../image/cart_arrow_down.png) right top no-repeat;
			cursor: pointer;
			em {
				color: #e2001a;
				font-family: 'Istok Web', sans-serif;
			}
		}
		.content {
			display: none;
			padding: 10px;
			background: #fff;
			.shadow("3px 3px 5px 0px rgba(0, 0, 0, 0.2)");
			min-height: 150px;
			.empty {
				padding-top: 50px;
				text-align: center;
			}
		}
		&.active {
			.content {
				display: block;
			}
		}
		.mini-cart-info {
			.box-sizing(border-box);
			table {
				border-collapse: collapse;
				width: 100%;
				margin-bottom: 5px;
			}
			.image {
				width: 1px;
				img {
					border: 1px solid #EEE;
					text-align: left;
				}
			}
			td {
				color: black;
				vertical-align: top;
				padding: 10px 5px;
				border-bottom: 1px solid #EEE;
				.total {
					text-align: right;
				}
			}
			.quantity {
				text-align: right;
			}
			.remove {
				text-align: right;
				img {
					cursor: pointer;
				}
			}
		}
		.mini-cart-total {
			.box-sizing(border-box);
			text-align: right;
			table {
				border-collapse: collapse;
				display: inline-block;
				margin-bottom: 5px;
			}
			td {
				padding: 4px;
				text-align: right;
			}

		}
		.checkout {
			.box-sizing(border-box);
			text-align: right;
			clear: both;
			.button {
				.buttons();
				.button-blue();
				display: inline-block;
			}
		}
	}
	.contacts-wrapper {
		background: url(../image/phone.png) left 25px no-repeat;
		padding-left: 3px;
		padding-top: 24px;
		float: left;
		min-height: 73px;
		.phones {
			/*font-family: 'Istoc Web', sans-serif;*/
			font-family: 'Cuprum', sans-serif;
			font-weight: bold;
			font-size: 19px;
			line-height: 21px;
			margin: 0px;
			color: #E2001A;
			text-align: right;
			span {
				color: green;
				font-weight: bold;
				font-size: 14px;
			}
		}
        a {
          text-decoration: none;
        }
	}
}

/* Main menu */
.menu-wrapper {
	background: url(../image/menu_bg.png);
	height: 45px;
	.main-menu {
		font-family: 'Cuprum', sans-serif;
		font-size: 15px;
		font-weight: bold;
		text-transform: uppercase;
		float: left;
		li {
			*height: 41px;
			float: left;
			*padding: 0 32px;
			position: relative;
			cursor: pointer;
			a {
				*margin: 13px 0 0;
				display: block;
				font-size: 15px;
				text-decoration: none;
				padding: 10px 15px 0px;
        height: 28px;
			}
			&:hover {
				*height: 38px;
				border-bottom: 3px solid #0081e5;
			}
			&:before {
				content: '';
				position: absolute;
				top: 0;
				left: 0;
				width: 3px;
				height: 100%;
				background: url(../image/menu_bullet_right.png) left 17px no-repeat;
			}
			&:after {
				content: '';
				position: absolute;
				top: 0;
				right: 0;
				width: 3px;
				height: 100%;
				background: url(../image/menu_bullet_left.png) left 17px no-repeat;
			}
			&:first-child {
				&:before {
					display: none;
				}
			}
			&:last-child {
				&:after {
					display: none;
				}
			}
			&.action {
				a {
					color: #e2001a;
				}
				&:hover {
					border-color: #e2001a;
				}
			}
		}
	}
	#search {
		background: #f9f9f9;
		border-width: 1px;
		border-style: solid;
		border-color: #eaeaea #fff #eaeaea #eaeaea;
        border-right-width: 0px;
		width: 198px;
		float: right;
		margin-top: 5px;
		input[type="text"] {
			border: none;
			background: transparent;
			padding-left: 5px;
			outline: 0;
			width: 130px;
			height: 18px;
			font-size: 12px;
		}
		.button-search {
			font-size: 10px;
			font-family: Arial, Helvetica, sans-serif;
			float: right;
			display: inline-block;
			margin: 0;
            margin-top: -1px;
			padding: 3px 12px;
			.gradient(#f9f9f9, #ededed);
			.shadow("0px 1px 1px rgba(0, 0, 0, 0.19), inset 0px 1px 1px #fff");
			border: 1px solid #eaeaea;
			cursor: pointer;
			height: 20px !important;
		}
	}
}

/* Breadcrumbs */
.breadcrumb {
	font: 11px 'Cuprum', sans-serif;
  text-transform: uppercase;
  width: 580px;
	height: 15px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	position: relative;
	margin-top: -2px;
	margin-bottom: 6px;
	float:left;
	/*clear: both;*/
	display: inline-block;
	a {
		display: inline-block;
		font: 11px 'Cuprum', sans-serif;
		text-transform: uppercase;
		text-decoration: none;
		margin: 0 5px;
		&:first-child {
			margin-left: 0;
		}
		&:last-child {
			/*color: #0081e5;*/
			position: relative;
			&:before, &:after {
				content: '';
				position: absolute;
				top: -8px;
				left: 50%;
				margin-bottom: 0px;
				margin-left: -5px;
				width: 0;
				height: 0;
				border-left: 5px solid transparent;
				border-right: 5px solid transparent;
				border-top: 5px solid rgba(0, 0, 0, 0.1);
			}
			&:after {
				margin-top: -2px;
				border-top: 5px solid white;
			}
		}
	}
}

/* Compare */
.product-compare {
  position: relative;
  margin-top: -2px;
  margin-bottom: 6px;
  /*clear: both;*/
  text-align: right;
  /*border:1px solid red;*/
  display: inline-block;
  float:right;
  a {
    display: inline-block;
    font: 11px 'Cuprum', sans-serif;
    text-transform: uppercase;
    text-decoration: none;
    margin: 0 5px;
    color: #0081e5;
  }

}

/* Content */
.content-wrapper {
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	padding-top: 20px;
	.heading {
		font: 16px 'Cuprum', sans-serif;
		font-weight: bold;
		text-transform: uppercase;
		color: #0081e5;
		margin-bottom: 10px;
	}
	&.sidebar-left {
		.content-container {
			float: right;
			padding-left: 0;
			width: 715px;
			.columns(225);
		}
		.sidebar {
			float: left;
			width: 225px;
			padding-left: 0px;
		}
	}
	&.sidebar-right {
		.content-container {
			float: left;
			padding-right: 0;
			width: 710px;
		}
		.sidebar {
			float: right;
			width: 220px;
			padding-right: 10px;
		}
	}
}

.bottom-bar {
	border-top: 1px solid #f0f0f0;
	background: #fff;
	padding: 15px;
	.styled {
		margin-bottom: 0;
		font-size: 11px;
	}
	.heading {
		font: 16px 'Cuprum', sans-serif;
		font-weight: bold;
		text-transform: uppercase;
		color: #0081e5;
		margin-bottom: 10px;
	}
}

/* Columns */
.one-half,
.one-third,
.two-third,
.one-fourth,
.two-fourth,
.three-fourth,
.one-fifth,
.two-fifth,
.three-fifth,
.four-fifth,
.one-sixth,
.two-sixth,
.three-sixth,
.four-sixth,
.five-sixth {
		float: left;
		margin-right: 20px;
		&.last { margin-right: 0px; }
}
.columns(-20);

/* Footer */
.footer {
	position: relative;
	width: 1000px;
	margin: 0 auto;
	height: 50px;
	.copyright {
		text-transform: uppercase;
		font: 12px 'Cuprum', sans-serif;
		font-weight: bold;
		margin-top: 23px;
		float: left;
	}
	.follow {
		float: right;
		margin-top: 23px;
		li {
			float: left;
			margin-left: 5px;
			width: 16px;
			height: 16px;
			background: url(../image/social_icons.png);
			a {
				display: block;
				height: 100%;
			}
		}
		.facebook {
			background-position: 0px 0px;
		}
		.vk {
			background-position: -21px 0px;
		}
		.g-plus {
			background-position: -42px 0px;
		}
		.mailru {
			background-position: -63px 0px;
		}
		.odnoklasniki {
			background-position: -84px 0px;
		}
		.twitter {
			background-position: -105px 0px;
		}
	}
}

/* Elements */
.align-right {
	float: right;
}

.align-left {
	float: left;
}

.divider {
	background: url(../image/list_divider.png) top left repeat-x;
	margin: 20px 0;
	height: 1px;
}

.success, .warning, .attention, .information {
	padding: 10px 10px 10px 33px;
	margin-bottom: 15px;
	color: #555555;
}
.success {
	background: #EAF7D9 url('../image/success.png') 10px center no-repeat;
	border: 1px solid #BBDF8D;
}
.warning {
	background: #FFD1D1 url('../image/warning.png') 10px center no-repeat;
	border: 1px solid #F8ACAC;
}
.attention {
	background: #FFF5CC url('../image/attention.png') 10px center no-repeat;
	border: 1px solid #F2DD8C;
}
.success .close, .warning .close, .attention .close, .information .close {
	float: right;
	padding-top: 4px;
	padding-right: 4px;
	cursor: pointer;
}

.sidebar .box-content > ul {
	margin: -15px 0;
	padding: 0 !important;
}

/* Buttons */
.buttons() {
	padding: 6px 12px 4px;
	font-family: 'Cuprum', sans-serif;
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
	text-transform: uppercase;
	margin: 0 5px;
}

.button-blue() {
	.gradient(#1f9cff, #0162ae);
	.shadow("0px 1px 3px rgba(0, 0, 0, 0.32), inset 0px 1px 2px rgba(255, 255, 255, 0.29)");
	border: 1px solid rgba(0, 75, 132, 0.5);
	color: #fff;
	&:hover {
		color: #fff;
		.gradient(#0061ac, #0487ee);
		.shadow("inset 0px 1px 2px rgba(255, 255, 255, 0.1)");
	}
}

.button-gray() {
	.gradient(#f9f9f9, #ededed);
	.shadow("0px 1px 3px rgba(0, 0, 0, 0.07), inset 0px 1px 1px #fff");
	border: 1px solid #E4E4E4;
	&:hover {
		.gradient(#f1f1f1, #f9f9f9);
		.shadow("inset 0px 1px 3px 1px rgba(0, 0, 0, 0.02)");
		border-color: #eaeaea;
		color: #000;
	}
}

.button-green() {
	.gradient(#1fcf02, #328400);
	.shadow("0px 1px 3px rgba(0, 0, 0, 0.32), inset 0px 1px 1px rgba(255, 255, 255, 0.3)");
	border: 1px solid #199b00;
	color: #fff;
	&:hover {
		.gradient(#328400, #1fcf02);
		.shadow("inset 0px 1px 2px rgba(255, 255, 255, 0.27)");
		color: #fff;
	}
}


.button-red() {
	.gradient(#ff0320, #ac0014);
	.shadow("0px 1px 3px rgba(0, 0, 0, 0.32), inset 0px 1px 1px rgba(255, 255, 255, 0.37)");
	border: 1px solid #ca0d25;
	color: #fff;
	&:hover {
		.gradient(#ac0014, #ff0320);
		.shadow("inset 0px 1px 2px rgba(255, 255, 255, 0.37)");
		color: #fff;
	}
}

#simplecheckout_button_login {
  .buttons();
  .button-green();
}

.checkout-heading {
	.content-wrapper.heading();
	a {
		padding-left: 15px;
	}
}

.checkout-content {
	.box();
	.button {
			.buttons();
			.button-blue();
	}
}

.checkout {
	> div {
		overflow: hidden;
	}
}

.button {
	&.blue {
		.buttons();
		.button-blue();
	}
	&.gray {
		.buttons();
		.button-gray();
	}
	&.green {
		.buttons();
		.button-green();
	}
	&.red {
		.buttons();
		.button-red();
	}
}


/* Lists in content boxes and widgets */
.box-list() {
	ul {
		padding: 5px 0;
		margin-bottom: 0;
	}
	li {
		background: url(../image/list_divider.png) top left repeat-x;
		margin-left: 10px; position: relative;
		a {
			margin: 8px 0;
			display: inline-block;
			background: url(../image/list_bullet.png) left center no-repeat;
			padding-left: 8px;
			text-decoration: none;
            &.active {color: #0487ee;}
		}
        a.smenu {
          background: url(../image/plus.png) right 2px no-repeat; display:block; width: 10px; height: 10px;
          position: absolute; top: 3px; right:-10px;

        }
        a.active+a.smenu {
          background: url(../image/minus.png) right 2px no-repeat;
        }
	}
	> ul {

		> li {
			margin-left: 0;
			&:first-child {
				background: none;
			}
		}
	}
	ul ul {
      display: none; padding: 0;
	}
    ul li.current ul {
      display: block;
    }
}

.styled {
	li {
		padding: 2px 0;
		a {
			background: url(../image/list_bullet.png) left center no-repeat;
			padding-left: 10px;
			text-decoration: none;
		}
	}
}

/* Product item */
.grid-item() {
	&:hover {
		border-color: #fff;
		.shadow("0px 1px 8px rgba(0, 0, 0, 0.2)");
	}
	.image {
		border: 1px solid #e4e4e4;
/*		margin-top: 19px;*/
		width: 178px;
		height: 127px;
	}
	.name {
		text-align: center;
		padding: 7px 0;
		a {
			text-decoration: none;
            font-weight: bold;
		}
	}
	.price {
		text-align: center;
		font-family: 'Istoc Web', sans-serif;
		font-size: 21px;
		font-weight: bold;
		padding: 7px 0;
		color: #0081E5;
		&:before {
			content: 'цена:';
			font-family: 'Cuprum', sans-serif;
			font-size: 14px;
			font-weight: normal;
			padding-right: 5px;
			color: #000000;
		}
		sub {
			font-size: 14px;
			bottom: 0;
		}
		.price-old {
			display: inline-block	; /* SEDOY */
			font-size: 14px;
			font-weight: bold;
			text-decoration: line-through;
			color: #000;
		}
		.price-new {
			color: #e2001a;
		}
	}
	.cart {
		padding: 7px 0;
/*		margin-bottom: 19px;*/
		text-align: center;
		.button {
			.buttons();
			.button-gray();
            &.incart {.button-green();}
			position: relative;
			padding-left: 27px;
			&:before {
				position: absolute;
				top: 6px;
				left: 10px;
				content: '';
				display: block;
				width: 12px;
				height: 13px;
				background: url(../image/button_cart_gray.png) center center no-repeat;
			}
		}
	}
	.description, .wishlist, .compare {
		display: none;
	}



	&.action, &.action.sale, &.action.gift, &.action.sale.gift, &.hit, &.hit.sale, &.hit.gift, &.hit.sale.gift {
		.cart {
			.button {
				.buttons();
				.button-blue();
				&.incart {.button-green();}
				position: relative;
				padding-left: 27px;
				&:before {
					background: url(../image/button_cart_blue.png) center center no-repeat;
				}
			}
		}
		/* .price {color: #e2001a;} */
	}

	&.action {
		.price {color: #e2001a;}
	}


	&.action {
		&:after {	content: '';	position: absolute;	width: 93px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/action.png');	background-repeat: no-repeat; }
	}

	&.action.sale {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/action-sale.png');	background-repeat: no-repeat; }
	}

	&.action.gift {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/action-gift.png');	background-repeat: no-repeat; }
	}

	&.action.sale.gift {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/action-sale-gift.png');	background-repeat: no-repeat; }
	}

	&.hit {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px;	left: -3px;	background: url('../image/ribbon/hit.png');	background-repeat: no-repeat;	}
	}

	&.hit.sale {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/hit-sale.png');	background-repeat: no-repeat; }
	}

	&.hit.gift {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/hit-gift.png');	background-repeat: no-repeat; }
	}

	&.hit.sale.gift {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/hit-sale-gift.png');	background-repeat: no-repeat; }
	}

	&.sale {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px;	left: -3px;	background: url('../image/ribbon/sale.png');	background-repeat: no-repeat;	}
	}

	&.gift {
		&:after {	content: '';	position: absolute;	width: 217px;	height: 74px;	top: -2px; left: -3px;	background: url('../image/ribbon/gift.png');	background-repeat: no-repeat; }
	}







/*
	&.action {
		&:after {
			content: '';
			position: absolute;
			width: 84px;
			height: 74px;
			top: -2px;
			left: -3px;
			background: url('../image/ribbon_action.png');
		}
		.cart {
			.button {
				.buttons();
				.button-blue();
                &.incart {.button-green();}
				position: relative;
				padding-left: 27px;
				&:before {
					background: url(../image/button_cart_blue.png) center center no-repeat;
				}
			}
		}
        .price {color: #e2001a;}
	}
  &.hit {
    &:after {
      content: '';
      position: absolute;
      width: 84px;
      height: 74px;
      top: -2px;
      left: -3px;
      background: url('../image/ribbon_hit.png');
    }
  }
  
  &.hit.sale {
    &:after {
      content: '';
      position: absolute;
      width: 217px;
      height: 76px;
      top: -2px;
      left: -3px;
      background: url('../image/ribbon_hit_sale.png');
    }
  }
  &.sale {
    &:after {
      content: '';
      position: absolute;
      width: 217px;
      height: 76px;
      top: -2px;
      left: -3px;
      background: url('../image/ribbon_sale.png');
    }
  }  
*/

  
    .compare a.active {color: #e2001a;}
}

.list-item() {
	padding: 19px;
	.left {
		float: left;
		width: 75%;
		padding-right: 15px;
		.box-sizing(border-box);
	}
	.right {
		float: right;
		width: 25%;
		background: url(../image/vertical_divider.png) left top repeat-y;
		padding-left: 15px;
		.box-sizing(border-box);
		min-height: 129px;
	}
	
	.image {
		float: left;
		margin: 0 20px 0 0;
	}
	.name {
		text-align: inherit;
		padding: 0;
	}
	.price {
		clear: both;
		margin-top: 50px;
	}
	.cart {
		margin-bottom: 0;
	}
	.description, .wishlist, .compare {
		display: block;
	}
	.wishlist, .compare {
		background: url(../image/plus.png) left 2px no-repeat;
		padding: 0 0 0 10px;
		margin: 0 3px;
		font: 11px 'Cuprum', sans-serif;
		font-weight: bold;
		text-transform: uppercase;
		float: left;
		cursor: pointer;
	}
}


/* Static box with help content */
.box-content-help {
	border: 1px solid #e4e4e4;
	background: #fff;
	padding: 19px;
	margin-bottom: 20px;
	position: relative;
	img {
		position: absolute;
		width: 77px;
		height: 78px;
		top: 50px;
		left: 30px;
	}
	h1 {
		font: 16px 'Cuprum', sans-serif;
		margin-bottom: 15px;
		padding-left: 100px;
		text-transform: uppercase;
	}
	ul {
		padding-left: 100px;
		margin-bottom: 0;
		li {
			background: url(../image/list_bullet_2.png) left center no-repeat;
			padding: 3px 0;
			a {
				text-decoration: none;
				padding-left: 15px;
			}
		}
	}
}


/* Content box */
.box {
	/* margin-bottom: 10px; */
	float: left;
	width: 100%;
	.box-heading {
		h1 {
			margin-bottom: 0;

		}
      a {
        text-decoration: none;color: #0081e5;
        &.fr {margin-right: 10px;}
        &:hover {color: #000;}
      }
		border-left: 3px solid #0081e5;
		.box-sizing(border-box);
		background: #fff;
		height: 31px;
		padding: 6px 0px 0px 17px;
		font-family: 'Cuprum', sans-serif;
		font-size: 16px;
		font-weight: bold;
		text-transform: uppercase;
		margin-bottom: 10px;
		.shadow("0px 1px 3px rgba(0, 0, 0, 0.20)");
		.sort {
			float: right;
			font-size: 12px;
			select {
				background-color: #f9f9f9;
				margin-left: 5px;
				text-transform: none;
			}
		}
		.displayText{
			display: none;
		}
		.display {
			margin: 1px 20px 0;
			float: right;
			a {
				font-weight: bold;
			}
		}
		.gridList {
			display: inline-block;
			height: 11px;
			width: 11px;
			background:url('../image/viewType.png') right 0px;
		}
		.gridListActive {
			display: inline-block;
			height: 11px;
			width: 11px;
			background:url('../image/viewType.png') right -14px;
		}
		.listView {
			display: inline-block;
			height: 11px;
			width: 11px;
			background:url('../image/viewType.png') left 0px;
			margin-left: 5px;
		}
		.listViewActive {
			display: inline-block;
			height: 11px;
			width: 11px;
			background:url('../image/viewType.png') left -14px;
			margin-left: 5px;
		}
	}
	.box-content {
		border: 1px solid #e4e4e4;
		background: #fff;
		padding: 19px;
		margin-bottom: 10px;
		position: relative;
		.box-list();
		&.product-item {
			.grid-item();
		}
	}
	
	
	.box-content.sale-product {
		border: 1px solid #e4e4e4;
		background: #fff;
		padding: 19px;
		margin-bottom: 20px;
		position: relative;
		.box-list();
		&.product-item {
			.grid-item();
		}
  	 &:after {
      content: '';
      position: absolute;
      width: 75px;
      height: 75px;
      top: 2px;
      left: 210px;
      background: url('../image/background_sale.png');
    }
	}
	
	
	
	
    .product-grid {
      .product-item {
/*        min-height: 275px; */
      }
    }
	.product-list {
		.one-third, .one-fourth {
			width: 100% !important;
			margin-right: 0;
		}
		.box-content {
			overflow: hidden;
			padding-bottom: 19px;
		}
		.product-item {
			.list-item();
		}
	}
}

/* Category widget box */
.box-category {
	margin-bottom: 10px;
	width: 100%;
	.shadow("0px 1px 3px rgba(0, 0, 0, 0.20)");
	.box-heading {
		border: 1px solid #0169b9;
		.box-sizing(border-box);
		background: #0081e5 url(../image/categories_arrow_down.png) 190px center no-repeat;
		color: #fff;
		height: 31px;
		padding: 6px 0px 0px 17px;
		font-family: 'Cuprum', sans-serif;
		font-size: 16px;
		font-weight: bold;
		text-transform: uppercase;
		margin-bottom: 0;
		cursor: pointer;
	}
	.box-content {
		background: #fff;
		padding: 20px 20px 0;
		.box-list();
	}
}

/* Contacts */
.our-contacts {
/*		margin-top: 19px;*/
	dt {
		font: 14px 'Cuprum', sans-serif;
		font-weight: bold;
		text-transform: uppercase;
		color: #0081e5;
		float: left;
		margin-right: 5px;
	}
	dd {
		margin-bottom: 5px;
	}
}

.contact-form {
/*	margin-bottom: 19px;*/
	dt {
		width: 140px;
		float: left;
		margin-top: 7px;
		text-align: right;
	}
	dd {
		padding-left: 145px;
		input, textarea {
			width: 100%;
			.box-sizing(border-box);
		}
	}
	dd + dd {
/*		display: block;*/
		margin-bottom: 10px;
		margin-top: 3px;
		color: red;
		padding-left: 145px;
	}
	img {
/*		padding-left: 145px;*/
	}
	input[name=captcha] {
		width: inherit;
	}
}

/* Login */
.login-new, .login-returned {
/*	margin: 0;*/
	min-height: 183px;
}

.login-returned {
	input[type=text], input[type=password] {
		width: 100%;
	}
	a {
		color: #0081e5;
		text-decoration: none;
	}
}

/* Register */
.register-form {
/*	margin: 19px 0;*/
	dt {
		width: 150px;
		float: left;
		margin-top: 5px;
		text-align: right;
	}
	dd {
		padding-left: 155px;
		margin-bottom: 10px;
	}
	dd + dd {
		margin-bottom: 10px;
		margin-top: -5px;
		color: red;
		padding-left: 155px;
	}
	.required {
		color: red;
	}
    input[type=text] {
        width: 250px;
    }
    select { width: 252px;}
    input[name=captcha] {width: 100px;}
}

/* Cart */
.cart-info-table() {
	width: 100%;
	thead td {
		height: 30px;
		background-color: #0081e5;
		color: #fff;
		border: 1px solid #026ec2;
	}
	tbody {
		td {
		min-height: 70px;
		background: #fff;
		border: 1px solid #e4e4e4;
		}
		.quantity input[type="image"], .quantity img {
			position: relative;
			top: 4px;
			cursor: pointer;
			vertical-align: top;
		}
	}
	td {
		vertical-align: middle;
		text-align: center;
	}
    tfoot {
      td:first-child {
        text-align: right;
      }
      td:last-child {
        border: 1px solid #e4e4e4;
        font-weight: bold;
      }
    }
}

.cart-info table {
    td {padding: 5px 5px;}
	.cart-info-table()
}

.wishlist-info table {
    td {padding: 5px 5px;}
	.cart-info-table()
}

table.list {
	.cart-info-table()
}

table.radio {
	width: 100%;
	thead td {
		height: 30px;
		background-color: #0081e5;
		color: #fff;
		border: 1px solid #026ec2;
		vertical-align: middle;
		padding-left: 10px;
	}
	tbody {
		border: 1px solid #e4e4e4;
		tr {
			td {
				background: #fff;
				padding: 5px;
				&:first-child {
					width: 1px;
				}
			}
			&.highlight {
				&:hover {
					td {
						background: #F1FFDD;
						cursor: pointer;
					}
				}
			}
		}
	}
}

#voucher, #coupon {
	&.box-content {
		padding: 10px 19px;
	}
}

.cart-total {
	overflow: auto;
	padding-top: 8px;
	margin-bottom: 15px;
	table {
		float: right;
	}
	td {
		padding: 3px;
		text-align: right;
	}
}

.pagination {
	text-align: center;
	.links  {
		a, b {
		.buttons();
		.button-gray();
		}
		b {
			.gradient(#f1f1f1, #f9f9f9);
			.shadow("inset 0px 1px 3px 1px rgba(0, 0, 0, 0.02)");
			border-color: #eaeaea;
			color: #000;


			/*.gradient(#f1f1f1, #f9f9f9);
			.shadow("inset 0px 1px 3px 1px rgba(0, 0, 0, 0.2)");
			border-color: transparent;
			color: #000;*/
		}
	}a {
	}
	.results {
		display: none;
	}
}

/* Product */
.product-info {
	overflow: auto;
	margin-bottom: 20px;
	/* min-height: 360px; */
	> .left {
		float: left;
		margin-right: 15px;
		+ .right {
			margin-left: 265px;
		}
	}
	.image {
		border: 1px solid #E7E7E7;
		float: left;
		/*margin-bottom: 20px;*/
		padding: 10px;
		text-align: center;
	}
	.image-additional {
		width: 260px;
		margin-left: -10px;
		clear: both;
		overflow: hidden;
		a {
			float: left;
			display: block;
			margin-left: 10px;
			margin-bottom: 10px;
			img {
				border: 1px solid #E7E7E7;
			}
            text-decoration: none; outline: none;
		}
        li {
          list-style: none;
          background: none;
          a {background: none;}
        }
	}
	.description {
		/*border-top: 1px solid #E7E7E7;*/
		border-bottom: 1px solid #E7E7E7;
		padding: 5px 5px 10px 5px;
		margin-bottom: 10px;
		line-height: 20px;
		color: #4D4D4D;
		span {
			font-weight: bold;
		}
        .title_product {
          font-size: 1.4em;
          color: #0081e5;
          margin-bottom: 5px;
          min-height: 40px;
        }
        .sub_param_product {
          font-weight: normal;
          font-size: 11px;
          line-height: 16px;
          margin-bottom: 10px;
        }
        .review {
          height: 5px; display: block;
          div.share {float: left; display: inline-block;}
          div.addreviews {float: right; display: inline-block; font-size: 11px;margin-top: 3px;}
        }
	}
	.price {
		font-family: 'Istoc Web', sans-serif;
		font-size: 24px;
		font-weight: bold;
		padding: 7px 0;
		&:before {
			content: 'цена:';
			font-family: 'Cuprum', sans-serif;
			font-size: 14px;
			font-weight: normal;
			padding-right: 5px;
		}
		sub {
			font-size: 14px;
			bottom: 0;
		}
		.price-old {
			display: inline-block	; /* SEDOY */
			font-size: 14px;
			font-weight: bold;
			text-decoration: line-through;
			color: #000;
		}
		.price-new {
			color: #e2001a;
		}
		
		div.price-other {
			display: inline-block;
			height: 27px;
			vertical-align: middle;
			padding: 5px 0 0 10px;
			font-size: 55%;
			color: gray;
			font-weight: normal;
		}
	}
	.options {
		border-bottom: 1px solid #E7E7E7;
		padding: 0px 5px 10px 5px;
		margin-bottom: 10px;
		color: black;
		div.h2 {
			color: black;
			font-size: 16px !important;
			font-weight: bold !important;
			margin-top: 0px;
			margin-bottom: 5px;
		}
	}
	.cart {
		border-bottom: 1px solid #E7E7E7;
		padding: 0px 5px 10px 5px;
		margin-bottom: 20px;
		color: #4D4D4D;
		overflow: auto;
		div {
			float: left;
			vertical-align: middle;
			> span {
				padding-top: 7px;
				display: block;
				color: #999;
			}
		}
		.wishlist, .compare {
			background: url(../image/plus.png) left 2px no-repeat;
			padding: 0 0 0 10px;
			margin: 0 3px;
			font: 11px 'Cuprum', sans-serif;
			font-weight: bold;
			text-transform: uppercase;
			cursor: pointer;
		}
      .button {
        .buttons();
        .button-gray();
        &.incart {.button-green();}
        &.red {
          .gradient(#ff0320, #ac0014);
          .shadow("0px 1px 3px rgba(0, 0, 0, 0.32), inset 0px 1px 1px rgba(255, 255, 255, 0.37)");
          border: 1px solid #ca0d25;
          color: #fff;
          &:hover {
            .gradient(#ac0014, #ff0320);
            .shadow("inset 0px 1px 2px rgba(255, 255, 255, 0.37)");
            color: #fff;
          }

        }
        position: relative; cursor: pointer;
        padding-left: 27px;
        &:before {
          position: absolute;
          top: 6px;
          left: 10px;
          content: '';
          display: block;
          width: 12px;
          height: 13px;
          background: url(../image/button_cart_blue.png) center center no-repeat;
        }
      }
	}
}

.htabs {
	height: 30px;
	line-height: 16px;
	border-bottom: 1px solid #DDD;
	a {
		border-top: 1px solid #DDD;
		border-left: 1px solid #DDD;
		border-right: 1px solid #DDD;
		background: #f9f9f9;
		padding: 7px 15px 6px 15px;
		float: left;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 13px;
		font-weight: bold;
		text-align: center;
		text-decoration: none;
		color: black;
		margin-right: 2px;
		display: none;
		&.selected {
			padding-bottom: 7px;
			background: white;
		}
	}
}

.tab-content {
	border-left: 1px solid #DDD;
	border-right: 1px solid #DDD;
	border-bottom: 1px solid #DDD;
	padding: 10px;
	margin-bottom: 20px;
	z-index: 2;
	overflow: auto;
	#review {
		background: url(../image/list_divider.png) bottom left repeat-x;
		margin-bottom: 10px;
		.content {
			padding: 10px 0;
		}
	}
	#review-title {
		font: 14px 'Cuprum', sans-serif;
		font-weight: bold;
		color: #0081e5;
		text-transform: uppercase;
	}
	dl {
		dt {
			width: 110px;
			float: left;
			margin-top: 4px;
			text-align: right;
		}
		dd {
			padding-left: 115px;
			margin-bottom: 10px;
		}
	}
}

table.form {
	width: 500px;
	td {
		vertical-align: bottom;
	}

	input[type="text"], input[type="password"], select, textarea {
		width: 100%;
		.box-sizing(border-box);
	}

	input, select, textarea {
		margin-top: 7px;
	}
}

/*.contact-form {
	dt {
		width: 140px;
		float: left;
		margin-top: 7px;
		text-align: right;
	}
	dd {
		padding-left: 145px;
		input, textarea {
			width: 100%;
			.box-sizing(border-box);
		}
	}
	dd + dd {
		margin-bottom: 10px;
		margin-top: 3px;
		color: red;
		padding-left: 145px;
	}
	input[name=captcha] {
		width: inherit;
	}
}*/

.compare-info {
  border-collapse: collapse;
  td {
    border-bottom: 1px solid #efefef;
    padding: 5px 10px;
    &.bt {border: none;}
    &.param {font-weight: bold; white-space: nowrap;}
  }
}

.latest, .bestseller {

  .name {
    font-size: 11px; font-weight: bold; height: 40px; overflow: hidden;
  }
  .product-item {
    min-height: 250px;

  }
}

.sub_notify {
  float: right;
  text-transform: lowercase;
  font-weight: normal;
  color: #0081e5;
  margin-right: 10px;
}

.price small {font-size: 12px;}


#sliderMain {
  height: 280px; overflow:hidden; position: relative;
  &:hover > .buttons {opacity: 1;}
  .viewport { float: left; width: 715px; height: 280px; overflow: hidden; position: relative;}
  .buttons { display: block; position: absolute; z-index: 100;
    width:40px;
    height:40px;
    background:url('../image/slider_arrows.png') no-repeat;
    text-indent:-9999px;
    border:0; top: 125px;
    opacity: 0;
  }
  .next {
    right: 17px; background-position:-45px -45px;
    &:hover {background-position:-45px 0px;}
  }
  .prev {
    left: 17px;background-position:0px -45px;
    &:hover {background-position:0px -1px;}
  }
  .overview {
    list-style: none; padding: 0; margin: 0;  position: absolute; left: 0; top: 0;
    li{ float: left; margin: 0 20px 0 0; padding: 1px; height: 280px; width: 715px;}
  }
  .pager {
    position: absolute; overflow:hidden; list-style: none; clear: both; top: 250px; right: 25px;
    li { float: left; }
  }
  .pagenum {
    display: block;
    width: 22px;
    height: 22px;
    background: url('../image/slider_bullets.png') no-repeat;
    text-indent:-9999px;
    border:0;
    margin-right:3px;
  }
  .active { background-position:0 -26px; }
}

#slider-code {
  height: 120px; overflow:hidden; text-decoration: none;
  .viewport { float: left; width: 260px; height: 90px; overflow: hidden; position: relative; }
  .buttons { display: block; text-decoration: none; font: 15px 'Cuprum', sans-serif;  }
  .overview {
    list-style: none; padding: 0; margin: 0;  position: absolute; left: 0; top: 0;
    li{ float: left; margin: 0 9px 0 1px; padding: 1px; height: 75px; width: 75px;list-style: none;}
  }
}

.freecall {
	font-size:11px;
	color: #666;
	font-weight: bold;
}

/*
.button {
	.buttons();
	.button-blue();
	display: inline-block;
	
}
*/

.review p {
	margin-top: 3px;
	margin-bottom: 3px;
}

.error {
	color: red;
}

#tab-description {
	h2 {
		font-size:125%;
		padding: 0;
		margin: 0;
	}
	ul { 
		margin: 0;
		padding: 0;
		li {
			border: none;
			list-style-type: disc;
			background: none;
			margin-left: 20px;
		}
	}
}


ul.normal { 
	margin: 0;
	padding: 0;
	li {
		border: none;
		list-style-type: disc;
		background: none !important;
		margin-left: 20px !important;
		border: none !important;
	}
}

ul.toc { 
	border-top: 1px #000 dotted;
	margin: 15px 0 0 0;
	padding: 15px 0 0 0 !important;
	li {
		border: none;
		list-style-type: square;
		background: none !important;
		margin-left: 20px !important;
		border: none !important;
		a {
			background: none !important;
			margin: 0 !important;
			padding: 0 !important;
			color: #666 !important;
			text-decoration: underline !important;
			&.active {
				font-weight: bold;
				color: #000 !important;
				text-decoration: none !important;
				
			}
		}
	}
}


ol.normal { 
	margin: 0;
	padding: 0;
	li {
		border: none;
		/* list-style-type: disc; */
		background: none !important;
		margin-left: 20px !important;
		border: none !important;
	}
}


.buy-1-click {
	display: inline-block;
	width: 130px;
	background-color: #0081E5;
	color: #FFF;
	font-weight: bold;
	text-align: center;
	padding: 5px;
	font-size: 14px;
	-webkit-border-radius: 1px;
	-moz-border-radius: 1px;
	border-radius: 1px;
	&:hover {
		color: #FFF;
		background-color: #0062AA;
	}
}

#myModal {
	font-family: Arial !important;
	.bt_modal-header {
		padding:  10px 15px !important;
		background-color: #0081E5;
		.bt_h3 {
			font-size: 16px;
			line-height: 20px;
			color: #FFF;
		}
	}
	.bt_modal-body {
		.bt_h3 {
			font-size: 18px;
			line-height: 22px;
			color: #FFF;
		}
		.help-inline {
			font-size:10px;
		}
		.description {
			font-size:  14px;
		}
		
		.span2 {
			text-align: right !important;
			float: right !important;
		}
		
		.price {
			color: #C80118;
			font-size: 18px;
			font-weight: bold;
		}
		
		#user_comment {
			height: 50px;
		}
		.alert-success {
			background-color: #FFF !important;
			border: none !important;
			color: #000 !important;
			font-size: 15px;
			font-weight: bold;
		}
	}
	.bt_modal-footer {
		padding: 10px !important;
	}
}

table.character {
	border-collapse: collapse;
	tr:nth-child(even) {
		background-color: #F9F9F9;
		td:nth-child(1) {
			font-weight: bold;
		}
		td:nth-child(2) {
			font-weight: normal;
		}
	}
	tr:nth-child(odd) {
		background-color: #FFFFFF;
		td:nth-child(1) {
			font-weight: bold;
		}
		td:nth-child(2) {
			font-weight: normal;
		}
	}

	tr td {
		border: 1px solid #EEE;
		padding: 8px !important;
		p {
			margin: 0 !important;
			padding: 0 !important;
		}
	}
}

table.option-image, table.quick-option-image {
	width: 100%;

	tr {
		/*
		border-radius: 5px;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		*/
		background-color: #FFF;

		&.active {
			background-color: #EEE;
		}

	}

	td {
		padding: 5px;
		vertical-align: middle;

		&:first-child {
			width: 20px;
		}

		input[type="radio"] {
			max-width: 20px;
			/*
			display: none;
			width: 0px;
			height: 0px;
			*/
		}

		label {
			cursor: pointer;
		}

		span.head {
			font-size: 115%;
			font-weight: bold;
			display: inline-block;
			margin-bottom: 6px;
		}
		span.price-old {
			font-size: 90%;
			text-decoration: line-through;
			display: inline-block;
			margin-right: 10px;
		}
		span.price-new {
			font-size: 100%;
			display: inline-block;
			font-weight: bold;
			color: #e2001a;

			span {
				color: #0081e5;
			}
		}

		a.more {
			display: block;
			float: right;
			margin-right: 4px;
			text-decoration: none;
		}
	}
}

b.action {
	color: green;
	display: block;
	font-size: 120%;
	margin: 10px 0 5px 0;

	span {
		color: #0081e5;
	}
}

div.option select option:first-child {
	background-color: red;
}

.director-name {
	font-weight: bold;
	color: #000;
}

.director-date {
	font-size: 11px;
	color: #555;
}

.director-enquiry {
	text-align: justify;
	margin: 5px 0;
}

.director-answer {
	text-align: justify;
	margin-left: 10%;
	color: #00f;
}

.director-divider {
	/* background: url(http://brief-med.ru/catalog/view/theme/brief/less/../image/list_divider.png) top left repeat-x; */
	margin-top: 20px;
	height: 20px;
	border-top: 1px solid #EEEEEE;
}

sup.required {
	color: red;
	font-weight: bold;
	display: inline-block;
	margin-left: 4px;
}