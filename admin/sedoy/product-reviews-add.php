<?php
    require 'connect.php';

    if ($ro_user['user_group_id'] == 1) {
        /*
        echo '			INSERT INTO	review
            SET 				product_id="'.$_POST['product_id'].'",
                                    customer_id=0,
                                    author="'.htmlspecialchars($_POST['author']).'",
                                    text="'.htmlspecialchars($_POST['text']).'",
                                    rating="'.$_POST['rating'].'",
                                    status=1,
                                    date_added="'.date('Y-m-d H:i:s', rand(time()-86400*$_POST['date_type'],time())).'",
                                    date_modified="0000-00-00 00:00:00"
        ';
        */ ?>
			<div style="text-align: center; padding: 20px; font-weight: bold; color: red;"><?php echo date('Y-d-m H:i:s')?> Комментарий размещен!</div>
		<?php
        @mysqli_query($ddb, '
			INSERT INTO	review
			SET 				product_id="'.$_POST['product_id'].'",
									customer_id=0,
									author="'.htmlspecialchars($_POST['author']).'",
									`text`="'.htmlspecialchars($_POST['text']).'",
									rating="'.$_POST['rating'].'",
									status=1,
									date_added="'.date('Y-m-d H:i:s', rand(time()-86400*$_POST['date_type'], time())).'",
									date_modified="0000-00-00 00:00:00"
		');
    }
