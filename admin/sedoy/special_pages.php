<?php
	//Специальные страницы
	/*header('Content-Type: text/html; charset=utf-8');
	session_start();*/

	require 'connect.php';	
	
	if ($_GET['p'] == '') $_GET['p'] = 1;

	if ($ro_user['user_group_id'] == 1) {
		/*
		if ($_GET['act'] == 'del') {
			$_GET['id'] = intval($_GET['id']);
			$used = @mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM product WHERE utp_set_id='.intval($_GET['id'])));
			if ($used == 0) {
				@mysqli_query($ddb, 'DELETE FROM utp_set WHERE utp_set_id='.intval($_GET['id']));
				@mysqli_query($ddb, 'DELETE FROM utp_set_item WHERE utp_set_id='.intval($_GET['id']));
				$tpl['msg'] = 'Предложение УТП удалено.';
			} else {
				$tpl['msg'] = 'Предложение УТП изпользуется в '.$used.' товарах и не может быть удалено.';
			}
			$_GET['act'] = '';
			$_GET['id'] = '';

		}
		*/
		
		if (isset($_POST['act']) && ($_POST['act'] == 'edit')) {
			@mysqli_query($ddb, 'DELETE FROM setting WHERE `group`="special-page" AND `key`="'.$_POST['page'].'"');
			@mysqli_query($ddb, 'INSERT INTO setting SET `group`="special-page", `key`="'.$_POST['page'].'", value="'.mysqli_escape_string($_POST['value']).'", serialized=0');
			@mysqli_query($ddb, 'DELETE FROM special_page_product WHERE page="'.$_POST['page'].'"');
			foreach ($_POST['product'] as $product_id => $sort) @mysqli_query($ddb, 'INSERT INTO special_page_product SET page="'.$_POST['page'].'", product_id='.$product_id.', sort='.$sort);
			$_GET['act'] = 'edit';
			$_GET['page'] = $_POST['page'];
			$tpl['msg'] = 'Специальная страница <b>"'.$_POST['page'].'"</b> обновлена.';
		}
		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br>
			'.((isset($tpl) && ($tpl['msg'] != '')) ? '<div class="msg">'.$tpl['msg'].'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="15%">
						<table class="list">
							<thead>
								<tr>
									<td>Страница</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><a href="?act=edit&page=empty-search" style="'.((isset($_GET['page']) && ($_GET['page'] == 'empty-search')) ? 'color:red; ' : '').'">Пустые результаты поиска</a></td>
								</tr>
								<tr>
									<td><a href="?act=edit&page=cart" style="'.((isset($_GET['page']) && ($_GET['page'] == 'cart')) ? 'color:red; ' : '').'">Корзина</a></td>
								</tr>
							</tbody>
						</table>
					</td>
					<td class="right" valign="top" width="85%">
		';
		if (isset($_GET['act']) && ($_GET['act'] == 'edit')) {
			$page = Result($ddb, 'SELECT * FROM setting WHERE `group`= "special-page" AND `key`="'.$_GET['page'].'"');
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						/*if (form.title.value == "") msg = msg + br + " - не заполнено наименование;";*/
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="edit">
				<input type="hidden" name="page" value="'.$_GET['page'].'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Текст:</td>
						<td width="85%" class="txt" align="left"><textarea name="value" id="value" class="txt" style="width:350px; height: 200px;">'.$page['value'].'</textarea></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="Обновить страницу"></td>
					</tr>
				</table>			
				<br><br>
				<h2>Товары на странице</h2>
				<table width="60%" style="border-color: gray; border-collapse: collapse;" border="1">
					<tr>
						<td width="10%" align="center"><b>Порядок</b></td>
						<td width="45%"><b>Категории</b></td>
						<td width="45%"><b>Товар</b></td>
					</tr>
			';
			$qu_product = '
				SELECT		p.product_id as product_id,
							p.model as product_model,
							pd.name as product_name,
							m.name as product_manufacturer,
							spp.page as special_page,
							spp.sort as sort,
							(
								SELECT		GROUP_CONCAT(cd.name SEPARATOR "<br>")
								FROM		category_description cd, product_to_category ptc
								WHERE		cd.category_id = ptc.category_id AND ptc.product_id = p.product_id
								ORDER BY	ptc.main_category DESC
							) as cat
				FROM		product p
							LEFT JOIN product_description pd ON p.product_id = pd.product_id
							LEFT JOIN manufacturer m ON m.manufacturer_id = p.manufacturer_id
							LEFT JOIN special_page_product spp ON p.product_id = spp.product_id AND spp.page="'.$_GET['page'].'"
				ORDER BY	sort IS NULL,
							cat,
							product_name,
							product_manufacturer,
							product_model,
							product_id
			';
			$re_product = @mysqli_query($ddb, $qu_product);
			while ($ro_product = @mysqli_fetch_assoc($re_product)) {
				$tpl['body_content'] .= '
					<tr'.(($ro_product['sort'] < 1) ? ' class="gray"' : '').'>
						<td align="center">
							<select name="product['.$ro_product['product_id'].']">
								<option value=""></option>
				';
				for ($i=1;$i<=50;$i++) $tpl['body_content'] .= '<option value="'.$i.'"'.(($i == $ro_product['sort']) ? ' selected' : '').'>'.$i.'</option>';
				$tpl['body_content'] .= '	
							</select>
						</td>
						<td>'.$ro_product['cat'].'</td>
						<td>'.$ro_product['product_name'].' '.$ro_product['product_manufacturer'].' '.$ro_product['product_model'].'</td>
					</tr>
				';
			}
			$tpl['body_content'] .= '
				</table>
				<script src="/admin/view/javascript/ckeditor/ckeditor.js"></script>
				<script>
					CKEDITOR.replace(\'value\',{toolbar : \'Minimum\'});
				</script>				
			';
		} else {
		}
		$tpl['body_content'] .= '
				</form>
				</td>
			</tr>
		</table>
		';
		$tpl['meta_title'] = 'Специальные страницы';
		$tpl['body_title'] = 'Специальные страницы';
		require 'template.php';	
	} else {
		header('Location: /');
	}

