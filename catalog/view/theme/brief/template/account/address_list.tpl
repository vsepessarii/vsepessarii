<?php echo $header; ?>

				<?php //echo $column_right; ?>
				<div class="content-container">

					<section class="box">
						<!--20:53:02--><div class="box-heading"><?php echo $heading_title; ?></div>

						<nav class="breadcrumb">
						    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
						    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
						    <?php } ?>
    					</nav>


						<div class="box-content" style="margin-top: 30px !important;">
						  <h2><?php echo $text_address_book; ?></h2>
						  <?php foreach ($addresses as $result) { ?>
						  <div class="content">
						    <table style="width: 100%;">
						      <tr>
						        <td><?php echo $result['address']; ?></td>
						        <td style="text-align: right;"><a href="<?php echo $result['update']; ?>" class="button blue align-right"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="button gray align-right"><?php echo $button_delete; ?></a></td>
						      </tr>
						    </table>
						  </div>
						  <?php } ?>
						  <div class="buttons">
						    <div class="left"><a href="<?php echo $back; ?>" class="button gray align-left"><?php echo $button_back; ?></a></div>
						    <div class="right"><a href="<?php echo $insert; ?>" class="button blue align-left"><?php echo $button_new_address; ?></a></div>
						  </div>
 							<div class="clear"></div>
  						</div>
					</section>

				<?php //echo $content_top; ?>
				<?php //echo $content_bottom; ?>
				</div>
				<?php echo $column_left; ?><?php echo $column_right; ?>

<?php echo $footer; ?>

<?php /*echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--20:56:31--><div><?php echo $heading_title; ?></div>
  <h2><?php echo $text_address_book; ?></h2>
  <?php foreach ($addresses as $result) { ?>
  <div class="content">
    <table style="width: 100%;">
      <tr>
        <td><?php echo $result['address']; ?></td>
        <td style="text-align: right;"><a href="<?php echo $result['update']; ?>" class="button"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="button"><?php echo $button_delete; ?></a></td>
      </tr>
    </table>
  </div>
  <?php } ?>
  <div class="buttons">
    <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
    <div class="right"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_new_address; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */?>