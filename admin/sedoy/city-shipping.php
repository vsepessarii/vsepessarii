<?php
	//города (таблицей)
	require 'connect.php';
	if ($ro_user['user_group_id'] == 1) {
		function TextVertical($text) {
			for($i=0;$i<mb_strlen($text,'utf-8');$i++) $tmp[] = mb_substr($text, $i,1,'utf-8');
			return implode('<br>',$tmp);
		}


		$qu_city = 'SELECT COUNT(city) AS cnt,city FROM city GROUP BY city HAVING cnt>1';
		$re_city = @mysqli_query($ddb, $qu_city);
		while ($ro_city = @mysqli_fetch_array($re_city)) $double[] = $ro_city['city'];

		$tpl['meta_title']	= 'Города доставки';
		$tpl['body_title']	= 'Города доставки';

		//заполняем массив способов доставки (из Мультидоставки)	
		$tmp = @mysqli_fetch_assoc(@mysqli_query($ddb, 'SELECT `value` FROM setting WHERE `key`="multiflat"'));
		$tmp = unserialize($tmp['value']);
		foreach ($tmp as $tmp) {
			if ($tmp['id'] != '') $shippings[$tmp['id']] = $tmp['name'];
		}

		if (isset($_POST['act']) && $_POST['act'] == 'save') {
			//запись
			@mysqli_query($ddb, 'TRUNCATE TABLE city_shipping');
			foreach ($_POST['check'] as $key => $value) if ($value == 'on') @mysqli_query($ddb, 'INSERT INTO city_shipping SET city="'.$_POST['city'][$key].'", shipping="'.$_POST['shipping'][$key].'"');
		}

		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				table {
					border-collapse: collapse;
				}

				td {
					padding: 5px !important;	
				}

				.vertical {
					-moz-transform: rotate(90deg);
					-webkit-transform: rotate(90deg);
					-o-transform: rotate(90deg);
					writing-mode: tb-rl;
					width: 25px;
					height: 150px;
				}

				tr.body:hover {
					background-color: #EEE;
				}

			</style>
			<script language="javascript">
				function ToggleRow(n) {
					var bg = $("td#head"+n).css("background-color");
					if (bg == "rgb(255, 255, 255)") {
						color = "#CCC";
					} else {
						color = "#FFF";
					}
					$("td#head"+n).css("background-color",color);
					$("td.row"+n).css("background-color",color);
				}
			</script>
			<center>
			<form method="post">
			<input type="hidden" name="act" value="save" />
			<table border="1" cellspacing="0">
				<tr>
					<td>Город</td>
		';
		reset($shippings);
		$i = 0;
		foreach ($shippings as $key => $value) {
			$tpl['body_content'] .= '<td title="'.$value.'" style="background-color: white; text-align: center;" onclick="ToggleRow('.$i.')" id="head'.$i.'">'.TextVertical($key).'</td>';
			$i++;
		}
		$tpl['body_content'] .= '						
					<td>Город</td>
				</tr>
		';
		$qu_country = '
			SELECT		country.country_id,
						country.name
			FROM		country,
						city
			WHERE		city.country_id = country.country_id
			GROUP BY	country.country_id
			ORDER BY	country.name
		';
		$re_country = @mysqli_query($ddb, $qu_country);
		$j = 0;
		while ($ro_country = @mysqli_fetch_array($re_country)) {
			$tpl['body_content'] .= '
				<tr>
				<td colspan="'.(2+count($shippings)).'" align="center">'.$ro_country['name'].'</td>
				</tr>
			';
			$qu_city = '
				SELECT		*
				FROM		city
				WHERE		city.country_id='.$ro_country['country_id'].'
				ORDER BY	sort,
							city
			';
			$re_city = @mysqli_query($ddb, $qu_city);
			while ($ro_city = @mysqli_fetch_array($re_city)) {
				$tpl['body_content'] .= '
					<tr class="body">
						<td'.((in_array($ro_city['city'],(isset($double) ? $double : []))) ? ' style="color:red;"' : '').'>'.$ro_city['city'].'</td>
				';
				reset($shippings);
				$i = 0;
				foreach ($shippings as $key => $value) {
					$tpl['body_content'] .= '
						<td class="row'.$i.'" title="'.$key.'">
							<input type="hidden" name="city['.$j.']" value="'.$ro_city['city'].'" />
							<input type="hidden" name="shipping['.$j.']" value="'.$key.'" />
							<input type="checkbox" name="check['.$j.']"'.((@mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM city_shipping WHERE city="'.$ro_city['city'].'" && shipping="'.$key.'"'))) ? ' checked' : '').' />
						</td>
					';
					$i++;
					$j++;
				}
				$tpl['body_content'] .= '
						<td'.((in_array($ro_city['city'],(isset($double) ? $double : []))) ? ' style="color:red;"' : '').'>'.$ro_city['city'].'</td>
					</tr>
				';
			}
		}
		$tpl['body_content'] .= '
			</table>
			<br>
			<input type="submit" value="Сохранить" />
			</form>
			<br>
			</center>
		';
		require 'template.php';	
	} else {
		header('Location: /');
	}

