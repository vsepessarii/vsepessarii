<?php
	//заполняем поле product.full_key
	$qu_product = 'SELECT * FROM product ORDER BY product_id LIMIT 9999';
	$re_product = mysqli_query($qu_product);
	while ($ro_product = mysqli_fetch_array($re_product)) {
		$full_key = $ro_product['model'].' '.$ro_product['sku'].' '.$ro_product['upc'].' '.$ro_product['ean'].' '.$ro_product['jan'];
		$ro_description = mysqli_fetch_array(mysqli_query('SELECT * FROM product_description WHERE product_id="'.$ro_product['product_id'].'"'));
		//var_dump($ro_description);
		$full_key .= ' '.$ro_description['name'].' '.strip_tags(htmlspecialchars_decode($ro_description['description'])).' '.$ro_description['meta_description'].' '.$ro_description['meta_keyword'].' '.$ro_description['seo_title'].' '.$ro_description['seo_h1'].' '.$ro_description['tag'];
		$ro_manufacturer = mysqli_fetch_array(mysqli_query('SELECT * FROM manufacturer WHERE manufacturer_id="'.$ro_product['manufacturer_id'].'"'));
		$full_key .= ' '.$ro_manufacturer['name'];
		$qu_attribute = 'SELECT text FROM product_attribute WHERE product_id="'.$ro_product['product_id'].'"';
		$re_attribute = mysqli_query($qu_attribute);
		while ($ro_attribute = mysqli_fetch_array($re_attribute)) $full_key .= ' '.$ro_attribute['text'];
		mysqli_query('UPDATE product SET full_key="'.strtolower(mysqli_escape_string($full_key)).'" WHERE product_id="'.$ro_product['product_id'].'"');
	}
?>