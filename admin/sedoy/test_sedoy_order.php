<?php
	require 'connect.php';
?>
<ul>
<?php
	$qu_option = '
		SELECT		option_description.option_id,
							option_description.name
		FROM			option_description,
							product_option
		WHERE			option_description.option_id = product_option.option_id &&
							product_option.product_id = 1503
	';
	$re_option = @mysqli_query($ddb, $qu_option);
	while ($ro_option = @mysqli_fetch_array($re_option)) {
		echo '<li>';
		echo $ro_option['name'];
		$qu_value = '
			SELECT		option_value_description.option_id,
								option_value_description.option_value_id,
								option_value_description.name
			FROM			option_value_description,
								product_option_value
			WHERE			option_value_description.option_value_id = product_option_value.option_value_id &&
 								product_option_value.product_id = 1503 &&
 								option_value_description.option_id = '.$ro_option['option_id'].'
		';
		//echo $qu_value;
		echo '<ul>';
		$re_value = @mysqli_query($ddb, $qu_value);
		while ($ro_value = @mysqli_fetch_array($re_value)) echo '<li>'.$ro_value['name'].'</li>';
		echo '</ul>';
		echo '</li>';
	}
?>
</ul> 					