<?php

class ControllerPaymentCashless extends Controller
{

    protected function index()
    {
        $this->language->load('payment/cashless');

        $this->data['text_instruction'] = $this->language->get('text_instruction');
        $this->data['text_payment'] = $this->language->get('text_payment');
        $this->data['text_printpay'] = str_replace('{href}', $this->url->link('payment/cashless/printpay', '', 'SSL'), $this->language->get('text_printpay'));

        if ($this->customer->isLogged())
        {
            $this->data['text_order_history'] = str_replace('{href}', $this->url->link('account/order', '', 'SSL'), $this->language->get('text_order_history'));
        }
		else
        {
            $this->data['text_order_history'] = '';
        }

        $this->data['button_confirm'] = $this->language->get('button_confirm');

        $this->data['continue'] = $this->url->link('checkout/success');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/cashless.tpl'))
        {
            $this->template = $this->config->get('config_template') . '/template/payment/cashless.tpl';
        }
		else
        {
            $this->template = 'default/template/payment/cashless.tpl';
        }

        $this->render();
    }


    // генерирует форму Счета
    public function printpay()
    {
		// Защита от прямого доступа к счету
		/*
		if ( ! isset($this->session->data['user_id']) AND ! $this->customer->isLogged())
		{
			return $this->forward('error/not_found');
		}
		*/

        $this->load->model('account/cashless');
        $this->load->model('checkout/order');
		$this->load->model('setting/setting');

        // если осуществлен запрос GET
        if ( ! empty($this->request->get['order_id']))
        {
            $tmp = $this->model_account_cashless->getOrder($this->request->get['order_id']);
            if (md5($tmp['date_added'].$tmp['ip']) != $this->request->get['key']) {
            	return $this->forward('error/not_found');
            } else {
            	$order_info = $tmp;
            }
        }
        else
        {
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        }

        $this->language->load('payment/cashless');

        $this->data['button_confirm'] = $this->language->get('button_confirm');
        $this->data['button_back'] = $this->language->get('button_back');

        $this->data['supplier_info'] = nl2br($this->config->get('supplier_info_' . $this->config->get('config_language_id')));

        // получаем товары из корзины
        $products = $this->cart->getProducts();
		$this->data['products'] = array();

		// Разом без ПДВ
		$this->data['total'] = 0;

		/*********************************************/
		// Данный фрагмент кода взят из ./catalog/controller/checkout/confirm.php

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();
//		echo '<pre>';print_r($this->cart->getProducts());exit;

		$this->load->model('setting/extension');

		// Также учитываем доставку, налоги и пр. модули
		$results = $this->model_setting_extension->getExtensions('total');

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('total/' . $result['code']);

				$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
			}
		}

		$this->data['products'] = array();

		foreach ($this->cart->getProducts() as $product) {
			$this->data['products'][] = array(
				'key'                 => $product['key'],
				'product_id'          => $product['product_id'],
				'name'                => $product['name'],
				'model'               => $product['model'],
				'quantity'            => $product['quantity'],
				'subtract'            => $product['subtract'],
				'price'               => $product['price'],
				'total'               => $product['price'] * $product['quantity'],
			);
		}

		$sort_order = array();

		foreach ($total_data as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $total_data);

		$this->data['totals'] = $total_data;

		/*********************************************/

        // если есть 'order_id'
        if(empty($products))
        {
			$this->load->model('account/order');

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$this->data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'return'   => $this->url->link('account/return/insert', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL')
				);
			}

			$this->data['totals'] = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
        }

        // подгружаем хелпер для вывода суммы прописью
        $this->load->helper('num2str');

        $this->data['order_id'] = $order_info['order_id'];

        // имя плательщика
        $this->data['name'] = $order_info['payment_firstname'].' '.$order_info['payment_lastname'];

        // телефон плательщика
        $this->data['telephone'] = $order_info['telephone'];

        // дата создания заказа
        $this->data['date_added'] = getdate(strtotime($order_info['date_added']));

        // срок действия счета (+2 дня от даты заказа)
        $this->data['deadline'] = date('d.m.y', strtotime($order_info['date_added']) + 172800);

        // "від 29 Листопада 2012 р."
        $this->data['date_added'] = $this->data['date_added']['mday'].' '.$this->language->get('text_month_'.$this->data['date_added']['mon']).' '.$this->data['date_added']['year'];

        // префикс + номер счета
        $this->data['invoice'] = $order_info['invoice_prefix'].$order_info['invoice_no'];

		$config = $this->model_setting_setting->getSetting('cashless');

		$this->data['stamp'] = DIR_IMAGE.$config['stamp_img'];

		$this->data['who_wrote_bill'] = $config['who_write_bill'];


        // генерация номера счет-фактуры
        // если есть данные о заказе и нет номера счет-фактуры, то...
        if (/*$order_info && */!$order_info['invoice_no'])
        {
            // определяем максимальное число номера
            // нарушил принцип MVC.
            $query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

            if ($query->row['invoice_no'])
            {
                // если номер больше нуля - прибавляем 1
                $invoice_no = $query->row['invoice_no'] + 1;
            }
			else
            {
                $invoice_no = 1;
            }

            // сохраняем полученные значения
            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int) $invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int) $order_info['order_id'] . "'");

            // выводим в браузер
            $this->data['invoice'] =  $order_info['invoice_prefix'] . $invoice_no;
        }


        if (!$order_info['payment_address_2'])
        {
            $this->data['address'] = $order_info['payment_zone'] . ', ' . $order_info['payment_city'] . ', ' . $order_info['payment_address_1'];
        }
		else
        {
            $this->data['address'] = $order_info['payment_zone'] . ', ' . $order_info['payment_city'] . ', ' . $order_info['payment_address_1'] . ', ' . $order_info['payment_address_2'];
        }

        $this->data['postcode'] = $order_info['payment_postcode'];

        $this->template = $this->config->get('config_template') . '/template/payment/cashless_printpay.tpl';

        if (!file_exists(DIR_TEMPLATE . $this->template))
        {
            $this->template = 'default/template/payment/cashless_printpay.tpl';
        }

        $this->response->setOutput($this->render());
    }


    // подтверждение заказа
    public function confirm()
    {
        $this->language->load('payment/cashless');

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        if ($order_info)
        {
            $comment = $this->language->get('text_instruction_mail') . "\n";
            $comment .= str_replace('{href}', $this->url->link('payment/cashless/printpay', 'order_id=' . $order_info['order_id'], 'SSL').'&key='.md5($order_info['date_added'].$order_info['ip']), $this->language->get('text_printpay_mail')) . "\n";
            $comment .= str_replace('%s',$order_info['order_id'],$this->language->get('text_payment_mail'));

            $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('cashless_order_status_id'), $comment, true);
        }
    }
}

?>