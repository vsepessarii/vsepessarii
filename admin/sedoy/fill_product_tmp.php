<?php
	@mysqli_query($ddb, 'TRUNCATE TABLE product_tmp');
	$qu_product = '
		SELECT	product.product_id as id,
						product_description.name as name,
						manufacturer.name as manufacturer,
						product.model as model,
						product.sku as sku,
						product.price_in as price_in,
						product.price as price
		FROM		product_description,
						product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
		WHERE		product.product_id = product_description.product_id
	';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		unset($options); unset($options_txt); $oid = 0;
		unset($values); unset($values_txt); $vid = 0;
		$qu_option = '
			SELECT		`product_option`.*, `option_description`.`name`
			FROM			`product_option`, `option`, `option_description`
			WHERE			`product_option`.`product_id` = "'.$ro_product['id'].'" &&
								`product_option`.`option_id` = `option`.`option_id` &&
								`option`.`option_id` = `option_description`.`option_id`
			ORDER BY	`option`.`sort_order`
		';
		$re_option = @mysqli_query($ddb, $qu_option);
		while ($ro_option = @mysqli_fetch_array($re_option)) {
			$options[$oid] = $ro_option['product_option_id'];
			$options_txt[$oid] = $ro_option['name'];
			$qu_value = '
				SELECT		`product_option_value`.*, `option_value_description`.`name`
				FROM			`product_option_value`, `option_value`, `option_value_description`
				WHERE			`product_option_value`.`product_option_id`="'.$ro_option['product_option_id'].'" &&
									`product_option_value`.`option_value_id`=`option_value`.`option_value_id` &&
									`option_value`.`option_value_id` = `option_value_description`.`option_value_id`
				ORDER BY	`option_value`.`sort_order`
			';
			$re_value = @mysqli_query($ddb, $qu_value);
			//echo mysqli_error($ddb);
			while ($ro_value = @mysqli_fetch_array($re_value)) {
				$values[$oid][$vid] = $ro_value['product_option_value_id'];
				$values_txt[$oid][$vid] = $ro_value['name'];
				$values_price[$oid][$vid] = ($ro_value['price_prefix'] == '+') ? $ro_value['price'] : '-1'*$ro_value['price'];  //поправка цены
				$vid++;
			}
			$vid = 0;
			$oid++;
		}
		if (@count($options) > 0) {
			//расчитываем максимальное количество вариантов
			$n = 1; for ($i=0;$i<count($options);$i++) $n = $n * count($values[$i]);
			//проходим по всем вариантам
			for ($i=0;$i<$n;$i++) {
				$d_price = 0; //поправка цены
				if (count($options) == 1) {
					//одно свойство
					$option = $options[0].':'.$values[0][$i];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i].')';
					$d_price = $d_price + $values_price[0][$i];
				} elseif (count($options) == 2) {
					//два свойства
					$i_0 = floor($i/count($values[1]));
					$i_1 = $i - $i_0*count($values[1]);
					$option = $options[0].':'.$values[0][$i_0].','.$options[1].':'.$values[1][$i_1];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i_0].'; '.$options_txt[1].': '.$values_txt[1][$i_1].')';
					$d_price = $d_price + $values_price[0][$i_0] + $values_price[1][$i_1];
				} elseif (count($options) == 3) {
					//три свойства
					$i_0 = floor($i/count($values[1])/count($values[2]));
					$i_1 = floor(($i-$i_0*count($values[0]))/count($values[2]));
					$i_2 = $i - $i_0*count($values[1]) - $i_1*count($values[2]);
					$option = $options[0].':'.$values[0][$i_0].','.$options[1].':'.$values[1][$i_1].','.$options[2].':'.$values[2][$i_2];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i_0].'; '.$options_txt[1].': '.$values_txt[1][$i_1].'; '.$options_txt[2].': '.$values_txt[2][$i_2].')';
					$d_price = $d_price + $values_price[0][$i_0] + $values_price[1][$i_1] + $values_price[2][$i_2];
				}
				@mysqli_query($ddb, '
					INSERT INTO	`product_tmp`
					SET					`id`="'.$ro_product['id'].'",
											`label`="'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' '.$option_label.' '.$ro_product['sku'].' ('.floatval($ro_product['price']+$d_price).' руб.)",
											`name`="'.$ro_product['name'].'",
											`manufacturer`="'.$ro_product['manufacturer'].'",
											`model`="'.$ro_product['model'].'",
											`sku`="'.$ro_product['sku'].'",
											`price`="'.floatval($ro_product['price']).'",
											`price_in`="'.floatval($ro_product['price_in']+$d_price).'",
											`option`="'.$option.'"
				');
			}
		} else {
			@mysqli_query($ddb, '
			INSERT INTO	`product_tmp`
			SET					`id`="'.$ro_product['id'].'",
									`label`="'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' '.$option_label.' '.$ro_product['sku'].' ('.floatval($ro_product['price']+$d_price).' руб.)",
									`name`="'.$ro_product['name'].'",
									`manufacturer`="'.$ro_product['manufacturer'].'",
									`model`="'.$ro_product['model'].'",
									`sku`="'.$ro_product['sku'].'",
									`price`="'.floatval($ro_product['price']).'",
									`price_in`="'.floatval($ro_product['price_in']).'",
									`option`=""
			');
		}						
		
	}

