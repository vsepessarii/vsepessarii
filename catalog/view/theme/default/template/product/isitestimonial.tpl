<?php echo $header; ?>
<div class="content-container">
	<section class="box">
		<h1 class="box-heading"><?php echo $heading_title; ?></h1>
		<?php
			if ($this->customer->isLogged() > 0) {
		?>
		<div class="box-content review" style="padding: 14px 19px;">
			<div style="padding-bottom: 10px; font-style: italic;"><?php echo $text_conditions ?></div>
  
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="testimonial">
				<div class="content">
	        <table width="100%">
		        <!--tr>
	            <td><?php echo $entry_title ?><br />
	              <input type="text" name="title" value="<?php echo $title; ?>" size = 90 />
	  	            <?php if ($error_title) { ?>
	              <span class="error"><?php echo $error_title; ?></span>
	              <?php } ?></td>
	          </tr-->
          <tr>
            <td><?php echo $entry_enquiry ?><span class="required">*</span><br />
              <textarea name="description" style="width: 99%;" rows="10"><?php echo $description; ?></textarea><br />

              <?php if ($error_enquiry) { ?>
              <span class="error"><?php echo $error_enquiry; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_name ?><br />
              <input type="text" name="name" value="<?php echo $name; ?>" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?>
		</td>
          </tr>
          <tr>
             <td><?php echo $entry_city ?><br />
			<input type="text" name="city" value="<?php echo $city; ?>" />
		</td>
          </tr>
          <tr>
            <td>
		  <?php echo $entry_email ?><br />
              <input type="text" name="email" value="<?php echo $email; ?>" />
              <?php if ($error_email) { ?>
              <span class="error"><?php echo $error_email; ?></span>
              <?php } ?></td>
          </tr>
          <tr>
            <td><br><?php echo $entry_rating; ?> &nbsp;&nbsp;&nbsp; 
        		<input type="radio" name="rating" value="1" id="rating-1" style="margin: 0;" <?php if ( $rating == 1 ) echo 'checked="checked"';?> /><label for="rating-1"> 1</label>
        		&nbsp;&nbsp;&nbsp;&nbsp;
        		<input type="radio" name="rating" value="2" id="rating-2" style="margin: 0;" <?php if ( $rating == 2 ) echo 'checked="checked"';?> /><label for="rating-2"> 2</label>
        		&nbsp;&nbsp;&nbsp;&nbsp;
        		<input type="radio" name="rating" value="3" id="rating-3" style="margin: 0;" <?php if ( $rating == 3 ) echo 'checked="checked"';?> /><label for="rating-3"> 3</label>
        		&nbsp;&nbsp;&nbsp;&nbsp;
        		<input type="radio" name="rating" value="4" id="rating-4" style="margin: 0;" <?php if ( $rating == 4 ) echo 'checked="checked"';?> /><label for="rating-4"> 4</label>
        		&nbsp;&nbsp;&nbsp;&nbsp;
        		<input type="radio" name="rating" value="5" id="rating-5" style="margin: 0;" <?php if ( $rating == 5 ) echo 'checked="checked"';?> /><label for="rating-5"> 5</label>
        		<br /><br>

          	</td>
          </tr>
          <tr>
            <td>
              
              <img src="index.php?route=information/contact/captcha" />
              <?php if ($error_captcha) { ?>
              <span class="error"><?php echo $error_captcha; ?></span>
              <?php } ?>
							
							<br>
		<?php echo $entry_captcha; ?><span class="required">*</span> <br>

              <input type="text" name="captcha" value="<?php echo $captcha; ?>" /><br>
		</td>
          </tr>
        </table>
	  </div>
      <div class="buttons">
        <table width=100% style="margin-bottom: 10px;">
          <tr>
            <td width=50%><a  onclick="$('#testimonial').submit();" class="button blue" style="margin-left: 0;"><span><?php echo $button_send; ?></span></a></td>
		<td style="text-align: right;"><a class="button blue" href="<?php echo $showall_url;?>" style="margin-right: 0;"><span><?php echo $show_all; ?></span></a>
		</td>
          </tr>
        </table>

      </div>
    </form>
	</div>

<?php 
	} else {
?>
		<div class="box-content review" style="padding: 14px 19px;">
			Оставлять комментарии могут только зарегистрированные пользователи.
			<a href="/index.php?route=account/login" class="button blue">Вход</a> 
			
			<a href="/index.php?route=account/simpleregister" class="button blue">Регистрация</a>
		</div>
<?php		
	}    
?>    
  <div class="bottom">
    <div class="left"></div>
    <div class="right"></div>
    <div class="center"></div>
  </div>
  </section>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $footer; ?> 