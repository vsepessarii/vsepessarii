<?php
	//отчет по товарам с привязкой к заказам
	/*
	session_start();
	$ddb = @mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmed_brief');
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	*/
	require 'connect.php';
	if ($ro_user['user_group_id'] == 1) {
		$br = chr(13).chr(10);
		$ret = '"№ заказа";"Дата";"ID";"Артикул";"Название";"Производитель";"Модель";"Опция 1";Опция 2";Опция 3";"Категория";"Подкатегория";"Вх. цена";"Исх. цена";"Количество (в заказе)";"Итого";"Прибыль";"%";"Город"'.$br;
		header('Content-Type: csv/plain; charset=cp-1251');
		header('Content-Disposition: attachment; filename="report_orders_products_'.date('Y-m-d H-i-s').'.csv"');
		$qu_product = '
			SELECT	*
			FROM 		tmp_report_orders_products
			WHERE		1
							'.(($_SESSION['report_orders_products_pid'] != '') ? ' && `PID` LIKE "%'.$_SESSION['report_orders_products_pid'].'%"' : '').' 	
							'.(($_SESSION['report_orders_products_oid'] != '') ? ' && `OID` LIKE "%'.$_SESSION['report_orders_products_oid'].'%"' : '').' 	
							'.(($_SESSION['report_orders_products_date_min'] != '') ? ' && `DATE`>="'.$_SESSION['report_orders_products_date_min'].' 00:00:00"' : '').' 	
							'.(($_SESSION['report_orders_products_date_max'] != '') ? ' && `DATE`<="'.$_SESSION['report_orders_products_date_max'].' 23:59:59"' : '').' 	
							'.(($_SESSION['report_orders_products_art'] != '') ? ' && `ART` LIKE "%'.$_SESSION['report_orders_products_art'].'%"' : '').' 	
							'.(($_SESSION['report_orders_products_name'] != '') ? ' && `NAME`.`name` LIKE "%'.$_SESSION['report_orders_products_name'].'%"' : '').' 	
							'.(($_SESSION['report_orders_products_manufacturer'] != '') ? ' && `MANUFACTURER`="'.$_SESSION['report_orders_products_manufacturer'].'"' : '').' 	
							'.(($_SESSION['report_orders_products_model'] != '') ? ' && `MODEL` LIKE "%'.$_SESSION['report_orders_products_model'].'%"' : '').' 	
							'.(($_SESSION['report_orders_products_price_in_min'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_orders_products_price_in_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_price_in_max'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_orders_products_price_in_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_price_out_min'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_orders_products_price_out_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_price_out_max'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_orders_products_price_out_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_quantity_min'] != '') ? ' && `QUANTITY`>="'.$_SESSION['report_orders_products_quantity_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_quantity_max'] != '') ? ' && `QUANTITY`<="'.$_SESSION['report_orders_products_quantity_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_status']) ? ' && `order`.`order_status_id`="'.$_SESSION['report_orders_products_status'].'"' : '').' 	
							'.(($_SESSION['report_orders_products_option_1'] != '') ? ' && `OPTION_1` LIKE "%'.$_SESSION['report_orders_products_option_1'].'%"' : '').'
							'.(($_SESSION['report_orders_products_option_2'] != '') ? ' && `OPTION_2` LIKE "%'.$_SESSION['report_orders_products_option_2'].'%"' : '').'
							'.(($_SESSION['report_orders_products_option_3'] != '') ? ' && `OPTION_3` LIKE "%'.$_SESSION['report_orders_products_option_3'].'%"' : '').'
							'.(($_SESSION['report_orders_products_category'] != '') ? ' && `CATEGORY`="'.$_SESSION['report_orders_products_category'].'"' : '').'
							'.(($_SESSION['report_orders_products_subcategory'] != '') ? ' && `SUBCATEGORY`="'.$_SESSION['report_orders_products_subcategory'].'"' : '').'
							'.(($_SESSION['report_orders_products_total_min'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_orders_products_total_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_total_max'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_orders_products_total_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_min'] != '') ? ' && `MARGIN`>="'.$_SESSION['report_orders_products_margin_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_max'] != '') ? ' && `MARGIN`<="'.$_SESSION['report_orders_products_margin_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_percent_min'] != '') ? ' && `MARGIN_PERCENT`>="'.$_SESSION['report_orders_products_margin_percent_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_percent_max'] != '') ? ' && `MARGIN_PERCENT`<="'.$_SESSION['report_orders_products_margin_percent_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_city'] != '') ? ' && `CITY`="'.$_SESSION['report_orders_products_city'].'"' : '').'
							'.(($_SESSION['report_orders_products_status']) ? ' && `STATUS`="'.$_SESSION['report_orders_products_status'].'"' : '').' 	

							/*
							'.(($_SESSION['report_orders_products_option_1'] != '') ? ' && `OPTION_1`="'.$_SESSION['report_orders_products_option_1'].'"' : '').'
							'.(($_SESSION['report_orders_products_option_2'] != '') ? ' && `OPTION_2`="'.$_SESSION['report_orders_products_option_2'].'"' : '').'
							'.(($_SESSION['report_orders_products_option_3'] != '') ? ' && `OPTION_3`="'.$_SESSION['report_orders_products_option_3'].'"' : '').'
							'.(($_SESSION['report_orders_products_category'] != '') ? ' && `CATEGORY`="'.$_SESSION['report_orders_products_category'].'"' : '').'
							'.(($_SESSION['report_orders_products_subcategory'] != '') ? ' && `SUBCATEGORY`="'.$_SESSION['report_orders_products_subcategory'].'"' : '').'
							'.(($_SESSION['report_orders_products_total_min'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_orders_products_total_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_total_max'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_orders_products_total_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_min'] != '') ? ' && `MARGIN`>="'.$_SESSION['report_orders_products_margin_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_max'] != '') ? ' && `MARGIN`<="'.$_SESSION['report_orders_products_margin_max'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_percent_min'] != '') ? ' && `MARGIN_PERCENT`>="'.$_SESSION['report_orders_products_margin_percent_min'].'"' : '').'
							'.(($_SESSION['report_orders_products_margin_percent_max'] != '') ? ' && `MARGIN_PERCENT`<="'.$_SESSION['report_orders_products_margin_percent_max'].'"' : '').'
							*/
			ORDER	BY	OID DESC
		';

		$re_product = @mysqli_query($ddb, $qu_product);
		$summ['quantity'] = 0;
		$summ['total'] = 0;
		$summ['margin'] = 0;
		$summ['margin_percent'] = 0;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$summ['quantity'] += $ro_product['QUANTITY'];
			$summ['total'] += $ro_product['TOTAL'];
			$summ['margin'] += $ro_product['MARGIN'];
			$summ['margin_percent'] += $ro_product['MARGIN_PERCENT'];
			$ret .= '"'.$ro_product['OID'].'";"'.$ro_product['DATE'].'";"'.$ro_product['PID'].'";"'.$ro_product['ART'].'";"'.$ro_product['NAME'].'";"'.$ro_product['MANUFACTURER'].'";"'.$ro_product['MODEL'].'";"'.
							$ro_product['OPTION_1'].'";"'.$ro_product['OPTION_2'].'";"'.$ro_product['OPTION_3'].'";"'.$ro_product['CATEGORY'].'";"'.$ro_product['SUBCATEGORY'].'";"'.str_replace('.',',',$ro_product['PRICE_IN']).
							'";"'.str_replace('.',',',$ro_product['PRICE_OUT']).'";"'.str_replace('.',',',$ro_product['QUANTITY']).'";"'.str_replace('.',',',$ro_product['TOTAL']).'";"'.str_replace('.',',',$ro_product['MARGIN']).'";"'.
							str_replace('.',',',$ro_product['MARGIN_PERCENT']).'";"'.$ro_product['CITY'].'"'.$br;
		}
		//$summ['profit_percent'] = @round($summ['profit_percent']/$nu_order_total,2);
		echo iconv('utf-8','cp1251',$ret);
	} else {
		header('Location: /');
	}

