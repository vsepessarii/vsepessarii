<?php
	//клонирование скидочных купонов
	require 'connect.php';
	if ($ro_user['user_group_id'] == 1) {
		if (isset($_POST['act']) && $_POST['act'] == 'clone') {
			//процесс клонирования
			$ro_coupon = Result($ddb, 'SELECT * FROM coupon WHERE coupon_id="'.intval($_POST['coupon_id']).'"');
			$coupon_id = $ro_coupon['coupon_id'];
			if ($coupon_id > 0) {
				$start_code = intval($_POST['start_code']);
				$end_code = intval($_POST['end_code']);
				$n = 0;
				for ($j = $start_code; $j<=$end_code; $j++) {
					$exist = Result($ddb, 'SELECT coupon_id FROM coupon WHERE code="'.$j.'"');
					if ($exist['coupon_id'] == '') {
						//такого купона нет - можно добавлять
						$n++;
						@mysqli_query($ddb, '
							INSERT INTO coupon
							SET		name="'.$ro_coupon['name'].'",
										code="'.$j.'",
										type="'.$ro_coupon['type'].'",
										discount="'.$ro_coupon['discount'].'",
										logged="'.$ro_coupon['logged'].'",
										shipping="'.$ro_coupon['shipping'].'",
										total="'.$ro_coupon['total'].'",
										date_start="'.$ro_coupon['date_start'].'",
										date_end="'.$ro_coupon['date_end'].'",
										uses_total="'.$ro_coupon['uses_total'].'",
										uses_customer="'.$ro_coupon['uses_customer'].'",
										status="'.$ro_coupon['status'].'",
										date_added="'.$ro_coupon['date_added'].'",
										clone_coupon_id="'.$coupon_id.'"
						');
						$clone_id = @mysqli_insert_id($ddb);
						
						//не клонируем coupon_history
						//клонируем coupon_product
						$qu_product = 'SELECT * FROM coupon_product WHERE coupon_id="'.$coupon_id.'"';
						$re_product = @mysqli_query($ddb, $qu_product);
						while ($ro_product = @mysqli_fetch_array($re_product)) @mysqli_query($ddb, '
							INSERT INTO coupon_product
							SET	coupon_id="'.$clone_id.'",
									product_id="'.$ro_product['product_id'].'"
						');
					}
				}
			}
			$tpl['msg'] = 'Создано '.$n.' копий купона '.$ro_coupon['code'].' '.$ro_coupon['name'];
		}

		$tpl['body_title'] = 'Клонирование купонов';
		$tpl['meta_title'] = 'Клонирование купонов';
		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
				
				.gray {
					border-color: #BBB !important;
					color: #BBB;	
				}
				
			</style>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			<form method="post">
			<input type="hidden" name="act" value="clone" />
			<table width="450">
				<tr>
					<td width="40%" align="right">Клонировать купон:</td>
					<td width="60%">
						<select name="coupon_id">
							<opton value=""></option>
		';
		$qu_coupon = 'SELECT * FROM coupon WHERE clone_coupon_id="0" ORDER BY code';
		$re_coupon = @mysqli_query($ddb, $qu_coupon);
		echo mysqli_error($ddb);
		while ($ro_coupon = @mysqli_fetch_array($re_coupon)) $tpl['body_content'] .= '<option value="'.(isset($ro_coupon['coupon_id']) ? $ro_coupon['coupon_id'] : '').'"'.((isset($_POST['coupon_id']) && isset($ro_coupon['coupon_id']) && $ro_coupon['coupon_id'] == $_POST['coupon_id']) ? ' selected' : '').'>'.$ro_coupon['code'].' '.$ro_coupon['name'].' '.round($ro_coupon['discount'],2).(($ro_coupon['type'] == 'P') ? '%' : ' руб.').(($ro_coupon['total'] > 0) ? ' (от '.round($ro_coupon['total'],2).' руб.)' : '').'</option>';
		$tpl['body_content'] .= '							
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">Диапазон номеров:</td>
					<td>
						<input name="start_code" value="'.(isset($start_code) ? $start_code : '').'" size="10" maxlength="10" />
						-
						<input name="end_code" value="'.(isset($end_code) ? $end_code : '').'" size="10" maxlength="10" />
					</td> 
				<tr>
					<td></td>
					<td><input type="submit" class="txt" value="Клонировать купон" /></td>
				</tr>
			</table>
			</form>
		';
		require 'template.php';	
	} else {
		header('Location: /');
	}

