<?php echo $header; ?>

<div class="content-container">
	<section class="box">
		<h1 class="box-heading"><?php echo $heading_title; ?></h1>
		<nav class="breadcrumb">
	    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	    <?php } ?>
    </nav>
		<div class="product-compare">
			<a href="https://vsepessarii.ru/index.php?route=product/compare" id="compare-total">Сравнение товаров (0)</a>
		</div>						
		
		<div class="box-content" style="margin-top: 30px !important;">
			<?php echo $success_text; ?>
			<br /><br />
			<a href="<?php echo $button_ok_url;?>" class="button blue" style="margin-left: 0;" id="inter"><span><?php echo $button_ok; ?></span></a>
		</div>
	</section>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $footer; ?>