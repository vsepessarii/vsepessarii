function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');

		$('.display').html('<span class="displayText">Display:</span> <div class="gridListActive"></div><a onclick="display(\'grid\');"><div class="listView"></div></a>');

		$.cookie('display', 'list');
	} else {
		$('.product-list').attr('class', 'product-grid');

		$('.display').html('<span class="displayText">Display:</span> <a onclick="display(\'list\');" class="gridList"></a><div  class="listViewActive"></div>');

		$.cookie('display', 'grid');
	}
}

jQuery(document).ready(function() {
	$('.box-category .box-heading').click(function() {
		$parent = $(this).parent();
		$.cookie('categories', ($('.box-content', $parent).css('display') == 'none'));
		$('.box-content', $parent).slideToggle('slow');
	});

	state = $.cookie('categories');
	if (state == 'false') {
		$('.box-category .box-content').css('display', 'none');
	} else {
		$('.box-category .box-content').css('display', 'block');
	}

	view = $.cookie('display');
	if (view) {
		display(view);
	} else {
		display('list');
	}

	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5
	});

	$('#tabs a').tabs({active:1});

    $('a.smenu').click(function(){
        $(this).next().toggle();
        if($(this).next().css('display')=="block"){
            $(this).css('background','url(/catalog/view/theme/brief/image/minus.png) right 2px no-repeat') ;
        } else {
            $(this).css('background','url(/catalog/view/theme/brief/image/plus.png) right 2px no-repeat') ;
        }
    })

});