<?php
	//отсылаем E-mail из очереди (таблица MAIL)
	/*
	$ddb = mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	mysqli_query("set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	mysqli_select_db('briefmed_brief');
	*/
	//убрал коннект, т.к. он был для теста

	$config_email  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_email"'));
	$config_name  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_name"'));
	$config_mail_protocol  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_mail_protocol"'));
	$config_smtp_host  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_host"'));
	$config_smtp_username  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_username"'));
	$config_smtp_password  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_password"'));
	$config_smtp_port  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_port"'));
	$config_smtp_timeout  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_timeout"'));


	require 'class/class.phpmailer.php';
	$qu_mail = 'SELECT * FROM mail WHERE DATE_SENT="0000-00-00 00:00:00" ORDER BY DATE_ADDED ASC LIMIT 1';
	$re_mail = mysqli_query($qu_mail);
	while ($ro_mail = mysqli_fetch_array($re_mail)) {
		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/html';
		if ($config_mail_protocol == 'mail') {
			$mail->IsMail();
		} else {
			$mail->IsSMTP();
			$mail->SMTPAuth = true; 
			$mail->Host = $config_smtp_host['value'];
			$mail->Port = $config_smtp_port['value'];
			$mail->Username = $config_smtp_username['value'];
			$mail->Password = $config_smtp_password['value'];  	
		}
		$mail->SetFrom($config_email['value'], $config_name['value']);
		//$mail->AddReplyTo($config_email['value'], $config_name['value']);
		$mail->AddReplyTo($ro_mail['EMAIL_FROM'], (($ro_mail['NAME_FROM'] != '') ? $ro_mail['NAME_FROM'] : $ro_mail['EMAIL_FROM']));
		$mail->AddAddress($ro_mail['EMAIL_TO'], (($ro_mail['NAME_TO'] != '') ? $ro_mail['NAME_TO'] : $ro_mail['EMAIL_TO']));
		$mail->Subject = $ro_mail['SUBJECT'];
		$mail->AltBody = $ro_mail['BODY_PLAIN'];
		$mail->Body = ($ro_mail['BODY_HTML'] != '') ? $ro_mail['BODY_HTML'] : nl2br($ro_mail['BODY_PLAIN']);
		//$mail->SMTPDebug = 2;
		
		if ($ro_mail['ATTACHMENT_NAME'] != '') $mail->AddStringAttachment($ro_mail['ATTACHMENT_BODY'],$ro_mail['ATTACHMENT_NAME']);
		//$mail->MsgHTML($text);
		$mail->Send();
		
		//if($mail->Send()) mysqli_query('UPDATE mail SET DATE_SENT=NOW() WHERE ID='.$ro_mail['ID']);
		mysqli_query('UPDATE mail SET DATE_SENT=NOW() WHERE ID='.$ro_mail['ID']);
		//echo $mail->ErrorInfo;
		//echo '<pre>'; var_dump($mail); echo '</pre>';
	}
	mysqli_query('DELETE FROM mail WHERE DATE_SENT<>"0000-00-00 00:00:00" && DATE_ADDED<DATE_ADD(NOW(), INTERVAL -1 MONTH)');
?>