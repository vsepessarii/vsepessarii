<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/category.png" alt="" /> <?php echo $heading_title; ?></h1>
			<div class="buttons">
				<form method="get" target="/admin/index.php" id="filter" style="display: inline-block; margin-right: 100px;">
					Статус: 
					<input type="hidden" name="route" value="catalog/category" />
					<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
					<select name="filter_status" onchange="$('form#filter').submit();">
						<option></option>
						<option value="1"<?php if ($_GET['filter_status'] == '1') echo ' selected';?>>Включен</option>
						<option value="0"<?php if ($_GET['filter_status'] == '0') echo ' selected';?>>Отключен</option>
					</select>
				</form>
				<a onclick="location = '<?php echo $insert; ?>'" class="button"><?php echo $button_insert; ?></a>
				<a onclick="$('#form').submit();" class="button"><?php echo $button_delete; ?></a>
			</div>
		</div>
		<div class="content">
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
					<thead>
						<tr>
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
							<td class="left"><?php echo $column_name; ?></td>
							<td class="right"><?php echo $column_sort_order; ?></td>
							<td class="right">Статус</td>
							<td class="right"><?php echo $column_action; ?></td>
						</tr>
					</thead>
					<tbody>
						<?php if ($categories) { ?>
						<?php foreach ($categories as $category) { if ($_GET['filter_status'] != '' && $_GET['filter_status'] != $category['status']) continue; ?>
						<tr>
							<td style="text-align: center;"><?php if ($category['selected']) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $category['category_id']; ?>" />
								<?php } ?></td>
							<?php if ($category['href']) { ?>
								<td class="left"><?php echo $category['indent']; ?><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></td>
							<?php } else { ?>
								<td class="left"><?php echo $category['indent']; ?><?php echo $category['name']; ?></td>
							<?php } ?>
							<td class="right"><?php echo $category['sort_order']; ?></td>
							<td class="right"><?php echo ($category['status']) ? 'Включен' : 'Отключен'; ?></td>
							<td class="right"><?php foreach ($category['action'] as $action) { ?>
								[ <a href="<?php echo $action['href']; ?>&filter_status=<?php echo $_GET['filter_status'];?>"><?php echo $action['text']; ?></a> ]
								<?php } ?></td>
						</tr>
						<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="4"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</form>
		</div>
	</div>
</div>
<?php echo $footer; ?>