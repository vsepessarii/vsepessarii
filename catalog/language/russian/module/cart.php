<?php
// Heading 
$_['heading_title'] = '<span onClick="location.href=\'/index.php?route=checkout/simplecheckout\'" style="cursor:pointer;">Ваша <em>корзина:</em></span>';

// Text
$_['text_items']    = '<em>%s</em> товар(а/ов) на сумму <em>%s</em>';
$_['text_empty']    = 'Ничего не куплено!';
$_['text_cart']     = 'Просмотр корзины';
$_['text_checkout'] = 'Оформление заказа';
?>