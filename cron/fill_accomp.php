<?php
	//обновление таблицы product_accomp
	mysqli_query('TRUNCATE TABLE product_accomp');

	$qu_product = 'SELECT product_id FROM product ORDER BY product_id';
	$re_product = mysqli_query($qu_product);
	while ($ro_product = mysqli_fetch_array($re_product)) {
		$id = $ro_product['product_id'];
		$tmp = mysqli_fetch_array(mysqli_query('SELECT model, sku, price_in, price, manufacturer_id, status FROM product WHERE product_id="'.$id.'"'));
		$model = $tmp['model'];
		$articul = $tmp['sku'];
		$price_in = $tmp['price_in'];
		$price_out = $tmp['price'];
		$status = $tmp['status'];
		$manufacturer_id = $tmp['manufacturer_id'];
		$tmp = mysqli_fetch_array(mysqli_query('SELECT name FROM product_description WHERE product_id="'.$id.'"'));
		$name = $tmp['name'];
		$tmp = mysqli_fetch_array(mysqli_query('SELECT name FROM manufacturer WHERE manufacturer_id="'.$manufacturer_id.'"'));
		$manufacturer = $tmp['name'];
		$qu_special = 'SELECT price FROM product_special WHERE product_id="'.$ro_product['product_id'].'"';
		$re_special = mysqli_query($qu_special);
		unset($tmp);
		while ($ro_special = mysqli_fetch_array($re_special)) $tmp[] = round($ro_special['price'],0);
		$price_special = @implode(' , ',$tmp);
			
		if (mysqli_num_rows(mysqli_query('SELECT * FROM product_to_category WHERE product_id="'.$id.'"')) == 1) {
			$tmp = mysqli_fetch_array(mysqli_query('
				SELECT	category_description.name
				FROM 		category_description, 
								product_to_category 
				WHERE 	category_description.category_id = product_to_category.category_id &&
								product_to_category.main_category = "0" &&
								product_to_category.product_id = "'.$id.'"
			'));
			$category = $tmp['name'];
			$tmp = mysqli_fetch_array(mysqli_query('
				SELECT	category_description.name
				FROM 		category_description, 
								product_to_category 
				WHERE 	category_description.category_id = product_to_category.category_id &&
								product_to_category.main_category = "1" &&
								product_to_category.product_id = "'.$id.'"
			'));
			$category = $tmp['name'];
			$subcategory = '';
		} else {
			$tmp = mysqli_fetch_array(mysqli_query('
				SELECT	category_description.category_id,
								category_description.name
				FROM 		category_description, 
								product_to_category 
				WHERE 	category_description.category_id = product_to_category.category_id &&
								product_to_category.main_category = "0" &&
								product_to_category.product_id = "'.$id.'"
			'));
			$subcategory = $tmp['name'];
			$subcategory_id = $tmp['category_id'];
			$tmp = mysqli_fetch_array(mysqli_query('
				SELECT	category_description.category_id,
								category_description.name
				FROM 		category_description, 
								product_to_category 
				WHERE 	category_description.category_id = product_to_category.category_id &&
								product_to_category.main_category = "1" &&
								product_to_category.product_id = "'.$id.'"
			'));
			 $category = $tmp['name'];
			 $category_id = $tmp['category_id'];
			 if (mysqli_num_rows(mysqli_query('SELECT category_id FROM category WHERE category_id="'.$category_id.'" && parent_id="'.$subcategory_id.'"')) > 0) {
			 	//если категория и субкатегория перепутаны
			 	$tmp = $subcategory;
			 	$subcategory = $category;
			 	$category = $tmp;
			 }
		}
		mysqli_query('
			INSERT INTO	`product_accomp`
			SET					`ID`="'.$id.'",
									`NAME`="'.$name.'",
									`MANUFACTURER`="'.$manufacturer.'",
									`MODEL`="'.$model.'",
									`ARTICUL`="'.$articul.'",
									`CATEGORY`="'.$category.'",
									`SUBCATEGORY`="'.$subcategory.'",
									`PRICE_IN`="'.$price_in.'",
									`PRICE_OUT`="'.$price_out.'",
									`PRICE_SPECIAL`="'.$price_special.'",
									`STATUS`="'.$status.'"
		');
	}
?>