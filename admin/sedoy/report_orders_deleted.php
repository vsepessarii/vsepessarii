﻿<?php
//отчет по удаленным заказам
error_reporting(0);
//	phpinfo();
require 'connect.php';

if ($ro_user['user_group_id'] == 1) {
	if ($_GET['s'] == '') $_GET['s'] = '`ID` DESC';
	if ($_POST['act'] == 'clear_filter') {
		$_SESSION['report_orders_deleted_id'] = '';
		$_SESSION['report_orders_deleted_date_added_from'] = '';
		$_SESSION['report_orders_deleted_date_added_to'] = '';
		$_SESSION['report_orders_deleted_time_added_from'] = '';
		$_SESSION['report_orders_deleted_time_added_to'] = '';
		$_SESSION['report_orders_deleted_date_deleted_from'] = '';
		$_SESSION['report_orders_deleted_date_deleted_to'] = '';
		$_SESSION['report_orders_deleted_time_deleted_from'] = '';
		$_SESSION['report_orders_deleted_time_deleted_to'] = '';
		$_SESSION['report_orders_deleted_fio'] = '';
		$_SESSION['report_orders_deleted_phone'] = '';
		$_SESSION['report_orders_deleted_email'] = '';
		$_SESSION['report_orders_deleted_total_from'] = '';
		$_SESSION['report_orders_deleted_total_to'] = '';
		$_SESSION['report_orders_deleted_status'] = '';
		$_SESSION['report_orders_deleted_user_added_id'] = '';
		$_SESSION['report_orders_deleted_user_deleted_id'] = '';
	}

	if ($_SESSION['report_orders_deleted_per_page'] == '') $_SESSION['report_orders_deleted_per_page'] = 25;
	if ($_POST['act'] == 'setting') {
		$_SESSION['report_orders_deleted_per_page'] = $_POST['report_orders_deleted_per_page'];
	}
	if ($_POST['act'] == 'filter') {
		$_SESSION['report_orders_deleted_id'] = $_POST['report_orders_deleted_id'];
		$_SESSION['report_orders_deleted_date_added_from'] = $_POST['report_orders_deleted_date_added_from'];
		$_SESSION['report_orders_deleted_date_added_to'] = $_POST['report_orders_deleted_date_added_to'];
		$_SESSION['report_orders_deleted_time_added_from'] = $_POST['report_orders_deleted_time_added_from'];
		$_SESSION['report_orders_deleted_time_added_to'] = $_POST['report_orders_deleted_time_added_to'];
		$_SESSION['report_orders_deleted_date_deleted_from'] = $_POST['report_orders_deleted_date_deleted_from'];
		$_SESSION['report_orders_deleted_date_deleted_to'] = $_POST['report_orders_deleted_date_deleted_to'];
		$_SESSION['report_orders_deleted_time_deleted_from'] = $_POST['report_orders_deleted_time_deleted_from'];
		$_SESSION['report_orders_deleted_time_deleted_to'] = $_POST['report_orders_deleted_time_deleted_to'];
		$_SESSION['report_orders_deleted_fio'] = $_POST['report_orders_deleted_fio'];
		$_SESSION['report_orders_deleted_phone'] = $_POST['report_orders_deleted_phone'];
		$_SESSION['report_orders_deleted_email'] = $_POST['report_orders_deleted_email'];
		$_SESSION['report_orders_deleted_total_from'] = $_POST['report_orders_deleted_total_from'];
		$_SESSION['report_orders_deleted_total_to'] = $_POST['report_orders_deleted_total_to'];
		$_SESSION['report_orders_deleted_status'] = $_POST['report_orders_deleted_status'];
		$_SESSION['report_orders_deleted_user_added_id'] = $_POST['report_orders_deleted_user_deleted_id'];
		$_GET['p'] = 1;
	}
	?>
	<html>
	<head>
		<title>Отчет по удаленным заказам (SedEdition)</title>
		<link type="text/css" href="/admin/index.php" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<script language="javascript">
			$(function() {
				$("#report_orders_deleted_date_added_from").datepicker();
				$("#report_orders_deleted_date_added_to").datepicker();
				$("#report_orders_deleted_date_deleted_from").datepicker();
				$("#report_orders_deleted_date_deleted_to").datepicker();
			});

			function ShowHideProducts(order_id) {
				var products = $('#products_'+order_id);
				if (products.css('display') == 'none') {
					products.css('display', 'block');
				} else {
					products.css('display', 'none');
				}
			}

			function ShowAllProducts() {
				$('td.order_products').each(function(i,elem) {
					var td_id = elem.id;
					var order_id = td_id.substr(9);
					var products = $('#products_'+order_id);
					products.css('display', 'block');
				});
			}

			function HideAllProducts() {
				$('td.order_products').each(function(i,elem) {
					var td_id = elem.id;
					var order_id = td_id.substr(9);
					var products = $('#products_'+order_id);
					products.css('display', 'none');
				});
			}
		</script>
	</head>
	<body>
	<style>
		td { word-wrap: break-word;}

		table.products {
			border-collapse: collapse;
			border: 1px solid gray;
			margin: 10px;
		}

		table.products tr.head td {
			background-color: #333;
			color: #FFF;
			font-weight: bold;
		}

		tr.order td {
			padding: 4px;
		}		
	</style>
	<div align="right">
		<br />
		<a href="#" onclick="ShowAllProducts(); return false;">Показать все</a>
		&nbsp;&nbsp;
		<a href="#" onclick="HideAllProducts(); return false;">Скрыть все</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="/index.php?route=common/home&token=<?php echo (isset($_SESSION['token']) ? $_SESSION['token'] : "")?>">Вернуться в панель управления</a>
		<!--
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="report_orders_csv.php">Скачать в CSV</a>
		-->
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="clear_filter" />
			<input type="submit" value="Сбросить фильтры" />
		</form>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="setting" />
			<!--
			Курьеру: <input name="courier_price" value="<?php echo $_SESSION['courier_price'];?>" />
			&nbsp;&nbsp;&nbsp;
			-->
			Выводить по:
			<select name="report_orders_deleted_per_page">
				<option value="25"<?php echo (isset($_SESSION) && $_SESSION['report_orders_deleted_per_page'] == '25') ? ' selected' : ''?>>25</option>
				<option value="50"<?php echo (isset($_SESSION) && $_SESSION['report_orders_deleted_per_page'] == '50') ? ' selected' : ''?>>50</option>
				<option value="100"<?php echo (isset($_SESSION) && $_SESSION['report_orders_deleted_per_page'] == '100') ? ' selected' : ''?>>100</option>
				<option value="200"<?php echo (isset($_SESSION) && $_SESSION['report_orders_deleted_per_page'] == '200') ? ' selected' : ''?>>200</option>
			</select>
			<input type="submit" value="Сохранить" />
		</form>
		<br /><br />
	</div>
	<table class="list">
		<thead>
		<tr>
			<td class="left" width="4%"><a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_id` ASC') ? '`order_id` DESC' : '`order_id` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_id` ASC') ? 'asc' : '').((isset($_GET['s'] ) && $_GET['s'] == '`order_id` DESC') ? 'desc' : '')?>">№</a></td>
			<td class="left" width="5%">
				<a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s'] ) && $_GET['s'] == '`order_date_added` ASC') ? '`order_date_added` DESC' : '`order_date_added` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_date_added` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_date_added` DESC') ? 'desc' : '')?>">Дата добавления</a>
				&nbsp;&nbsp;&nbsp;
				<a href="" onclick="$('#report_orders_deleted_date_added_from').val('<?php echo date('Y-m-d');?>'); $('#report_orders_deleted_date_added_to').val('<?php echo date('Y-m-d');?>'); return false" style="color: red;">Сегодня</a>
			</td>
			<td class="left" width="4%"><a href="?p=<?php echo isset($_GET['p']) ? $_GET['p'] : ''?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_time_added` ASC') ? '`order_time_added` DESC' : '`order_time_added` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_time_added` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_time_added` DESC') ? 'desc' : '')?>">Время добавления</a></td>
			<td class="left" width="5%">
				<a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_date_deleted` ASC') ? '`order_date_deleted` DESC' : '`order_date_deleted` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_date_deleted` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_date_deleted` DESC') ? 'desc' : '')?>">Дата удаления</a>
				&nbsp;&nbsp;&nbsp;
				<a href="" onclick="$('#report_orders_deleted_date_deleted_from').val('<?php echo date('Y-m-d');?>'); $('#report_orders_deleted_date_deleted_to').val('<?php echo date('Y-m-d');?>'); return false" style="color: red;">Сегодня</a>
			</td>
			<td class="left" width="4%"><a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_time_deleted` ASC') ? '`TIME` DESC' : '`order_time_deleted` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_time_deleted` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_time_deleted` DESC') ? 'desc' : '')?>">Время удаления</a></td>
			<td class="left" width="10%"><a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_fio` ASC') ? '`order_fio` DESC' : '`order_fio` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_fio` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_fio` DESC') ? 'desc' : '')?>">ФИО</a></td>
			<td class="left" width="10%">Телефон</td>
			<td class="left" width="10%">E-mail</td>
			<td class="left" width="2%"><a href="?p=<?php echo (isset($_GET['p']) ? $_GET['p'] : '')?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`order_total` ASC') ? '`order_total` DESC' : '`order_total` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`order_total` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`order_total` DESC') ? 'desc' : '')?>">ИТОГО</a></td>
			<td class="left" width="5%">Статус</td>
			<td class="left" width="5%">Добавил</td>
			<td class="left" width="5%">Удалил</td>
			<td class="left" width="3%">Действие</td>
		</tr>
		</thead>
		<tbody>
		<form method="post" id="filter">
			<input type="hidden" name="act" value="filter" />
			<tr class="filter">
				<td><input type="text" name="report_orders_deleted_id" value="<?php echo (isset($_SESSION['report_orders_deleted_id']) ? $_SESSION['report_orders_deleted_id'] : '');?>" style="width: 100%;"></td>
				<td>
					<input type="text" name="report_orders_deleted_date_added_from" id="report_orders_deleted_date_added_from" value="<?php echo isset($_SESSION['report_orders_deleted_date_added_from']) ? $_SESSION['report_orders_deleted_date_added_from'] : '';?>" style="width: 100%;">
					<input type="text" name="report_orders_deleted_date_added_to" id="report_orders_deleted_date_added_to" value="<?php echo isset($_SESSION['report_orders_deleted_date_added_to']) ? $_SESSION['report_orders_deleted_date_added_to'] : '';?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_deleted_time_added_from" id="report_orders_deleted_time_added_from" value="<?php echo isset($_SESSION['report_orders_deleted_time_added_from']) ? $_SESSION['report_orders_deleted_time_added_from'] : '';?>" style="width: 100%;">
					<input type="text" name="report_orders_deleted_time_added_to" id="report_orders_deleted_time_added_to" value="<?php echo isset($_SESSION['report_orders_deleted_time_added_to']) ? $_SESSION['report_orders_deleted_time_added_to'] : '';?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_deleted_date_deleted_from" id="report_orders_deleted_date_deleted_from" value="<?php echo isset($_SESSION['report_orders_deleted_date_deleted_from']) ? $_SESSION['report_orders_deleted_date_deleted_from'] : '';?>" style="width: 100%;">
					<input type="text" name="report_orders_deleted_date_deleted_to" id="report_orders_deleted_date_deleted_to" value="<?php echo isset($_SESSION['report_orders_deleted_date_deleted_to']) ? $_SESSION['report_orders_deleted_date_deleted_to'] : '';?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_deleted_time_deleted_from" id="report_orders_deleted_time_deleted_from" value="<?php echo isset($_SESSION['report_orders_deleted_time_deleted_from']) ? $_SESSION['report_orders_deleted_time_deleted_from'] : '';?>" style="width: 100%;">
					<input type="text" name="report_orders_deleted_time_deleted_to" id="report_orders_deleted_time_deleted_to" value="<?php echo isset( $_SESSION['report_orders_deleted_time_deleted_to']) ?  $_SESSION['report_orders_deleted_time_deleted_to'] : '';?>" style="width: 100%;">
				</td>


				<td><input type="text" name="report_orders_deleted_fio" value="<?php echo isset($_SESSION['report_orders_deleted_fio']) ? $_SESSION['report_orders_deleted_fio'] : '';?>" style="width: 100%;"></td>
				<td><input type="text" name="report_orders_deleted_phone" value="<?php echo isset($_SESSION['report_orders_deleted_phone']) ? $_SESSION['report_orders_deleted_phone'] : '' ?>" style="width: 100%;"></td>
				<td><input type="text" name="report_orders_deleted_email" value="<?php echo isset($_SESSION['report_orders_deleted_email']) ? $_SESSION['report_orders_deleted_email'] : '';?>" style="width: 100%;"></td>
				<td></td>
				<td>
					<select name="report_orders_deleted_status" style="width: 100%;">
						<option value=""></option>
						<?php
						$qu = 'SELECT DISTINCT order_status FROM order_delete_log ORDER BY order_status';
						$re = @mysqli_query($ddb, $qu);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['order_status'].'"'.(($ro['order_status'] == $_SESSION['report_orders_deleted_status']) ? ' selected' : '').'>'.$ro['order_status'].'</option>';
						?>
					</select>
				</td>
				<td>
					<select name="report_orders_deleted_user_added_id" style="width: 100%;">
						<option value=""></option>
						<?php
						$qu = '
							SELECT		user.user_id,
										user.username
							FROM		user,
										order_delete_log
							WHERE		user.user_id = order_delete_log.order_user_added_id
							GROUP BY	user.user_id
							ORDER BY	user.username
						';
						$re = @mysqli_query($ddb, $qu);
						echo mysqli_error($ddb);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['user_id'].'"'.(($ro['user_id'] == $_SESSION['report_orders_deleted_user_added_id']) ? ' selected' : '').'>'.$ro['username'].' ('.$ro['user_id'].')</option>';
						?>
					</select>
				</td>
				<td>
					<select name="report_orders_deleted_user_deleted_id" style="width: 100%;">
						<option value=""></option>
						<?php
						$qu = '
							SELECT		user.user_id,
										user.username
							FROM		user,
										order_delete_log
							WHERE		user.user_id = order_delete_log.order_user_deleted_id
							GROUP BY	user.user_id
							ORDER BY	user.username
						';
						$re = @mysqli_query($ddb, $qu);
						echo mysqli_error($ddb);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['user_id'].'"'.(($ro['user_id'] == $_SESSION['report_orders_deleted_user_deleted_id']) ? ' selected' : '').'>'.$ro['username'].' ('.$ro['user_id'].')</option>';
						?>
					</select>
				</td>
				<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
			</tr>
		</form>
		<?php
		$qu_order = '
			SELECT		*
			FROM		order_delete_log
			WHERE		1
						'.(($_SESSION['report_orders_deleted_id']) ? '&& order_id LIKE "%'.$_SESSION['report_orders_deleted_id'].'%"' : '').'
						'.(($_SESSION['report_orders_deleted_date_added_from']) ? '&& order_date_added >= "'.$_SESSION['report_orders_deleted_date_added_from'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_date_added_to']) ? '&& order_date_added <= "'.$_SESSION['report_orders_deleted_date_added_to'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_time_added_from']) ? '&& order_time_added >= "'.$_SESSION['report_orders_deleted_time_added_from'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_time_added_to']) ? '&& order_time_added <= "'.$_SESSION['report_orders_deleted_time_added_to'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_date_deleted_from']) ? '&& order_date_deleted >= "'.$_SESSION['report_orders_deleted_date_deleted_from'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_date_deleted_to']) ? '&& order_date_deleted <= "'.$_SESSION['report_orders_deleted_date_deleted_to'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_time_deleted_from']) ? '&& order_time_deleted >= "'.$_SESSION['report_orders_deleted_time_deleted_from'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_time_deleted_to']) ? '&& order_time_deleted <= "'.$_SESSION['report_orders_deleted_time_deleted_to'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_fio']) ? '&& order_fio LIKE "%'.$_SESSION['report_orders_deleted_fio'].'%"' : '').'
						'.(($_SESSION['report_orders_deleted_phone']) ? '&& order_tel LIKE "%'.$_SESSION['report_orders_deleted_phone'].'%"' : '').'
						'.(($_SESSION['report_orders_deleted_email']) ? '&& order_email LIKE "%'.$_SESSION['report_orders_deleted_email'].'%"' : '').'
						'.(($_SESSION['report_orders_deleted_status']) ? '&& order_status = "'.$_SESSION['report_orders_deleted_status'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_user_added_id']) ? '&& order_user_added_id = "'.$_SESSION['report_orders_deleted_user_added_id'].'"' : '').'
						'.(($_SESSION['report_orders_deleted_user_deleted_id']) ? '&& order_user_deleted_id = "'.$_SESSION['report_orders_deleted_user_deleted_id'].'"' : '').'						
			ORDER BY	'.$_GET['s'];
		$re_order = @mysqli_query($ddb, $qu_order);
		$nu_order_total = @mysqli_num_rows($re_order);
		$summ['order_total'] = 0;
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$summ['order_total'] += $ro_order['order_total'];
		}
		$summ_page['order_total'] = 0;
		$nu_pages = ceil($nu_order_total/$_SESSION['report_orders_deleted_per_page']);
		$qu_order .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_orders_deleted_per_page']).','.$_SESSION['report_orders_deleted_per_page'];
		$re_order = @mysqli_query($ddb, $qu_order);
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$order_user_added = Result($ddb, 'SELECT user_id, username FROM user WHERE user_id='.$ro_order['order_user_added_id']);
			$order_user_deleted = Result($ddb, 'SELECT user_id, username FROM user WHERE user_id='.$ro_order['order_user_deleted_id']);
			$summ_page['order_total'] += $ro_order['order_total'];
			?>
			<tr class="order">
				<td><?php echo $ro_order['order_id']?></td>
				<td><?php echo $ro_order['order_date_added']?></td>
				<td><?php echo $ro_order['order_time_added']?></td>
				<td><?php echo $ro_order['order_date_deleted']?></td>
				<td><?php echo $ro_order['order_time_deleted']?></td>
				<td><?php echo $ro_order['order_fio']?></td>
				<td><?php echo $ro_order['order_tel']?></td>
				<td><?php echo $ro_order['order_email']?></td>
				<td><?php echo $ro_order['order_total']?></td>
				<td><?php echo $ro_order['order_status']?></td>
				<td><?php echo ($order_user_added['user_id']) ? $order_user_added['username'].' ('.$order_user_added['user_id'].')' : 'Гость';?></td>
				<td><?php echo ($order_user_deleted['user_id']) ? $order_user_deleted['username'].' ('.$order_user_deleted['user_id'].')' : 'Н/д'?></td>
				<td><a href="#" onclick="ShowHideProducts(<?php echo $ro_order['id']?>); return false;">Товары</a></td>
			</tr>
			<tr>
				<td class="order_products" id="products_<?php echo $ro_order['id']?>" style="display:none;" colspan="13" width="100%">
					<table class="products" border="1" style="display: inline-block; vertical-align: top;">
						<tr class="head">
							<td width="450">Товар</td>
							<td width="80" align="center">Кол-во</td>
							<td width="80" align="center">Цена (исх.)</td>
							<td width="80" align="center">Сумма</td>
						</tr>
						<?php
							$products = json_decode($ro_order['order_product'], true);
							#var_dump($products);
							foreach ($products as $product) echo '
								<tr>
									<td><a href="/index.php?route=product/product&product_id='.$product['product_id'].'" target="_blank">'.htmlspecialchars_decode($product['product_name']).'</a></td>
									<td align="center">'.round($product['product_qty']).'</td>
									<td align="center">'.round($product['product_price'],2).'</td>
									<td align="center">'.round($product['product_total'],2).'</td>
								</tr>
							';
						?>
						</table>
				</td>
			</tr>
		<?php
		}
		?>
		<tr class="order">
			<td colspan="9"><strong>Итог по странице</strong></td>
			<td><strong><?php echo round($summ_page['order_total'],2)?></strong></td>
			<td colspan="3"></td>
		</tr>
		<tr class="order">
			<td colspan="9"><strong>Итог по странице</strong></td>
			<td><strong><?php echo round($summ['order_total'],2)?></strong></td>
			<td colspan="3"></td>
		</tr>
		</tbody>
	</table>
	<div class="pagination">
		<div class="links">
			<?php
			for ($i=max(1,$_GET['p']-10);$i<=min($nu_pages,$_GET['p']+10);$i++) {
				echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.$_GET['s'].'">'.$i.'</a> ';
			}
			?>
		</div>
		<div class="results">Показано с <?php echo (($_GET['p']-1)*$_SESSION['report_orders_deleted_per_page']+1);?> по <?php echo min($_GET['p']*$_SESSION['report_orders_deleted_per_page'],$nu_order_total);?> из <?php echo $nu_order_total;?> (всего страниц: <?php echo $nu_pages;?>)</div>
	</div>
	<script>
	/*
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_deleted_date_added_from');
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_deleted_date_added_to');
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_deleted_date_deleted_from');
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_deleted_date_deleted_to');
		*/
	</script>
	</body>
	</html>
<?php
} else {
	header('Location: /');
}

