<?
	header('Content-Type: text/html; charset=utf-8');
	session_start();

	require 'connect.php';
	
	if ($_GET['p'] == '') $_GET['p'] = 1;

	if ($ro_user['user_group_id'] == 1) {
?>
<html>
	<head>
		<title>Таблица товаров (SedEdition)</title>
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
	</head>
	<body>
		<table class="list" border="1">
			<thead>
				<tr>
					<td class="left">Категории</td>
					<td class="left">Артикул</td>
					<td class="left">Название</td>
					<td class="left">Производитель</td>
					<td class="left">Модель</td>
					<td class="left">Описание</td>
					<td class="left">Ссылка</td>
				</tr>
			</thead>
			<tbody>
<?
	$qu_product = '	SELECT		product.product_id,
														product.model,
														product.sku,
														product.quantity,
														product.image,
														product.manufacturer_id,
														product.price,
														product.status,
														product_description.name,
														product_description.description
									FROM			product, product_description'.(($_SESSION['product_category'] > 0) ? ', product_to_category' : '').'
									WHERE			product.product_id=product_description.product_id
														'.(($_SESSION['product_category'] > 0) ? ' && product.product_id=product_to_category.product_id && product_to_category.category_id="'.intval($_SESSION['product_category']).'"' : '').'
														'.(($_SESSION['product_name'] != '') ? ' && product_description.name LIKE "%'.$_SESSION['product_name'].'%"' : '').'
														'.(($_SESSION['product_model'] != '') ? ' && product.model LIKE "%'.$_SESSION['product_model'].'%"' : '').'
														'.(($_SESSION['product_sku'] != '') ? ' && product.sku LIKE "%'.$_SESSION['product_sku'].'%"' : '').'
														'.(($_SESSION['product_manufacturer'] > 0) ? ' && product.manufacturer_id="'.$_SESSION['product_manufacturer'].'"' : '').'
														'.(($_SESSION['product_price'] != '') ? ' && product.price LIKE "%'.$_SESSION['product_price'].'%"' : '').'
														'.(($_SESSION['product_quantity'] != '') ? ' && product.quantity LIKE "%'.$_SESSION['product_quantity'].'%"' : '').'
														'.(($_SESSION['product_status'] != '') ? ' && product.status="'.$_SESSION['product_status'].'"' : '').'
									ORDER	BY	'.(($_GET['s'] != '') ? $_GET['s'].', ' : '').'product.product_id
									LIMIT 1000
	';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_man = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM manufacturer WHERE manufacturer_id="'.$ro_product['manufacturer_id'].'"'));
		$cats = '';
		$qu_cats = 'SELECT category_description.category_id, category_description.name FROM category_description, product_to_category WHERE category_description.category_id=product_to_category.category_id && product_to_category.product_id="'.$ro_product['product_id'].'" ORDER BY product_to_category.main_category DESC';
		$re_cats = @mysqli_query($ddb, $qu_cats);
		while ($ro_cats = @mysqli_fetch_array($re_cats)) {
			$ro_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cats['category_id'].'"'));
			$cats .= $ro_cats['name'].' > ';
		}
		$cats = mb_substr($cats,0,-2,'utf-8');
		$ro_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="product_id='.$ro_product['product_id'].'"'));
?>
				<tr>
					<td class="left"><?=$cats;?></td>
					<td class="left"><?=$ro_product['sku']?></td>					
					<td class="left"><?=$ro_product['name']?></td>					
					<td class="left"><?=$ro_man['name']?></td>					
					<td class="left"><?=$ro_product['model']?></td>					
					<td class="left"><?=strip_tags(htmlspecialchars_decode($ro_product['description']))?></td>					
					<td class="left">https://vsepessarii.ru/<?=$ro_url['keyword']?>.html</td>
				</tr>
<?
	}
?>
			</tbody>
		</table>
	</body>
</html>
<?		
	} else {
		header('Location: /');
	}
?>