<?php
// Heading
$_['heading_title']       = 'Атрибуты категорий';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Модуль атрибуты категорий успешно обновлен';
$_['text_no_results']     = 'Атрибуты не найдены';
$_['text_no_settings']    = 'Нету настроек для этого модуля';
$_['text_content_top']    = 'Верх';
$_['text_content_bottom'] = 'Низ';
$_['text_column_left']    = 'Лево';
$_['text_column_right']   = 'Право';

// Entry
$_['entry_layout']     = 'Схема:';
$_['entry_position']   = 'Позиция:';
$_['entry_status']     = 'Статус:';
$_['entry_sort_order'] = 'Сортировка:';

// Error
$_['error_permission'] = 'Предупредение: у вас нету прав изменять этот модуль!';

// Column
$_['column_attribute'] = 'Название атрибута';
$_['column_attribute_group']= 'Группа атрибута';

// Tab
$_['tab_attributes'] = 'Атрибуты';

// Button
$_['button_cancel'] = 'Отмена';
