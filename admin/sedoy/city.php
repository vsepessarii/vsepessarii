<?php
	//города
	/*
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	$ddb = @mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmed_brief');
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	*/
	require 'connect.php';
	if ($ro_user['user_group_id'] == 1) {

		$tmp = @mysqli_fetch_assoc(@mysqli_query($ddb, 'SELECT `value` FROM setting WHERE `key`="multiflat"'));
		$tmp = unserialize($tmp['value']);
		foreach ($tmp as $tmp) {
			if ($tmp['id'] != '') $shippings[$tmp['id']] = $tmp['name'];
		}
		//echo '<pre>'; var_dump($shippings); echo '</pre>';

		if ($_GET['act'] == 'del') {
			$_GET['id'] = intval($_GET['id']);
			$city = @mysqli_fetch_assoc(@mysqli_query($ddb, 'SELECT * FROM city WHERE city_id='.$_GET['id']));
			//var_dump($city);
			@mysqli_query($ddb, 'DELETE FROM city WHERE city_id='.$city['city_id']);
			@mysqli_query($ddb, 'DELETE FROM city_shipping WHERE city="'.$_GET['city'].'"');

			$tpl['msg'] = 'Город удален.';
			$_GET['act'] = '';
			$_GET['id'] = '';
		}
		
		switch ($_POST['act']) {
			case 'add':
				@mysqli_query($ddb, '
					INSERT INTO city 
					SET		country_id="'.mysqli_escape_string($_POST['country_id']).'",
							sort="'.mysqli_escape_string($_POST['sort']).'",
							city="'.floatval(str_replace(',','.',$_POST['city'])).'"
				');
				$_GET['id'] = @mysqli_insert_id($ddb);

				@mysqli_query($ddb, 'DELETE FROM city_shipping WHERE city="'.mysqli_escape_string($_POST['city']).'"');
				reset($shippings);
				foreach ($shippings as $key => $value) if ($_POST[$key] == 'on') @mysqli_query($ddb, 'INSERT INTO city_shipping SET city="'.mysqli_escape_string($_POST['city']).'", shipping="'.$key.'"');

				$_GET['act'] = 'edit';
				$tpl['msg'] = 'Город <b>'.$_POST['city'].'</b> добавлен.';
				break;
				
			case 'edit':
				$_POST['id'] = intval($_POST['id']);
				@mysqli_query($ddb, '
					UPDATE	city
					SET		country_id="'.htmlspecialchars($_POST['country_id']).'",
							sort="'.mysqli_escape_string($_POST['sort']).'",
							city="'.mysqli_escape_string($_POST['city']).'"
					WHERE	city_id="'.$_POST['id'].'"
				');

				@mysqli_query($ddb, 'DELETE FROM city_shipping WHERE city="'.mysqli_escape_string($_POST['city']).'"');
				reset($shippings);
				foreach ($shippings as $key => $value) if ($_POST[$key] == 'on') @mysqli_query($ddb, 'INSERT INTO city_shipping SET city="'.mysqli_escape_string($_POST['city']).'", shipping="'.$key.'"');

				$_GET['act'] = 'edit';
				$_GET['id'] = $_POST['id'];
				$tpl['msg'] = 'Город <b>'.$_POST['city'].'</b> изменен.';
				break;
		}

		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"><a href="'.$_SERVER['PHP_SELF'].'?act=add" class="button">Новый город</a></td>
					<td width="75%" align="right"><!--a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a-->
					</td>
				</tr>
			</table>
			<br>
			'.(($tpl['msg'] != '') ? '<div class="msg">'.$tpl['msg'].'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="45%">
						<table class="list">
							<thead>
								<tr>
									<td width="2%"></td>
									<td width="25%">Город</td>
									<td width="10%">Страна</td>
									<td width="55%">Доставка</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM city ORDER BY sort, city';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$country = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM country WHERE country_id='.$ro['country_id']));
			unset($ship);
			$qu_ship = 'SELECT * FROM city_shipping WHERE city="'.$ro['city'].'"';
			$re_ship = @mysqli_query($ddb, $qu_ship);
			while ($ro_ship = @mysqli_fetch_assoc($re_ship)) $ship[] = $ro_ship['shipping'];

			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="javascript:conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.$_GET['page'].'&id='.$ro['city_id'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['city_id'].'" style="'.(($ro['city_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['city'].'</a></td>
									<td><a href="?act=edit&id='.$ro['city_id'].'" style="'.(($ro['city_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$country['name'].'</a></td>
									<td><a href="?act=edit&id='.$ro['city_id'].'" style="'.(($ro['city_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.implode(', ', $ship).'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="55%">
		';
		if ($_GET['act'] == 'add' || $_GET['act'] == 'edit') {
			if ($_GET['act'] == 'add') {
				$tpl['body_title'] = 'Добавить город';
			} elseif ($_GET['act'] == 'edit') {
				$tpl['body_title'] = 'Обновить город';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM city WHERE city_id="'.intval($_GET['id']).'"'));
			}
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.city.value == "") msg = msg + br + " - не заполнен город;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.$_GET['act'].'">
				<input type="hidden" name="id" value="'.$_GET['id'].'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Город:</td>
						<td width="85%" class="txt" align="left"><input name="city" id="city" title="Город" class="txt" style="width:200px;" value="'.$ro['city'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">Страна:</td>
						<td class="txt" align="left">
							<select name="country_id">
							<option></option>
			';
			$qu_country = 'SELECT * FROM country ORDER BY status DESC, name ASC';
			$re_country = @mysqli_query($ddb, $qu_country);
			while ($ro_country = @mysqli_fetch_assoc($re_country)) $tpl['body_content'] .= '<option value="'.$ro_country['country_id'].'"'.(($ro_country['country_id'] == $ro['country_id']) ? ' selected' : '').'>'.$ro_country['name'].'</option>';
			$tpl['body_content'] .= '							
						</select>
						</td>
					</tr>
					<tr>
						<td class="txt" align="left" colspan="2">
			';
			reset($shippings);
			foreach ($shippings as $key => $value) $tpl['body_content'] .= '<input type="checkbox" id="'.$key.'" name="'.$key.'"'.((@mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM city_shipping WHERE city="'.mysqli_escape_string($ro['city']).'" && shipping="'.$key.'"'))) ? ' checked' : '').' /> <label for="'.$key.'">'.$value.'</label><br>';
			$tpl['body_content'] .= '
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="'.(($_GET['act'] == 'add') ? 'Добавить' : 'Обновить').' город"></td>
					</tr>
				</table>			
				</form>
			';
		} else {
			$tpl['body_title'] = 'Города';
		}
		$tpl['meta_title'] = 'Города';
		require 'template.php';	
	} else {
		header('Location: /');
	}

