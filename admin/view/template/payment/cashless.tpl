<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
    <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div id="languages" class="htabs">
        <?php foreach ($languages as $language) { ?>
        <a href="#language<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a>
        <?php } ?>
      </div>
          <?php foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>">
                  <table class="form">
                      <tr>
                          <td><span class="required">*</span> <?php echo $supplier_info; ?></td>
                          <td><textarea name="supplier_info_<?php echo $language['language_id']; ?>" cols="70" rows="7" placeholder="<?php echo $supplier_placeholder; ?>"><?php echo isset(${'supplier_info_' . $language['language_id']}) ? ${'supplier_info_' . $language['language_id']} : ''; ?></textarea><br />
                              <?php if (isset(${'error_supplier_info_' . $language['language_id']}))
                              { ?>
                                  <span class="error"><?php echo ${'error_supplier_info_' . $language['language_id']}; ?></span>
			      <?php } ?></td>
                      </tr>
                  </table>
          </div>
	  <?php } ?>
        <table class="form">
            <tr>
                <td><?php echo $entry_stamp; ?></td>
                <td>
                    <div class="image"><img src="<?php echo $stamp; ?>" alt="" id="thumb-stamp"/>
                        <input type="hidden" name="stamp_img" value="<?php echo $stamp_img; ?>" id="stamp"/>
                        <br/>
                        <a onclick="image_upload('stamp', 'thumb-stamp');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                onclick="$('#thumb-stamp').attr('src', '<?php echo $no_image; ?>'); $('#stamp').attr('value', '');"><?php echo $text_clear; ?></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Кто выписывает счет:</td>
                <td><input type="text" name="who_write_bill" value="<?php echo $who_write_bill; ?>" /></td>
            </tr>
          <tr>
            <td><?php echo $entry_order_status; ?></td>
            <td><select name="cashless_order_status_id">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $cashless_order_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="cashless_status">
                <?php if ($cashless_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="cashless_sort_order" value="<?php echo $cashless_sort_order; ?>" size="1" /></td>
          </tr>
        </table>
      </form>
        <form action="<?php echo $update; ?>" method="post" id="update_tax">
            <table class="form">
                <tr>
                    <td><?php echo $text_tax_class; ?></td>
                    <td>
                        <select name="tax_class_id" style="padding: 1px 7px">
                            <?php foreach ($entry_tax_class as $row): ?>
                            <option value="<?php echo $row['tax_class_id']; ?>"><?php echo $row['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <span onClick="return window.confirm('<?php echo $atten_update; ?>')"><a onclick="$('#update_tax').submit();" class="button"><?php echo $button_update; ?></a></span>
                    </td>
                </tr>
            </table>
        </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '<?php echo $text_image_manager; ?>',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
                        dataType: 'text',
                        success: function(data) {
                            $('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
    //--></script>
<script type="text/javascript"><!--
$('#languages a').tabs();
//--></script>
<?php echo $footer; ?>