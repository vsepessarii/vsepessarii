<?php
// Text
$_['text_title']          = 'Квитанция Сбербанка РФ (для платежей из России и Казахстана)';
$_['text_instruction']    = '<center style="font-size:130%;">Для печати квитанции нажмите на кнопку &laquo;Печать Квитанции&raquo; (откроется в новом окне).</center>';
$_['text_instruction_mail']    = '<center>Для печати квитанции нажмите на ссылку &laquo;Печать Квитанции&raquo; (откроется в новом окне).';
$_['text_payment']        = '<center style="font-size:130%;">Распечатайте квитанцию, затем нажмите кнопку в конце данной странице "Подтверждение заказа".</center>';
$_['text_payment_mail']  = 'После осуществления оплаты <strong>необходимо уведомить по телефону менеджера магазина</strong> с указанием номера заказа и <strong>выслать скан платежки/оплаченной квитанции</strong> через <a href="/contacts.html" target="_blank">форму обратной связи</a> или по электронной почте <a href="mailto:info@vsepessarii.ru" target="_blank">info@vsepessarii.ru</a> (с темой "Оплата заказа "№%s"). Ваш заказ будет обработан <strong>после того, как деньги поступят на наш расчетный счёт</strong>, и мы получим <strong>подтверждение, что это оплата именно Вашего заказа</strong>.</center>';
$_['text_order_history']  = 'Квитанция хранится в <a href="{href}" target="_blank">Истории заказов</a>';
//$_['text_payment_coment'] = '<center style="font-size:130%;">Заказ не будет обработан, пока деньги не поступят на наш расчётный счёт.</center>';
$_['text_payment_coment'] = '';
$_['text_printpay']       = '<center><a href="{href}" class="button red" style="text-decoration:none;" target="_blank"><span style="font-size:14px !important;">Печать Квитанции</span></a></center>';
$_['text_printpay_mail']       = '<center><a href="{href}" class="button red" style="text-decoration:none;" target="_blank"><span style="font-size:14px !important;">Печать Квитанции</span></a></center>';
$_['text_confirm']        =	'Внимание дождитесь загрузки квитанции!!!\n\n-Затем проверьте еще раз правильность введенных Вами платежных данных.\n- Если обнаружена ошибка, то нажмите "Отмена" и отредактируйте форму.\n- Если все нормально, нажмите "ОК" - бланк будет отправлен на печать.';
?>