<div id="sliderMain">
    <a href="#" class="buttons prev">left</a>
    <div class="viewport">
        <ul class="overview">
            <?php $i = 1;
            foreach ($banners as $banner) { ?>
                <?php if ($banner['link']) { ?>
                    <li>
                        <a href="<?php echo $banner['link']; ?>"
                           onclick="yaCounter23350270.reachGoal('banner_<?php echo $i; ?>'); return true;">
                            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/>
                        </a>
                    </li>
                <?php } else { ?>
                    <li><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/></li>
                <?php } ?>
                <?php $i++;
            } ?>
        </ul>
    </div>
    <a href="#" class="buttons next">right</a>
    <ul class="pager">
        <?php $i = 0;
        foreach ($banners as $banner) { ?>
            <li><a rel="<?php echo $i; ?>" class="pagenum" href="#"></a></li>
            <?php $i++;
        } ?>
    </ul>
</div><br>

<script type="text/javascript"><!--
    $(document).ready(function () {
        $('#sliderMain').tinycarousel({pager: true, interval: true, intervaltime: 5000});
    });
    --></script>