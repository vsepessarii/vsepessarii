<?php
	require 'connect.php';

	if ($ro_user['user_group_id'] == 1) {
		if (isset($_POST['act']) && $_POST['act'] == 'filter') {
			$_SESSION['product_id']           = isset($_POST['product_id']) ? $_POST['product_id'] : null;
			$_SESSION['product_size']         = isset($_POST['product_size']) ? $_POST['product_id'] : null;
			$_SESSION['product_name']         = isset($_POST['product_name']) ? $_POST['product_name'] : null;
			$_SESSION['product_model']        = isset($_POST['product_model']) ? $_POST['product_model'] : null;
			$_SESSION['product_sku']          = isset($_POST['product_sku']) ? $_POST['product_sku'] : null;
			$_SESSION['product_category']     = isset($_POST['product_category']) ? $_POST['product_category'] : null;
			$_SESSION['product_manufacturer'] = isset($_POST['product_manufacturer']) ? $_POST['product_manufacturer'] : null;
			$_SESSION['product_price']        = isset($_POST['product_price']) ? $_POST['product_price'] : null;
			$_SESSION['product_price_in']     = isset($_POST['product_price_in']) ? $_POST['product_price_in'] : null;
			$_SESSION['product_quantity']     = isset($_POST['product_quantity']) ? $_POST['product_quantity'] : null;
			$_SESSION['product_shipping']     = isset($_POST['product_shipping']) ? $_POST['product_shipping'] : null;
			$_SESSION['product_consult_gin']  = isset($_POST['product_consult_gin']) ? $_POST['product_consult_gin'] : null;
			$_SESSION['product_status']       = isset($_POST['product_status']) ? $_POST['product_status'] : null;
			$_GET['p'] = 1;
		} elseif (isset($_POST['act']) && $_POST['act'] == 'edit') {
			$qu_product = 'SELECT product_id FROM product ORDER BY product_id';
			$re_product = @mysqli_query($ddb, $qu_product);
			while ($ro_product = @mysqli_fetch_array($re_product)) {
				$id = $ro_product['product_id'];
				if ($_POST['product'][$id] == '1') {
					$qu_upd_1 = '
						UPDATE	product	
						SET			model="' . htmlspecialchars($_POST['model'][$id]) . '",
										sku="' . htmlspecialchars($_POST['sku'][$id]) . '",
										quantity="' . htmlspecialchars($_POST['quantity'][$id]) . '",
										manufacturer_id="' . htmlspecialchars($_POST['manufacturer'][$id]) . '",
										price="' . htmlspecialchars($_POST['price'][$id]) . '",
										price_in="' . htmlspecialchars($_POST['price_in'][$id]) . '",
										shipping="' . htmlspecialchars($_POST['shipping'][$id]) . '",
										consult_gin="' . htmlspecialchars($_POST['consult_gin'][$id]) . '",
										status="' . htmlspecialchars($_POST['status'][$id]) . '"
						WHERE		product_id="' . $id . '"
					';
					$qu_upd_2 = '
						UPDATE	product_description	
						SET			`name`="' . htmlspecialchars($_POST['name'][$id]) . '"
						WHERE		product_id="' . $id . '"
					';
					@mysqli_query($ddb, $qu_upd_1);
					@mysqli_query($ddb, $qu_upd_2);
					//echo mysqli_error($ddb);
//					echo $qu_upd.'<br><Br>';
				}
			}
		}


?>
<html>
	<head>
		<title>Товары (SedEdition)</title>
		<link type="text/css" href="<?php echo $app_url ?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
	</head>
	<body>
		<table class="list">
			<thead>
				<tr>
					<td class="center"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product_description.product_id asc') ? 'product_description.product_id desc' : 'product_description.product_id asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product_description.product_id asc') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == 'product_description.product_id desc') ? 'desc' : '')?>">ID</a></td>
					<td class="center">Изображения</td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product_description.name asc') ? 'product_description.name desc' : 'product_description.name asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product_description.name asc') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == 'product_description.name desc') ? 'desc' : '')?>">Название товара</a></td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product.model asc') ? 'product.model desc' : 'product.model asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product.model asc') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == 'product.model desc') ? 'desc' : '')?>">Модель</a></td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product.sku asc') ? 'product.sku desc' : 'product.sku asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product.sku asc') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == 'product.sku desc') ? 'desc' : '')?>">Артикул</a></td>
					<td class="left">Категория</td>
					<td class="left">Производитель</td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product.price_in asc') ? 'product.price_in desc' : 'product.price_in asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product.price_in asc') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == 'product.price_in desc') ? 'desc' : '')?>">Цена (вход)</a></td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product.quantity asc') ? 'product.quantity desc' : 'product.quantity asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product.quantity asc') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == 'product.quantity desc') ? 'desc' : '')?>">Количество</a></td>
					<td class="left"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == 'product.price asc') ? 'product.price desc' : 'product.price asc';?>" class="<?php echo (isset($_GET['s']) && ($_GET['s'] == 'product.price asc') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == 'product.price desc') ? 'desc' : '')?>">Цена на сайте</a></td>
					<td class="left">Доставка</td>
					<td class="left">Консультация</td>
					<td class="left">Статус</td>
					<td class="right">Действие</td>
				</tr>
			</thead>
			<tbody>
				<form method="post" id="filter">
				<input type="hidden" name="act" value="filter" />
				<tr class="filter">
					<td align="left"><input type="text" name="product_id" value="<?php echo $_SESSION['product_id'];?>" size="3"></td>
					<td align="left"><input type="text" name="product_size" value="<?php echo $_SESSION['product_size'];?>" size="8"></td>
					<td><input type="text" name="product_name" value="<?php echo $_SESSION['product_name'];?>" class="ui-autocomplete-input" ></td>
					<td><input type="text" name="product_model" value="<?php echo $_SESSION['product_model'];?>" class="ui-autocomplete-input"></td>
					<td><input type="text" name="product_sku" value="<?php echo $_SESSION['product_sku'];?>" class="ui-autocomplete-input"></td>
					<td>
						<select name="product_category" style="width: 170px;" onchange="this.form.submit();">
							<option value="0"></option>
<?php
	$qu_cat_1 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id=category_description.category_id && category.parent_id="0" ORDER BY category.sort_order';
	$re_cat_1 = @mysqli_query($ddb, $qu_cat_1);
	while ($ro_cat_1 = @mysqli_fetch_array($re_cat_1)) {
		echo '<option value="'.$ro_cat_1['category_id'].'"'.(($_SESSION['product_category'] == $ro_cat_1['category_id']) ? ' selected' : '').'>'.$ro_cat_1['name'].'</option>';
		$qu_cat_2 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id=category_description.category_id && category.parent_id="'.$ro_cat_1['category_id'].'" ORDER BY category.sort_order';
		$re_cat_2 = @mysqli_query($ddb, $qu_cat_2);
		while ($ro_cat_2 = @mysqli_fetch_array($re_cat_2)) {
			echo '<option value="'.$ro_cat_2['category_id'].'"'.(($_SESSION['product_category'] == $ro_cat_2['category_id']) ? ' selected' : '').'>'.$ro_cat_1['name'].' / '.$ro_cat_2['name'].'</option>';
			$qu_cat_3 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id=category_description.category_id && category.parent_id="'.$ro_cat_2['category_id'].'" ORDER BY category.sort_order';
			$re_cat_3 = @mysqli_query($ddb, $qu_cat_3);
			while ($ro_cat_3 = @mysqli_fetch_array($re_cat_3)) {
				echo '<option value="'.$ro_cat_3['category_id'].'"'.(($_SESSION['product_category'] == $ro_cat_3['category_id']) ? ' selected' : '').'>'.$ro_cat_1['name'].' / '.$ro_cat_2['name'].' / '.$ro_cat_3['name'].'</option>';
				$qu_cat_4 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id=category_description.category_id && category.parent_id="'.$ro_cat_3['category_id'].'" ORDER BY category.sort_order';
				$re_cat_4 = @mysqli_query($ddb, $qu_cat_4);
				while ($ro_cat_4 = @mysqli_fetch_array($re_cat_4)) {
					echo '<option value="'.$ro_cat_4['category_id'].'"'.(($_SESSION['product_category'] == $ro_cat_4['category_id']) ? ' selected' : '').'>'.$ro_cat_1['name'].' / '.$ro_cat_2['name'].' / '.$ro_cat_3['name'].' / '.$ro_cat_4['name'].'</option>';
				}
			}
		}
	}
?>
						</select>
					</td>
					<td>
						<select name="product_manufacturer" style="width: 90px;" onchange="this.form.submit();">
							<option value="0"></option>
<?php
	$qu_man = 'SELECT manufacturer_id, name FROM manufacturer ORDER BY sort_order';
	$re_man = @mysqli_query($ddb, $qu_man);
	while ($ro_man = @mysqli_fetch_array($re_man)) echo '<option value="'.$ro_man['manufacturer_id'].'"'.(($_SESSION['product_manufacturer'] == $ro_man['manufacturer_id']) ? ' selected' : '').'>'.$ro_man['name'].'</option>';
?>
						</select>
					</td>
					<td align="left"><input type="text" name="product_price_in" value="<?php echo $_SESSION['product_price_in'];?>" size="8"></td>
					<td align="right"><input type="text" name="product_quantity" value="<?php echo $_SESSION['product_quantity'];?>" style="text-align: right;"></td>
					<td align="left"><input type="text" name="product_price" value="<?php echo $_SESSION['product_price'];?>" size="8"></td>
					<td>
						<select name="product_shipping" onchange="this.form.submit();">
							<option value=""></option>
							<option value="1"<?php echo (($_SESSION['product_shipping'] == '1') ? ' selected' : '')?>>Да</option>
							<option value="0"<?php echo (($_SESSION['product_shipping'] == '0') ? ' selected' : '')?>>Нет</option>
						</select>
					</td>
					<td>
						<select name="product_consult_gin" onchange="this.form.submit();">
							<option value=""></option>
							<option value="1"<?php echo (($_SESSION['product_consult_gin'] == '1') ? ' selected' : '')?>>Да</option>
							<option value="0"<?php echo (($_SESSION['product_consult_gin'] == '0') ? ' selected' : '')?>>Нет</option>
						</select>
					</td>
					<td>
						<select name="product_status" onchange="this.form.submit();">
							<option value=""></option>
							<option value="1"<?php echo (($_SESSION['product_status'] == '1') ? ' selected' : '')?>>Включено</option>
							<option value="0"<?php echo (($_SESSION['product_status'] == '0') ? ' selected' : '')?>>Отключено</option>
						</select>
					</td>
					<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
				</tr>
				</form>
				<form action="" method="post" id="edit">
				<input type="hidden" name="act" value="edit" />
<?php
	$qu_product = '
		SELECT		product.product_id,
					product.model,
					product.sku,
					product.quantity,
					product.image,
					product.manufacturer_id,
					product.price,
					product.price_in,
					product.shipping,
					product.consult_gin,
					product.status,
					product_description.name
		FROM		product, product_description'.(($_SESSION['product_category'] > 0) ? ', product_to_category' : '').'
		WHERE		product.product_id=product_description.product_id
					'.(($_SESSION['product_id'] != '') ? ' && product_description.product_id LIKE "%'.$_SESSION['product_id'].'%"' : '').'
					'.(($_SESSION['product_category'] > 0) ? ' && product.product_id=product_to_category.product_id && product_to_category.category_id="'.intval($_SESSION['product_category']).'"' : '').'
					'.(($_SESSION['product_name'] != '') ? ' && product_description.name LIKE "%'.$_SESSION['product_name'].'%"' : '').'
					'.(($_SESSION['product_model'] != '') ? ' && product.model LIKE "%'.$_SESSION['product_model'].'%"' : '').'
					'.(($_SESSION['product_sku'] != '') ? ' && product.sku LIKE "%'.$_SESSION['product_sku'].'%"' : '').'
					'.(($_SESSION['product_manufacturer'] > 0) ? ' && product.manufacturer_id="'.$_SESSION['product_manufacturer'].'"' : '').'
					'.(($_SESSION['product_price'] != '') ? ' && product.price LIKE "%'.$_SESSION['product_price'].'%"' : '').'
					'.(($_SESSION['product_price_in'] != '') ? ' && product.price_in LIKE "%'.$_SESSION['product_price_in'].'%"' : '').'
					'.(($_SESSION['product_quantity'] != '') ? ' && product.quantity LIKE "%'.$_SESSION['product_quantity'].'%"' : '').'
					'.(($_SESSION['product_shipping'] != '') ? ' && product.shipping="'.$_SESSION['product_shipping'].'"' : '').'
					'.(($_SESSION['product_consult_gin'] != '') ? ' && product.consult_gin="'.$_SESSION['product_consult_gin'].'"' : '').'
					'.(($_SESSION['product_status'] != '') ? ' && product.status="'.$_SESSION['product_status'].'"' : '').'
		ORDER BY	'.((isset($_GET['s']) && $_GET['s'] != '') ? $_GET['s'].', ' : '').'product.product_id
	';
	//echo $qu_product;
/*
			$_SESSION['product_name'] = $_POST['product_name'];
			$_SESSION['product_model'] = $_POST['product_model'];
			$_SESSION['product_sku'] = $_POST['product_sku'];
			$_SESSION['product_category'] = $_POST['product_category'];
			$_SESSION['product_manufacturer'] = $_POST['product_manufacturer'];
			$_SESSION['product_price'] = $_POST['product_price'];
			$_SESSION['product_quantity'] = $_POST['product_quantity'];
			$_SESSION['product_status'] = $_POST['product_status'];
*/
	$nu_product = @mysqli_num_rows(@mysqli_query($ddb, $qu_product));
	$nu_pages = ceil($nu_product/25);
	$qu_product .= ' LIMIT '.(($_GET['p']-1)*25).',25';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		unset($special);
		$qu_special = 'SELECT price FROM product_special WHERE product_id="'.$ro_product['product_id'].'"';
		$re_special = @mysqli_query($ddb, $qu_special);
		while ($ro_special = @mysqli_fetch_array($re_special)) $special[] = round($ro_special['price'],2);


		$image_src = '/home/briefmag/domains/brief-med.ru/public_html/image/'.$ro_product['image'];
		$size_txt = '';
		if (file_exists($image_src)) {
			$size = getimagesize($image_src);
			$size_txt = '<br>'.(($size[0] < $_SESSION['product_size']) ? '<font color="red"><strong>'.$size[0].'</strong></font>' : $size[0]).' x '.(($size[1] < $_SESSION['product_size']) ? '<font color="red"><strong>'.$size[1].'</strong></font>' : $size[1]);
		}
		$cats = '';
		$qu_cats = 'SELECT category_description.category_id, category_description.name FROM category_description, product_to_category WHERE category_description.category_id=product_to_category.category_id && product_to_category.product_id="'.$ro_product['product_id'].'" ORDER BY product_to_category.main_category DESC';
//		echo $qu_cats.'<br>';
		$re_cats = @mysqli_query($ddb, $qu_cats);
		while ($ro_cats = @mysqli_fetch_array($re_cats)) {
			$ro_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cats['category_id'].'"'));
			$cats .= '<a href="/'.$ro_url['keyword'].'.html" target="_blank">'.$ro_cats['name'].'</a><br>';
		}
		$ro_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="product_id='.$ro_product['product_id'].'"'));
?>
				<tr>
					<td class="center"><?php echo $ro_product['product_id'];?></td>
					<td class="center"><img src="<?php echo $app_url; ?>/image/<?php echo $ro_product['image'];?>" alt="<?php echo $ro_product['name']?>" style="padding: 1px; border: 1px solid #DDDDDD; max-height: 40px; max-width: 40px;"><input type="hidden" name="product[<?php echo $ro_product['product_id']?>]" value="1" /><?php echo $size_txt;?></td>
					<td class="left"><input name="name[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['name']?>" style="width: 95%;" /></td>
					<td class="left"><input name="model[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['model']?>" style="width: 95%;" /></td>
					<td class="left"><input name="sku[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['sku']?>" style="width: 95%;" /></td>
					<td class="left"><?php echo $cats;?></td>
					<td class="left">
						<select name="manufacturer[<?php echo $ro_product['product_id']?>]">
<?php
		$qu_man = 'SELECT manufacturer_id, name FROM manufacturer ORDER BY sort_order';
		$re_man = @mysqli_query($ddb, $qu_man);
		while ($ro_man = @mysqli_fetch_array($re_man)) echo '<option value="'.$ro_man['manufacturer_id'].'"'.(($ro_man['manufacturer_id'] == $ro_product['manufacturer_id']) ? ' selected' : '').'>'.$ro_man['name'].'</option>';
?>
						</select>
					</td>
					<td class="left"><input name="price_in[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['price_in']?>" size="8" /></td>
					<td class="left"><input name="quantity[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['quantity']?>" /></td>
					<td class="left"><input name="price[<?php echo $ro_product['product_id']?>]" value="<?php echo $ro_product['price']?>" size="8" /><?php echo (isset($special) && count($special) > 0) ? '<br/><center style="font-size:11px; color:red; font-weight:bold;">'.implode(' , ',$special).'</center>' : '';?></td>
					<td class="left">
						<select name="shipping[<?php echo $ro_product['product_id']?>]">
							<option value="1"<?php echo ($ro_product['shipping'] == '1') ? ' selected' : '';?>>Да</option>
							<option value="0"<?php echo ($ro_product['shipping'] == '0') ? ' selected' : '';?>>Нет</option>
						</select>
					</td>
					<td class="left">
						<select name="consult_gin[<?php echo $ro_product['product_id']?>]">
							<option value="1"<?php echo ($ro_product['consult_gin'] == '1') ? ' selected' : '';?>>Да</option>
							<option value="0"<?php echo ($ro_product['consult_gin'] == '0') ? ' selected' : '';?>>Нет</option>
						</select>
					</td>
					<td class="left">
						<select name="status[<?php echo $ro_product['product_id']?>]">
							<option value="1"<?php echo ($ro_product['status'] == '1') ? ' selected' : '';?>>Включено</option>
							<option value="0"<?php echo ($ro_product['status'] == '0') ? ' selected' : '';?>>Отключено</option>
						</select>
					</td>
					<td class="right">
						[<a href="/index.php?route=catalog/product/update&token=<?php echo $_SESSION['token']?>&product_id=<?php echo $ro_product['product_id']?>" target="_blank">Изменить</a>]<br />
						[<a href="/<?php echo $ro_url['keyword']?>.html" target="_blank">Посмотреть</a>]
					</td>
				</tr>
<?php
	}
?>
				<tr>
					<td colspan="14" align="center" style="padding: 10px;">
						<a onclick="edit.submit();" class="button">Сохранить изменения</a>
					</td>
				</tr>
				</form>

			</tbody>
		</table>
		<div class="pagination">
			<div class="links">
<?php
		for ($i=1;$i<=$nu_pages;$i++) {
			echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.(isset($_GET['s']) ? $_GET['s'] : '').'">'.$i.'</a> ';
		}
?>
			</div>
			<div class="results">Показано с <?php echo (($_GET['p']-1)*25+1);?> по <?php echo min($_GET['p']*25,$nu_product);?> из <?php echo $nu_product;?> (всего страниц: <?php echo $nu_pages;?>)</div>
		</div>
	</body>
</html>
<?php
	} else {
		header('Location: /');
	}

