<?php
class ModelCheckoutOrder extends Model {	
	public function addOrder($data) {
		$country = @mysqli_fetch_assoc(@mysqli_query($ddb, 'SELECT country.* FROM city, country WHERE city.city="'.$data['shipping_city'].'" AND city.country_id = country.country_id'));
		$this->db->query("
			INSERT INTO `" . DB_PREFIX . "order` 
			SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', 
			store_id = '" . (int)$data['store_id'] . "', 
			store_name = '" . $this->db->escape($data['store_name']) . "', 
			store_url = '" . $this->db->escape($data['store_url']) . "', 
			customer_id = '" . (int)$data['customer_id'] . "', 
			customer_group_id = '" . (int)$data['customer_group_id'] . "', 
			user_id = '" . (int)$_SESSION['user_id'] . "', 
			firstname = '" . $this->db->escape($data['firstname']) . "', 
			lastname = '" . $this->db->escape($data['lastname']) . "', 
			email = '" . $this->db->escape($data['email']) . "', 
			telephone = '" . $this->db->escape($data['telephone']) . "', 
			fax = '" . $this->db->escape($data['fax']) . "', 
			payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', 
			payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', 
			payment_company = '" . $this->db->escape($data['payment_company']) . "', 
			payment_company_id = '" . $this->db->escape($data['payment_company_id']) . "', 
			payment_tax_id = '" . $this->db->escape($data['payment_tax_id']) . "', 
			payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', 
			payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', 
			payment_city = '" . $this->db->escape($data['payment_city']) . "', 
			payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', 
			-- payment_country = '" . $this->db->escape($data['payment_country']) . "', 
			-- payment_country_id = '" . (int)$data['payment_country_id'] . "', 
			payment_country = '" . $country['name'] . "', 
			payment_country_id = '" . $country['country_id'] . "', 
			payment_zone = '" . $this->db->escape($data['payment_zone']) . "', 
			payment_zone_id = '" . (int)$data['payment_zone_id'] . "', 
			payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', 
			payment_method = '" . $this->db->escape($data['payment_method']) . "', 
			payment_code = '" . $this->db->escape($data['payment_code']) . "', 
			shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', 
			shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', 
			shipping_company = '" . $this->db->escape($data['shipping_company']) . "', 
			shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', 
			shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', 
			shipping_city = '" . $this->db->escape($data['shipping_city']) . "', 
			shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', 
			-- shipping_country = '" . $this->db->escape($data['shipping_country']) . "', 
			-- shipping_country_id = '" . (int)$data['shipping_country_id'] . "',
			shipping_country = '" . $country['name'] . "', 
			shipping_country_id = '" . $country['country_id'] . "', 
			shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', 
			shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', 
			shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', 
			shipping_method = '" . htmlspecialchars_decode($this->db->escape($data['shipping_method'])) . "', 
			shipping_code = '" . $this->db->escape($data['shipping_code']) . "', 
			comment = '" . $this->db->escape($data['comment']) . "', 
			total = '" . (float)$data['total'] . "',
			delta = '".rand(0,20)."', 
			affiliate_id = '" . (int)$data['affiliate_id'] . "', 
			commission = '" . (float)$data['commission'] . "', 
			language_id = '" . (int)$data['language_id'] . "', 
			currency_id = '" . (int)$data['currency_id'] . "', 
			currency_code = '" . $this->db->escape($data['currency_code']) . "', 
			currency_value = '" . (float)$data['currency_value'] . "', 
			ip = '" . $this->db->escape($data['ip']) . "', 
			forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', 
			user_agent = '" . $this->db->escape($data['user_agent']) . "', 
			accept_language = '" . $this->db->escape($data['accept_language']) . "', 
			date_added = NOW(), 
			date_modified = NOW(),
			courier_id='".$data['courier_id']."',
			quick='".@intval($data['quick'])."'
		");

		$order_id = $this->db->getLastId();
    
		//ручной лог начало
		@mysqli_query($ddb, '
			INSERT INTO log SET	SID="'.session_id().'",
													IP="'.$_SERVER['REMOTE_ADDR'].'",
													URL="'.$_SERVER['REQUEST_URI'].'",
													REFERER="'.$_SERVER['HTTP_REFERER'].'",
													COMMENT="Создание заказа '.$order_id.'"
		');
		//ручной лог конец

    

		foreach ($data['products'] as $product) { 
			$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT price_in FROM '.DB_PREFIX.'product WHERE product_id='.(int)$product['product_id']));
			$this->db->query("
				INSERT INTO	" . DB_PREFIX . "order_product
				SET 				order_id = '" . (int)$order_id . "', 
										product_id = '" . (int)$product['product_id'] . "', 
										name = '" . $this->db->escape($product['name']) . "', 
										model = '" . $this->db->escape($product['model']) . "', 
										quantity = '" . (int)$product['quantity'] . "', 
										price_in = '" . (float)$tmp['price_in'] . "', 
										price = '" . (float)$product['price'] . "', 
										total = '" . (float)$product['total'] . "', 
										tax = '" . (float)$product['tax'] . "', 
										reward = '" . (int)$product['reward'] . "'
			");
 
			$order_product_id = $this->db->getLastId();

			foreach ($product['option'] as $option) {
				$tmp = @mysqli_fetch_assoc(@mysqli_query($ddb, '
					SELECT		product.product_id AS act_id,
								product.price_in AS price_in,
								product_option_value.price AS price
					FROM		product,
								product_option_value,
								option_value_description
					WHERE		product_option_value.product_option_value_id='.(int)$option['product_option_value_id'].' &&
								product_option_value.option_value_id = option_value_description.option_value_id &&
								option_value_description.act_id = product.product_id
				'));

				$this->db->query("
					INSERT INTO 	" . DB_PREFIX . "order_option
					SET				order_id = '" . (int)$order_id . "',
									order_product_id = '" . (int)$order_product_id . "',
									product_option_id = '" . (int)$option['product_option_id'] . "',
									product_option_value_id = '" . (int)$option['product_option_value_id'] . "',
									name = '" . $this->db->escape($option['name']) . "',
									`value` = '" . $this->db->escape($option['value']) . "',
									`type` = '" . $this->db->escape($option['type']) . "',
									act_id = ".intval($tmp['act_id']).",
									price_in = ".floatval($tmp['price_in']).",
									price = ".floatval($tmp['price'])."
				");
			}
				
			foreach ($product['download'] as $download) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($download['name']) . "', filename = '" . $this->db->escape($download['filename']) . "', mask = '" . $this->db->escape($download['mask']) . "', remaining = '" . (int)($download['remaining'] * $product['quantity']) . "'");
			}	
		}
		
		foreach ($data['vouchers'] as $voucher) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");
		}
			
		foreach ($data['totals'] as $total) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . htmlspecialchars_decode($this->db->escape($total['title'])) . "', text = '" . htmlspecialchars_decode($this->db->escape($total['text'])) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
		}

		$this->load->model('retailcrm/order');
		$this->model_retailcrm_order->sendToCrm($data, $order_id);

		return $order_id;
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");
			
		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}			
			
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");
			
			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';				
			}
			
			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");
			
			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}
			
			$this->load->model('localisation/language');
			
			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);
			
			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}
		 			
			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],				
				'customer_id'             => $order_query->row['customer_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],				
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],	
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],				
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],	
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_filename'       => $language_filename,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'], 
				'user_agent'              => $order_query->row['user_agent'],	
				'accept_language'         => $order_query->row['accept_language'],				
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added']
			);
		} else {
			return false;	
		}
	}	

	public function confirm($order_id, $order_status_id, $comment = '', $notify = false) {
		
    //file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Начало функции подтверждения заказа'.chr(13).chr(10),FILE_APPEND);
		
		//отправляем SMS администратору
    $config_sms_alert = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT value FROM '.DB_PREFIX.'setting WHERE `key`="config_sms_alert"'));
    if ($config_sms_alert['value'] && $order_status_id == 1) {
      $order = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM `'.DB_PREFIX.'order` WHERE order_id="'.$order_id.'"'));
			$config_sms_to = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT value FROM '.DB_PREFIX.'setting WHERE `key`="config_sms_to"'));
      $config_sms_copy = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT value FROM '.DB_PREFIX.'setting WHERE `key`="config_sms_copy"'));
      $config_sms_message = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT value FROM '.DB_PREFIX.'setting WHERE `key`="config_sms_message"'));
      $sms_body = $config_sms_message['value'];
      $sms_body = str_replace('{ID}',$order_id,$sms_body);
      $sms_body = str_replace('{DATE}',date('d.m.Y'),$sms_body);
      $sms_body = str_replace('{TIME}',date('H:i:s'),$sms_body);
      $sms_body = str_replace('{SUM}',(float)$order['total'],$sms_body);
      $sms_body = str_replace('{PHONE}',$this->db->escape($order['telephone']),$sms_body);
			//$phone = preg_replace('~\D+~','',$_POST['phone']);
      $phones = $config_sms_to['value'].';'.$config_sms_copy['value'];
      $phones = str_replace(chr(13).chr(10),';',$phones);
      for ($i=1;$i<10;$i++) $phones = str_replace(';;',';',$phones);
      $phones = trim($phones,';');
      if (strlen($phones) > 8) {
      	@mysqli_query($ddb, 'INSERT INTO sms SET PHONE="'.$phones.'", TEXT="'.mysqli_escape_string($sms_body).'"');
        /*
				$src = '<?xml version="1.0" encoding="UTF-8"?>    
  								<SMS> 
  									<operations>  
										<operation>SEND</operation> 
  									</operations> 
  									<authentification>    
  										<username>makcyk@gmail.com</username>   
  										<password>a2d4g6j8!!!!</password>   
  									</authentification>   
  									<message> 
  										<sender>SMS</sender>    
  										<text>'.$sms_body.'</text>   
  									</message>    
  									<numbers> 
  										<number messageID="msg11">'.$phones.'</number> 
  									</numbers>    
  								</SMS>';
			//echo htmlspecialchars($src);  

  			$Curl = curl_init();    
  			$CurlOptions = array(   
  				CURLOPT_URL=>'http://atompark.com/members/sms/xml.php',  
  				CURLOPT_FOLLOWLOCATION=>false,   
  				CURLOPT_POST=>true,  
  				CURLOPT_HEADER=>false,   
  				CURLOPT_RETURNTRANSFER=>true,    
  				CURLOPT_CONNECTTIMEOUT=>15,  
  				CURLOPT_TIMEOUT=>100,    
  				CURLOPT_POSTFIELDS=>array('XML'=>$src),   
  			);  
  			curl_setopt_array($Curl, $CurlOptions); 
  			if(false === ($Result = curl_exec($Curl))) {    
  				//throw new Exception('Http request failed'); //убрал, чтоб не зависало если нет связи
  			}   
  			curl_close($Curl);
  			*/
      }      
    }		
		
		$order_info = $this->getOrder($order_id);
		 
		if ($order_info && !$order_info['order_status_id']) {
			// Fraud Detection
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Проверка проходит'.chr(13).chr(10),FILE_APPEND);
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}

			// Blacklist
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBlacklisted($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBlacklisted($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
				
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET user_id=".intval($_SESSION['user_id']).", order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '1', comment = '" . $this->db->escape(($comment && $notify) ? $comment : '') . "', date_added = NOW()");

			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
			
			foreach ($order_product_query->rows as $order_product) {
				$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");
			
				foreach ($order_option_query->rows as $option) {
					$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
				}
			}

			
			//ручной лог начало
			@mysqli_query($ddb, '
				INSERT INTO log SET	SID="'.session_id().'",
														IP="'.$_SERVER['REMOTE_ADDR'].'",
														URL="'.$_SERVER['REQUEST_URI'].'",
														REFERER="'.$_SERVER['HTTP_REFERER'].'",
														COMMENT="Подтверждение заказа '.$order_id.'"
			');
			//ручной лог конец
			
			$this->cache->delete('product');
			
			// Downloads
			$order_download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
			
			// Gift Voucher
				$this->load->model('checkout/voucher');

			$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");
			
			foreach ($order_voucher_query->rows as $order_voucher) {
				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $order_voucher);
				
				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher['order_voucher_id'] . "'");
			}			
			
			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				$this->model_checkout_voucher->confirm($order_id);
			}
			
			// Order Totals			
			$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");
			
			foreach ($order_total_query->rows as $order_total) {
				$this->load->model('total/' . $order_total['code']);
				
				if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
				}
			}
			
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1'.chr(13).chr(10),FILE_APPEND);
			
			// Send out order confirmation mail
			$language = new Language($order_info['language_directory']);
			$language->load($order_info['language_filename']);
			$language->load('mail/order');
		 
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
			
			if ($order_status_query->num_rows) {
				$order_status = $order_status_query->row['name'];	
			} else {
				$order_status = '';
			}
							
			$subject = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);
		
			// HTML Mail
			$template = new Template();
			
			$template->data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
			
			$template->data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
			$template->data['text_link'] = $language->get('text_new_link');
			$template->data['text_download'] = $language->get('text_new_download');
			$template->data['text_order_detail'] = $language->get('text_new_order_detail');
			$template->data['text_instruction'] = $language->get('text_new_instruction');
			$template->data['text_order_id'] = $language->get('text_new_order_id');
			$template->data['text_date_added'] = $language->get('text_new_date_added');
			$template->data['text_payment_method'] = $language->get('text_new_payment_method');	
			$template->data['text_shipping_method'] = $language->get('text_new_shipping_method');
			$template->data['text_email'] = $language->get('text_new_email');
			$template->data['text_telephone'] = $language->get('text_new_telephone');
			$template->data['text_ip'] = $language->get('text_new_ip');
			$template->data['text_payment_address'] = $language->get('text_new_payment_address');
			$template->data['text_shipping_address'] = $language->get('text_new_shipping_address');
			$template->data['text_product'] = $language->get('text_new_product');
			$template->data['text_model'] = $language->get('text_new_model');
			$template->data['text_quantity'] = $language->get('text_new_quantity');
			$template->data['text_price'] = $language->get('text_new_price');
			$template->data['text_total'] = $language->get('text_new_total');
			$template->data['text_footer'] = $language->get('text_new_footer');
			$template->data['text_powered'] = $language->get('text_new_powered');
			
			$template->data['logo'] = HTTP_IMAGE . $this->config->get('config_logo');		
			$template->data['store_name'] = $order_info['store_name'];
			$template->data['store_url'] = $order_info['store_url'];
			$template->data['customer_id'] = $order_info['customer_id'];
			$template->data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
			
			if ($order_download_query->num_rows) {
				$template->data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
			} else {
				$template->data['download'] = '';
			}

			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1.1'.chr(13).chr(10),FILE_APPEND);			
			
			$template->data['order_id'] = $order_id;
			$template->data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));    	
			$template->data['payment_method'] = $order_info['payment_method'];
			$template->data['shipping_method'] = $order_info['shipping_method'];
			$template->data['email'] = $order_info['email'];
			$template->data['telephone'] = $order_info['telephone'];
			$template->data['ip'] = $order_info['ip'];
			
			if ($comment && $notify) {
				$template->data['comment'] = nl2br($comment);
			} else {
				$template->data['comment'] = '';
			}
						
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']  
			);

			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1.2'.chr(13).chr(10),FILE_APPEND);
					
			$template->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));						
									
			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);
		
			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']  
			);
		
			$template->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			// Products
			$template->data['products'] = array();
				
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1.3'.chr(13).chr(10),FILE_APPEND);
				
			foreach ($order_product_query->rows as $product) {
				$option_data = array();
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
				

/*
				$row_manufacturer = $this->db->query("SELECT " . DB_PREFIX . "manufacturer.name FROM " . DB_PREFIX . "manufacturer, " . DB_PREFIX . "product WHERE " . DB_PREFIX . "manufacturer.manufacturer_id = " . DB_PREFIX . "product.manufacturer_id && " . DB_PREFIX . "product.product_id = '" . $product['product_id'] . "'");
				foreach ($row_manufacturer->rows as $man) {
					$ttt = $man['name'];
				}
				$row_keyword = $this->db->query('SELECT `keyword` FROM `'. DB_PREFIX .'url_alias` WHERE `query`="product_id='.$product['product_id'].'"'); 
				foreach ($row_keyword->rows as $keyword) {
					$ttt1 = $keyword['keyword'];
					//print_r($man);
				}
				$url = 'SELECT `keyword` FROM `'. DB_PREFIX .'url_alias` WHERE `query`="product_id='.$product['product_id'].'"';

//				$ttt = $row_manufacturer->rows;
//				$ttt = $ttt['name'];
//				$manufacturer = $row_manufacturer[0];
*/				
				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
					}
					
						$option_data[] = array(
							'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
				}

				$row_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, "SELECT " . DB_PREFIX . "manufacturer.name FROM " . DB_PREFIX . "manufacturer, " . DB_PREFIX . "product WHERE " . DB_PREFIX . "manufacturer.manufacturer_id = " . DB_PREFIX . "product.manufacturer_id && " . DB_PREFIX . "product.product_id = '" . $product['product_id'] . "'"));			  
//				echo $manufacturer['name'].'<br>';
				$template->data['products'][] = array(
					'name'     => $product['name'],
					'manufacturer' => $row_manufacturer['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
				);
			}
	
			// Vouchers
			$template->data['vouchers'] = array();
			
			foreach ($order_voucher_query->rows as $voucher) {
				$template->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}
	
			$template->data['totals'] = $order_total_query->rows;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
				$html = $template->fetch($this->config->get('config_template') . '/template/mail/order.tpl');
			} else {
				$html = $template->fetch('default/template/mail/order.tpl');
			}
			
			// Text Mail
			$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
			$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
			$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
			
			if ($comment && $notify) {
				$text .= $language->get('text_new_instruction') . "\n\n";
				$text .= $comment . "\n\n";
			}
			
			// Products
			//$text .= $language->get('text_new_products') . "\n"; //2014-05-09
			
			foreach ($order_product_query->rows as $product) {
				$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($option['value']) > 20 ? utf8_substr($option['value'], 0, 20) . '..' : $option['value']) . "\n";
				}
			}
			
			foreach ($order_voucher_query->rows as $voucher) {
				$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
			}
			
			$text .= "\n";
			
			//$text .= $language->get('text_new_order_total') . "\n"; //2014-05-09
						
			foreach ($order_total_query->rows as $total) {
				$text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
			}			
			
			$text .= "\n";
			
			if ($order_info['customer_id']) {
				$text .= $language->get('text_new_link') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
			}
		
			if ($order_download_query->num_rows) {
				$text .= $language->get('text_new_download') . "\n";
				$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
			}
			
			if ($order_info['comment']) {
				$text .= $language->get('text_new_comment') . "\n\n";
				$text .= $order_info['comment'] . "\n\n";
			}
			
			$text .= $language->get('text_new_footer') . "\n\n";
		
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1.4'.chr(13).chr(10),FILE_APPEND);
		
			$mail = new Mail(); 
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($html);
			$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 1.5'.chr(13).chr(10),FILE_APPEND);			
			$mail->send();
			
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Ошибка отправки: '.$mail->errorMessage().chr(13).chr(10),FILE_APPEND );
			//file_put_contents('/home/briefmag/domains/vsepessarii.ru/public_html/email/'.date('Y-m-d H-i-s'),$html);

			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Промежуточная 2'.chr(13).chr(10),FILE_APPEND);

			// Admin Alert Mail
			if ($this->config->get('config_alert_mail')) {
				$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);
				
				// Text 
				/*
				$text  = $language->get('text_new_received') . "\n\n";
				$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
				$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
				$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
				$text .= $language->get('text_new_products') . "\n";
				*/
				/*
				foreach ($order_product_query->rows as $product) {
					$ro_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT manufacturer.name FROM manufacturer, product WHERE manufacturer.manufacturer_id=product.manufacturer_id && product.product_id="'.$product['product_id'].'"'));
					$ro_keyword = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT `keyword` FROM url_alias WHERE `query`="product_id='.$product['product_id'].'"'));
					$text .= $product['quantity'] . 'x ' . $product['name'] . ' ' . $ro_manufacturer['name'] . ' ' . $product['model'] . ' (https://vsepessarii.ru/'.$ro_keyword['keyword'].'.html) ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					
					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
					
					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}
											
						$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
					}
				}
				*/
				foreach ($order_voucher_query->rows as $voucher) {
					$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}
				
				$text .= "\n";

				//$text .= $language->get('text_new_order_total') . "\n";
				
				foreach ($order_total_query->rows as $total) {
					$text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
				}			
				
				$text .= "\n";
				
				//SEDOY делаем текст с нуля руками
				$ro_order = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM `order` WHERE order_id="'.$order_id.'"'));
				//$ro_ship = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM order_total WHERE order_id="'.$ro_order['order_id'].'" && code="shipping"')); 
				
				if ($ro_order['quick']) $subject = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8').' - БЫСТРЫЙ заказ '.$order_id; //2014-05-09
				
				$reg = @file_get_contents('http://eduscan.net/i/phone.php?num='.str_replace(array(' ','+','-','(',')',' '),'',$ro_order['telephone'])); //2014-05-09
				
				$br = "\n";
				$text = 'Вы получили новый заказ'.$br.$br;
				$text = 'Номер: '.$ro_order['order_id'].$br.
								'Дата добавления: '.date('d.m.Y H:i:s', strtotime($ro_order['date_added'])).$br.
								//'Дата изменения: '.date('d.mY H:i:s', strtotime($ro_order['date_modified'])).$br.
								'Имя: '.$ro_order['firstname'].$br.
								'Фамилия: '.$ro_order['lastname'].$br.
								'E-mail: '.$ro_order['email'].$br.
								'Телефон: '.$ro_order['telephone'].$br.
								'Регион: '.$reg.$br.  //2014-05-09
								'Проверка региона: http://abcdef.mtsoft.ru/index.shtml?n='.str_replace(array(' ','+','-','(',')',' '),'',$ro_order['telephone']).$br. //2014-05-09
								'Страна: '.$ro_order['shipping_country'].$br.
								'Город: '.$ro_order['shipping_city'].$br.
								'Адрес: '.$ro_order['shipping_address_1'].$br.
								'Способ доставки: '.$ro_order['shipping_method'].$br.
								'Способ оплаты: '.$ro_order['payment_method'].$br.
								//'Стоимость доставки: '.$ro_['shipping_method'].$br.
								'Комментарий заказчика: '.$ro_order['comment'].$br.$br
								
								;
				
				foreach ($order_product_query->rows as $product) {
					$ro_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT manufacturer.name FROM manufacturer, product WHERE manufacturer.manufacturer_id=product.manufacturer_id && product.product_id="'.$product['product_id'].'"'));
					$ro_keyword = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT `keyword` FROM url_alias WHERE `query`="product_id='.$product['product_id'].'"'));
					$text .= $product['quantity'] . 'x ' . $product['name'] . ' ' . $ro_manufacturer['name'] . ' ' . $product['model'] . ' (https://vsepessarii.ru/'.$ro_keyword['keyword'].'.html) ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					
					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
					
					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}
											
						$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
					}
				}
				
				//$text .= $language->get('text_new_order_total') . "\n";
				foreach ($order_total_query->rows as $total) {
					$text .= $total['title'] . ': ' . html_entity_decode($total['text'], ENT_NOQUOTES, 'UTF-8') . "\n";
				}			
				
				
/*				
				if ($order_info['comment']) {
					$text .= $language->get('text_new_comment') . "\n\n";
					$text .= $order_info['comment'] . "\n\n";
				}
*/
			
				$mail = new Mail(); 
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();
				
				//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Ошибка отправки: '.$mail->errorMessage().chr(13).chr(10),FILE_APPEND );
				
				// Send to additional alert emails
				$emails = explode(',', $this->config->get('config_alert_emails'));
				
				foreach ($emails as $email) {
					if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
						$mail->setTo($email);
						$mail->send();
					}
				}				
			} else {
				//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Отправка сообщения администратору отключена',FILE_APPEND );
			}		

			// Send Admins SMS if configure
			if ($this->config->get('config_sms_alert')) {
				$options = array(
					'to'       => $this->config->get('config_sms_to'),
					'copy'     => $this->config->get('config_sms_copy'),
					'from'     => $this->config->get('config_sms_from'),
					'username' => $this->config->get('config_sms_gate_username'),
					'password' => $this->config->get('config_sms_gate_password'),
					'message'  => str_replace(array('{ID}', '{DATE}', '{TIME}', '{SUM}', '{PHONE}'), 
											  array($order_id, date('d.m.Y'), date('H:i'), floatval($order_info['total']), $order_info['telephone']), 
											  $this->config->get('config_sms_message'))
				);

				$this->load->library('sms');

				$sms = new Sms($this->config->get('config_sms_gatename'), $options);
				$sms->send();
			}
		} else {
			//file_put_contents('/home/r/ridator/vsepessarii.ru/public_html/sedoy/log.txt',date('Y-m-d H:i:s').' Не проходит проверка'.chr(13).chr(10),FILE_APPEND );
		}
	}
	
	public function update($order_id, $order_status_id, $comment = '', $notify = false) {
		$order_info = $this->getOrder($order_id);

		if ($order_info && $order_info['order_status_id']) {
			// Fraud Detection
			if ($this->config->get('config_fraud_detection')) {
				$this->load->model('checkout/fraud');
				
				$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);
				
				if ($risk_score > $this->config->get('config_fraud_score')) {
					$order_status_id = $this->config->get('config_fraud_status_id');
				}
			}			

			// Blacklist
			$status = false;
			
			$this->load->model('account/customer');
			
			if ($order_info['customer_id']) {
								
				$results = $this->model_account_customer->getIps($order_info['customer_id']);
				
				foreach ($results as $result) {
					if ($this->model_account_customer->isBlacklisted($result['ip'])) {
						$status = true;
						
						break;
					}
				}
			} else {
				$status = $this->model_account_customer->isBlacklisted($order_info['ip']);
			}
			
			if ($status) {
				$order_status_id = $this->config->get('config_order_status_id');
			}		
						
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET user_id=".intval($_SESSION['user_id']).", order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
	
			// Send out any gift voucher mails
			if ($this->config->get('config_complete_status_id') == $order_status_id) {
				$this->load->model('checkout/voucher');
	
				$this->model_checkout_voucher->confirm($order_id);
			}	
	
			if ($notify) {
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_filename']);
				$language->load('mail/order');
			
				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
	
				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";
				
				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");
				
				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . "\n\n";
					$message .= $order_status_query->row['name'] . "\n\n";					
				}
				
				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}
				
				if ($comment) { 
					$message .= $language->get('text_update_comment') . "\n\n";
					$message .= $comment . "\n\n";
				}
					
				$message .= $language->get('text_update_footer');

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($order_info['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($order_info['store_name']);
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
		}
	}
}
?>