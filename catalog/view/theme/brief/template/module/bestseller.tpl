<?php if ($products) {?>
<section class="box bestseller">
<!--21:03:16--><div class="box-heading"><?php echo $heading_title; ?></div>
<?php $i=0; foreach ($products as $product) { $i++; ?>
<div class="one-third<?php echo ($i % 3 == 0)? " last":"";?>" >
<article class="box-content product-item<?php echo ($product['special'])? " action":"";?>">
<?php if ($product['thumb']) { ?>
<div class="image">
    <a href="<?php echo $product['href']; ?>">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"/>
    </a>
</div>
<?php } ?>
<div class="name">
    <h6><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
    <?php if (isset($product['manufacturer'])) { ?>
    <h6>
        <small>Производитель: <?php echo $product['manufacturer']; ?></small>
    </h6>
    <?php } ?>
</div>
<?php if ($product['price']) { ?>
<div class="price">
    <?php if (!$product['special']) { ?>
    <?php echo $product['price']; ?>
    <?php } else { ?>
    <span class="price-old"><?php echo intval($product['price']); ?></span> <span
        class="price-new"><?php echo $product['special']; ?></span>
    <?php } ?>
</div>
<?php } ?>
<div class="cart">
    <a href="<?php echo $product['href']; ?>" class="button"
       onclick="addToCart('<?php echo $product['product_id']; ?>'); return false;"><?php echo $button_cart; ?></a>
</div>
</article>
</div>
<?php } ?>
</section>
<?php } ?>
