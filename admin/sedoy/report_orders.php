﻿<?php
//отчет по заказам
error_reporting(0);
//	phpinfo();
require 'connect.php';

//методы доставки
$tmp = Result($ddb, 'SELECT value FROM setting WHERE `KEY`="multiflat"');
$tmp = unserialize($tmp['value']);
$shipping_method = array (
	'free.free' => 'Бесплатная',
	'flat.flat' => 'Фиксированная'
);
foreach ($tmp as $key => $value) $shipping_method['multiflat.multiflat'.$key] = $value['name_short'];

$payment_method = array(
	'cashless' => 'Безнал',
	'cod' => 'Наличными',
	'onpay' => 'ONPAY',
	'sberbank_transfer' => 'Сбербанк',
	'yandexplusplus' => 'ЯД',
	'yandexplusplus_card' => 'Visa/MC ЯД',
	'yandex_transfer' => 'Яндекс Деньги'
);

//echo microtime_float().'<br />';
if ($ro_user['user_group_id'] == 1) {
	if ($_GET['s'] == '') $_GET['s'] = '`ID` DESC';
	if ($_POST['act'] == 'clear_filter') {
		$_SESSION['report_orders_id'] = '';
		$_SESSION['report_orders_date_from'] = '';
		$_SESSION['report_orders_date_to'] = '';
		$_SESSION['report_orders_time_from'] = '';
		$_SESSION['report_orders_time_to'] = '';
		$_SESSION['report_orders_fio'] = '';
		$_SESSION['report_orders_phone'] = '';
		$_SESSION['report_orders_email'] = '';
		$_SESSION['report_orders_price_in_from'] = '';
		$_SESSION['report_orders_price_in_to'] = '';
		$_SESSION['report_orders_price_out_from'] = '';
		$_SESSION['report_orders_price_out_to'] = '';
		$_SESSION['report_orders_price_delivery'] = '';
		$_SESSION['report_orders_total_from'] = '';
		$_SESSION['report_orders_total_to'] = '';
		$_SESSION['report_orders_margin_from'] = '';
		$_SESSION['report_orders_margin_to'] = '';
		$_SESSION['report_orders_profit_from'] = '';
		$_SESSION['report_orders_profit_to'] = '';
		$_SESSION['report_orders_profit_percent_from'] = '';
		$_SESSION['report_orders_profit_percent_to'] = '';
		$_SESSION['report_orders_delivery_method'] = '';
		$_SESSION['report_orders_payment_method'] = '';
		$_SESSION['report_orders_status'] = '';
	}

	if ($_SESSION['courier_price'] == '') $_SESSION['courier_price'] = 250;
	if ($_SESSION['report_orders_per_page'] == '') $_SESSION['report_orders_per_page'] = 25;
	if ($_POST['act'] == 'setting') {
		$_SESSION['courier_price'] = $_POST['courier_price'];
		$_SESSION['report_orders_per_page'] = $_POST['report_orders_per_page'];
	}
	if ($_POST['act'] == 'filter') {
		$_SESSION['report_orders_id'] = $_POST['report_orders_id'];
		$_SESSION['report_orders_date_from'] = $_POST['report_orders_date_from'];
		$_SESSION['report_orders_date_to'] = $_POST['report_orders_date_to'];
		$_SESSION['report_orders_time_from'] = $_POST['report_orders_time_from'];
		$_SESSION['report_orders_time_to'] = $_POST['report_orders_time_to'];
		$_SESSION['report_orders_fio'] = $_POST['report_orders_fio'];
		$_SESSION['report_orders_phone'] = $_POST['report_orders_phone'];
		$_SESSION['report_orders_email'] = $_POST['report_orders_email'];
		$_SESSION['report_orders_price_in_from'] = $_POST['report_orders_price_in_from'];
		$_SESSION['report_orders_price_in_to'] = $_POST['report_orders_price_in_to'];
		$_SESSION['report_orders_price_out_from'] = $_POST['report_orders_price_out_from'];
		$_SESSION['report_orders_price_out_to'] = $_POST['report_orders_price_out_to'];
		$_SESSION['report_orders_price_delivery'] = $_POST['report_orders_price_delivery'];
		$_SESSION['report_orders_total_from'] = $_POST['report_orders_total_from'];
		$_SESSION['report_orders_total_to'] = $_POST['report_orders_total_to'];
		$_SESSION['report_orders_margin_from'] = $_POST['report_orders_margin_from'];
		$_SESSION['report_orders_margin_to'] = $_POST['report_orders_margin_to'];
		$_SESSION['report_orders_profit_from'] = $_POST['report_orders_profit_from'];
		$_SESSION['report_orders_profit_to'] = $_POST['report_orders_profit_to'];
		$_SESSION['report_orders_profit_percent_from'] = $_POST['report_orders_profit_percent_from'];
		$_SESSION['report_orders_profit_percent_to'] = $_POST['report_orders_profit_percent_to'];
		$_SESSION['report_orders_delivery_method'] = $_POST['report_orders_delivery_method'];
		$_SESSION['report_orders_payment_method'] = $_POST['report_orders_payment_method'];
		$_SESSION['report_orders_status'] = $_POST['report_orders_status'];
		$_GET['p'] = 1;
	}

	if ($_POST['act'] = 'table_save') {
		foreach ($_POST['table_status'] as $table_order_id => $table_status_id) {
			if ($table_status_id != $_POST['table_status_old'][$table_order_id]) {
				AlertOrderStatusChange($table_order_id, $table_status_id);
				@mysqli_query($ddb, '
					INSERT INTO	order_history
					SET			user_id='.intval($_SESSION['user_id']).',
								order_id="'.intval($table_order_id).'",
								order_status_id="'.intval($table_status_id).'",
								notify="'.(($_POST['notify'] == 'on') ? '1' : '0').'",
								date_added=NOW()
				');
				@mysqli_query($ddb, 'UPDATE `order` SET order_status_id="'.intval($table_status_id).'" WHERE order_id="'.intval($table_order_id).'"');
			}
		}
	}

	@mysqli_query($ddb, 'TRUNCATE TABLE tmp_report_order');
	$qu_order = 'SELECT * FROM `order` WHERE order_status_id>0 '.(($_SESSION['report_orders_date_from']) ? '  && date_added>="'.$_SESSION['report_orders_date_from'].' 00:00:00"' : '').(($_SESSION['report_orders_date_to']) ? '  && date_added<="'.$_SESSION['report_orders_date_to'].' 23:59:59"' : '').' ORDER BY order_id';
	//echo $qu_order;
	$re_order = @mysqli_query($ddb, $qu_order);
	while ($ro_order = @mysqli_fetch_array($re_order)) {
		$id = $ro_order['order_id'];
		$date = date('Y-m-d',strtotime($ro_order['date_added']));
		$time = date('H:i:s', strtotime($ro_order['date_added']));
		$fio = trim($ro_order['firstname'].' '.$ro_order['lastname']);
		$phone = $ro_order['telephone'];
		$email = $ro_order['email'];

		//сумма входящих и исходящих цен
		$price_in = 0;
		$price_out = 0;
		$qu_product = '
				SELECT	order_product.order_product_id as order_product_id,
						order_product.product_id as product_id,
						order_product.quantity as quantity,
						order_product.price_in as price_in,
						order_product.price as price_out
				FROM	order_product
				WHERE	order_product.order_id = "'.$ro_order['order_id'].'"
			';
		$re_product = @mysqli_query($ddb, $qu_product);
		echo mysqli_error($ddb);
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$option = Result($ddb, '
				SELECT	price_in
				FROM	order_option,
						product_option
				WHERE	product_option.option_id = 24 &&
						product_option.product_option_id = order_option.product_option_id &&
						order_option.order_product_id = '.$ro_product['order_product_id'].'
			');

			//var_dump($option);

			//$ro_price_in = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT price_in FROM product WHERE product_id="'.$ro_product['product_id'].'"'));
			//echo 'SELECT price_in FROM product WHERE product_id="'.$ro_product['product_id'].'"<br>';
			$price_in += $ro_product['quantity']*($ro_product['price_in']+$option['price_in']);
			$price_out += $ro_product['quantity']*$ro_product['price_out'];
		}
		$ro_delivery = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM order_total WHERE order_id="'.$ro_order['order_id'].'" && code="shipping"'));
		$price_delivery = $ro_delivery['value'];
		$total = $ro_order['total'];
		$margin = $total - $price_in;
		$tax = ($total-$price_delivery)*0.06;

		$delivery_method = $shipping_method[$ro_order['shipping_code']];

		/*
		switch ($ro_order['shipping_code']) {
			case 'free.free':
				$delivery_method = 'Бесплатная';
				break;

			case 'multiflat.multiflat0<p align="left"></p>':
				$delivery_method = 'В МКАД';
				break;

			case 'multiflat.multiflat5':
				$delivery_method = 'За МКАД от';
				break;

			case 'multiflat.multiflat1':
				$delivery_method = 'За МКАД до';
				break;

			case 'multiflat.multiflat2':
				$delivery_method = 'Срочная';
				break;

			case 'multiflat.multiflat4':
				$delivery_method = 'РФ';
				break;

			case 'multiflat.multiflat3':
			case 'pickup.pickup':
				$delivery_method = 'Самовывоз';
				break;

			case 'flat.flat':
				$delivery_method = 'Фиксированная';
				break;

			default:
				$delivery_method = '';
				break;
		}
		*/

		//считаем стоимость услуг курьера
		$ro_courier = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM courier WHERE courier_id="'.$ro_order['courier_id'].'"'));
		if ($delivery_method == 'Самовывоз') {
			$courier_price = 0;
		} elseif ($ro_courier['courier_id'] > 0) {
			$courier_price = $ro_order['courier_price'];
		} else {
			$courier_price = $_SESSION['courier_price'];
		}
		$profit = ($total - $price_delivery)*0.94-$price_in - ($courier_price - $price_delivery);
		$profit_percent = @round($profit*100/$total,2);

		switch ($ro_order['payment_code']) {
			case 'cod':
				$payment_method = 'Наличные';
				break;

			case 'onpay':
				$payment_method = 'ONPAY';
				break;

			default:
				$payment_method = '';
				break;
		}

		$ro_status = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM order_status WHERE order_status_id="'.$ro_order['order_status_id'].'"'));
		$status = $ro_status['name'];
		@mysqli_query($ddb, '
				INSERT INTO	`tmp_report_order`
				SET					`ID`="'.$id.'",
										`DATE`="'.$date.'",
										`TIME`="'.$time.'",
										`FIO`="'.$fio.'",
										`PHONE`="'.$phone.'",
										`EMAIL`="'.$email.'",
										`PRICE_IN`="'.$price_in.'",
										`PRICE_OUT`="'.$price_out.'",
										`PRICE_DELIVERY`="'.$price_delivery.'",
										`TOTAL`="'.$total.'",
										`MARGIN`="'.$margin.'",
										`TAX`="'.$tax.'",
										`COURIER`="'.htmlspecialchars($ro_courier['name']).'",
										`COURIER_PRICE`="'.$courier_price.'",
										`PROFIT`="'.$profit.'",
										`PROFIT_PERCENT`="'.$profit_percent.'",
										`DELIVERY_METHOD`="'.$delivery_method.'",
										`PAYMENT_METHOD`="'.$payment_method.'",
										`STATUS`="'.$status.'",
										`STATUS_ID`="'.$ro_order['order_status_id'].'"
			');
		//echo mysqli_error($ddb);
	}
	//echo microtime_float().'<br />';

	?>
	<html>
	<head>
		<title>Отчет по заказам (SedEdition)</title>
		<link type="text/css" href="<?php echo $app_url ?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<script language="javascript">
			$(function() {
				$( "#report_orders_date_from" ).datepicker();
				$( "#report_orders_date_to" ).datepicker();
			});

			function ShowHideProducts(order_id) {
				var products = $('#products_'+order_id);
				if (products.css('display') == 'none') {
					$.get('ajax/order_products.php', { order_id: order_id }).done(function(data) {
						products.html(data);
					});
					products.css('display', 'table-cell');
				} else {
					products.html('');
					products.css('display', 'none');
				}
			}

			function ShowAllProducts() {
				$('td.order_products').each(function(i,elem) {
					var td_id = elem.id;
					var order_id = td_id.substr(9);
					var products = $('#products_'+order_id);
					$.get('ajax/order_products.php', { order_id: order_id }).done(function(data) {
						products.html(data);
					});
					products.css('display', 'table-cell');
				});
			}

			function HideAllProducts() {
				$('td.order_products').each(function(i,elem) {
					var td_id = elem.id;
					var order_id = td_id.substr(9);
					var products = $('#products_'+order_id);
					products.html('');
					products.css('display', 'none');
				});
			}
		</script>
	</head>
	<body>
	<style>
		td { word-wrap: break-word;}
	</style>
	<div align="right">
		<br />
		<a href="#" onclick="ShowAllProducts(); return false;">Показать все</a>
		&nbsp;&nbsp;
		<a href="#" onclick="HideAllProducts(); return false;">Скрыть все</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="/index.php?route=common/home&token=<?php echo $_SESSION['token']?>">Вернуться в панель управления</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="report_orders_csv.php">Скачать в CSV</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="clear_filter" />
			<input type="submit" value="Сбросить фильтры" />
		</form>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="setting" />
			Курьеру: <input name="courier_price" value="<?php echo $_SESSION['courier_price'];?>" />
			&nbsp;&nbsp;&nbsp;
			Выводить по:
			<select name="report_orders_per_page">
				<option value="25"<?php echo ($_SESSION['report_orders_per_page'] == '25') ? ' selected' : ''?>>25</option>
				<option value="50"<?php echo ($_SESSION['report_orders_per_page'] == '50') ? ' selected' : ''?>>50</option>
				<option value="100"<?php echo ($_SESSION['report_orders_per_page'] == '100') ? ' selected' : ''?>>100</option>
				<option value="200"<?php echo ($_SESSION['report_orders_per_page'] == '200') ? ' selected' : ''?>>200</option>
			</select>
			<input type="submit" value="Сохранить" />
		</form>
		<br /><br />
	</div>
	<table class="list">
		<thead>
		<tr>
			<td class="left" width="2%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`ID` ASC') ? '`ID` DESC' : '`ID` ASC';?>" class="<?php echo (($_GET['s'] == '`ID` ASC') ? 'asc' : '').(($_GET['s'] == '`ID` DESC') ? 'desc' : '')?>">№</a></td>
			<td class="left" width="7%">
				<a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`DATE` ASC') ? '`DATE` DESC' : '`DATE` ASC';?>" class="<?php echo (($_GET['s'] == '`DATE` ASC') ? 'asc' : '').(($_GET['s'] == '`DATE` DESC') ? 'desc' : '')?>">Дата</a>
				&nbsp;&nbsp;&nbsp;
				<a href="" onclick="$('#report_orders_date_from').val('<?php echo date('Y-m-d');?>'); $('#report_orders_date_to').val('<?php echo date('Y-m-d');?>'); return false" style="color: red;">Сегодня</a>
			</td>
			<td class="left" width="4%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`TIME` ASC') ? '`TIME` DESC' : '`TIME` ASC';?>" class="<?php echo (($_GET['s'] == '`TIME` ASC') ? 'asc' : '').(($_GET['s'] == '`TIME` DESC') ? 'desc' : '')?>">Время</a></td>
			<td class="left" width="10%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`FIO` ASC') ? '`FIO` DESC' : '`FIO` ASC';?>" class="<?php echo (($_GET['s'] == '`FIO` ASC') ? 'asc' : '').(($_GET['s'] == '`FIO` DESC') ? 'desc' : '')?>">ФИО</a></td>
			<td class="left" width="10%">Телефон</td>
			<td class="left" width="10%">E-mail</td>
			<td class="left" width="2%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`PRICE_IN` ASC') ? '`PRICE_IN` DESC' : '`PRICE_IN` ASC';?>" class="<?php echo (($_GET['s'] == '`PRICE_IN` ASC') ? 'asc' : '').(($_GET['s'] == '`PRICE_IN` DESC') ? 'desc' : '')?>">Цена (вх.)</a></td>
			<td class="left" width="2%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`PRICE_OUT` ASC') ? '`PRICE_OUT` DESC' : '`PRICE_OUT` ASC';?>" class="<?php echo (($_GET['s'] == '`PRICE_OUT` ASC') ? 'asc' : '').(($_GET['s'] == '`PRICE_OUT` DESC') ? 'desc' : '')?>">Цена (исх.)</a></td>
			<td class="left" width="2%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`PRICE_DELIVERY` ASC') ? '`PRICE_DELIVERY` DESC' : '`PRICE_DELIVERY` ASC';?>" class="<?php echo (($_GET['s'] == '`PRICE_DELIVERY` ASC') ? 'asc' : '').(($_GET['s'] == '`PRICE_DELIVERY` DESC') ? 'desc' : '')?>">Цена (дост.)</a></td>
			<td class="left" width="2%"><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`TOTAL` ASC') ? '`TOTAL` DESC' : '`TOTAL` ASC';?>" class="<?php echo (($_GET['s'] == '`TOTAL` ASC') ? 'asc' : '').(($_GET['s'] == '`TOTAL` DESC') ? 'desc' : '')?>">ИТОГО</a></td>
			<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`MARGIN` ASC') ? '`MARGIN` DESC' : '`MARGIN` ASC';?>" class="<?php echo (($_GET['s'] == '`MARGIN` ASC') ? 'asc' : '').(($_GET['s'] == '`MARGIN` DESC') ? 'desc' : '')?>">Брутто маржа</a></td>
			<td class="left" width="">Налог 6%</td>
			<td class="left" width="">Курьеру</td>
			<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`PROFIT` ASC') ? '`PROFIT` DESC' : '`PROFIT` ASC';?>" class="<?php echo (($_GET['s'] == '`PROFIT` ASC') ? 'asc' : '').(($_GET['s'] == '`PROFIT` DESC') ? 'desc' : '')?>">МАРЖА</a></td>
			<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo ($_GET['s'] == '`PROFIT_PERCENT` ASC') ? '`PROFIT_PERCENT` DESC' : '`PROFIT_PERCENT` ASC';?>" class="<?php echo (($_GET['s'] == '`PROFIT_PERCENT` ASC') ? 'asc' : '').(($_GET['s'] == '`PROFIT_PERCENT` DESC') ? 'desc' : '')?>">%</a></td>
			<td class="left" width="">Тип доставки</td>
			<td class="left" width="">Тип оплаты</td>
			<td class="left" width="">Статус</td>
			<td class="left" width="">Действие</td>
		</tr>
		</thead>
		<tbody>
		<form method="post" id="filter">
			<input type="hidden" name="act" value="filter" />
			<tr class="filter">
				<td><input type="text" name="report_orders_id" value="<?php echo $_SESSION['report_orders_id'];?>" style="width: 100%;"></td>
				<td>
					<input type="text" name="report_orders_date_from" id="report_orders_date_from" value="<?php echo $_SESSION['report_orders_date_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_date_to" id="report_orders_date_to" value="<?php echo $_SESSION['report_orders_date_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_time_from" id="report_orders_time_from" value="<?php echo $_SESSION['report_orders_time_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_time_to" id="report_orders_time_to" value="<?php echo $_SESSION['report_orders_time_to'];?>" style="width: 100%;">
				</td>
				<td><input type="text" name="report_orders_fio" value="<?php echo $_SESSION['report_orders_fio'];?>" style="width: 100%;"></td>
				<td><input type="text" name="report_orders_phone" value="<?php echo $_SESSION['report_orders_phone'];?>" style="width: 100%;"></td>
				<td><input type="text" name="report_orders_email" value="<?php echo $_SESSION['report_orders_email'];?>" style="width: 100%;"></td>
				<td>
					<input type="text" name="report_orders_price_in_from" value="<?php echo $_SESSION['report_orders_price_in_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_price_in_to" value="<?php echo $_SESSION['report_orders_price_in_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_price_out_from" value="<?php echo $_SESSION['report_orders_price_out_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_price_out_to" value="<?php echo $_SESSION['report_orders_price_out_to'];?>" style="width: 100%;">
				</td>
				<td>
					<select name="report_orders_price_delivery">
						<option value=""></option>
						<?php
						$qu = 'SELECT PRICE_DELIVERY FROM tmp_report_order GROUP BY PRICE_DELIVERY ORDER BY PRICE_DELIVERY';
						$re = @mysqli_query($ddb, $qu);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['PRICE_DELIVERY'].'"'.(($ro['PRICE_DELIVERY'] == $_SESSION['report_orders_price_delivery']) ? ' selected' : '').'>'.$ro['PRICE_DELIVERY'].'</option>';
						?>
					</select>
				</td>
				<td>
					<input type="text" name="report_orders_total_from" value="<?php echo $_SESSION['report_orders_total_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_total_to" value="<?php echo $_SESSION['report_orders_total_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_margin_from" value="<?php echo $_SESSION['report_orders_margin_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_margin_to" value="<?php echo $_SESSION['report_orders_margin_to'];?>" style="width: 100%;">
				</td>
				<td></td>
				<td></td>
				<td>
					<input type="text" name="report_orders_profit_from" value="<?php echo $_SESSION['report_orders_profit_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_profit_to" value="<?php echo $_SESSION['report_orders_profit_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_orders_profit_percent_from" value="<?php echo $_SESSION['report_orders_profit_percent_from'];?>" style="width: 100%;">
					<input type="text" name="report_orders_profit_percent_to" value="<?php echo $_SESSION['report_orders_profit_percent_to'];?>" style="width: 100%;">
				</td>
				<td>
					<select name="report_orders_delivery_method" style="width: 100%;">
						<option value=""></option>
						<?
						$qu = 'SELECT DELIVERY_METHOD FROM tmp_report_order GROUP BY DELIVERY_METHOD ORDER BY DELIVERY_METHOD';
						$re = @mysqli_query($ddb, $qu);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['DELIVERY_METHOD'].'"'.(($ro['DELIVERY_METHOD'] == $_SESSION['report_orders_delivery_method']) ? ' selected' : '').'>'.$ro['DELIVERY_METHOD'].'</option>';
						?>
					</select>
				</td>
				<td>
					<select name="report_orders_payment_method" style="width: 100%;">
						<option value=""></option>
						<?
						$qu = 'SELECT PAYMENT_METHOD FROM tmp_report_order GROUP BY PAYMENT_METHOD ORDER BY PAYMENT_METHOD';
						$re = @mysqli_query($ddb, $qu);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['PAYMENT_METHOD'].'"'.(($ro['PAYMENT_METHOD'] == $_SESSION['report_orders_payment_method']) ? ' selected' : '').'>'.$ro['PAYMENT_METHOD'].'</option>';
						?>
					</select>
				</td>
				<td>
					<select name="report_orders_status" style="width: 100%;">
						<option value=""></option>
						<?
						$qu = 'SELECT STATUS FROM tmp_report_order GROUP BY STATUS ORDER BY STATUS';
						$re = @mysqli_query($ddb, $qu);
						while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['STATUS'].'"'.(($ro['STATUS'] == $_SESSION['report_orders_status']) ? ' selected' : '').'>'.$ro['STATUS'].'</option>';
						?>
					</select>
				</td>
				<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
			</tr>
		</form>
		<form method="post">
		<input type="hidden" name="act" value="table_save">
		<?php
		$qu_order = '
						SELECT	*
						FROM 		tmp_report_order
						WHERE		1
										'.(($_SESSION['report_orders_id'] != '') ? ' && `ID` LIKE "%'.$_SESSION['report_orders_id'].'%"' : '').'
										'.(($_SESSION['report_orders_date_from'] != '') ? ' && `DATE`>="'.$_SESSION['report_orders_date_from'].'"' : '').'
										'.(($_SESSION['report_orders_date_to'] != '') ? ' && `DATE`<="'.$_SESSION['report_orders_date_to'].'"' : '').'
										'.(($_SESSION['report_orders_time_from'] != '') ? ' && `TIME`>="'.$_SESSION['report_orders_time_from'].'"' : '').'
										'.(($_SESSION['report_orders_time_to'] != '') ? ' && `TIME`<="'.$_SESSION['report_orders_time_to'].'"' : '').'
										'.(($_SESSION['report_orders_fio'] != '') ? ' && `FIO` LIKE "%'.$_SESSION['report_orders_fio'].'%"' : '').'
										'.(($_SESSION['report_orders_phone'] != '') ? ' && `PHONE` LIKE "%'.$_SESSION['report_orders_phone'].'%"' : '').'
										'.(($_SESSION['report_orders_email'] != '') ? ' && `EMAIL` LIKE "%'.$_SESSION['report_orders_email'].'%"' : '').'
										'.(($_SESSION['report_orders_price_in_from'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_orders_price_in_from'].'"' : '').'
										'.(($_SESSION['report_orders_price_in_to'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_orders_price_in_to'].'"' : '').'
										'.(($_SESSION['report_orders_price_out_from'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_orders_price_out_from'].'"' : '').'
										'.(($_SESSION['report_orders_price_out_to'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_orders_price_out_to'].'"' : '').'
										'.(($_SESSION['report_orders_price_delivery'] != '') ? ' && `PRICE_DELIVERY`="'.$_SESSION['report_orders_price_delivery'].'"' : '').'
										'.(($_SESSION['report_orders_total_from'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_orders_total_from'].'"' : '').'
										'.(($_SESSION['report_orders_total_to'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_orders_total_to'].'"' : '').'
										'.(($_SESSION['report_orders_margin_from'] != '') ? ' && `MARGIN`>="'.$_SESSION['report_orders_margin_from'].'"' : '').'
										'.(($_SESSION['report_orders_margin_to'] != '') ? ' && `MARGIN`<="'.$_SESSION['report_orders_margin_to'].'"' : '').'
										'.(($_SESSION['report_orders_profit_from'] != '') ? ' && `PROFIT`>="'.$_SESSION['report_orders_profit_from'].'"' : '').'
										'.(($_SESSION['report_orders_profit_to'] != '') ? ' && `PROFIT`<="'.$_SESSION['report_orders_profit_to'].'"' : '').'
										'.(($_SESSION['report_orders_profit_percent_from'] != '') ? ' && `PROFIT_PERCENT`>="'.$_SESSION['report_orders_profit_percent_from'].'"' : '').'
										'.(($_SESSION['report_orders_profit_percent_to'] != '') ? ' && `PROFIT_PERCENT`<="'.$_SESSION['report_orders_profit_percent_to'].'"' : '').'
										'.(($_SESSION['report_orders_delivery_method'] != '') ? ' && `DELIVERY_METHOD`="'.$_SESSION['report_orders_delivery_method'].'"' : '').'
										'.(($_SESSION['report_orders_payment_method'] != '') ? ' && `PAYMENT_METHOD`="'.$_SESSION['report_orders_payment_method'].'"' : '').'
										'.(($_SESSION['report_orders_status'] != '') ? ' && `STATUS`="'.$_SESSION['report_orders_status'].'"' : '').'
						ORDER	BY	'.$_GET['s'];
		$re_order = @mysqli_query($ddb, $qu_order);
		$nu_order_total = @mysqli_num_rows($re_order);
		$summ['price_in'] = 0;
		$summ['price_out'] = 0;
		$summ['price_delivery'] = 0;
		$summ['total'] = 0;
		$summ['margin'] = 0;
		$summ['tax'] = 0;
		$summ['courier_price'] = 0;
		$summ['profit'] = 0;
		$summ['profit_percent'] = 0;
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$summ['price_in'] += $ro_order['PRICE_IN'];
			$summ['price_out'] += $ro_order['PRICE_OUT'];
			$summ['price_delivery'] += $ro_order['PRICE_DELIVERY'];
			$summ['total'] += $ro_order['TOTAL'];
			$summ['margin'] += $ro_order['MARGIN'];
			$summ['tax'] += $ro_order['TAX'];
			$summ['courier_price'] += $ro_order['COURIER_PRICE'];
			$summ['profit'] += $ro_order['PROFIT'];
		}
		$summ_page['price_in'] = 0;
		$summ_page['price_out'] = 0;
		$summ_page['price_delivery'] = 0;
		$summ_page['total'] = 0;
		$summ_page['margin'] = 0;
		$summ_page['tax'] = 0;
		$summ_page['courier_price'] = 0;
		$summ_page['profit'] = 0;
		$summ_page['profit_percent'] = 0;
		$nu_pages = ceil($nu_order_total/$_SESSION['report_orders_per_page']);
		$qu_order .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_orders_per_page']).','.$_SESSION['report_orders_per_page'];
		$re_order = @mysqli_query($ddb, $qu_order);
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$summ_page['price_in'] += $ro_order['PRICE_IN'];
			$summ_page['price_out'] += $ro_order['PRICE_OUT'];
			$summ_page['price_delivery'] += $ro_order['PRICE_DELIVERY'];
			$summ_page['total'] += $ro_order['TOTAL'];
			$summ_page['margin'] += $ro_order['MARGIN'];
			$summ_page['tax'] += $ro_order['TAX'];
			$summ_page['courier_price'] += $ro_order['COURIER_PRICE'];
			$summ_page['profit'] += $ro_order['PROFIT'];
			?>
			<tr >
				<td><a href="#" onclick="ShowHideProducts(<?php echo $ro_order['ID'];?>); return false;"><?php echo $ro_order['ID']?></a></td>
				<td><?php echo $ro_order['DATE']?></td>
				<td><?php echo $ro_order['TIME']?></td>
				<td><?php echo $ro_order['FIO']?></td>
				<td><?php echo $ro_order['PHONE']?></td>
				<td><?php echo $ro_order['EMAIL']?></td>
				<td><?php echo $ro_order['PRICE_IN']?></td>
				<td><?php echo $ro_order['PRICE_OUT']?></td>
				<td><?php echo $ro_order['PRICE_DELIVERY']?></td>
				<td><strong><?php echo $ro_order['TOTAL']?></strong></td>
				<td><?php echo $ro_order['MARGIN']?></td>
				<td><?php echo $ro_order['TAX']?></td>
				<td><?php echo $ro_order['COURIER_PRICE']?></td>
				<td><strong><?php echo $ro_order['PROFIT']?></strong></td>
				<td><?php echo $ro_order['PROFIT_PERCENT']?></td>
				<td><?php echo $ro_order['DELIVERY_METHOD']?></td>
				<td><?php echo $ro_order['PAYMENT_METHOD']?></td>
				<td>
					<input type="hidden" name="table_status_old[<?php echo $ro_order['ID']?>]" value="<?php echo $ro_order['STATUS_ID']?>" />
					<select name="table_status[<?php echo $ro_order['ID']?>]">
					<?
						$qu_status = 'SELECT * FROM order_status ORDER BY order_status_id';
						$re_status = @mysqli_query($ddb, $qu_status);
						while ($ro_status = @mysqli_fetch_assoc($re_status)) echo '<option value="'.$ro_status['order_status_id'].'"'.(($ro_status['order_status_id'] == $ro_order['STATUS_ID']) ? ' selected' : '').'>'.$ro_status['name'].'</option>';
					?>
					</select>
					<!--<?php echo $ro_order['STATUS']?>-->
					
				</td>
				<td class="right">
					[<a href="/sedoy/order_new.php?id=<?php echo $ro_order['ID']?>" target="_blank">New Edit</a>]<br />
				</td>
			</tr>
			<tr>
				<td colspan="20" style="display: none;" class="order_products" id="products_<?php echo $ro_order['ID'];?>">
				</td>
			</tr>
		<?php
		}
		?>
		<tr>
			<td colspan="6"><strong>Итог по странице</strong></td>
			<td><strong><?php echo round($summ_page['price_in'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['price_out'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['price_delivery'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['total'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['margin'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['tax'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['courier_price'],2)?></strong></td>
			<td><strong><?php echo round($summ_page['profit'],2)?></strong></td>
			<td><strong><?php echo @round($summ_page['profit']/$summ_page['total']*100,2)?></strong></td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td colspan="6"><strong>Итог по всем страницам</strong></td>
			<td><strong><?php echo round($summ['price_in'],2)?></strong></td>
			<td><strong><?php echo round($summ['price_out'],2)?></strong></td>
			<td><strong><?php echo round($summ['price_delivery'],2)?></strong></td>
			<td><strong><?php echo round($summ['total'],2)?></strong></td>
			<td><strong><?php echo round($summ['margin'],2)?></strong></td>
			<td><strong><?php echo round($summ['tax'],2)?></strong></td>
			<td><strong><?php echo round($summ['courier_price'],2)?></strong></td>
			<td><strong><?php echo round($summ['profit'],2)?></strong></td>
			<td><strong><?php echo @round($summ['profit']/$summ['total']*100,2)?></strong></td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<td colspan="19" align="center">
				<input type="submit" value="Сохранить изменения в таблице заказов" />
			</td>
		</tr>
		</form>
		</tbody>
	</table>
	<div class="pagination">
		<div class="links">
			<?php
			for ($i=max(1,$_GET['p']-10);$i<=min($nu_pages,$_GET['p']+10);$i++) {
				echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.$_GET['s'].'">'.$i.'</a> ';
			}
			?>
		</div>
		<div class="results">Показано с <?php echo (($_GET['p']-1)*$_SESSION['report_orders_per_page']+1);?> по <?php echo min($_GET['p']*$_SESSION['report_orders_per_page'],$nu_order_total);?> из <?php echo $nu_order_total;?> (всего страниц: <?php echo $nu_pages;?>)</div>
	</div>
	<script>
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_date_from');
		new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_date_to');
	</script>
	</body>
	</html>
<?php
} else {
	header('Location: /');
}
?>