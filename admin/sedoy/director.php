<?php
	//обращения к директору
	require 'connect.php';
	if ($ro_user['user_group_id'] == 1) {
		if (isset($_GET['act']) && $_GET['act'] == 'del') {
			$_GET['id'] = intval($_GET['id']);
			@mysqli_query($ddb, 'DELETE FROM director WHERE id="'.$_GET['id'].'"');
			$tpl['msg'] = 'Обращение директору удалено.';
			$_GET['act'] = '';
			$_GET['id'] = '';
		}

		if(isset($_POST['act'])){
			switch ($_POST['act']) {
				case 'edit':
					$_POST['id'] = intval($_POST['id']);
					@mysqli_query($ddb, '
					UPDATE	director
					SET		date_answer="'.htmlspecialchars($_POST['date_answer']).'",
							publish="'.htmlspecialchars($_POST['publish']).'",
							name="'.htmlspecialchars($_POST['name']).'",
							email="'.htmlspecialchars($_POST['email']).'",
							phone="'.htmlspecialchars($_POST['phone']).'",
							enquiry="'.htmlspecialchars($_POST['enquiry']).'",
							answer="'.htmlspecialchars($_POST['answer']).'"
					WHERE	id="'.$_POST['id'].'"
				');
					$_GET['act'] = 'edit';
					$_GET['id'] = $_POST['id'];
					$tpl['msg'] = 'Обращение директору изменено.';
					break;
			}
		}
		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			
			<!--table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br-->
			'.((isset($tpl) && $tpl['msg'] != '') ? '<div class="msg">'.(isset($tpl) ? $tpl['msg'] : '').'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="60%">
						<table class="list">
							<thead>
								<tr>
									<td width="2%" align="center"></td>
									<td width="12%" align="center">Дата</td>
									<td width="5%" align="center">№</td>
									<td width="20%" align="center">Имя</td>
									<td width="61%" align="center">Тема</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM director ORDER BY `date` DESC';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="javascript:conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.$_GET['page'].'&id='.$ro['id'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['id'].'" style="'.(($ro['id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['date'].'</a></td>
									<td><a href="?act=edit&id='.$ro['id'].'" style="'.(($ro['id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['id'].'</a></td>
									<td><a href="?act=edit&id='.$ro['id'].'" style="'.(($ro['id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['name'].'</a></td>
									<td><a href="?act=edit&id='.$ro['id'].'" style="'.(($ro['id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['subject'].'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="40%">
		';
		if (isset($_GET['act']) && $_GET['act'] == 'edit') {
			$tpl['body_title'] = 'Обновить обращение директору';
			$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM director WHERE id="'.intval($_GET['id']).'"'));
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.title.value == "") msg = msg + br + " - не заполнена станция;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.$_GET['act'].'">
				<input type="hidden" name="id" value="'.$_GET['id'].'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">№:</td>
						<td width="85%" class="txt" align="left">'.$ro['id'].'</td>
					</tr>
					<tr>
						<td class="txt" align="right">Дата:</td>
						<td class="txt" align="left">'.$ro['date'].'</td>
					</tr>
					<tr>
						<td class="txt" align="right">Тема:</td>
						<td class="txt" align="left">'.$ro['subject'].'</td>
					</tr>
					<tr>
						<td class="txt" align="right">№ заказа:</td>
						<td class="txt" align="left">'.$ro['order_num'].'</td>
					</tr>
					<tr>
						<td class="txt" align="right">Публиковать:</td>
						<td class="txt" align="left">
							<input type="radio" name="publish" value="1"'.(($ro['publish'] == '1') ? ' checked' : '').'> Да
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="publish" value="0"'.(($ro['publish'] == '0') ? ' checked' : '').'> Нет
						</td>
					</tr>
					<tr>
						<td class="txt" align="right">Имя:</td>
						<td class="txt" align="left"><input name="name" id="name" class="txt" style="width:350px;" value="'.$ro['name'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">E-mail:</td>
						<td class="txt" align="left"><input name="email" id="email" class="txt" style="width:350px;" value="'.$ro['email'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">Телефон:</td>
						<td class="txt" align="left"><input name="phone" id="phone" class="txt" style="width:350px;" value="'.$ro['phone'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">Вопрос:</td>
						<td class="txt" align="left"><textarea name="enquiry" id="enquiry" class="txt" style="width:350px; height:100px;">'.$ro['enquiry'].'</textarea></td>
					</tr>
					<tr>
						<td class="txt" align="right">Дата/время ответа:</td>
						<td class="txt" align="left"><input name="date_answer" id="date_answer" class="txt" style="width:120px;" value="'.$ro['date_answer'].'"> <span style="cursor: pointer;" onClick="$(\'input#date_answer\').val(\''.date('Y-m-d H:i:s').'\')">Сейчас</span></td>
					</tr>
					<tr>
						<td class="txt" align="right">Ответ:</td>
						<td class="txt" align="left"><textarea name="answer" id="answer" class="txt" style="width:350px; height:100px;">'.$ro['answer'].'</textarea></td>
					</tr>
					'.(($ro['file_name'] != '') ? '
					<tr>
						<td class="txt" align="right">Файл:</td>
						<td class="txt" align="left"><a href="director_file.php?id='.$ro['id'].'">'.$ro['file_name'].'</a></td>
					</tr>
					' : '').'
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="Обновить обращение директору"></td>
					</tr>
				</table>			
				</form>
			';
		} else {
			$tpl['body_title'] = 'Обращения директору';
		}
		$tpl['meta_title'] = 'Обращения директору';	
		require 'template.php';	
	} else {
		header('Location: /');
	}

