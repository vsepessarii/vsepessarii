<?php
	//header('Content-Type: text/xml; charset=utf-8');
	//die();
	function mt() {
		$t = explode(' ',microtime());
		return round($t[0] + $t[1],4);
	}
	//echo '1 - '.mt().'<br>';

	require 'connect.php';

	$time = time();	
	$qu = '
		SELECT	macros_special.ID AS ID,
						macros_special_action.ACTION AS ACTION,
						macros_special_product.PID AS PID,
						macros_special_product.PRICE AS PRICE
		FROM		macros_special,
						macros_special_action,
						macros_special_product
		WHERE		macros_special.STATUS="1" &&
						macros_special_product.MID = macros_special.ID &&
						macros_special_action.MID = macros_special.ID &&
						(
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*00).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*00).'" && 
										macros_special_action.DAY="'.date('d',$time-60*00).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*00).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*00).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*00).'"
							)
							||
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*10).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*10).'" && 
										macros_special_action.DAY="'.date('d',$time-60*10).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*10).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*10).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*10).'"
							)
							||
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*20).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*20).'" && 
										macros_special_action.DAY="'.date('d',$time-60*20).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*20).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*20).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*20).'"
							)
						)
	';
	//echo $qu;
	$re = mysqli_query($qu);
	while ($ro = mysqli_fetch_array($re)) {
		if ($ro['ACTION'] == '-') {
			mysqli_query('DELETE FROM product_special WHERE product_id="'.$ro['PID'].'" && macros="'.$ro['ID'].'"');
		} elseif ($ro['ACTION'] == '+') {
			mysqli_query('DELETE FROM product_special WHERE product_id="'.$ro['PID'].'" && macros="'.$ro['ID'].'"');
			mysqli_query('
				INSERT INTO product_special
				SET	product_id="'.$ro['PID'].'",
						customer_group_id="1",
						priority="0",
						price="'.$ro['PRICE'].'",
						date_start="0000-00-00",
						date_end="0000-00-00",
						macros="'.$ro['ID'].'"
			');
		}
	}
	//echo '2 - '.mt().'<br>';
	
	require 'send_sms.php';
	require 'send_mail.php';
	//echo '4 - '.mt().'<br>';
?>