﻿<?php
	//отчет по суммам (ежедневный)
	error_reporting(0);
	require 'connect.php';
	//идентификаторы товарных групп
	$id = array(
		'arabin_asq' => '441',
		'arabin_other' => '442, 443, 445, 449, 455, 456, 457, 458, 459, 460, 462, 1010',
		'yunona_obstetric' => '440', 
		'yunona_gynecological' => '439',
		'arabin_all' => '441, 442, 443, 445, 449, 455, 456, 457, 458, 459, 460, 462, 1010',
		'yunona_all' => '439, 440',
		'portex_all' => '1909',
		'alfa_all' => '1911',
		'all' => '441, 442, 443, 445, 449, 455, 456, 457, 458, 459, 460, 462, 1010, 439, 440, 1911'
	);

	//echo microtime_float().'<br />';
	if ($ro_user['user_group_id'] == 1) {
		if ($_GET['s'] == '') $_GET['s'] = '`date` asc';
		if ($_POST['act'] == 'clear_filter') {
			$_SESSION['report_summ_daily_date_from'] = '';
			$_SESSION['report_summ_daily_date_to'] = '';
			$_SESSION['report_summ_daily_summ_in_from'] = '';
			$_SESSION['report_summ_daily_summ_in_to'] = '';
			$_SESSION['report_summ_daily_summ_out_from'] = '';
			$_SESSION['report_summ_daily_summ_out_to'] = '';
			$_SESSION['report_summ_daily_price_delivery'] = '';
			$_SESSION['report_summ_daily_total_from'] = '';
			$_SESSION['report_summ_daily_total_to'] = '';
			$_SESSION['report_summ_daily_tax_from'] = '';
			$_SESSION['report_summ_daily_tax_to'] = '';
			$_SESSION['report_summ_daily_courier_price_from'] = '';
			$_SESSION['report_summ_daily_courier_price_to'] = '';
			$_SESSION['report_summ_daily_margin_netto_from'] = '';
			$_SESSION['report_summ_daily_margin_netto_to'] = '';
			$_SESSION['report_summ_daily_margin_brutto_from'] = '';
			$_SESSION['report_summ_daily_margin_brutto_to'] = '';
			$_SESSION['report_summ_daily_profitability_from'] = '';
			$_SESSION['report_summ_daily_profitability_to'] = '';

			$_SESSION['report_summ_daily_orders_from'] = '';
			$_SESSION['report_summ_daily_orders_to'] = '';

			$_SESSION['report_summ_daily_amount_arabin_asq_from'] = '';
			$_SESSION['report_summ_daily_amount_arabin_asq_to'] = '';
			$_SESSION['report_summ_daily_amount_arabin_other_from'] = '';
			$_SESSION['report_summ_daily_amount_arabin_other_to'] = '';
			$_SESSION['report_summ_daily_amount_yunona_obstetric_from'] = '';
			$_SESSION['report_summ_daily_amount_yunona_obstetric_to'] = '';
			$_SESSION['report_summ_daily_amount_yunona_gynecological_from'] = '';
			$_SESSION['report_summ_daily_amount_yunona_gynecological_to'] = '';
			$_SESSION['report_summ_daily_amount_arabin_all_from'] = '';
			$_SESSION['report_summ_daily_amount_arabin_all_to'] = '';
			$_SESSION['report_summ_daily_amount_yunona_all_from'] = '';
			$_SESSION['report_summ_daily_amount_yunona_all_to'] = '';
			$_SESSION['report_summ_daily_amount_portex_all_from'] = '';
			$_SESSION['report_summ_daily_amount_portex_all_to'] = '';
			$_SESSION['report_summ_daily_amount_alfa_all_from'] = '';
			$_SESSION['report_summ_daily_amount_alfa_all_to'] = '';
			$_SESSION['report_summ_daily_amount_all_from'] = '';
			$_SESSION['report_summ_daily_amount_all_to'] = '';
		}

		if ($_SESSION['report_summ_daily_courier_price'] == '') $_SESSION['report_summ_daily_courier_price'] = 250;
		if ($_SESSION['report_summ_daily_per_page'] == '') $_SESSION['report_summ_daily_per_page'] = 25;

		if ($_POST['act'] == 'setting') {
			$_SESSION['report_summ_daily_courier_price'] = $_POST['_report_summ_dailycourier_price'];
			$_SESSION['report_summ_daily_per_page'] = $_POST['report_summ_daily_per_page'];
		}



		if ($_POST['act'] == 'filter') {
			$_SESSION['report_summ_daily_date_from'] = $_POST['report_summ_daily_date_from'];
			$_SESSION['report_summ_daily_date_to'] = $_POST['report_summ_daily_date_to'];
			$_SESSION['report_summ_daily_summ_in_from'] = $_POST['report_summ_daily_summ_in_from'];
			$_SESSION['report_summ_daily_summ_in_to'] = $_POST['report_summ_daily_summ_in_to'];
			$_SESSION['report_summ_daily_summ_out_from'] = $_POST['report_summ_daily_summ_out_from'];
			$_SESSION['report_summ_daily_summ_out_to'] = $_POST['report_summ_daily_summ_out_to'];
			$_SESSION['report_summ_daily_price_delivery'] = $_POST['report_summ_daily_price_delivery'];
			$_SESSION['report_summ_daily_total_from'] = $_POST['report_summ_daily_total_from'];
			$_SESSION['report_summ_daily_total_to'] = $_POST['report_summ_daily_total_to'];
			$_SESSION['report_summ_daily_tax_from'] = $_POST['report_summ_daily_tax_from'];
			$_SESSION['report_summ_daily_tax_to'] = $_POST['report_summ_daily_tax_to'];
			$_SESSION['report_summ_daily_courier_price_from'] = $_POST['report_summ_daily_courier_price_from'];
			$_SESSION['report_summ_daily_courier_price_to'] = $_POST['report_summ_daily_courier_price_to'];
			$_SESSION['report_summ_daily_margin_netto_from'] = $_POST['report_summ_daily_margin_netto_from'];
			$_SESSION['report_summ_daily_margin_netto_to'] = $_POST['report_summ_daily_margin_netto_to'];
			$_SESSION['report_summ_daily_margin_brutto_from'] = $_POST['report_summ_daily_margin_brutto_from'];
			$_SESSION['report_summ_daily_margin_brutto_to'] = $_POST['report_summ_daily_margin_brutto_to'];
			$_SESSION['report_summ_daily_profitability_from'] = $_POST['report_summ_daily_profitability_from'];
			$_SESSION['report_summ_daily_profitability_to'] = $_POST['report_summ_daily_profitability_to'];

			$_SESSION['report_summ_daily_orders_from'] = $_POST['report_summ_daily_orders_from'];
			$_SESSION['report_summ_daily_orders_to'] = $_POST['report_summ_daily_orders_to'];

			$_SESSION['report_summ_daily_amount_arabin_asq_from'] = $_POST['report_summ_daily_amount_arabin_asq_from'];
			$_SESSION['report_summ_daily_amount_arabin_asq_to'] = $_POST['report_summ_daily_amount_arabin_asq_to'];
			$_SESSION['report_summ_daily_amount_arabin_other_from'] = $_POST['report_summ_daily_amount_arabin_other_from'];
			$_SESSION['report_summ_daily_amount_arabin_other_to'] = $_POST['report_summ_daily_amount_arabin_other_to'];
			$_SESSION['report_summ_daily_amount_yunona_obstetric_from'] = $_POST['report_summ_daily_amount_yunona_obstetric_from'];
			$_SESSION['report_summ_daily_amount_yunona_obstetric_to'] = $_POST['report_summ_daily_amount_yunona_obstetric_to'];
			$_SESSION['report_summ_daily_amount_yunona_gynecological_from'] = $_POST['report_summ_daily_amount_yunona_gynecological_from'];
			$_SESSION['report_summ_daily_amount_yunona_gynecological_to'] = $_POST['report_summ_daily_amount_yunona_gynecological_to'];
			$_SESSION['report_summ_daily_amount_arabin_all_from'] = $_POST['report_summ_daily_amount_arabin_all_from'];
			$_SESSION['report_summ_daily_amount_arabin_all_to'] = $_POST['report_summ_daily_amount_arabin_all_to'];
			$_SESSION['report_summ_daily_amount_yunona_all_from'] = $_POST['report_summ_daily_amount_yunona_all_from'];
			$_SESSION['report_summ_daily_amount_yunona_all_to'] = $_POST['report_summ_daily_amount_yunona_all_to'];
			$_SESSION['report_summ_daily_amount_portex_all_from'] = $_POST['report_summ_daily_amount_portex_all_from'];
			$_SESSION['report_summ_daily_amount_portex_all_to'] = $_POST['report_summ_daily_amount_portex_all_to'];
			$_SESSION['report_summ_daily_amount_alfa_all_from'] = $_POST['report_summ_daily_amount_alfa_all_from'];
			$_SESSION['report_summ_daily_amount_alfa_all_to'] = $_POST['report_summ_daily_amount_alfa_all_to'];
			$_SESSION['report_summ_daily_amount_all_from'] = $_POST['report_summ_daily_amount_all_from'];
			$_SESSION['report_summ_daily_amount_all_to'] = $_POST['report_summ_daily_amount_all_to'];
			
			$_GET['p'] = 1;

		}

		if ($_SESSION['report_summ_daily_date_from'] == '') $_SESSION['report_summ_daily_date_from'] = date('Y-m-01',time());
		if ($_SESSION['report_summ_daily_date_to'] == '') $_SESSION['report_summ_daily_date_to'] = date('Y-m-t',time());

		//$_SESSION['report_summ_daily_date_from'] = "2001-01-01";

		@mysqli_query($ddb, 'TRUNCATE TABLE tmp_report_summ_daily');

		//собираем даты
		$qu_date = 'SELECT DATE(`order`.`date_added`) as `date` FROM `order` WHERE DATE(`order`.`date_added`)>="'.$_SESSION['report_summ_daily_date_from'].'" && DATE(`order`.`date_added`)<="'.$_SESSION['report_summ_daily_date_to'].'" GROUP BY `date` ORDER BY `date`';
		//echo $qu_date;
		$re_date = @mysqli_query($ddb, $qu_date);
		//echo mysqli_error($ddb);
		while ($ro_date = @mysqli_fetch_array($re_date)) {
			$date = $ro_date['date'];
			//echo $date.'<br>';
			//расчитываем дневные данные
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`price_in`*`order_product`.`quantity`) AS `summ_in`, 
								SUM(`order_product`.`price`*`order_product`.`quantity`) AS `summ_out`
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59"
			');
			echo mysqli_error($ddb);
			$summ_in = $tmp['summ_in'];
			$summ_out = $tmp['summ_out'];
			
			$tmp = Result($ddb, '
				SELECT	SUM(`order_total`.`value`) AS `price_delivery` 
				FROM		`order`,
								`order_total`
				WHERE		`order`.`order_id` = `order_total`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_total`.`code` = "shipping"
			');
			$price_delivery = $tmp['price_delivery'];
			
			$total = $summ_out + $price_delivery;
			
			$tax = round($summ_out*0.06, 2);
			
			$tmp = Result($ddb, '
				SELECT	SUM(`courier_price`) AS `courier_price` 
				FROM		`order`
				WHERE		`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`<="'.$date.' 23:59:59"
			');
			$courier_price = $tmp['courier_price'];
			
			$margin_brutto = $total - $summ_in - $courier_price;
			$margin_netto = $margin_brutto - $tax;
			
			$profitability = round($margin_netto/$summ_out*100,2);
			
			$tmp = Result($ddb, '
				SELECT	COUNT(`order_id`) AS `orders` 
				FROM		`order`
				WHERE		`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`<="'.$date.' 23:59:59"
			');
			$orders = $tmp['orders'];


			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['arabin_asq'].')
			');
			$amount_arabin_asq = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['arabin_other'].')
			');
			$amount_arabin_other = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['yunona_obstetric'].')
			');
			$amount_yunona_obstetric = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['yunona_gynecological'].')
			');
			$amount_yunona_gynecological = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['arabin_all'].')
			');
			$amount_arabin_all = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['yunona_all'].')
			');
			$amount_yunona_all = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['alfa_all'].')
			');
			$amount_alfa_all = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['portex_all'].')
			');
			$amount_portex_all = $tmp['quantity'];
			 
			$tmp = Result($ddb, '
				SELECT	SUM(`order_product`.`quantity`) AS `quantity` 
				FROM		`order`,
								`order_product`
				WHERE		`order`.`order_id` = `order_product`.`order_id` &&
								`order`.`order_status_id` > 0 &&				
								`order`.`date_added`>="'.$date.' 00:00:00" &&
								`order`.`date_added`<="'.$date.' 23:59:59" &&
								`order_product`.`product_id` IN('.$id['all'].')
			');
			$amount_all = $tmp['quantity'];
			 
			@mysqli_query($ddb, '
				INSERT INTO	tmp_report_summ_daily
				SET					`date`="'.$date.'",
										`summ_in`="'.$summ_in.'",
										`summ_out`="'.$summ_out.'",
										`price_delivery`="'.$price_delivery.'",
										`total`="'.$total.'",
										`tax`="'.$tax.'",
										`courier_price`="'.$courier_price.'",
										`margin_netto`="'.$margin_netto.'",
										`margin_brutto`="'.$margin_brutto.'",
										`profitability`="'.$profitability.'",
										`orders`="'.$orders.'",
										`amount_arabin_asq`="'.$amount_arabin_asq.'",
										`amount_arabin_other`="'.$amount_arabin_other.'",
										`amount_yunona_obstetric`="'.$amount_yunona_obstetric.'",
										`amount_yunona_gynecological`="'.$amount_yunona_gynecological.'",
										`amount_arabin_all`="'.$amount_arabin_all.'",
										`amount_yunona_all`="'.$amount_yunona_all.'",
										`amount_portex_all`="'.$amount_portex_all.'",
										`amount_alfa_all`="'.$amount_alfa_all.'",
										`amount_all`="'.$amount_all.'"
			');			
		}
		//die('111');

?>

<html>
	<head>
		<title>Отчет по суммам (ежедневный)</title>
		<link type="text/css" href="<?php echo $app_url?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<script language="javascript">
			$(function() {
	  	  $( "#report_summ_daily_date_from" ).datepicker();
		    $( "#report_summ_daily_date_to" ).datepicker();
		  });
		</script>
	</head>
	<body>
		<style>
			td { word-wrap: break-word;}
		</style>
		<div align="right">
			<br />
			<a href="/index.php?route=common/home&token=<?=$_SESSION['token']?>">Вернуться в панель управления</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="report_summ_daily_csv.php?s=<?=$_GET['s']?>">Скачать в CSV</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				<input type="hidden" name="act" value="clear_filter" />
				<input type="submit" value="Сбросить фильтры" />
			</form>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				Выводить по:

				<select name="report_summ_daily_per_page">
					<option value="25"<?=($_SESSION['report_summ_daily_per_page'] == '25') ? ' selected' : ''?>>25</option>
					<option value="50"<?=($_SESSION['report_summ_daily_per_page'] == '50') ? ' selected' : ''?>>50</option>
					<option value="100"<?=($_SESSION['report_summ_daily_per_page'] == '100') ? ' selected' : ''?>>100</option>
					<option value="200"<?=($_SESSION['report_summ_daily_per_page'] == '200') ? ' selected' : ''?>>200</option>
				</select>
				<input type="submit" value="Сохранить" />
			</form>
			<br /><br />
		</div>
		<table class="list">
			<thead>
				<tr>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`date` ASC') ? '`date` DESC' : '`date` ASC';?>" class="<?=(($_GET['s'] == '`date` ASC') ? 'asc' : '').(($_GET['s'] == '`date` DESC') ? 'desc' : '')?>">Дата</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`summ_in` ASC') ? '`summ_in` DESC' : '`summ_in` ASC';?>" class="<?=(($_GET['s'] == '`summ_in` ASC') ? 'asc' : '').(($_GET['s'] == '`summ_in` DESC') ? 'desc' : '')?>">Сумма (вх.)</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`summ_out` ASC') ? '`summ_out` DESC' : '`summ_out` ASC';?>" class="<?=(($_GET['s'] == '`summ_out` ASC') ? 'asc' : '').(($_GET['s'] == '`summ_out` DESC') ? 'desc' : '')?>">Сумма (исх.)</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`price_delivery` ASC') ? '`price_delivery` DESC' : '`price_delivery` ASC';?>" class="<?=(($_GET['s'] == '`price_delivery` ASC') ? 'asc' : '').(($_GET['s'] == '`price_delivery` DESC') ? 'desc' : '')?>">Сумма (дост)</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`total` ASC') ? '`total` DESC' : '`total` ASC';?>" class="<?=(($_GET['s'] == '`total` ASC') ? 'asc' : '').(($_GET['s'] == '`total` DESC') ? 'desc' : '')?>">ИТОГО</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`tax` ASC') ? '`tax` DESC' : '`tax` ASC';?>" class="<?=(($_GET['s'] == '`tax` ASC') ? 'asc' : '').(($_GET['s'] == '`tax` DESC') ? 'desc' : '')?>">Налог 6%</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`courier_price` ASC') ? '`courier_price` DESC' : '`courier_price` ASC';?>" class="<?=(($_GET['s'] == '`courier_price` ASC') ? 'asc' : '').(($_GET['s'] == '`courier_price` DESC') ? 'desc' : '')?>">Курьеру</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`margin_netto` ASC') ? '`margin_netto` DESC' : '`margin_netto` ASC';?>" class="<?=(($_GET['s'] == '`margin_netto` ASC') ? 'asc' : '').(($_GET['s'] == '`margin_netto` DESC') ? 'desc' : '')?>">МАРЖА</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`margin_brutto` ASC') ? '`margin_brutto` DESC' : '`margin_brutto` ASC';?>" class="<?=(($_GET['s'] == '`margin_brutto` ASC') ? 'asc' : '').(($_GET['s'] == '`margin_brutto` DESC') ? 'desc' : '')?>">Брутто МАРЖА</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`profitability` ASC') ? '`profitability` DESC' : '`profitability` ASC';?>" class="<?=(($_GET['s'] == '`profitability` ASC') ? 'asc' : '').(($_GET['s'] == '`profitability` DESC') ? 'desc' : '')?>">Рентабельность %</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`orders` ASC') ? '`orders` DESC' : '`orders` ASC';?>" class="<?=(($_GET['s'] == '`orders` ASC') ? 'asc' : '').(($_GET['s'] == '`orders` DESC') ? 'desc' : '')?>">Заказов</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_arabin_asq` ASC') ? '`amount_arabin_asq` DESC' : '`amount_arabin_asq` ASC';?>" class="<?=(($_GET['s'] == '`amount_arabin_asq` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_arabin_asq` DESC') ? 'desc' : '')?>">Arabin ASQ</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_arabin_other` ASC') ? '`amount_arabin_other` DESC' : '`amount_arabin_other` ASC';?>" class="<?=(($_GET['s'] == '`amount_arabin_other` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_arabin_other` DESC') ? 'desc' : '')?>">Другие Arabin</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_yunona_obstetric` ASC') ? '`amount_yunona_obstetric` DESC' : '`amount_yunona_obstetric` ASC';?>" class="<?=(($_GET['s'] == '`amount_yunona_obstetric` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_yunona_obstetric` DESC') ? 'desc' : '')?>">Акушерский Юнона</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_yunona_gynecological` ASC') ? '`amount_yunona_gynecological` DESC' : '`amount_yunona_gynecological` ASC';?>" class="<?=(($_GET['s'] == '`amount_yunona_gynecological` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_yunona_gynecological` DESC') ? 'desc' : '')?>">Маточный Юнона</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_arabin_all` ASC') ? '`amount_arabin_all` DESC' : '`amount_arabin_all` ASC';?>" class="<?=(($_GET['s'] == '`amount_arabin_all` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_arabin_all` DESC') ? 'desc' : '')?>">Арабины</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_yunona_all` ASC') ? '`amount_yunona_all` DESC' : '`amount_yunona_all` ASC';?>" class="<?=(($_GET['s'] == '`amount_yunona_all` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_yunona_all` DESC') ? 'desc' : '')?>">Юноны</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_portex_all` ASC') ? '`amount_portex_all` DESC' : '`amount_portex_all` ASC';?>" class="<?=(($_GET['s'] == '`amount_portex_all` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_portex_all` DESC') ? 'desc' : '')?>">Портекс</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_alfa_all` ASC') ? '`amount_alfa_all` DESC' : '`amount_alfa_all` ASC';?>" class="<?=(($_GET['s'] == '`amount_alfa_all` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_alfa_all` DESC') ? 'desc' : '')?>">Альфа-пластик</a></td>
					<td class="left" width="2%"><a href="?p=<?=$_GET['p']?>&s=<?=($_GET['s'] == '`amount_all` ASC') ? '`amount_all` DESC' : '`amount_all` ASC';?>" class="<?=(($_GET['s'] == '`amount_all` ASC') ? 'asc' : '').(($_GET['s'] == '`amount_all` DESC') ? 'desc' : '')?>">ИТОГО Пессарии</a></td>
				</tr>
			</thead>
			<tbody>
				<form method="post" id="filter">
				<input type="hidden" name="act" value="filter" />
				<center><input type="submit" style="text-align: center;" value="Применить фильтр"  /></center>
				<tr class="filter">
					<td>
						<input type="text" name="report_summ_daily_date_from" id="report_summ_daily_date_from" value="<?=$_SESSION['report_summ_daily_date_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_date_to" id="report_summ_daily_date_to" value="<?=$_SESSION['report_summ_daily_date_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_summ_in_from" id="report_summ_daily_summ_in_from" value="<?=$_SESSION['report_summ_daily_summ_in_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_summ_in_to" id="report_summ_daily_summ_in_to" value="<?=$_SESSION['report_summ_daily_summ_in_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_summ_out_from" id="report_summ_daily_summ_out_from" value="<?=$_SESSION['report_summ_daily_summ_out_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_summ_out_to" id="report_summ_daily_summ_out_to" value="<?=$_SESSION['report_summ_daily_summ_out_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_price_delivery_from" id="report_summ_daily_price_delivery_from" value="<?=$_SESSION['report_summ_daily_price_delivery_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_price_delivery_to" id="report_summ_daily_price_delivery_to" value="<?=$_SESSION['report_summ_daily_price_delivery_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_total_from" id="report_summ_daily_total_from" value="<?=$_SESSION['report_summ_daily_total_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_total_to" id="report_summ_daily_total_to" value="<?=$_SESSION['report_summ_daily_total_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_tax_from" id="report_summ_daily_tax_from" value="<?=$_SESSION['report_summ_daily_tax_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_tax_to" id="report_summ_daily_tax_to" value="<?=$_SESSION['report_summ_daily_tax_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_courier_price_from" id="report_summ_daily_courier_price_from" value="<?=$_SESSION['report_summ_daily_courier_price_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_courier_price_to" id="report_summ_daily_courier_price_to" value="<?=$_SESSION['report_summ_daily_courier_price_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_margin_netto_from" id="report_summ_daily_margin_netto_from" value="<?=$_SESSION['report_summ_daily_margin_netto_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_margin_netto_to" id="report_summ_daily_margin_netto_to" value="<?=$_SESSION['report_summ_daily_margin_netto_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_margin_brutto_from" id="report_summ_daily_margin_brutto_from" value="<?=$_SESSION['report_summ_daily_margin_brutto_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_margin_brutto_to" id="report_summ_daily_margin_brutto_to" value="<?=$_SESSION['report_summ_daily_margin_brutto_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_profitability_from" id="report_summ_daily_profitability_from" value="<?=$_SESSION['report_summ_daily_profitability_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_profitability_to" id="report_summ_daily_profitability_to" value="<?=$_SESSION['report_summ_daily_profitability_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_orders_from" id="report_summ_daily_orders_from" value="<?=$_SESSION['report_summ_daily_orders_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_orders_to" id="report_summ_daily_orders_to" value="<?=$_SESSION['report_summ_daily_orders_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_arabin_asq_from" id="report_summ_daily_amount_arabin_asq_from" value="<?=$_SESSION['report_summ_daily_amount_arabin_asq_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_arabin_asq_to" id="report_summ_daily_amount_arabin_asq_to" value="<?=$_SESSION['report_summ_daily_amount_arabin_asq_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_arabin_other_from" id="report_summ_daily_amount_arabin_other_from" value="<?=$_SESSION['report_summ_daily_amount_arabin_other_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_arabin_other_to" id="report_summ_daily_amount_arabin_other_to" value="<?=$_SESSION['report_summ_daily_amount_arabin_other_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_yunona_obstetric_from" id="report_summ_daily_amount_yunona_obstetric_from" value="<?=$_SESSION['report_summ_daily_amount_yunona_obstetric_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_yunona_obstetric_to" id="report_summ_daily_amount_yunona_obstetric_to" value="<?=$_SESSION['report_summ_daily_amount_yunona_obstetric_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_yunona_gynecological_from" id="report_summ_daily_amount_yunona_gynecological_from" value="<?=$_SESSION['report_summ_daily_amount_yunona_gynecological_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_yunona_gynecological_to" id="report_summ_daily_amount_yunona_gynecological_to" value="<?=$_SESSION['report_summ_daily_amount_yunona_gynecological_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_arabin_all_from" id="report_summ_daily_amount_arabin_all_from" value="<?=$_SESSION['report_summ_daily_amount_arabin_all_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_arabin_all_to" id="report_summ_daily_amount_arabin_all_to" value="<?=$_SESSION['report_summ_daily_amount_arabin_all_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_yunona_all_from" id="report_summ_daily_amount_yunona_all_from" value="<?=$_SESSION['report_summ_daily_amount_yunona_all_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_yunona_all_to" id="report_summ_daily_amount_yunona_all_to" value="<?=$_SESSION['report_summ_daily_amount_yunona_all_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_portex_all_from" id="report_summ_daily_amount_portex_all_from" value="<?=$_SESSION['report_summ_daily_amount_portex_all_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_portex_all_to" id="report_summ_daily_amount_portex_all_to" value="<?=$_SESSION['report_summ_daily_amount_portex_all_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily_amount_alfa_all_from" id="report_summ_daily_amount_alfa_all_from" value="<?=$_SESSION['report_summ_daily_amount_alfa_all_from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_alfa_all_to" id="report_summ_daily_amount_alfa_all_to" value="<?=$_SESSION['report_summ_daily_amount_alfa_all_to'];?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_summ_daily__from" id="report_summ_daily__from" value="<?=$_SESSION['report_summ_daily__from'];?>" style="width: 100%;">
						<input type="text" name="report_summ_daily_amount_all_to" id="report_summ_daily__to" value="<?=$_SESSION['report_summ_daily_amount_all_to'];?>" style="width: 100%;">
					</td>
				</tr>
				</form>
				<?php
					$qu_day = '
						SELECT	*
						FROM 		tmp_report_summ_daily
						WHERE		1
										'.(($_SESSION['report_summ_daily_date_from'] != '') ? ' && `date`>="'.$_SESSION['report_summ_daily_date_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_date_to'] != '') ? ' && `date`<="'.$_SESSION['report_summ_daily_date_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_summ_in_from'] != '') ? ' && `summ_in`>="'.$_SESSION['report_summ_daily_summ_in_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_summ_in_to'] != '') ? ' && `summ_in`<="'.$_SESSION['report_summ_daily_summ_in_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_summ_out_from'] != '') ? ' && `summ_out`>="'.$_SESSION['report_summ_daily_summ_out_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_summ_out_to'] != '') ? ' && `summ_out`<="'.$_SESSION['report_summ_daily_summ_out_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_price_delivery_from'] != '') ? ' && `price_delivery`>="'.$_SESSION['report_summ_daily_price_delivery_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_price_delivery_to'] != '') ? ' && `price_delivery`<="'.$_SESSION['report_summ_daily_price_delivery_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_total_from'] != '') ? ' && `total`>="'.$_SESSION['report_summ_daily_total_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_total_to'] != '') ? ' && `total`<="'.$_SESSION['report_summ_daily_total_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_tax_from'] != '') ? ' && `tax`>="'.$_SESSION['report_summ_daily_tax_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_tax_to'] != '') ? ' && `tax`<="'.$_SESSION['report_summ_daily_tax_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_courier_price_from'] != '') ? ' && `courier_price`>="'.$_SESSION['report_summ_daily_courier_price_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_courier_price_to'] != '') ? ' && `courier_price`<="'.$_SESSION['report_summ_daily_courier_price_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_margin_netto_from'] != '') ? ' && `margin_netto`>="'.$_SESSION['report_summ_daily_margin_netto_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_margin_netto_to'] != '') ? ' && `margin_netto`<="'.$_SESSION['report_summ_daily_margin_netto_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_margin_brutto_from'] != '') ? ' && `margin_brutto`>="'.$_SESSION['report_summ_daily_margin_brutto_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_margin_brutto_to'] != '') ? ' && `margin_brutto`<="'.$_SESSION['report_summ_daily_margin_brutto_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_profitability_from'] != '') ? ' && `profitability`>="'.$_SESSION['report_summ_daily_profitability_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_profitability_to'] != '') ? ' && `profitability`<="'.$_SESSION['report_summ_daily_profitability_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_orders_from'] != '') ? ' && `orders`>="'.$_SESSION['report_summ_daily_orders_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_orders_to'] != '') ? ' && `orders`<="'.$_SESSION['report_summ_daily_orders_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_asq_from'] != '') ? ' && `amount_arabin_asq`>="'.$_SESSION['report_summ_daily_amount_arabin_asq_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_asq_to'] != '') ? ' && `amount_arabin_asq`<="'.$_SESSION['report_summ_daily_amount_arabin_asq_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_other_from'] != '') ? ' && `amount_arabin_other`>="'.$_SESSION['report_summ_daily_amount_arabin_other_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_other_to'] != '') ? ' && `amount_arabin_other`<="'.$_SESSION['report_summ_daily_amount_arabin_other_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_obstetric_from'] != '') ? ' && `amount_yunona_obstetric`>="'.$_SESSION['report_summ_daily_amount_yunona_obstetric_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_obstetric_to'] != '') ? ' && `amount_yunona_obstetric`<="'.$_SESSION['report_summ_daily_amount_yunona_obstetric_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_gynecological_from'] != '') ? ' && `amount_yunona_gynecological`>="'.$_SESSION['report_summ_daily_amount_yunona_gynecological_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_gynecological_to'] != '') ? ' && `amount_yunona_gynecological`<="'.$_SESSION['report_summ_daily_amount_yunona_gynecological_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_all_from'] != '') ? ' && `amount_arabin_all`>="'.$_SESSION['report_summ_daily_amount_arabin_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_all_to'] != '') ? ' && `amount_arabin_all`<="'.$_SESSION['report_summ_daily_amount_arabin_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_all_from'] != '') ? ' && `amount_yunona_all`>="'.$_SESSION['report_summ_daily_amount_yunona_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_all_to'] != '') ? ' && `amount_yunona_all`<="'.$_SESSION['report_summ_daily_amount_yunona_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_portex_all_from'] != '') ? ' && `amount_portex_all`>="'.$_SESSION['report_summ_daily_amount_portex_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_portex_all_to'] != '') ? ' && `amount_portex_all`<="'.$_SESSION['report_summ_daily_amount_portex_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_alfa_all_from'] != '') ? ' && `amount_alfa_all`>="'.$_SESSION['report_summ_daily_amount_alfa_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_alfa_all_to'] != '') ? ' && `amount_alfa_all`<="'.$_SESSION['report_summ_daily_amount_alfa_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_all_from'] != '') ? ' && `amount_all`>="'.$_SESSION['report_summ_daily_amount_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_all_to'] != '') ? ' && `amount_all`<="'.$_SESSION['report_summ_daily_amount_all_to'].'"' : '').'
						ORDER	BY	'.$_GET['s'];

				$re_day = @mysqli_query($ddb, $qu_day);
				//echo mysqli_error($ddb);
				$nu_day = @mysqli_num_rows($re_day);

				unset($summ);
				while ($ro_day = @mysqli_fetch_array($re_day)) {
					$summ['summ_in'] += $ro_day['summ_in'];
					$summ['summ_out'] += $ro_day['summ_out'];
					$summ['price_delivery'] += $ro_day['price_delivery'];
					$summ['total'] += $ro_day['total'];
					$summ['tax'] += $ro_day['tax'];
					$summ['courier_price'] += $ro_day['courier_price'];
					$summ['margin_netto'] += $ro_day['margin_netto'];
					$summ['margin_brutto'] += $ro_day['margin_brutto'];
					$summ['orders'] += $ro_day['orders'];
					$summ['amount_arabin_asq'] += $ro_day['amount_arabin_asq'];
					$summ['amount_arabin_other'] += $ro_day['amount_arabin_other'];
					$summ['amount_yunona_obstetric'] += $ro_day['amount_yunona_obstetric'];
					$summ['amount_yunona_gynecological'] += $ro_day['amount_yunona_gynecological'];
					$summ['amount_arabin_all'] += $ro_day['amount_arabin_all'];
					$summ['amount_yunona_all'] += $ro_day['amount_yunona_all'];
					$summ['amount_portex_all'] += $ro_day['amount_portex_all'];
					$summ['amount_alfa_all'] += $ro_day['amount_alfa_all'];
					$summ['amount_all'] += $ro_day['amount_all'];
				}

				unset($summ_page);
				$nu_pages = ceil($nu_day/$_SESSION['report_summ_daily_per_page']);
				$qu_day .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_summ_daily_per_page']).','.$_SESSION['report_summ_daily_per_page'];
				//echo $qu_day;
				$re_day = @mysqli_query($ddb, $qu_day);
				while ($ro_day = @mysqli_fetch_array($re_day)) {
					$summ_page['summ_in'] += $ro_day['summ_in'];
					$summ_page['summ_out'] += $ro_day['summ_out'];
					$summ_page['price_delivery'] += $ro_day['price_delivery'];
					$summ_page['total'] += $ro_day['total'];
					$summ_page['tax'] += $ro_day['tax'];
					$summ_page['courier_price'] += $ro_day['courier_price'];
					$summ_page['margin_netto'] += $ro_day['margin_netto'];
					$summ_page['margin_brutto'] += $ro_day['margin_brutto'];
					$summ_page['orders'] += $ro_day['orders'];
					$summ_page['amount_arabin_asq'] += $ro_day['amount_arabin_asq'];
					$summ_page['amount_arabin_other'] += $ro_day['amount_arabin_other'];
					$summ_page['amount_yunona_obstetric'] += $ro_day['amount_yunona_obstetric'];
					$summ_page['amount_yunona_gynecological'] += $ro_day['amount_yunona_gynecological'];
					$summ_page['amount_arabin_all'] += $ro_day['amount_arabin_all'];
					$summ_page['amount_yunona_all'] += $ro_day['amount_yunona_all'];
					$summ_page['amount_portex_all'] += $ro_day['amount_portex_all'];
					$summ_page['amount_alfa_all'] += $ro_day['amount_alfa_all'];
					$summ_page['amount_all'] += $ro_day['amount_all'];
					?>
						<tr >
							<td><?=$ro_day['date']?></td>
							<td><?=$ro_day['summ_in']?></td>
							<td><?=$ro_day['summ_out']?></td>
							<td><?=$ro_day['price_delivery']?></td>
							<td><?=$ro_day['total']?></td>
							<td><?=$ro_day['tax']?></td>
							<td><?=$ro_day['courier_price']?></td>
							<td><?=$ro_day['margin_netto']?></td>
							<td><?=$ro_day['margin_brutto']?></td>
							<td><?=$ro_day['profitability']?></td>
							<td><?=$ro_day['orders']?></td>
							<td><?=$ro_day['amount_arabin_asq']?></td>
							<td><?=$ro_day['amount_arabin_other']?></td>
							<td><?=$ro_day['amount_yunona_obstetric']?></td>
							<td><?=$ro_day['amount_yunona_gynecological']?></td>
							<td><?=$ro_day['amount_arabin_all']?></td>
							<td><?=$ro_day['amount_yunona_all']?></td>
							<td><?=$ro_day['amount_portex_all']?></td>
							<td><?=$ro_day['amount_alfa_all']?></td>
							<td><?=$ro_day['amount_all']?></td>
						</tr>
					<?php
				}
				?>
						<tr >
							<td><strong>Страница</strong></td>
							<td><strong><?=$summ_page['summ_in']?></strong></td>
							<td><strong><?=$summ_page['summ_out']?></strong></td>
							<td><strong><?=$summ_page['price_delivery']?></strong></td>
							<td><strong><?=$summ_page['total']?></strong></td>
							<td><strong><?=$summ_page['tax']?></strong></td>
							<td><strong><?=$summ_page['courier_price']?></strong></td>
							<td><strong><?=$summ_page['margin_netto']?></strong></td>
							<td><strong><?=$summ_page['margin_brutto']?></strong></td>
							<td><strong><?=round($summ_page['margin_netto']/$summ_page['total']*100,2)?></strong></td>
							<td><strong><?=$summ_page['orders']?></strong></td>
							<td><strong><?=$summ_page['amount_arabin_asq']?></strong></td>
							<td><strong><?=$summ_page['amount_arabin_other']?></strong></td>
							<td><strong><?=$summ_page['amount_yunona_obstetric']?></strong></td>
							<td><strong><?=$summ_page['amount_yunona_gynecological']?></strong></td>
							<td><strong><?=$summ_page['amount_arabin_all']?></strong></td>
							<td><strong><?=$summ_page['amount_yunona_all']?></strong></td>
							<td><strong><?=$summ_page['amount_portex_all']?></strong></td>
							<td><strong><?=$summ_page['amount_alfa_all']?></strong></td>
							<td><strong><?=$summ_page['amount_all']?></strong></td>
						</tr>

						<tr >
							<td><strong>ИТОГО</strong></td>
							<td><strong><?=$summ['summ_in']?></strong></td>
							<td><strong><?=$summ['summ_out']?></strong></td>
							<td><strong><?=$summ['price_delivery']?></strong></td>
							<td><strong><?=$summ['total']?></strong></td>
							<td><strong><?=$summ['tax']?></strong></td>
							<td><strong><?=$summ['courier_price']?></strong></td>
							<td><strong><?=$summ['margin_netto']?></strong></td>
							<td><strong><?=$summ['margin_brutto']?></strong></td>
							<td><strong><?=round($summ['margin_netto']/$summ['total']*100,2)?></strong></td>
							<td><strong><?=$summ['orders']?></strong></td>
							<td><strong><?=$summ['amount_arabin_asq']?></strong></td>
							<td><strong><?=$summ['amount_arabin_other']?></strong></td>
							<td><strong><?=$summ['amount_yunona_obstetric']?></strong></td>
							<td><strong><?=$summ['amount_yunona_gynecological']?></strong></td>
							<td><strong><?=$summ['amount_arabin_all']?></strong></td>
							<td><strong><?=$summ['amount_yunona_all']?></strong></td>
							<td><strong><?=$summ['amount_portex_all']?></strong></td>
							<td><strong><?=$summ['amount_alfa_all']?></strong></td>
							<td><strong><?=$summ['amount_all']?></strong></td>
						</tr>
			</tbody>
		</table>
		<div class="pagination">
			<div class="links">
<?php
		for ($i=1;$i<=$nu_pages;$i++) {
			echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.$_GET['s'].'">'.$i.'</a> ';
		}
?>
			</div>
			<div class="results">Показано с <?=(($_GET['p']-1)*$_SESSION['report_summ_daily_per_page']+1);?> по <?=min($_GET['p']*$_SESSION['report_summ_daily_per_page'],$nu_day);?> из <?=$nu_day;?> (всего страниц: <?=$nu_pages;?>)</div>
		</div>
					<script>
				new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_date_from');
				new Calendar({format: "%Y-%m-%d"}).assignTo('report_orders_date_to');
			</script>
	</body>
</html>
<?php
	} else {
		header('Location: /');
	}
?>