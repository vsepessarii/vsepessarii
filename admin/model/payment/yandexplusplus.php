<?php
class ModelPaymentyandexplusplus extends Model {
	private $key;
	private $iv;
	
	public function encrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value, MCRYPT_MODE_ECB, $iv)), '+/=', '-_,');
	}
	
	public function decrypt($value, $key) {
		$key = hash('sha256', $key, true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_ECB, $iv));
	}
}
?>