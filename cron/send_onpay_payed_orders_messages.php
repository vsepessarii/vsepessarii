<?php
	//отсылаем письма админу у заказах, оплаченных через Onpay
	$qu_order = 'SELECT * FROM `order` WHERE payment_code="onpay" && alert_sent="" && "order_status_id"="1" ORDER BY order_id DESC LIMIT 3';
	$re_order = mysqli_query($qu_order);
	//echo mysqli_error();
	while ($ro_order = mysqli_fetch_array($re_order)) {
		$br = "\n";
		$text = 'Вы получили новый заказ'.$br.$br;
		$text = 'Номер: '.$ro_order['order_id'].$br.
						'Дата добавления: '.date('d.m.Y H:i:s', strtotime($ro_order['date_added'])).$br.
						'Имя: '.$ro_order['firstname'].$br.
						'Фамилия: '.$ro_order['lastname'].$br.
						'E-mail: '.$ro_order['email'].$br.
						'Телефон: '.$ro_order['telephone'].$br.
						'Страна: '.$ro_order['shipping_country'].$br.
						'Город: '.$ro_order['shipping_city'].$br.
						'Адрес: '.$ro_order['shipping_address_1'].$br.
						'Способ доставки: '.$ro_order['shipping_method'].$br.
						'Способ оплаты: '.$ro_order['payment_method'].$br.
						'Комментарий заказчика: '.$ro_order['comment'].$br.$br;
		
		$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
		$re_product = mysqli_query($qu_product);
		$total = 0;
		while ($ro_product = mysqli_fetch_array($re_product)) {
			$ro_manufacturer = mysqli_fetch_array(mysqli_query('SELECT manufacturer.name FROM manufacturer, product WHERE manufacturer.manufacturer_id=product.manufacturer_id && product.product_id="'.$ro_product['product_id'].'"'));
			$ro_keyword = mysqli_fetch_array(mysqli_query('SELECT `keyword` FROM url_alias WHERE `query`="product_id='.$ro_product['product_id'].'"'));
			$text .= $ro_product['quantity'] . 'x ' . $ro_product['name'] . ' ' . $ro_manufacturer['name'] . ' ' . $ro_product['model'] . ' (https://vsepessarii.ru/'.$ro_keyword['keyword'].'.html) ' . round($ro_product['total'],2) . ' р.' . $br;
			$total += $ro_product['total'];
			$qu_option = 'SELECT * FROM order_option WHERE order_id="'.$ro_order['order_id'].'" && order_product_id="'.$ro_product['order_product_id'].'"';
			$re_option = mysqli_query($qu_option);
			while ($ro_option = mysqli_fetch_array($re_option)) $text .= chr(9).'-'.$ro_option['name'].': '.$ro_option['value'].$br;
		}
		$qu_total = 'SELECT * FROM order_total WHERE order_id="'.$ro_order['order_id'].'" ORDER BY order_total_id ASC';
		$re_total = mysqli_query($qu_total);
		while ($ro_total = mysqli_fetch_array($re_total)) $text .= $ro_total['title'].': '.$ro_total['text'].$br;


		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/plain'; 
		$mail->IsHTML(false);
		//$mail->IsSMTP();
		//$mail->SMTPDebug  = 2;
		//$mail->Debugoutput = 'html';
		//$mail->Host       = $ro_mail['SMTP_SERVER']; echo $ro_mail['SMTP_SERVER'].'<br>'; 
		//$mail->Port       = $ro_mail['SMTP_PORT']; echo $ro_mail['SMTP_PORT'].'<br>'; 
		//$mail->SMTPAuth   = true;
		//$mail->Username   = $ro_mail['SMTP_LOGIN']; echo $ro_mail['SMTP_LOGIN'].'<br>'; 
		//$mail->Password   = $ro_mail['SMTP_PASS']; echo $ro_mail['SMTP_PASS'].'<br>';
		//var_dump($mail);

		//Set who the message is to be sent from
		$mail->SetFrom('brief.med@gmail.com', 'Brief Medicine');
		//$mail->AddReplyTo($sender_email,$sender_name);
		$mail->AddAddress('brief-med@yandex.ru', 'Brief Medicine');
		$mail->Subject = 'Brief Medicine - заказ '.$ro_order['order_id'];
		//$mail->MsgHTML($ro_mail['MAIL_BODY'], dirname(__FILE__));
		$mail->Body = $text;
		//$mail->AddAttachment('images/phpmailer-mini.gif');
		if($mail->Send())  {
			mysqli_query('UPDATE `order` SET alert_sent="'.date('Y-m-d H:i:s').'" WHERE order_id="'.$ro_order['order_id'].'"');
			//echo "Message could not be sent. <p>";
      //echo "Mailer Error: " . $mail->ErrorInfo;
		}
	}
?>