<?php
// Heading
$_['heading_title']    = 'Attribute Category';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Attribute Category!';
$_['text_no_results']     = 'Attribites not found';
$_['text_no_settings']    = 'No settings for this module';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_layout']     = 'Layout:';
$_['entry_position']   = 'Position:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module Attribute Category!';

// Column
$_['column_attribute']= 'Attribute name';
$_['column_attribute_group']= 'Attribute group';

// Tab
$_['tab_attributes'] = 'Attributes';

// Button
$_['button_cancel'] = 'Cancel';
