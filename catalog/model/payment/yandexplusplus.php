<?php
class ModelPaymentyandexplusplus extends Model { 
public function getMethod($address, $total) {
		$this->load->language('payment/yandexplusplus');
		
		if ($total <= 0) {
			$status = true;
		} else {
			$status = false;
		}
		


    if ($this->config->get('yandexplusplus_status')) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('yandexplusplus_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

			if (!$this->config->get('yandexplusplus_geo_zone_id')) {
				$status = TRUE;
			} elseif ($query->num_rows) {
				$status = TRUE;
			} else {
				$status = FALSE;
			}
		} else {
			$status = FALSE;
		}
		if ($this->config->get('yandexplusplus_maxpay')){
			if ($this->currency->format($total, 'RUB', $this->currency->getValue('RUB'), false) <= $this->config->get('yandexplusplus_maxpay')) {
				$status = true;
			} else {
				$status = false;
			}
		}
    
		$method_data = array();
		if ($status) {  
			if ($this->config->get('yandexplusplus_name_attach')) {
				$metname = htmlspecialchars_decode($this->config->get('yandexplusplus_name'));
			}
			else{
				$metname = $this->language->get('text_title');
			}
			$method_data = array( 
				'code'       => 'yandexplusplus',
				'title'      => $metname,
				'sort_order' => $this->config->get('yandexplusplus_sort_order')
			);
		}
		
    	return $method_data;
  	}
}
?>