<?php
	//отсылаем письма админу о заказах, которые уже 20 минут висят в потерянных
	
	$qu_order = 'SELECT order_id FROM `order` WHERE lost_order_notify="0" && order_status_id="0" && date_added < ADDDATE(NOW(), INTERVAL -30 MINUTE) ORDER BY order_id DESC';
	$re_order = mysqli_query($qu_order);
	unset($id);
	if (mysqli_num_rows($re_order) > 0) {
		while ($ro_order = mysqli_fetch_array($re_order)) $id[] = $ro_order['order_id'];
		$br = "\n";
		$text = 'Номера потерянных заказов: '.implode(',',$id);

		$config_email  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_email"'));
		$config_name  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_name"'));
		$config_mail_protocol  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_mail_protocol"'));
		$config_smtp_host  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_host"'));
		$config_smtp_username  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_username"'));
		$config_smtp_password  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_password"'));
		$config_smtp_port  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_port"'));
		$config_smtp_timeout  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_timeout"'));
			
		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/html';
		if ($config_mail_protocol == 'mail') {
			$mail->IsSendmail();
		} else {
			$mail->IsSMTP();
			$mail->SMTPAuth = true; 
			$mail->Host = $config_smtp_host['value'];
			$mail->Port = $config_smtp_port['value'];
			$mail->Username = $config_smtp_username['value'];
			$mail->Password = $config_smtp_password['value'];  	
			
		} 
		$mail->IsHTML(true);
		$mail->SetFrom($config_email['value'], $config_name['value']);
		$mail->AddReplyTo($config_email['value'], $config_name['value']);
		$mail->AddAddress($config_email['value'], $config_name['value']);
		$mail->Subject = 'Все Пессарии - потерянные заказы';
		$mail->AltBody = $text;
		$mail->MsgHTML($text);
		if($mail->Send()) mysqli_query('UPDATE `order` SET lost_order_notify="1" WHERE order_id IN('.implode(',',$id).')');
		echo $mail->ErrorInfo;
	}
?>