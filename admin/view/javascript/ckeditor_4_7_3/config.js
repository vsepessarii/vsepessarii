/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'ru';
	config.uiColor = '#c9c9c1';
	config.toolbar = 'Basic';
    config.skin = 'moono-dark';

    config.extraPlugins = 'info';
    config.allowedContent = true;
    // config.allowedContent = {
    //     $1: {
    //         // Use the ability to specify elements as an object.
    //         elements: CKEDITOR.dtd,
    //         attributes: true,
    //         styles: true,
    //         classes: true
    //     }
    // };
    // config.disallowedContent = 'script; *[on*]';
    config.basicEntities = false;
    config.fillEmptyBlocks = false;
    config.autoParagraph = false;
};
