<?php
	require '../connect.php';
	require '../functions.php';
	error_reporting(0);
	mb_internal_encoding('utf-8');
	ini_set('display_errors', FALSE);
	ini_set('display_startup_errors', FALSE);
	
	if ($ro_user['user_group_id'] != 1) die('Access denied');

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	date_default_timezone_set('Europe/London');

	/** PHPExcel_IOFactory */
	require_once '../class/PHPExcel/IOFactory.php';

	//echo date('H:i:s') , " Load from Excel5 template" , EOL;

	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("akt.xls");

//	die('123');




	//$objPHPExcel->setActiveSheetIndex(0);

	//$objPHPExcel->getActiveSheet()->setCellValue('BC40', '77.20');
	
	$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');

	$ro_shipping = Result($ddb, 'SELECT * FROM order_total WHERE order_id='.$ro_order['order_id'].' && code="shipping"');
	
	$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
	$nu_product = @mysqli_num_rows(@mysqli_query($ddb, $qu_product));
	$re_product = @mysqli_query($ddb, $qu_product);

	
	$order_date = substr($ro_order['date_added'],0,10);
	$order_date = Date2Str($order_date);

	$client =	'Наименование: '.htmlspecialchars_decode($ro_order['doc_firm']).
				', Адрес: '.htmlspecialchars_decode($ro_order['doc_address']).
				', ИНН '.$ro_order['doc_inn'].
				', Банковские реквизиты:'.
				', р/сч '.$ro_order['doc_rs'].', '.htmlspecialchars_decode($ro_order['doc_bank']).',  БИК '.$ro_order['doc_bik'].
				', КПП '.$ro_order['doc_kpp'].', ОГРН '.$ro_order['doc_ogrn'];

	$dogovor = 'Индивидуальный предприниматель Васильев Максим Александрович, именуемое в дальнейшем «Поставщик», в лице И.П. Васильева М.А., действующего на основании ОГРН 312470527000018, и '.$ro_order['doc_firm'].' именуемое в дальнейшем «Заказчик», в лице '.$ro_order['doc_face'].', действующего на основании  устава, заключили настоящий договор о нижеследующем:';

	$objPHPExcel->getActiveSheet()->setCellValue('F7', '№ '.$ro_order['order_id'].' от '.$order_date);
	$objPHPExcel->getActiveSheet()->setCellValue('F5', $client);
	$objPHPExcel->getActiveSheet()->setCellValue('X10', $ro_shipping['value']);
	$objPHPExcel->getActiveSheet()->setCellValue('AC10', $ro_shipping['value']);
	$objPHPExcel->getActiveSheet()->setCellValue('AC11', $ro_shipping['value']);
	$objPHPExcel->getActiveSheet()->setCellValue('AC13', $ro_shipping['value']);
	$objPHPExcel->getActiveSheet()->setCellValue('L15', Summ2Str($ro_shipping['value']));	
	$objPHPExcel->getActiveSheet()->setCellValue('R21', $ro_order['doc_firm']);

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="akt_'.$ro_order['order_id'].'.xls"');
	header('Cache-Control: max-age=0');
	//echo date('H:i:s') , " Write to Excel5 format" , EOL;
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('tmp.xls');
	echo file_get_contents('tmp.xls');
	unlink('tmp.xls');
	/*
	$objWriter->save('torg12_'.date('H-i-s').'.xls');
	echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
	echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
	echo date('H:i:s') , " Done writing file" , EOL;
	echo 'File has been created in ' , getcwd() , EOL;
	*/

