<?php
	//новый товар в новый заказ
	require '../connect.php';
	$tr_id = $_GET['tr_id'];
	$options = explode(',',$_GET['options']);

	$ro_product = Result($ddb, '
		SELECT	product.product_id as id,
						product_description.name as name,
						manufacturer.name as manufacturer,
						product.model as model,
						product.sku as sku,
						product.price_in as price_in,
						product.price as price
		FROM		product_description,
						product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
		WHERE		product.product_id = product_description.product_id &&
						product.product_id = "'.intval($_GET['product_id']).'"
	');
	
	if ($_GET['options'] != '') {
		$options_html = '<ul>';
		
		unset($options_txt);
		foreach($options as $option) {
			$option = explode(':',$option);
			$type = Result($ddb, 'SELECT option_description.name FROM option_description, product_option WHERE option_description.option_id = product_option.option_id && product_option.product_option_id="'.$option[0].'"');
			$value = Result($ddb, 'SELECT option_value_description.name FROM option_value_description, product_option_value WHERE option_value_description.option_value_id = product_option_value.option_value_id && product_option_value.product_option_value_id="'.$option[1].'"');
			$options_html .= '<li>'.$type['name'].': '.$value['name'].'</li>';
			$options_txt[] = $type['name'].':::'.$value['name'];
		}
		$options_html .= '</ul>';
	} else {
		$options_html = '';
	}

	########	Fix Special Price (2016-08-07)
	$ro_special = Result($ddb, 'SELECT * FROM product_special WHERE product_id='.$ro_product['id'].' AND (date_start >= "'.date('Y-m-d').'" OR date_start = "0000-00-00") AND (date_end <= "'.date('Y-m-d').'" OR date_end = "0000-00-00") ORDER BY priority ASC LIMIT 1');
	if ($ro_special['price'] > 0) $ro_product['price'] = $ro_special['price'];
	
	$tmp = '
		<tr id="tr_'.$tr_id.'">
			<td align="center">&nbsp;<span style="cursor: pointer; color:red;" onclick="$(\'#tr_'.$tr_id.'\').remove(); CalcMarginSumm();">X</span>&nbsp;</td>
			<td align="left" id="name_'.$tr_id.'">
				<a href="/index.php?route=catalog/product/update&token='.$_SESSION['token'].'&product_id='.$ro_product['id'].'" target="_blank">'.$ro_product['name'].'</a> (<a href="/index.php?route=product/product&product_id='.$ro_product['id'].'" target="_blank">фронт</a>) '.$options_html.'
				<input type="hidden" name="product_id_'.$tr_id.'" id="product_id_'.$tr_id.'" value="'.$ro_product['id'].'" />
				<input type="hidden" name="product_name_'.$tr_id.'" id="product_name_'.$tr_id.'" value="'.$ro_product['name'].'" />
				<input type="hidden" name="product_model_'.$tr_id.'" id="product_model_'.$tr_id.'" value="'.$ro_product['model'].'" />
				<input type="hidden" name="option_'.$tr_id.'" id="option_'.$tr_id.'" value="'.$_GET['options'].'" />
				<input type="hidden" name="option_txt_'.$tr_id.'" id="option_txt_'.$tr_id.'" value="'.@implode(',,,',$options_txt).'" />
			</td>
			<td align="center" id="manufacturer_'.$tr_id.'">'.$ro_product['manufacturer'].'</td>
			<td align="center" id="model_'.$tr_id.'">'.$ro_product['model'].'</td>
			<td align="center" id="sku_'.$tr_id.'">'.$ro_product['sku'].'</td>
			<td align="center"><input name="quantity_'.$tr_id.'" id="quantity_'.$tr_id.'" onkeyup="CalcMarginSumm();" value="1" class="tc w35" /></td>
			<td align="center"><input name="price_'.$tr_id.'" id="price_'.$tr_id.'" onkeyup="CalcMarginSumm();" value="'.round($ro_product['price'],2).'" maxlength="8" class="tc w60" /></td>
			<td align="center"><input id="price_in_'.$tr_id.'" name="price_in_'.$tr_id.'" readonly="readonly" value="'.$ro_product['price_in'].'"></td>
			<td align="center"><input id="margin_'.$tr_id.'" readonly="readonly" value=""></td>
			<td align="center"><input id="summ_'.$tr_id.'" name="summ_'.$tr_id.'" readonly="readonly" value=""></td>
		</tr>
	';
	echo $tmp;

