<?php
	//header('Content-Type: text/xml; charset=utf-8');
	//die();
	$ddb = mysqli_connect('localhost','ridator_pessarii','pessarii');
	mysqli_query("set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	mysqli_select_db('ridator_pessarii');
	
	$time = time();	
	$qu = '
		SELECT	macros_special.ID AS ID,
						macros_special_action.ACTION AS ACTION,
						macros_special_product.PID AS PID,
						macros_special_product.PRICE AS PRICE
		FROM		macros_special,
						macros_special_action,
						macros_special_product
		WHERE		macros_special.STATUS="1" &&
						macros_special_product.MID = macros_special.ID &&
						macros_special_action.MID = macros_special.ID &&
						(
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*00).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*00).'" && 
										macros_special_action.DAY="'.date('d',$time-60*00).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*00).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*00).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*00).'"
							)
							||
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*10).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*10).'" && 
										macros_special_action.DAY="'.date('d',$time-60*10).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*10).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*10).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*10).'"
							)
							||
							(
								(
									(
										macros_special_action.YEAR="'.date('Y',$time-60*20).'" && 
										macros_special_action.MONTH="'.date('m',$time-60*20).'" && 
										macros_special_action.DAY="'.date('d',$time-60*20).'"
									)
									|| macros_special_action.WEEKDAY="'.date('D',$time-60*20).'"
								) && 
								macros_special_action.HOUR="'.date('H',$time-60*20).'" && 
								macros_special_action.MINUTE="'.date('i',$time-60*20).'"
							)
						)
	';
	//echo $qu;
	$re = mysqli_query($qu);
	while ($ro = mysqli_fetch_array($re)) {
		if ($ro['ACTION'] == '-') {
			mysqli_query('DELETE FROM product_special WHERE product_id="'.$ro['PID'].'" && macros="'.$ro['ID'].'"');
		} elseif ($ro['ACTION'] == '+') {
			mysqli_query('DELETE FROM product_special WHERE product_id="'.$ro['PID'].'" && macros="'.$ro['ID'].'"');
			mysqli_query('
				INSERT INTO product_special
				SET	product_id="'.$ro['PID'].'",
						customer_group_id="1",
						priority="0",
						price="'.$ro['PRICE'].'",
						date_start="0000-00-00",
						date_end="0000-00-00",
						macros="'.$ro['ID'].'"
			');
		}
	}
	
	//заполняем чистый телефон, количество заказов от одного клиента и периоды между заказами клиента
	//чистим номера
	$qu = 'SELECT * FROM `order` ORDER BY order_id';
	$re = mysqli_query($qu);
	while ($ro = mysqli_fetch_array($re)) {
		$ph = $ro['telephone'];
		$ph = trim($ph);
		$ph = str_replace(array(' ','(',')','+','-'),'',$ph);
		$ph = substr($ph,-10);
		mysqli_query('UPDATE `order` SET phone_clear="'.$ph.'" WHERE order_id="'.$ro['order_id'].'"');
	}
	//заполняем количество заказов от клиента и период между заказами
	$qu = 'SELECT * FROM `order` ORDER BY order_id';
	$re = mysqli_query($qu);
	while ($ro = mysqli_fetch_array($re)) {
		$ph = $ro['phone_clear'];
		$nu = mysqli_num_rows(mysqli_query('SELECT order_id FROM `order` WHERE phone_clear="'.$ph.'"'));
		
		$per = 0;
		$tmp = mysqli_fetch_array(mysqli_query('SELECT date_added FROM `order` WHERE phone_clear="'.$ph.'" && date_added<"'.$ro['date_added'].'" ORDER BY date_added ASC LIMIT 1'));
		if ($tmp['date_added'] != '') {
			$per = ceil(strtotime($ro['date_added'])/86400)-ceil(strtotime($tmp['date_added'])/86400);
		}
		mysqli_query('UPDATE `order` SET orders_num="'.$nu.'", orders_period="'.$per.'" WHERE order_id="'.$ro['order_id'].'"');
	}
	
	


?>