<div id="login-form" style="display:none;">
<div class="simplecheckout-block-heading box-heading">Я уже зарегистрирован</div>
<div class="simplecheckout-block-content">
    <div class="simplecheckout-warning-block" <?php if (!$error_login) { ?>style="visibility:hidden;"<?php } ?>><?php echo $error_login ?></div>
    <table class="simplecheckout-customer-one-column">
        <tr>
            <td class="simplecheckout-login-left"><?php echo $entry_email; ?></td>
            <td class="simplecheckout-login-right"><input type="text" name="email" value="<?php echo $email; ?>" /></td>
        </tr>
        <tr>
            <td class="simplecheckout-login-left"><?php echo $entry_password; ?></td>
            <td class="simplecheckout-login-right"><input type="password" name="password" value="" /></td>
        </tr>
        <tr>
            <td style="padding-top: 8px;"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></td>
            <td style="padding-top: 8px;" class="simplecheckout-login-right"><a id="simplecheckout_button_login" onclick="simplecheckout_login()" class="button button-green"><span><?php echo $button_login; ?></span></a></td>
        </tr>
    </table>
</div>
<?php if ($redirect) { ?>
<script type='text/javascript'>
location = '<?php echo $redirect ?>';
</script>
<?php } ?>
</div>
<a href="#" onclick="$('#login-form-link').toggle(); $('#login-form').toggle(); $('#guest-form-link').toggle(); $('#guest-form').toggle(); return false;" id="login-form-link" style="display:block;">Я уже зарегистрирован</a>
<a href="#" onclick="$('#login-form-link').toggle(); $('#login-form').toggle(); $('#guest-form-link').toggle(); $('#guest-form').toggle(); return false;" id="guest-form-link" style="display:none;">Оформить заказ без регистрации</a>