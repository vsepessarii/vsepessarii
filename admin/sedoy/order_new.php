<?php
	//die();
	require 'connect.php';
	require 'class/class.phpmailer.php';

	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	if ($ro_user['user_group_id'] != 1) die();

	$tmp = Result($ddb, 'SELECT value FROM setting WHERE `KEY`="multiflat"');
	$tmp = unserialize($tmp['value']);
	
	//echo '<pre>'; var_dump($tmp); echo '</pre>';
	
	foreach ($tmp as $key => $value) {
		$shipping['multiflat.multiflat'.$key] = array ( 'title' => $value['name_short'], 'title_full' => htmlspecialchars_decode($value['name']), 'price' => $value['cost'], 'min_price' => 0); 
	}
		
	/*
	$shipping = array (
		'multiflat.multiflat0' => array ( 'title' => 'В пределах МКАД (до 1500 р.)', 'title_full' => htmlspecialchars_decode($tmp[0]['name']), 'price' => $tmp[0]['cost'], 'min_price' => 0),
		'multiflat.multiflat1' => array ( 'title' => 'До 10 км за МКАД (по согласованию)', 'title_full' => htmlspecialchars_decode($tmp[1]['name']), 'price' => $tmp[1]['cost'], 'min_price' => 0),
		'multiflat.multiflat2' => array ( 'title' => 'Срочная в день заказа (в пределах МКАД по согласованию)', 'title_full' => htmlspecialchars_decode($tmp[2]['name']), 'price' => $tmp[2]['cost'], 'min_price' => 0),
		'multiflat.multiflat5' => array ( 'title' => 'Выходные в МКАД + 5км с 8 до 12 (по согласованию)', 'title_full' => htmlspecialchars_decode($tmp[5]['name']), 'price' => $tmp[5]['cost'], 'min_price' => 0),
		'multiflat.multiflat4' => array ( 'title' => 'Транспортной компанией (стоимость уточняется)', 'title_full' => htmlspecialchars_decode($tmp[4]['name']), 'price' => $tmp[4]['cost'], 'min_price' => 0),
		'multiflat.multiflat3' => array ( 'title' => 'Самовывоз (по согласованию)', 'title_full' => htmlspecialchars_decode($tmp[3]['name']), 'price' => $tmp[3]['cost'], 'min_price' => 0),
		'multiflat.multiflat7' => array ( 'title' => 'Доставка в Казахстан', 'title_full' => htmlspecialchars_decode($tmp[3]['name']), 'price' => $tmp[3]['cost'], 'min_price' => 0),
		'multiflat.multiflat8' => array ( 'title' => 'Доставка в Беларусь', 'title_full' => htmlspecialchars_decode($tmp[3]['name']), 'price' => $tmp[3]['cost'], 'min_price' => 0)
	);
	*/
	
	//var_dump($shipping);

	/*
	$tmp = Result($ddb, 'SELECT value FROM setting WHERE `KEY`="multiflat"');
	$tmp = unserialize($tmp['value']);
	$shipping = array (
		'multiflat.multiflat0' => array ( 'title' => 'Новая почта до отделения', 'title_full' => htmlspecialchars_decode($tmp[0]['name']), 'price' => $tmp[0]['cost'], 'min_price' => 0),
		'multiflat.multiflat1' => array ( 'title' => 'Новая почта по адресу', 'title_full' => htmlspecialchars_decode($tmp[1]['name']), 'price' => $tmp[1]['cost'], 'min_price' => 0)
	);
	*/

	$payment = array (
		'cod' => 'Оплата наличными при получении товара',
		'yandexplusplus' => 'Яндекс.Деньги',
		'yandexplusplus_card' => 'Visa, Mastercard, Maestro',
		'cashless' => 'Оплата по безналичному расчету (для юридических лиц)',
		'sberbank_transfer' => 'Квитанция Сбербанка РФ (для платежей из Беларуси, Казахстана и России)'
	);

	//сохранение заказа
	if (isset($_POST['act']) && $_POST['act'] == 'save') {
		$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');
		AlertOrderStatusChange($ddb, $ro_order['order_id'], $_POST['status_id'], $_POST);
		
		/*
		//НОВАЯ ОБРАБОТКА ОТПРАВКИ ОПОВЕЩЕНИЙ (начало)

		//заполняем массив замен
		$courier = Result($ddb, 'SELECT * FROM courier WHERE courier_id="'.intval($_POST['courier_id']).'"');

		$replace['[ORDER_ID]'] = $ro_order['order_id'];
		$replace['[DECL]'] = $_POST['decl'];
		$replace['[ORDER_DATE]'] = date('d.m.Y', strtotime($ro_order['date_added']));
		$replace['[SHIP_DATE]'] = date('d.m.Y', strtotime($_POST['ship_date']));
		$replace['[ORDER_SUM]'] = $_POST['summ_after_ship'];
		$replace['[COURIER_NAME]'] = $courier['name'];
		$replace['[COURIER_PHONE]'] = $courier['phone'];
		
		if ($_POST['status_id'] != $ro_order['order_status_id']) {
			//статус изменился
			$alert = Result($ddb, 'SELECT * FROM order_status_alert WHERE id="'.$_POST['status_id'].'"');

			switch (intval($_POST['country_id'])) {
				case '176':
					$country_suffix = 'ru';
					break;
				case '20':
					$country_suffix = 'by';
					break;
				case '109':
					$country_suffix = 'kz';
					break;
			}
			
			//отправка SMS, если нужно
			if ($alert['sms_send']) {
				$phone = preg_replace('~\D+~','',$_POST['phone']);
				$text = $alert['sms_text_'.$country_suffix];
				$text = SedReplace($text,$replace);
				@mysqli_query($ddb, 'INSERT INTO sms SET PHONE="'.$phone.'", TEXT="'.mysqli_escape_string($text).'"');
			}
			
			//отправка email, если нужно
			if ($alert['email_send']) {
				$topic = $alert['email_topic_'.$country_suffix];
				$topic = SedReplace($topic,$replace);
				$text = $alert['email_text_'.$country_suffix];
				$text = SedReplace($text,$replace);
			
				$mail = new PHPMailer();
				$mail -> CharSet = "UTF-8";
				$mail->ContentType = 'text/html';
				$mail->XMailer = 'Microsoft Office Outlook, Build 12.0.4210';
				if (GetSetting('config_mail_protocol') == 'mail') {
					$mail->IsSendmail();
				} else {
					$mail->IsSMTP();
					$mail->SMTPAuth = true; 
					$mail->Host = GetSetting('config_smtp_host');
					$mail->Port = GetSetting('config_smtp_port');
					$mail->Username = GetSetting('config_smtp_username');
					$mail->Password = GetSetting('config_smtp_password');
				} 
				$mail->IsHTML(true);
				$mail->SetFrom(GetSetting('config_email'), GetSetting('config_name'));
				$mail->AddReplyTo(GetSetting('config_email'), GetSetting('config_name'));
				$mail->AddAddress($ro_order['email'], $ro_order['firstname']);
				$mail->Subject = $topic;
				$mail->AltBody = 'Для чтения сообщения используйте HTML-просмотр!';
				$mail->MsgHTML(html_entity_decode($text));
				$mail->Send();
				echo $mail->ErrorInfo;
			}
		}
		//НОВАЯ ОБРАБОТКА ОТПРАВКИ ОПОВЕЩЕНИЙ (конец)
		*/
			
		//история заказа order_history
		$ro_curr_status = Result($ddb, 'SELECT order_status.order_status_id, order_status.name FROM order_status, order_history WHERE order_status.order_status_id = order_history.order_status_id && order_history.order_id = "'.$ro_order['order_id'].'" ORDER BY order_history.date_added DESC LIMIT 1');
		if (($_POST['status_id'] > 0) && ($_POST['status_id'] != $ro_curr_status['order_status_id'] || $_POST['comment'] != '' || (isset($_POST['notify'] ) && $_POST['notify'] == 'on'))) {
			@mysqli_query($ddb, '
				INSERT INTO order_history
				SET	user_id='.intval($_SESSION['user_id']).',
				order_id="'.$ro_order['order_id'].'",
				order_status_id="'.intval($_POST['status_id']).'",
				notify="'.(($_POST['notify'] == 'on') ? '1' : '0').'",
				comment="'.mysqli_escape_string($ddb, $_POST['comment']).'",
				date_added=NOW()
			');
			@mysqli_query($ddb, '
				UPDATE `order`
				SET	order_status_id="'.intval($_POST['status_id']).'"
				WHERE order_id="'.$ro_order['order_id'].'"
			');
		}


		
		
		//суммы в заказе
		@mysqli_query($ddb, 'DELETE FROM order_total WHERE order_id="'.$ro_order['order_id'].'" && code="discount"');
		if ($_POST['discount_percent'] > 0) @mysqli_query($ddb, '
			INSERT INTO order_total
			SET	order_id="'.$ro_order['order_id'].'",
					code="discount",
					value="'.round($_POST['discount_summ'],2).'",
					text="'.round($_POST['discount_summ'],2).' р.",
					title="Скидка '.round($_POST['discount_percent'],2).'%",
					sort_order="3"
		');	
		
		@mysqli_query($ddb, 'DELETE FROM order_total WHERE order_id="'.$ro_order['order_id'].'" && code="shipping"');
		@mysqli_query($ddb, '
			INSERT INTO order_total
			SET	value="'.round($_POST['shipping_price'],2).'",
					text="'.round($_POST['shipping_price'],2).' р.",
					title="'.mysqli_escape_string($shipping[$_POST['shipping_code']]['title_full']).'",
					order_id="'.$ro_order['order_id'].'",
					code="shipping",
					sort_order="5"
		');	
		@mysqli_query($ddb, '
			UPDATE order_total
			SET	value="'.round($_POST['summ_before_disc'],2).'",
					text="'.round($_POST['summ_before_disc'],2).' р."
			WHERE	order_id="'.$ro_order['order_id'].'" &&
						code="sub_total"
		');
		
		@mysqli_query($ddb, '
			UPDATE order_total
			SET	value="'.round($_POST['summ_after_ship'],2).'",
					text="'.round($_POST['summ_after_ship'],2).' р."
			WHERE	order_id="'.$ro_order['order_id'].'" &&
						code="total"
		');
		
		//заказ

		$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM country WHERE country_id="'.intval($_POST['country_id']).'"'));

		mysqli_query($ddb, '
			UPDATE	`order`
			SET			firstname="'.htmlspecialchars($_POST['client']).'",
							lastname="",
							email="'.htmlspecialchars($_POST['email']).'",
							telephone="'.htmlspecialchars($_POST['phone']).'",
							payment_firstname="'.htmlspecialchars($_POST['client']).'",
							payment_lastname="",
							payment_address_1="'.htmlspecialchars($_POST['adres']).'",
							payment_country_id="'.intval($_POST['country_id']).'",
							payment_country="'.htmlspecialchars($tmp['name']).'",
							payment_city="'.htmlspecialchars($_POST['city']).'",
							payment_code="'.$_POST['payment_code'].'",
							shipping_firstname="'.htmlspecialchars($_POST['client']).'",
							shipping_lastname="",
							shipping_address_1="'.htmlspecialchars($_POST['adres']).'",
							shipping_country_id="'.intval($_POST['country_id']).'",
							shipping_country="'.htmlspecialchars($tmp['name']).'",
							shipping_city="'.htmlspecialchars($_POST['city']).'",
							shipping_method="'.htmlspecialchars($shipping[$_POST['shipping_code']]['title_full']).'",
							shipping_code="'.$_POST['shipping_code'].'",
							total="'.round($_POST['summ_after_ship'],2).'",
							date_modified=NOW(),
							date_shipped="'.(($_POST['ship_date'] > '0000-00-00') ? $_POST['ship_date'].' '.(isset($_POST['ship_hour']) ? $_POST['ship_hour'] : '').':'.(isset($_POST['ship_min']) ? $_POST['ship_min'] : '').':00' : '').'",
							time_shipped_from="'.$_POST['time_shipped_from'].'",
							time_shipped_to="'.$_POST['time_shipped_to'].'",
							courier_id="'.intval($_POST['courier_id']).'",
							courier_price="'.round($_POST['courier_price'],2).'",
							operator_id="'.intval($_POST['operator_id']).'",
							supplier="'.(isset($_POST['supplier']) ? $_POST['supplier'] : '').'",
							metro_id="'.htmlspecialchars($_POST['metro_id']).'",
							discount_percent="'.round($_POST['discount_percent'],6).'",
							discount_summ="'.round($_POST['discount_summ']).'",
							decl="'.htmlspecialchars($_POST['decl']).'",
							pack="'.htmlspecialchars($_POST['pack']).'",
							doc_firm="'.(isset($_POST['doc_firm']) ? htmlspecialchars($_POST['doc_firm']) : '').'",
							doc_inn="'.(isset($_POST['doc_inn']) ? htmlspecialchars($_POST['doc_inn']) : '').'",
							doc_kpp="'.(isset($_POST['doc_kpp']) ? htmlspecialchars($_POST['doc_kpp']) : '').'",
							doc_ogrn="'.(isset($_POST['doc_ogrn']) ? htmlspecialchars($_POST['doc_ogrn']) : '').'",
							doc_address="'.(isset($_POST['doc_address']) ? htmlspecialchars($_POST['doc_address']) : '').'",
							doc_face="'.(isset($_POST['doc_face']) ? htmlspecialchars($_POST['doc_face']) : '').'",
							doc_rs="'.(isset($_POST['doc_rs']) ? htmlspecialchars($_POST['doc_rs']) : '').'",
							doc_bank="'.(isset($_POST['doc_bank']) ? htmlspecialchars($_POST['doc_bank']) : '').'",
							doc_bik="'.(isset($_POST['doc_bik']) ? htmlspecialchars($_POST['doc_bik']) : '').'"
			WHERE order_id="'.$ro_order['order_id'].'"					
		');
		//echo htmlspecialchars($_POST['doc_firm']);
		echo mysqli_error($ddb);
		
		@mysqli_query($ddb, 'DELETE FROM order_product WHERE order_id="'.$ro_order['order_id'].'"');
		@mysqli_query($ddb, 'DELETE FROM order_option WHERE order_id="'.$ro_order['order_id'].'"');
		for ($i=1;$i<=intval($_POST['autoinc']); $i++) {
			if (intval($_POST['product_id_'.$i]) > 0) {
				@mysqli_query($ddb, '
					INSERT INTO order_product
					SET	order_id="'.$ro_order['order_id'].'",
							product_id="'.intval($_POST['product_id_'.$i]).'",
							name="'.mysqli_escape_string($_POST['product_name_'.$i]).'",
							model="'.mysqli_escape_string($_POST['product_model_'.$i]).'",
							quantity="'.floatval($_POST['quantity_'.$i]).'",
							price="'.floatval($_POST['price_'.$i]).'",
							price_in="'.floatval($_POST['price_in_'.$i]).'",
							total="'.floatval($_POST['summ_'.$i]).'",
							tax="0",
							reward="0"
				');
				$order_product_id = @mysqli_insert_id($ddb);
				if ($_POST['option_'.$i] != '') {
					//указаны опции - записываем
					$options = explode(',',$_POST['option_'.$i]);
					$options_txt = explode(',,,',$_POST['option_txt_'.$i]);
					for($j=0;$j<count($options);$j++) {
						$option = explode(':',$options[$j]);
						$option_txt = explode(':::',$options_txt[$j]);
						@mysqli_query($ddb, '
							INSERT INTO order_option
							SET	order_id="'.$ro_order['order_id'].'",
									order_product_id="'.$order_product_id.'",
									product_option_id="'.$option[0].'",
									product_option_value_id="'.$option[1].'",
									name="'.mysqli_escape_string($option_txt[0]).'",
									value="'.mysqli_escape_string($option_txt[1]).'",
									type="select"
						');
					}
				}
			} 
		}
		$msg = date('d.m.Y H:i:s').': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;заказ '.$ro_order['order_id'].' сохранен.';
	}
	if (isset($_GET['act']) && $_GET['act'] == 'mail') {
		//отправка клиенту уведомления об измененном заказе
		$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE `order_id`="'.intval($_GET['id']).'"');
	
		$title = 'ВСЕ ПЕССАРИИ.РУ - обновленная информация по заказу '.$ro_order['order_id'];
		$store_url = 'https://vsepessarii.ru';
		$store_name = GetSetting('config_name');
		$logo = 'https://vsepessarii.ru/image/data/logo_vse_pessarii.png';
		$text_greeting = 'Благодарим Вас за интерес к товарам нашего магазина. Ваш заказ получен. Для подтверждения и уточнения даты/времени доставки с Вами свяжутся наши операторы.';
		
		$customer_id = '';
		$text_link = '';
		$link = '';
		
		$download = '';
		$text_download = '';
		$download = '';
		
		$text_order_detail = 'Детализация заказа';
		$text_order_id = '№ заказа:';
		$order_id = $ro_order['order_id'];
		$text_date_added = 'Дата заказа:';
		$date_added = date('d.m.Y', strtotime($ro_order['date_added']));
		$text_payment_method = 'Способ оплаты:';
		$payment_method = $ro_order['payment_method'];
		$text_shipping_method = 'Способ доставки:';
		$shipping_method = $ro_order['shipping_method'];
		$text_email = 'E-mail:';
		$email = $ro_order['email'];
		$text_telephone = 'Телефон:';
		$telephone = $ro_order['telephone'];
		$text_ip = 'IP адрес:';
		$ip = $ro_order['ip'];
		$text_instruction = 'Инструкции';
		$comment = $ro_order['comment'];
		
		$text_payment_address = 'Адрес плательщика';
		unset($tmp);
		if ($ro_order['payment_firstname'].$ro_order['payment_lastname']) $tmp[] = $ro_order['payment_firstname'].' '.$ro_order['payment_lastname'];
		if ($ro_order['payment_company']) $tmp[] = $ro_order['payment_company'];
		if ($ro_order['payment_address_1']) $tmp[] = $ro_order['payment_address_1'];
		if ($ro_order['payment_address_2']) $tmp[] = $ro_order['payment_address_2'];
		if ($ro_order['payment_city'].$ro_order['payment_postcode']) $tmp[] = $ro_order['payment_city'].' '.$ro_order['payment_postcode'];
		if ($ro_order['payment_zone']) $tmp[] = $ro_order['payment_zone'];
		if ($ro_order['payment_zone_code']) $tmp[] = $ro_order['payment_zone_code'];
		if ($ro_order['payment_country']) $tmp[] = $ro_order['payment_country'];
		$payment_address = implode(',<br />',$tmp);
		$text_shipping_address = 'Адрес доставки';
		unset($tmp);
		if ($ro_order['shipping_firstname'].$ro_order['shipping_lastname']) $tmp[] = $ro_order['shipping_firstname'].' '.$ro_order['shipping_lastname'];
		if ($ro_order['shipping_company']) $tmp[] = $ro_order['shipping_company'];
		if ($ro_order['shipping_address_1']) $tmp[] = $ro_order['shipping_address_1'];
		if ($ro_order['shipping_address_2']) $tmp[] = $ro_order['shipping_address_2'];
		if ($ro_order['shipping_city'].$ro_order['shipping_postcode']) $tmp[] = $ro_order['shipping_city'].' '.$ro_order['shipping_postcode'];
		if ($ro_order['shipping_zone']) $tmp[] = $ro_order['shipping_zone'];
		if ($ro_order['shipping_zone_code']) $tmp[] = $ro_order['shipping_zone_code'];
		if ($ro_order['shipping_country']) $tmp[] = $ro_order['shipping_country'];
		$shipping_address = implode(',<br />',$tmp);
		
		$text_product = 'Товар:';
		$text_model = 'Модель';
		$text_quantity = 'Количество';
		$text_price = 'Цена';
		$text_total = 'Итого:';
		
		unset($products);
		$qu_product = '
			SELECT		order_product.order_product_id as order_product_id,
								order_product.name as name,
								order_product.model as model,
								order_product.quantity as quantity,
								order_product.price as price,
								order_product.total as total,
								manufacturer.name as manufacturer
			FROM			order_product,
								product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
			WHERE			product.product_id = order_product.product_id &&
								order_product.order_id = "'.$ro_order['order_id'].'"
		';
		$re_product = @mysqli_query($ddb, $qu_product);
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			unset($option);
			$qu_option = 'SELECT * FROM order_option WHERE order_product_id="'.$ro_product['order_product_id'].'"';
			$re_option = @mysqli_query($ddb, $qu_option);
			while ($ro_option = @mysqli_fetch_array($re_option)) {
				$option[] = array(
					'name' => $ro_option['name'],
					'value' => $ro_option['value']
				);
			}
			$products[] = array(
				'name' => $ro_product['name'],
				'manufacturer' => $ro_product['manufacturer'],
				'model' => $ro_product['model'],
				'quantity' => $ro_product['quantity'],
				'price' => $ro_product['price'],
				'total' => $ro_product['total'],
				'option' => $option
			);
		}
		
		$vouchers = array();
		
		unset($totals);
		$qu_total = 'SELECT * FROM order_total WHERE order_id="'.$ro_order['order_id'].'" ORDER BY sort_order';
		$re_total = @mysqli_query($ddb, $qu_total);
		while ($ro_total = @mysqli_fetch_array($re_total)) $totals[] = array(
			'title' => $ro_total['title'],
			'text' => $ro_total['text']
		);
		
		$text_footer = 'С уважением, Администрация интернет-магазина '.GetSetting('config_name').' <a href="https://vsepessarii.ru">https://vsepessarii.ru</a>.';
		ob_start();
		require '../../catalog/view/theme/default/template/mail/order.tpl';
		$mail_text = ob_get_clean();
		
		//переменные, которые касаются отправки email, и которые можно заполнить 1 раз
		/*
		$config_email  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_email"');
		$config_name  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_name"');
		$config_mail_protocol  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_mail_protocol"');
		$config_smtp_host  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_smtp_host"');
		$config_smtp_username  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_smtp_username"');
		$config_smtp_password  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_smtp_password"');
		$config_smtp_port  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_smtp_port"');
		$config_smtp_timeout  = Result($ddb, 'SELECT `value` FROM `setting` WHERE `key`="config_smtp_timeout"');
		*/
		
		$topic = $title;
		$text = $mail_text;
	
		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/html';
		if (GetSetting('config_mail_protocol') == 'mail') {
			$mail->IsSendmail();
		} else {
			$mail->IsSMTP();
			$mail->SMTPAuth = true; 
			$mail->Host = GetSetting('config_smtp_host');
			$mail->Port = GetSetting('config_smtp_port');
			$mail->Username = GetSetting('config_smtp_username');
			$mail->Password = GetSetting('config_smtp_password');
		} 
		$mail->IsHTML(true);
		$mail->SetFrom(GetSetting('config_email'), GetSetting('config_name'));
		$mail->AddReplyTo(GetSetting('config_email'), GetSetting('config_name'));
		$mail->AddAddress($ro_order['email'], $ro_order['firstname']);
		$mail->Subject = $topic;
		$mail->AltBody = 'Для чтения сообщения используйте HTML-просмотр!';
		$mail->MsgHTML(html_entity_decode($text));
		$mail->Send();
		echo $mail->ErrorInfo;	
		
		$msg = date('d.m.Y H:i:s').': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;уведомление клиенту об изменении заказа '.$ro_order['order_id'].' отправлено.';	
		 
	}
	
	if (isset($_GET['act']) && $_GET['act'] == 'copy') {
		//копирование заказа
		$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE `order_id`="'.intval($_GET['id']).'"');
		@mysqli_query($ddb, '
			INSERT INTO `order`
			SET invoice_no="'.$ro_order['invoice_no'].'",
					invoice_prefix="'.$ro_order['invoice_prefix'].'",
					store_id="'.$ro_order['store_id'].'",
					store_name="'.$ro_order['store_name'].'",
					store_url="'.$ro_order['store_url'].'",
					customer_id="'.$ro_order['customer_id'].'",
					customer_group_id="'.$ro_order['customer_group_id'].'",
					firstname="'.$ro_order['firstname'].'",
					lastname="'.$ro_order['lastname'].'",
					email="'.$ro_order['email'].'",
					telephone="'.$ro_order['telephone'].'",
					fax="'.$ro_order['fax'].'",
					payment_firstname="'.$ro_order['payment_firstname'].'",
					payment_lastname="'.$ro_order['payment_lastname'].'",
					payment_company="'.$ro_order['payment_company'].'",
					payment_company_id="'.$ro_order['payment_company_id'].'",
					payment_tax_id="'.$ro_order['payment_tax_id'].'",
					payment_address_1="'.$ro_order['payment_address_1'].'",
					payment_address_2="'.$ro_order['payment_address_2'].'",
					payment_city="'.$ro_order['payment_city'].'",
					payment_postcode="'.$ro_order['payment_postcode'].'",
					payment_country="'.$ro_order['payment_country'].'",
					payment_country_id="'.$ro_order['payment_country_id'].'",
					payment_zone="'.$ro_order['payment_zone'].'",
					payment_zone_id="'.$ro_order['payment_zone_id'].'",
					payment_address_format="'.$ro_order['payment_address_format'].'",
					payment_method="'.$ro_order['payment_method'].'",
					payment_code="'.$ro_order['payment_code'].'",
					shipping_firstname="'.$ro_order['shipping_firstname'].'",
					shipping_lastname="'.$ro_order['shipping_lastname'].'",
					shipping_company="'.$ro_order['shipping_company'].'",
					shipping_address_1="'.$ro_order['shipping_address_1'].'",
					shipping_address_2="'.$ro_order['shipping_address_2'].'",
					shipping_city="'.$ro_order['shipping_city'].'",
					shipping_postcode="'.$ro_order['shipping_postcode'].'",
					shipping_country="'.$ro_order['shipping_country'].'",
					shipping_country_id="'.$ro_order['shipping_country_id'].'",
					shipping_zone="'.$ro_order['shipping_zone'].'",
					shipping_zone_id="'.$ro_order['shipping_zone_id'].'",
					shipping_address_format="'.$ro_order['shipping_address_format'].'",
					shipping_method="'.$ro_order['shipping_method'].'",
					shipping_code="'.$ro_order['shipping_code'].'",
					comment="'.$ro_order['comment'].'",
					total="'.$ro_order['total'].'",
					order_status_id="1",
					affiliate_id="'.$ro_order['affiliate_id'].'",
					commission="'.$ro_order['commission'].'",
					language_id="'.$ro_order['language_id'].'",
					currency_id="'.$ro_order['currency_id'].'",
					currency_code="'.$ro_order['currency_code'].'",
					currency_value="'.$ro_order['currency_value'].'",
					ip="'.$ro_order['ip'].'",
					forwarded_ip="'.$ro_order['forwarded_ip'].'",
					user_agent="'.$ro_order['user_agent'].'",
					accept_language="'.$ro_order['accept_language'].'",
					date_added=NOW(),
					date_modified="0000-00-00 00:00:00",
					date_shipped="0000-00-00 00:00:00",
					alert_sent="'.$ro_order['alert_sent'].'",
					courier_id="'.$ro_order['courier_id'].'",
					courier_price="'.$ro_order['courier_price'].'",
					supplier="'.$ro_order['supplier'].'",
					metro_id="'.$ro_order['metro_id'].'",
					discount_percent="'.$ro_order['discount_percent'].'",
					discount_summ="'.$ro_order['discount_summ'].'"
		');
		$new_order_id = @mysqli_insert_id($ddb); //ИД нового заказа
		
		//копируем товары
		$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
		$re_product = @mysqli_query($ddb, $qu_product);
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			@mysqli_query($ddb, '
				INSERT INTO order_product
				SET	order_id="'.$new_order_id.'",
						product_id="'.$ro_product['product_id'].'",
						name="'.$ro_product['name'].'",
						model="'.$ro_product['model'].'",
						quantity="'.$ro_product['quantity'].'",
						price="'.$ro_product['price'].'",
						total="'.$ro_product['total'].'",
						tax="'.$ro_product['tax'].'",
						reward="'.$ro_product['reward'].'"
			');
			$new_product_id = @mysqli_insert_id($ddb); //ИД нового товара в новом заказе
			$qu_option = 'SELECT * FROM product_option WHERE order_product_id="'.$ro_product['order_product_id'].'"';
			$re_option = @mysqli_query($ddb, $qu_option);
			while ($ro_option = @mysqli_fetch_array($re_option)) {
				@mysqli_query($ddb, '
					INSERT INTO product_option
					SET order_id="'.$ro_option['order_id'].'",
							order_product_id="'.$new_product_id.'",
							product_option_id="'.$ro_option['product_option_id'].'",
							product_option_value_id="'.$ro_option['product_option_value_id'].'",
							name="'.$ro_option['name'].'",
							value="'.$ro_option['value'].'",
							type="'.$ro_option['type'].'"
				');
			}
		}
		
		//копируем суммы
		$qu_total = 'SELECT * FROM order_total WHERE order_id="'.$ro_order['order_id'].'"';
		$re_total = @mysqli_query($ddb, $qu_total);
		while ($ro_total = @mysqli_fetch_array($re_total)) {
			@mysqli_query($ddb, '
				INSERT INTO order_total
				SET	order_id="'.$ro_total['order_id'].'",
						code="'.$ro_total['code'].'",
						title="'.$ro_total['title'].'",
						text="'.$ro_total['text'].'",
						value="'.$ro_total['value'].'",
						sort_order="'.$ro_total['sort_order'].'"
			');
		}
		
		$msg = date('d.m.Y H:i:s').': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заказ скопирован. Номер копии '.$new_order_id.'. <a href="?id='.$new_order_id.'">Перейти к редактированию.</a>';
	}
	

	/*
	$ro_simple = Result($ddb, 'SELECT * FROM setting WHERE `key`="simple_fields_main"');
	$tmp = unserialize($ro_simple['value']);
	$tmp = explode(';'.chr(13),$tmp['main_city']['values']['ru']);
	foreach ($tmp as $tmp_1) {
		$tmp_2 = explode('=',$tmp_1);
		$cities[] = trim($tmp_2[1]);
	}
	*/

	$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');
	$ro_coupon = Result($ddb, 'SELECT * FROM `order_total` WHERE `order_id`="'.$ro_order['order_id'].'" && `code`="coupon"');
	$ro_discount = Result($ddb, 'SELECT * FROM `order_total` WHERE `order_id`="'.$ro_order['order_id'].'" && `code`="discount"');
	$ro_shipping = Result($ddb, 'SELECT * FROM `order_total` WHERE `order_id`="'.$ro_order['order_id'].'" && `code`="shipping"');
	$ro_subtotal = Result($ddb, 'SELECT * FROM `order_total` WHERE `order_id`="'.$ro_order['order_id'].'" && `code`="sub_total"');
	$ro_total = Result($ddb, 'SELECT * FROM `order_total` WHERE `order_id`="'.$ro_order['order_id'].'" && `code`="total"');
	//$ro_curr_status = Result($ddb, 'SELECT order_status.order_status_id, order_status.name FROM order_status, order_history WHERE order_status.order_status_id = order_history.order_status_id && order_history.order_id = "'.$ro_order['order_id'].'" ORDER BY order_history.date_added DESC LIMIT 1');
	$ro_curr_status = Result($ddb, 'SELECT `order`.order_status_id, order_status.name FROM `order`, order_status WHERE order_status.order_status_id = `order`.order_status_id && `order`.order_id = "'.$ro_order['order_id'].'"');
	
	//отправляем уведомление, если надо
	/*
	if ($_POST['notify'] == 'on') {
		$html_body = '
			<div style="width: 680px;">
				<a moz-do-not-send="true" href="http://ridator.ru/" title="Интернет-магазин Бриф-Мед.РУ">
					<img moz-do-not-send="true" src="http://ridator.ru/image/data/logo.png" alt="Интернет-магазин Бриф-Мед.РУ" style="margin-bottom: 20px; border: none;">
				</a>
				<p style="margin-top: 0px; margin-bottom: 20px;">Благодарим Вас за интерес к товарам нашего магазина. Ваш заказ получен. Для подтверждения и уточнения даты/времени доставки с Вами свяжутся наши операторы.</p>
				<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
					<thead>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;" colspan="2">Детализация заказа</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
								<b>№ заказа:</b> '.$ro_order['order_id'].'<br> 
								<b>Дата заказа:</b> '.date('d.m.Y',strtotime($ro_order['date_added'])).'<br>
								<b>Способ оплаты:</b> '.$ro_order['payment_method'].'<br>
								<b>Способ доставки:</b> '.$ro_order['shipping_method'].'
							</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
								<b>E-mail:</b> <a class="moz-txt-link-abbreviated" href="mailto:'.$ro_order['email'].'">'.$ro_order['email'].'</a><br>
								<b>Телефон:</b> '.$ro_order['telephone'].'<br>
								<b>IP адрес:</b> '.$ro_order['ip'].'<br>
							</td>
						</tr>
					</tbody>
				</table>
			
				<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;"> 
					<thead>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Адрес плательщика</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Адрес доставки</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
								'.$ro_order['payment_firstname'].'<br>
								'.$ro_order['payment_address_1'].'<br>
								'.$ro_order['payment_city'].'
							</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
								'.$ro_order['shipping_firstname'].'<br>
								'.$ro_order['shipping_address_1'].'<br>
								'.$ro_order['shipping_city'].'
							</td>
						</tr>
					</tbody>
				</table>
			
				<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
					<thead>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Товар:</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Производитель</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Модель</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Количество</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Цена</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Итого:</td>
						</tr>
					</thead>
					<tbody>
		';
		$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'" ORDER BY order_product_id';
		$re_product = @mysqli_query($ddb, $qu_product);
		while ($ro_product = @mysqli_fetch_assoc($re_product)) {
			$ro_manufacturer = Result($ddb, 'SELECT manufacturer.name FROM manufacturer, product WHERE product.manufacturer_id = manufacturer.manufacturer_id && product.product_id="'.$ro_product['product_id'].'"');
			unset($option);
			$qu_option = 'SELECT * FROM order_option WHERE order_product_id="'.$ro_product['order_product_id'].'"';
			//echo $qu_option.'<br>';
			$re_option = @mysqli_query($ddb, $qu_option);
			while ($ro_option = @mysqli_fetch_array($re_option)) $option[] = '<small> - '.$ro_option['name'].': '.$ro_option['value'].'</small>';

			$html_body .= '
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">
								'.$ro_product['name'].'
								'.((isset($option)) ? '<br>'.@implode('<br>',$option) : '').'
							</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">'.$ro_manufacturer['name'].'</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">'.$ro_product['model'].'</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.round($ro_product['quantity'],0).'</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.round($ro_product['price'],2).'p.</td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.round($ro_product['total'],2).'p.</td>
						</tr>
			';
		}
		$html_body .= '
					</tbody>
					<tfoot>
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b>'.$ro_subtotal['title'].':</b></td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.$ro_subtotal['text'].'</td>
						</tr>
		'.(($ro_discount['text'] != '') ? '
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b>'.$ro_discount['title'].':</b></td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.$ro_discount['text'].'</td>
						</tr>
		' : '').'
		'.(($ro_shipping['text'] != '') ? '
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b>'.$ro_shipping['title'].':</b></td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.$ro_shipping['text'].'</td>
						</tr>
		' : '').'
		'.(($ro_coupon['text'] != '') ? '
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b>'.$ro_coupon['title'].':</b></td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.$ro_coupon['text'].'</td>
						</tr>
		' : '').'
		'.(($ro_total['text'] != '') ? '
						<tr>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="5"><b>'.$ro_total['title'].':</b></td>
							<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">'.$ro_total['text'].'</td>
						</tr>
		' : '').'
					</tfoot>
				</table>
			
				<p style="margin-top: 0px; margin-bottom: 20px;"> </p>
				<p style="margin-top: 0px; margin-bottom: 20px;">С уважением, Администрация интернет-магазина Бриф-Мед.РУ <a moz-do-not-send="true" href="http://ridator.ru">http://ridator.ru</a>.</p>
			</div>		
		';
		
		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/html';
		$mail->IsSendmail(); 
		$mail->IsHTML(true);
		
		//$mail->IsSMTP();
		//$mail->SMTPDebug  = 2;
		//$mail->Debugoutput = 'html';
		//$mail->Host       = $ro_mail['SMTP_SERVER']; echo $ro_mail['SMTP_SERVER'].'<br>'; 
		//$mail->Port       = $ro_mail['SMTP_PORT']; echo $ro_mail['SMTP_PORT'].'<br>'; 
		//$mail->SMTPAuth   = true;
		//$mail->Username   = $ro_mail['SMTP_LOGIN']; echo $ro_mail['SMTP_LOGIN'].'<br>'; 
		//$mail->Password   = $ro_mail['SMTP_PASS']; echo $ro_mail['SMTP_PASS'].'<br>';
		//var_dump($mail);

		//Set who the message is to be sent from
		$mail->SetFrom('ridator@yandex.ru', 'Ridator');
		$mail->AddReplyTo('ridator@yandex.ru', 'Ridator');
		$mail->AddAddress($ro_order['email'], $ro_order['firstname']);
		$mail->Subject = 'Ridator - заказ '.$ro_order['order_id'];
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($html_body);
		//$mail->Body = '2233';
		//$mail->AddAttachment('images/phpmailer-mini.gif');
		$mail->Send();
		echo $mail->ErrorInfo;
	}
	*/	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="MESMERiZE" />

	<title>Форма заказа</title>
	<link rel="icon" href="icon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="js/jquery.ui.datepicker-ru.js"></script>
	<script>
		function Float(i) {
			if (i != '') {
				return parseFloat(i);
			} else {
				return parseFloat(0);
			}
		}

 		$(function() {
			$( "#tabs" ).tabs();
	    $('#ship_date').datepicker();
	    CalcMarginSumm();
	    
			function split( val ) {
				return val.split( /,\s*/ );
			}
    
			function extractLast( term ) {
				return split( term ).pop();
			}
 
			$("#metro")
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
  
			.autocomplete({
				source: function( request, response ) {
					$.getJSON( "ajax/metro.php", {
						term: extractLast( request.term )
					}, response );
				},
				search: function() {
					// custom minLength
					var term = extractLast( this.value );
					if ( term.length < 2 ) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});

			$("#supplier")
			// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) {
					event.preventDefault();
				}
			})
  
			.autocomplete({
				source: function( request, response ) {
					$.getJSON( "ajax/supplier.php", {
						term: extractLast( request.term )
					}, response );
				},
				search: function() {
					// custom minLength
					var term = extractLast( this.value );
					if ( term.length < 2 ) {
						return false;
					}
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});


		});
		
		function ChangeCourier() {
			price = 0;
			<?php
				$qu_courier = 'SELECT * FROM courier ORDER BY courier_id';
				$re_courier = @mysqli_query($ddb, $qu_courier);
				while ($ro_courier = @mysqli_fetch_array($re_courier)) echo 'if($("#courier_id").val() == "'.$ro_courier['courier_id'].'") $("#courier_price").val("'.$ro_courier['price'].'"); ';
			?> 
		}
		
		function ChangeCountry(country_id) {
			$( "#cities" ).load( "ajax/cities.php", { country_id: country_id });
		}
		
		function ChangeShipping() {
			price = 0;
			<?php
				foreach ($shipping as $key => $value) echo 'if($("#shipping_code").val() == "'.$key.'") $("#shipping_price").val("'.$value['price'].'"); ';
			?> 
			CalcMarginSumm();
		}
		
		function Validate(form) {
			i = 0;
			$('*[rel=req]').each(function() {
				$(this).css('border', '1px solid gray');
				if ($(this).val() == '') {
					$(this).css('border', '1px solid red');
					window.i++;
				}
			});
			//alert(i);
			if (i == 0) {
				form.submit();
				return true;
			} else {
				alert('Ошибка!');
				return false;
			}
		}
		
		$(function() {
			function log( message ) {
				$( "<div>" ).text( message ).prependTo( "#log" );
				$( "#log" ).scrollTop( 0 );
			}
 
			$( "#product" ).autocomplete({
      	source: "ajax/order_new_product.php",
      	minLength: 2,
				select: function( event, ui ) {
					if (ui.item) {
						autoinc = $('#autoinc').val();
						autoinc++;
						$.get('ajax/order_new_add_product.php?product_id=' + ui.item.id + '&tr_id=' + autoinc + '&options=' + ui.item.option, function(data) {
							$('#new-product').before(data);
							$('#autoinc').val(autoinc);
							$("#product").val('');
							CalcMarginSumm();
							$('#discount_summ').val(Round2(Float(($('#summ_before_disc').val()*$('#discount_percent').val()/100))));
							CalcMarginSumm();
						});
					}
				}
	    });
	  });		
		
		function isFloat (n) {
  		return n===+n && n!==(n|0);
		}

		function isInteger (n) {
  		return n===+n && n===(n|0);
		}		
		
		function Round2(t) {
			//alert(t);
			if (!isFloat(t) && !isInteger(t)) return 0;
			t = t + 0;
			return t.toFixed(2);
		}
		
		function CalcMarginSumm() {
			margin_before_disc = 0;
			summ_before_disc = 0;
			for (i=1;i<=$('#autoinc').val();i++) {
				if ($('#tr_' + i).length) {
					//элемент найден, считаем маржу
					margin = ($('#price_'+i).val()*0.94-$('#price_in_'+i).val())*$('#quantity_'+i).val();
					margin_before_disc = margin_before_disc + margin;
					
					summ = $('#price_'+i).val()*$('#quantity_'+i).val();
					summ_before_disc = summ_before_disc + summ;
					
					$('#margin_'+i).val(Round2(margin));
					$('#summ_'+i).val(Round2(summ));
				}

				percent_before_disc = margin_before_disc/summ_before_disc*100;
				
			}

			//после скидки в процентах или сумме и купону
			summ_after_disc = summ_before_disc-$('#discount_summ').val()-$('#discount_coupon').val();
			margin_after_disc = margin_before_disc-$('#discount_summ').val()-$('#discount_coupon').val();
			percent_after_disc = margin_after_disc/summ_after_disc*100;

			//после доставки
			summ_after_ship = Float(summ_after_disc) + Float($('#shipping_price').val());
			margin_after_ship = Float(margin_after_disc) + Float($('#shipping_price').val());
			percent_after_ship  = margin_after_ship/summ_after_ship*100 + 0;

			$('#margin_before_disc').val(Round2(margin_before_disc));
			$('#percent_before_disc').val(Round2(percent_before_disc));
			$('#summ_before_disc').val(Round2(summ_before_disc));
				
			$('#summ_after_disc').val(Round2(summ_after_disc));
			$('#margin_after_disc').val(Round2(margin_after_disc));
			$('#percent_after_disc').val(Round2(percent_after_disc));

			$('#summ_after_ship').val(Round2(summ_after_ship));
			$('#margin_after_ship').val(Round2(margin_after_ship));
			$('#percent_after_ship').val(Round2(percent_after_ship));

		}
		
		//расчет процента скидки, если введена сумма
		function CalcDiscountPercent() {
			$('#discount_percent').val(Round2($('#discount_summ').val()/$('#summ_before_disc').val()*100));
			CalcMarginSumm();
		}
		
		//расчет суммы скидки,если введен процент
		function CalcDiscountSumm() {
			$('#discount_summ').val(Round2($('#summ_before_disc').val()*$('#discount_percent').val()/100));
			CalcMarginSumm();
		}
		
	</script>
</head>
<style>
	* {
		font-family: Calibri !important;
		font-size:14px !important;
	}

	ul {
		margin: 0;
	}
	
	table {
		border-collapse: collapse;
		border-color: #000;
	}
	
	.w90p { width: 90%; } .w100p { width: 100%;	}
	
	.w10 {width: 10px;} .w15 {width: 15px;} .w20 {width: 20px;} .w25 {width: 25px;} .w30 {width: 30px;} .w35 {width: 35px;} .w40 {width: 40px;} .w45 {width: 45px;} .w50 {width: 50px;} .w55 {width: 55px;} 
	.w60 {width: 60px;} .w65 {width: 65px;} .w70 {width: 70px;} .w75 {width: 75px;} .w80 {width: 80px;} .w85 {width: 85px;} .w90 {width: 90px;} .w95 {width: 95px;} .w100 {width: 100px;} .w105 {width: 105px;} 
	
	input, select, textarea {
		border: 1px solid gray;
	}

	input:read-only {
		border:none;
		background-color: #FFF;
		color: #000;
		width: 90% !important;
		text-align: center;
	}
	
	.tc { text-align: center;	}
	.tl { text-align: left;	}
	.tr { text-align: right;	}

	.ui-autocomplete-loading {background: white url('img/ui-anim_basic_16x16.gif') right center no-repeat;}
	
	div.msg {
		width:1260px;
		text-align: center;
		-moz-border-radius: 3px; 
		-webkit-border-radius: 3px; 
		border-radius: 3px 3px;
		background-color: green;
		color: #FFF;
		font-size: 13px;
		font-weight: bold;
		padding: 10px 0;
		margin-bottom: 10px;
	}
</style>
<body>
	<center>
	<div style="width: 1260px;">
		<?php echo (isset($msg) && ($msg != '')) ? '<div class="msg">'.$msg.'</div>' : '';?>
		<form action="?id=<?php echo $ro_order['order_id'];?>" method="post" onsubmit="Validate(this); return false;">
		<input type="hidden" name="act" value="save" />
		<?php
			$qu_product = '
				SELECT		product.product_id as id,
									order_product.order_product_id as order_product_id, 
									product_description.name as name,
									manufacturer.name as manufacturer,
									product.model as model,
									product.sku as sku,
									order_product.quantity as quantity,
									order_product.price as price,
									order_product.price_in as price_in
				FROM			order_product,
									product_description,
									product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
				WHERE			product.product_id = product_description.product_id &&
									order_product.product_id = product.product_id &&
									order_product.order_id = "'.$ro_order['order_id'].'"
				ORDER BY	order_product.order_product_id	 
			';
			$re_product = @mysqli_query($ddb, $qu_product);
		?>
		<input type="hidden" id="autoinc" name="autoinc" value="<?php echo mysqli_num_rows($re_product);?>" />
		<div id="tabs">
		  <ul>
				<li><a href="#tabs-main">Главная</a></li>
	    	<li><a href="#tabs-history">История</a></li>
	    	<?php if ($ro_order['payment_code'] == 'cashless') echo '<li><a href="#tabs-jur">ЮЛ</a></li>'; ?>
	    	<li style="text-align: right; float: right;"><input type="submit" style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; background-color: green; color:#FFF;"  value="Сохранить заказ" /></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;"><span style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; background-color: red; color:#FFF;" onclick="location.href='/sedoy/order_new.php?id=<?php echo $ro_order['order_id']?>';">Отменить изменения</span></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;"><span style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333;" onclick="location.href='/index.php?route=sale/order&token=<?php echo $_SESSION['token']?>';">К списку заказов</a></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;"><a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333;" onclick="location.href='/sedoy/torg12.xls.php?id=<?php echo $ro_order['order_id']?>';">ТОРГ-12</a></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;"><a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333;" onclick="window.open('/sedoy/sberbank_print.php?id=<?php echo $ro_order['order_id']?>');">СБРФ</a></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;"><a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333;" onclick="window.open('/index.php?route=sale/order/invoice&token=<?php echo $_SESSION['token']?>&order_id=<?php echo $ro_order['order_id']?>');">Показать счет</a></li>
	    	<li style="text-align: right; float: right; padding-right: 20px;">
					<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; background-color: blue; color:#FFF;" onclick="
						if (confirm('Скопировать заказ?')) {
							location.href='/sedoy/order_new.php?act=copy&id=<?php echo $ro_order['order_id']?>';
						}
					">Скопировать</a>
					
				</li>
	    	<li style="text-align: right; float: right; padding-right: 40px;">
					<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; background-color: blue; color:#FFF;" onclick="
						if (confirm('Отправить уведомление клиенту об изменении заказа?')) {
							location.href='/sedoy/order_new.php?act=mail&id=<?php echo $ro_order['order_id']?>';
						}
					">Уведомление</a>
				</li>
			</ul>
		  <div id="tabs-main">
				<table width="100%" border="1" cellpadding="3px">
					<tr>
						<td width="25%" align="left"><strong>№ заказа:</strong></td>
						<td width="25%" align="left"><?php echo $ro_order['order_id'].(($ro_order['quick']) ? 'Б' : '');?></td>
						<td width="25%" align="left"><strong>Дата и время заказа:</strong></td>
						<td width="25%" align="left"><?php echo date('d.m.Y H:i',strtotime($ro_order['date_added']))?></td>
					</tr>
					<tr>
						<td align="left"><strong>Покупатель:</strong></td>
						<td align="left"><input name="client" class="w90p" value="<?php echo trim($ro_order['firstname'].' '.$ro_order['lastname']);?>" rel="req" /></td>
						<td align="left"><strong>E-mail:</strong></td>
						<td align="left"><input name="email" class="w90p" value="<?php echo $ro_order['email']?>" rel="req" /></td>
					</tr>		
					<tr>
						<td align="left"><strong>Телефон:</strong></td>
						<td align="left">
							<input name="phone" class="w60p" value="<?php echo trim($ro_order['telephone']);?>" rel="req" />
							&nbsp;&nbsp;&nbsp;
							<a href="http://eduscan.net/i/phone.php?num=<?php echo str_replace(array(' ','+','-','(',')',' '),'',$ro_order['telephone']);?>" target="_blank">Регион</a>
						</td>
						<td align="left"><strong>Страна:</strong></td>
						<td align="left">
							<select name="country_id" class="w90p" rel="req" onchange="ChangeCountry(this.value);">
								<option value=""></option>
								<?php
									$qu_country = 'SELECT * FROM country WHERE STATUS="1" ORDER BY status="1"';
									$re_country = @mysqli_query($ddb, $qu_country);
									while ($ro_country = @mysqli_fetch_array($re_country)) echo '<option value="'.$ro_country['country_id'].'"'.(($ro_country['country_id'] == $ro_order['payment_country_id']) ? ' selected' : '').'>'.$ro_country['name'].'</option>';
								?>
							</select>
						</td>
					</tr>		
					<tr>
						<td align="left"><strong>Зарегистрированный покупатель:</strong></td>
						<td align="left"><?php echo ($ro_order['customer_id'] > 0) ? 'да' : 'нет'?></td>
						<td align="left"><strong>Город:</strong></td>
						<td align="left">
							<select name="city" class="w90p" rel="req" id="cities">
								<option value=""></option>
								<?php
									$qu_city = 'SELECT * FROM city WHERE country_id="'.$ro_order['payment_country_id'].'" ORDER BY sort, city';
									$re_city = @mysqli_query($ddb, $qu_city);
									while ($ro_city = @mysqli_fetch_array($re_city)) echo '<option value="'.$ro_city['city'].'"'.(($ro_order['payment_city'] == $ro_city['city']) ? ' selected' : '').'>'.$ro_city['city'].'</option>';
								?>
							</select>
						</td>
					</tr>		
					<tr>
						<td align="left"><strong>Комментарий покупателя:</strong></td>
						<td align="left"><?php echo $ro_order['comment'];?></td>
						<td align="left"><strong>Адрес доставки:</strong></td>
						<td align="left">
							<input name="adres" class="w90p" value="<?php echo trim($ro_order['shipping_address_1'])?>" />
							<a href="http://maps.yandex.ru/?text=<?php echo trim($ro_order['shipping_city']).' '.trim($ro_order['shipping_address_1'])?>" target="_blank">M</a>							
						</td>
					</tr>		
				</table>
				<br />
				<table width="100%" border="1">
					<tr>
						<td width="%" align="center"><strong></strong></td>
						<td width="%" align="center"><strong>Товар</strong></td>
						<td width="%" align="center"><strong>Производитель</strong></td>
						<td width="%" align="center"><strong>Модель</strong></td>
						<td width="%" align="center"><strong>Артикул</strong></td>
						<td width="%" align="center"><strong>Кол-во</strong></td>
						<td width="%" align="center"><strong>Цена, р.</strong></td>
						<td width="%" align="center"><strong>Входящая цена, р.</strong></td>
						<td width="%" align="center"><strong>Маржа, р</strong></td>
						<td width="%" align="center"><strong>Сумма, р</strong></td>
					</tr>
	<?php $i = 1;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
	?>
					<tr id="tr_<?php echo $i;?>">
						<td width="20" align="center"><span style="cursor: pointer; color:red;" onclick="$('#tr_<?php echo $i;?>').remove();  CalcMarginSumm();">X</span></td>
						<td align="left" id="name_<?php echo $i;?>">
							<a href="/index.php?route=catalog/product/update&token=<?php echo $_SESSION['token'];?>&product_id=<?php echo $ro_product['id'];?>" target="_blank"><?php echo $ro_product['name'];?></a> &nbsp; (<a href="/index.php?route=product/product&product_id=<?php echo $ro_product['id'];?>" target="_blank">фронт</a>)
	<?php
		unset($options);
		unset($options_txt);
		$qu_option = 'SELECT * FROM order_option WHERE order_product_id="'.$ro_product['order_product_id'].'"';
		$re_option = @mysqli_query($ddb, $qu_option);
		if (@mysqli_num_rows($re_option)) {
			echo '<ul>';
			while ($ro_option = @mysqli_fetch_array($re_option)) {
				$tmp = Result($ddb, 'SELECT option_id FROM product_option WHERE product_option_id='.$ro_option['product_option_id']);
				echo '<li'.(($tmp['option_id'] == 24) ? ' style="font-weight: bold; color: red;"' : '').'>'.$ro_option['name'].': '.$ro_option['value'].(($ro_option['price'] > 0) ? ' ('.floatval($ro_option['price']).' руб.)' : '').'</li>';
				$options[] = $ro_option['product_option_id'].':'.$ro_option['product_option_value_id'];
				$options_txt[] = $ro_option['name'].':::'.$ro_option['value'];

				/*
				echo '<li>'.$ro_option['name'].': '.$ro_option['value'].(($ro_option['price'] > 0) ? ' ('.floatval($ro_option['price']).' руб.)' : '').'</li>';
				$options[] = $ro_option['product_option_id'].':'.$ro_option['product_option_value_id'];
				$options_txt[] = $ro_option['name'].':::'.$ro_option['value'];
				*/
			}
			echo '</ul>';
		}
	?>							
							<input type="hidden" name="product_id_<?php echo $i;?>" id="product_id_<?php echo $i;?>" value="<?php echo $ro_product['id'];?>" />
							<input type="hidden" name="product_name_<?php echo $i;?>" id="product_name_<?php echo $i;?>" value="<?php echo $ro_product['name'];?>" />
							<input type="hidden" name="product_model_<?php echo $i;?>" id="product_model_<?php echo $i;?>" value="<?php echo $ro_product['model'];?>" />
							<input type="hidden" name="option_<?php echo $i;?>" id="option_<?php echo $i;?>" value="<?php echo @implode(',',$options);?>" />
							<input type="hidden" name="option_txt_<?php echo $i;?>" id="option_txt_<?php echo $i;?>" value="<?php echo @implode(',,,',$options_txt);?>" />
						</td>
						<td align="center" id="manufacturer_<?php echo $i;?>"><?php echo $ro_product['manufacturer']?></td>
						<td align="center" id="model_<?php echo $i;?>"><?php echo $ro_product['model']?></td>
						<td align="center" id="sku_<?php echo $i;?>"><?php echo $ro_product['sku']?></td>
						<td align="center"><input name="quantity_<?php echo $i;?>" id="quantity_<?php echo $i;?>" value="<?php echo $ro_product['quantity']?>" onkeyup="CalcMarginSumm();" class="tc w35" /></td>
						<td align="center"><input name="price_<?php echo $i;?>" id="price_<?php echo $i;?>" onkeyup="CalcMarginSumm();" value="<?php echo sprintf('%01.2f',$ro_product['price']);?>" maxlength="8" class="tc w60" /></td>
						<td align="center"><input name="price_in_<?php echo $i;?>" id="price_in_<?php echo $i;?>" readonly="readonly" value="<?php echo sprintf('%01.2f',$ro_product['price_in']);?>"></td>
						<td align="center"><input id="margin_<?php echo $i;?>" readonly="readonly" value=""></td>
						<td align="center"><input id="summ_<?php echo $i;?>" name="summ_<?php echo $i;?>" readonly="readonly" value=""></td>
					</tr>
	<?php
			$i++;
		}
	?>
					<tr id="new-product">
						<td align="left" colspan="10">
							<input name="" id="product" class="w90p" placeholder="Добавить товар" onfocus="this.value = '';" />
						</td>
					</tr>
					<tr>
						<td align="right" colspan="8"><strong>СУММА ПО ТОВАРАМ:</strong></td>
						<td align="center"><input id="margin_before_disc" readonly="readonly" style="width:60px !important;" /> (<input id="percent_before_disc" readonly="readonly" style="width: 35px !important;" />%)</td>
						<td align="center"><input id="summ_before_disc" name="summ_before_disc" readonly="readonly" /></td>
					</tr>
					<tr>
						<td align="right" colspan="8"><strong>СКИДКА:</strong></td>
						<td align="center"><input name="discount_percent" id="discount_percent" value="<?php echo $ro_order['discount_percent'];?>" class="tc w55" onkeyup="CalcDiscountSumm();" /> %</td>
						<td align="center"><input name="discount_summ" id="discount_summ" value="<?php echo $ro_order['discount_summ'];?>" maxlength="8" class="tc w80" onkeyup="CalcDiscountPercent();" /></td>
					</tr>
					<tr>
						<td align="right" colspan="8"><strong>Скидочный купон №:</strong></td>
						<td align="center"><?php echo ($ro_coupon['code']) ? $ro_coupon['title'] : ''; ?></td>
						<td align="center"><input id="discount_coupon" readonly="readonly" value="<?php echo ($ro_coupon['code']) ? sprintf('%01.2f',-1*$ro_coupon['value']) : '0.00';?>" /></td>
					</tr>
					<tr>
						<td align="right" colspan="8"><strong>ИТОГО ПО ТОВАРАМ:</strong></td>
						<td align="center"><strong><input id="margin_after_disc" readonly="readonly" style="width: 60px !important;" /> (<input id="percent_after_disc" readonly="readonly" style="width: 35px !important;" />%)</strong></td>
						<td align="center"><strong><input id="summ_after_disc" name="summ_after_disc" readonly="readonly" /></strong></td>
					</tr>
				</table>
				<br />
				<table width="100%" border="1">
					<tr>
						<td align="left"><strong>Оператор:</strong></td>
						<td colspan="3" align="left">
							<select class="w90p" name="operator_id" id="operator_id" rel="req">
								<option></option>
								<?php
									$qu_operator = 'SELECT * FROM operator ORDER BY title';
									$re_operator = @mysqli_query($ddb, $qu_operator);
									while ($ro_operator = @mysqli_fetch_array($re_operator)) echo '<option value="'.$ro_operator['operator_id'].'"'.(($ro_operator['operator_id'] == $ro_order['operator_id']) ? ' selected' : '').'>'.$ro_operator['title'].'</option>';
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="left"><strong>Вид оплаты:</strong></td>
						<td colspan="3" align="left">
							<select class="w90p" name="payment_code">
								<option></option>
								<?php
									foreach ($payment as $key => $value) {
										echo '<option value="'.$key.'"'.(($ro_order['payment_code'] == $key) ? ' selected' : '').'>'.$value.'</option>';
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="25%"><strong>Вид доставки:</strong></td>
						<td align="left" width="25%">
							<select class="w90p" name="shipping_code" id="shipping_code" onchange="ChangeShipping();">
								<option></option>
								<?php
									foreach ($shipping as $key => $value) {
										echo '<option value="'.$key.'"'.(($ro_order['shipping_code'] == $key) ? ' selected' : '').'>'.$value['title'].'</option>';
									}
								?>
							</select>
						</td>
						<td align="left" width="25%"><strong>Цена доставки:</strong></td>
						<td align="center" width="25%"><input class="w50 tc" name="shipping_price" id="shipping_price" onkeyup="CalcMarginSumm();" value="<?php echo round($ro_shipping['value'],2);?>" /></td>
					</tr>
					<tr>
						<td align="right"><strong>Упаковка (для курьера):</strong></td>
						<td align="left">
							<select name="pack">
								<option></option>
								<option value="конверт"<?php echo (($ro_order['pack'] == 'конверт') ? ' selected' : '')?>>конверт</option>
								<option value="пакет"<?php echo (($ro_order['pack'] == 'пакет') ? ' selected' : '')?>>пакет</option>
							</select>
						</td>
						
						<td align="right"><strong>ИТОГО ПО ЗАКАЗУ:</strong></td>
						<td align="center"><input name="summ_after_ship" id="summ_after_ship" readonly="readonly" value="" /></td>
					</tr>
					<!--					
					<tr>
						<td colspan="3" align="right"><strong>ИТОГО ПО ЗАКАЗУ:</strong></td>
						<td align="center"><input name="summ_after_ship" id="summ_after_ship" readonly="readonly" value="" /></td>
					</tr>
					-->
					<tr>
						<td colspan="3" align="right"><strong>МАРЖА ПО ЗАКАЗУ:</strong></td>
						<td align="center"><input style="width:70px !important;" name="margin_after_ship" id="margin_after_ship" readonly="readonly" value="" /> (<input style="width: 50px !important;" name="percent_after_ship" id="percent_after_ship" readonly="readonly" value="" />%)</strong></td>
					</tr>
				</table>
				<br />
				<table width="100%" border="1">
					<tr>
						<td width="20%" align="left"><strong>Текущий статус заказа:</strong></td>
						<td width="30%" align="left"><?php echo $ro_curr_status['name'];?></td>
						<td width="20%" align="left"><strong>Дата и время доставки:</strong></td>
						<td width="30%">
							<input type="text" size="10" id="ship_date" name="ship_date" rel="req" value="<?php echo ($ro_order['date_shipped'] != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($ro_order['date_shipped'])) : '';?>" />
							&nbsp;&nbsp;&nbsp;&nbsp;
							с
							<select name="time_shipped_from">
								<option value=""></option>
								<?php
									for ($h = 9; $h<=20; $h++) for ($m=0; $m<=45; $m +=60) {
										$tmp = sprintf('%02d',$h).':'.sprintf('%02d',$m);
										echo '<option value="'.$tmp.'"'.(($tmp == $ro_order['time_shipped_from']) ? ' selected' : '').'>'.$tmp.'</option>';
									}
								?>
							</select>			
							по
							<select name="time_shipped_to">
								<option value=""></option>
								<?php
									for ($h = 9; $h<=20; $h++) for ($m=0; $m<=45; $m +=60) {
										$tmp = sprintf('%02d',$h).':'.sprintf('%02d',$m);
										echo '<option value="'.$tmp.'"'.(($tmp == $ro_order['time_shipped_to']) ? ' selected' : '').'>'.$tmp.'</option>';
									}
								?>
							</select>			
										
						</td>
					</tr>
					<tr>
						<td align="left"><strong>Курьер:</strong></td>
						<td align="left">
							<select class="w90p" name="courier_id" id="courier_id" onchange="ChangeCourier();" rel="req">
								<option></option>
								<?php
									$qu_courier = 'SELECT * FROM courier ORDER BY name';
									$re_courier = @mysqli_query($ddb, $qu_courier);
									while ($ro_courier = @mysqli_fetch_array($re_courier)) echo '<option value="'.$ro_courier['courier_id'].'"'.(($ro_courier['courier_id'] == $ro_order['courier_id']) ? ' selected' : '').'>'.$ro_courier['name'].' ('.$ro_courier['price'].' руб.)</option>';
								?>
							</select>
						</td>
						<td align="left"><strong>Номер накладной перевозчика:</strong></td>
						<td>
							<input name="decl" class="tc w200" maxlength="64" value="<?php echo $ro_order['decl']?>" />
							
							<iframe style="width: 180px; height: 26px; vertical-align: middle; overflow-x: hidden; overflow-y: hidden; border: none;" src="order_new_pony.php?number=<?php echo $ro_order['decl']?>"></iframe>
							<!--
							<select name="ship_hour">
								<?php for ($i=0;$i<24;$i++) echo '<option value="'.sprintf('%02d',$i).'"'.((date('H',strtotime($ro_order['date_shipped'])) == sprintf('%02d',$i)) ? ' selected' : '').'>'.sprintf('%02d',$i).'</option>'; ?>
							</select>
							:
							<select name="ship_min">
								<?php for ($i=0;$i<60;$i=$i+5) echo '<option value="'.sprintf('%02d',$i).'"'.((date('m',strtotime($ro_order['date_shipped'])) == sprintf('%02d',$i)) ? ' selected' : '').'>'.sprintf('%02d',$i).'</option>'; ?>
							</select>
							-->
							
						</td>
					</tr>
					<tr>
						<td align="left"><strong>Оплата курьеру:</strong></td>
						<td align="left"><input name="courier_price" id="courier_price" class="tc w40" maxlength="4" value="<?php echo $ro_order['courier_price']?>" /></td>
						<td align="left"><strong>Метро:</strong></td>
						<td>
							<select name="metro_id">
								<option value=""></option>
	<?php
		$qu_metro = 'SELECT * FROM metro ORDER BY title';
		$re_metro = @mysqli_query($ddb, $qu_metro);
		while ($ro_metro = @mysqli_fetch_array($re_metro)) echo '<option value="'.$ro_metro['metro_id'].'"'.(($ro_order['metro_id'] == $ro_metro['metro_id']) ? ' selected' : '').'>'.$ro_metro['title'].'</option>';
	?>								
							</select>
							<!--
							<input name="metro" id="metro" class="w90p" value="<?php echo $ro_order['metro'];?>" />
							-->
						</td>
					</tr>
				</table>
		  </div>
		  <div id="tabs-history">
				<table width="100%" border="1">
					<tr>
						<td width="20%" align="left"><strong>Дата и время добавления</strong></td>
						<td width="45%"align="left"><strong>Комментарий</strong></td>
						<td width="17%"align="left"><strong>Статус</strong></td>
						<td width="18%"><strong>Пользователь</strong></td>
					</tr>
	<?php
		$qu_history = '
			SELECT		user.username,
						user.user_id,
						order_history.date_added as date_added, 
						order_history.comment as comment,
						order_status.name as status,
						order_history.notify as notify
			FROM		order_history LEFT JOIN user ON user.user_id = order_history.user_id,
						order_status
			WHERE		order_history.order_status_id = order_status.order_status_id &&
						order_history.order_id = "'.$ro_order['order_id'].'"
			ORDER BY	order_history.date_added DESC
		';

		$re_history = @mysqli_query($ddb, $qu_history);
		while ($ro_history = @mysqli_fetch_array($re_history)) {
			?>
					<tr>
							<td align="left"><?php echo date('d.m.Y H:i:s', strtotime($ro_history['date_added']));?></td>
							<td align="left"><?php echo $ro_history['comment'];?></td>
							<td align="left"><?php echo $ro_history['status'];?></td>
							<td><?php echo (($ro_history['user_id']) ? $ro_history['username'] : 'Гость') /*($ro_history['notify'] == '1') ? 'Да' : 'Нет'*/ ?></td>
					</tr>					
			<?php } ?>
				</table>
				<br />
				<table width="100%" border="1">
					<tr>
						<td width="20%" align="left"><strong>Установить статус заказа:</strong></td>
						<td width="80%" align="left">
							<select class="w90p" name="status_id">
								<option></option>
								<?php
									$qu_status = 'SELECT * FROM order_status ORDER BY name';
									$re_status = @mysqli_query($ddb, $qu_status);
									while ($ro_status = @mysqli_fetch_array($re_status)) echo '<option value="'.$ro_status['order_status_id'].'"'.(($ro_status['order_status_id'] == $ro_curr_status['order_status_id']) ? ' selected' : '').'>'.$ro_status['name'].'</option>';
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="left"><strong>Уведомление покупателю:</strong></td>
						<td align="left"><input type="checkbox" name="notify" /></td>
					</tr>
					<tr>
						<td align="left" valign="top"><strong>Комментарий:</strong></td>
						<td align="left" valign="top"><textarea class="w90p" rows="4" name="comment"></textarea></td>
					</tr>
				</table>
				<!--
				<?php echo trim(str_replace(array(' ','(',')','-'),'',$ro_order['telephone']))?><br />
				Ваш заказ <?php echo $ro_order['order_id'];?> отправлен. Номер декл. <?php echo $ro_order['decl']?>. ВсеПессарии.com
				<br />
				Ваш заказ <?php echo $ro_order['order_id'];?> прибыл. Номер декл. <?php echo $ro_order['decl']?>. ВсеПессарии.com
				--> 
		  </div>

			<?php if ($ro_order['payment_code'] == 'cashless') { ?>
			<div id="tabs-jur">
		 		<table>
		 			<tr>
		 				<td>Наименование:</td>
		 				<td><input name="doc_firm" value="<?php echo $ro_order['doc_firm']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>ИНН:</td>
		 				<td><input name="doc_inn" value="<?php echo $ro_order['doc_inn']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>КПП:</td>
		 				<td><input name="doc_kpp" value="<?php echo $ro_order['doc_kpp']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>ОГРН:</td>
		 				<td><input name="doc_ogrn" value="<?php echo $ro_order['doc_ogrn']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>Адрес:</td>
		 				<td><input name="doc_address" value="<?php echo $ro_order['doc_address']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>В лице:</td>
		 				<td><input name="doc_face" value="<?php echo $ro_order['doc_face']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>Р/с:</td>
		 				<td><input name="doc_rs" value="<?php echo $ro_order['doc_rs']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>Банк:</td>
		 				<td><input name="doc_bank" value="<?php echo $ro_order['doc_bank']?>"></td>
		 			</tr>
		 			<tr>
		 				<td>БИК:</td>
		 				<td><input name="doc_bik" value="<?php echo $ro_order['doc_bik']?>"></td>
		 			</tr>
		 		</table>
		 		<br><br>
		 		<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; text-decoration: none;" href="/sedoy/templates/schet.xls.php?id=<?php echo $ro_order['order_id']?>">Счет</a>
		 		&nbsp;&nbsp;&nbsp;
		 		<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; text-decoration: none;" href="/sedoy/templates/torg12.xls.php?id=<?php echo $ro_order['order_id']?>">ТОРГ-12</a>
		 		&nbsp;&nbsp;&nbsp;
		 		<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; text-decoration: none;" href="/sedoy/templates/dogovor.xls.php?id=<?php echo $ro_order['order_id']?>">Договор</a>
		 		&nbsp;&nbsp;&nbsp;
		 		<a style="display: inline-block; cursor: pointer; padding: 2px 7px; border: 1px solid #333; text-decoration: none;" href="/sedoy/templates/akt.xls.php?id=<?php echo $ro_order['order_id']?>">Акт</a>

			</div>
			<?php } ?>


		</div>
		</form>	
	</div>
	</center>
</body>
</html>
