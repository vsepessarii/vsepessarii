<?php
class ControllerPaymentyandexplusplus extends Controller {
	protected function index() {
		$this->language->load('payment/yandexplusplus');
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$action= 'index.php?route=account/yandexplusplus/pay';
		$paymentredir = $action .
				'&paymentType=' . $order_info['payment_code'] .
				'&order_id='	. $order_info['order_id'] . 
				'&first=1';

		$online_url = HTTPS_SERVER . 'index.php?route=account/yandexplusplus' .
					'&order_id='	. $order_info['order_id'];

	  	$this->data['continue'] = $this->url->link('checkout/success');

		$this->load->language('account/yandexplusplus');

		$this->data['pay_url'] = $paymentredir;
		$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['payment_url'] = $this->url->link('checkout/success');
		$this->data['button_later'] = $this->language->get('button_pay_later');

		if ($this->config->get('yandexplusplus_instruction_attach')){
			$this->data['text_instruction'] = $this->language->get('text_instruction');

			$instros = explode('$', ($this->config->get('yandexplusplus_instruction')));
			$instroz = "";
			foreach ($instros as $instro) {
				if ($instro == 'href' || $instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
				    if ($instro == 'href'){
				        $instro_other = $online_url;
				    }
				    if ($instro == 'orderid'){
				        $instro_other = $order_info['order_id'];
					}
					if ($instro == 'itogo'){
					    $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
					}
					if ($instro == 'komis'){
						if($this->config->get('yandexplusplus_komis')){
					    	$instro_other = $this->config->get('yandexplusplus_komis') . '%';
						}
						else{$instro_other = '';}
					}
					if ($instro == 'total-komis'){
						if($this->config->get('yandexplusplus_komis')){
					    	$instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
						}
						else{$instro_other = '';}
					}
					if ($instro == 'plus-komis'){
						if($this->config->get('yandexplusplus_komis')){
					    	$instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
						}
						else{$instro_other = '';}
					}
				}
				else {
				    $instro_other = nl2br(htmlspecialchars_decode($instro));
				}
				    $instroz .=  $instro_other;
			}
				$this->data['yandexplusplusi'] = $instroz;
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/yandexplusplus.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/yandexplusplus.tpl';
		} else {
            $this->template = 'default/template/payment/yandexplusplus.tpl';
        }
		
		$this->render();		 
	}
	
	public function confirm() {
  		$this->language->load('payment/yandexplusplus');
		$this->load->model('checkout/order');
			if ($this->config->get('yandexplusplus_mail_instruction_attach')){
				$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				$out_summ = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
				$inv_id = $this->session->data['order_id'];
				$action= HTTPS_SERVER . 'index.php?route=account/yandexplusplus';
				$online_url = $action .

				'&order_id='	. $order_info['order_id'];

		    	//$comment  = $this->language->get('text_instruction') . "\n\n";
		    	$instros = explode('$', ($this->config->get('yandexplusplus_mail_instruction')));
				      $instroz = "";
				      foreach ($instros as $instro) {
				      	if ($instro == 'href' || $instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
				      		if ($instro == 'href'){
				            	$instro_other = $online_url;
				        	}
				            if ($instro == 'orderid'){
				            	$instro_other = $inv_id;
					       	}
					       	if ($instro == 'itogo'){
					            $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
					       	}
					       	if ($instro == 'komis'){
								if($this->config->get('yandexplusplus_komis')){
							    	$instro_other = $this->config->get('yandexplusplus_komis') . '%';
								}
								else{$instro_other = '';}
							}
							if ($instro == 'total-komis'){
								if($this->config->get('yandexplusplus_komis')){
							    	$instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
								}
								else{$instro_other = '';}
							}
							if ($instro == 'plus-komis'){
								if($this->config->get('yandexplusplus_komis')){
							    	$instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
								}
								else{$instro_other = '';}
							}
				       	}
				       	else {
				       		$instro_other = nl2br($instro);
				       	}
				       	$instroz .=  $instro_other;
				      }
				$comment .= $instroz;
		    	$comment = htmlspecialchars_decode($comment);
		    	$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('yandexplusplus_on_status_id'), $comment, true);
	    	}
	    	else{
				$this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('yandexplusplus_on_status_id'), true);
			}
	}
}
?>