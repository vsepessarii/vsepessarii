<?php 
class ControllerInformationDirector extends Controller {
	private $error = array(); 
  	public function index() {
		//die();
		$this->language->load('information/director');

    	$this->document->setTitle($this->language->get('heading_title'));  
	 
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      		@mysqli_query($ddb, '
      			INSERT INTO	director
      			SET			subject="'.mysqli_escape_string($this->request->post['subject']).'",
      						order_num="'.mysqli_escape_string($this->request->post['order_num']).'",
      						-- publish="'.mysqli_escape_string($this->request->post['publish']).'",
      						publish="0",
      						name="'.mysqli_escape_string($this->request->post['name']).'",
      						email="'.mysqli_escape_string($this->request->post['email']).'",
      						phone="'.mysqli_escape_string($this->request->post['phone']).'",
      						enquiry="'.mysqli_escape_string($this->request->post['enquiry']).'"
      		');
      		$id = @mysqli_insert_id();
      		if ($_FILES['attach']['tmp_name'] != '') @mysqli_query($ddb, '
      			UPDATE	director
      			SET		file_name="'.mysqli_escape_string($_FILES['attach']['name']).'",
      					file_data="'.mysqli_escape_string(file_get_contents($_FILES['attach']['tmp_name'])).'"
      			WHERE	ID='.$id.'
      		');

			$mail = new Mail();
			$mail->protocol = 'mail';
			$mail->setTo($this->config->get('config_email'));
	  		$mail->setFrom($this->request->post['email']);
	  		$mail->setSender($this->request->post['name']);
	  		$mail->setSubject(html_entity_decode('Письмо директору с vsepessarii.ru - '.$this->request->post['subject'].' №'.$id, ENT_QUOTES, 'UTF-8'));
	  		$mail->setHtml('
	  			<b>Номер обращения:</b> '.$id.'<br>
	  			<b>Дата и время добавления:</b> '.date('d.m.Y H:i:s',time()).'<br>
	  			<b>Номер заказа:</b> '.mysqli_escape_string($this->request->post['order_num']).'<br>
	  			<b>Имя:</b> '.mysqli_escape_string($this->request->post['name']).'<br>
	  			<b>E-mail:</b> '.mysqli_escape_string($this->request->post['email']).'<br>
	  			<b>Телефон:</b> '.mysqli_escape_string($this->request->post['phone']).'<br>
	  			<b>Текст вопроса:</b><br>'.mysqli_escape_string(nl2br(strip_tags(str_replace('\r\n','', $this->request->post['enquiry']))))
	  		);
	  		if ($_FILES['attach']['tmp_name'] != '') $mail->ProAddAttachment($_FILES['attach']['name'], $_FILES['attach']['tmp_name']);
      		$mail->send();


	  		$this->redirect($this->url->link('information/director/success'));
    	}

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/contact'),
        	'separator' => $this->language->get('text_separator')
      	);	
			
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_location'] = $this->language->get('text_location');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_address'] = $this->language->get('text_address');
    	$this->data['text_telephone'] = $this->language->get('text_telephone');
    	$this->data['text_fax'] = $this->language->get('text_fax');

    	$this->data['entry_name'] = $this->language->get('entry_name');
    	$this->data['entry_email'] = $this->language->get('entry_email');
    	$this->data['entry_phone'] = $this->language->get('entry_phone');
    	$this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		$this->data['subjects'] = array(
			'',
			'Жалоба на оператора',
			'Проблема с доставкой',
			'Проблема с переводом денежных средств',
			'Предложение по сотрудничеству',
			'Предложение по сайту',
			'Другое'
		);


		if (isset($this->error['subject'])) {
    		$this->data['error_subject'] = $this->error['subject'];
		} else {
			$this->data['error_subject'] = '';
		}
		
		/*
		if (isset($this->error['publish'])) {
    		$this->data['error_publish'] = $this->error['publish'];
		} else {
			$this->data['error_publish'] = '';
		}
		*/
		
		if (isset($this->error['name'])) {
    		$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}		
		
		if (isset($this->error['phone'])) {
			$this->data['error_phone'] = $this->error['phone'];
		} else {
			$this->data['error_phone'] = '';
		}		
		
		if (isset($this->error['enquiry'])) {
			$this->data['error_enquiry'] = $this->error['enquiry'];
		} else {
			$this->data['error_enquiry'] = '';
		}		
		
 		if (isset($this->error['captcha'])) {
			$this->data['error_captcha'] = $this->error['captcha'];
		} else {
			$this->data['error_captcha'] = '';
		}	

    	$this->data['entry_button_send'] = $this->language->get('entry_button_send');
    
		$this->data['action'] = $this->url->link('information/contact');
    	$this->data['director'] = $this->config->get('config_director');
    	
		if (isset($this->request->post['subject'])) {
			$this->data['subject'] = $this->request->post['subject'];
		} else {
			$this->data['subject'] = '';
		}

		if (isset($this->request->post['order_num'])) {
			$this->data['order_num'] = $this->request->post['order_num'];
		} else {
			$this->data['order_num'] = '';
		}

		/*
		if (isset($this->request->post['publish'])) {
			$this->data['publish'] = $this->request->post['publish'];
		} else {
			$this->data['publish'] = '';
		}
		*/

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} else {
			$this->data['name'] = $this->customer->getFirstName();
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} else {
			$this->data['email'] = $this->customer->getEmail();
		}
		
		if (isset($this->request->post['phone'])) {
			$this->data['phone'] = $this->request->post['phone'];
		} else {
			$this->data['phone'] = '';
		}
		
		if (isset($this->request->post['enquiry'])) {
			$this->data['enquiry'] = $this->request->post['enquiry'];
		} else {
			$this->data['enquiry'] = '';
		}
		
		if (isset($this->request->post['captcha'])) {
			$this->data['captcha'] = $this->request->post['captcha'];
		} else {
			$this->data['captcha'] = '';
		}		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/director.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/information/director.tpl';
		} else {
			$this->template = 'default/template/information/director.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
				
 		$this->response->setOutput($this->render());		
  	}

  	public function success() {
		$this->language->load('information/director');

		$this->document->setTitle($this->language->get('heading_title')); 

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	);

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/contact'),
        	'separator' => $this->language->get('text_separator')
      	);	
		
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_message'] = $this->language->get('text_message');

    	$this->data['button_continue'] = 'OK';

    	$this->data['continue'] = '/director.html';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
				
 		$this->response->setOutput($this->render()); 
	}
	
  	private function validate() {
    	if (utf8_strlen($this->request->post['subject']) < 3) {
      		$this->error['subject'] = $this->language->get('error_subject');
    	}

    	/*
    	if ($this->request->post['publish'] == '') {
      		$this->error['publish'] = $this->language->get('error_publish');
    	}
    	*/

    	if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}

    	if (!preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
      		$this->error['email'] = $this->language->get('error_email');
    	}

    	//if (!preg_match('/^[0-9]{10}$/i', $this->request->post['phone'])) {
    	if ((utf8_strlen($this->request->post['phone']) < 10) || (utf8_strlen($this->request->post['phone']) > 32)) {
      		$this->error['phone'] = $this->language->get('error_phone');
    	}

    	if ((utf8_strlen($this->request->post['enquiry']) < 10) || (utf8_strlen($this->request->post['enquiry']) > 10000)) {
      		$this->error['enquiry'] = $this->language->get('error_enquiry');
    	}

    	if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
      		$this->error['captcha'] = $this->language->get('error_captcha');
    	}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}  	  
  	}

	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}	
}
?>
