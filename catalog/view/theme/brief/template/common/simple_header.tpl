<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $content_top; ?>
<div class="content-container">
    <section class="box">
        <!--20:57:53--><div class="box-heading"><?php echo $heading_title; ?></div>
        <nav class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </nav>
        <?php if (isset($error_warning)) { ?>
        <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php } ?>
        <div style="display: block; clear: both; height: 0px;"></div>
