<?php echo $header; ?>  
<div class="content-container">
	<section class="box">
		<h1 class="box-heading"><?php echo $heading_title; ?></h1>

		<div class="box-content review" style="padding: 14px 19px;">
			<?php echo $heading_description; ?>
		</div>

		<?php if (true/*$testimonials*/) { ?>
     <?php foreach ($testimonials as $testimonial) { ?>
  		<div class="box-content review" style="padding: 14px 19px;">
			
			<div style="text-align: justify;">
				<div style="text-align: left;">
					<?php if ($testimonial['rating']) { ?>
					<i><?php echo '<b>'.$testimonial['name'].', '.$testimonial['city'].'</b><br /><div style="font-size:10px; color: #666;">'.$testimonial['date_added'].'</div>'; ?></i>
					<img src="/catalog/view/theme/default/image/blogstars-<?php echo $testimonial['rating'] . '.png'; ?>" style="margin-top: 2px; margin-bottom: 3px;" />
					<?php } ?><br>
				</div>
				<?php echo $testimonial['description']; ?>
			</div>
		</div>				
		<?php } ?>

		<?php if ( isset($pagination)) { ?>
			<div class="pagination"><?php echo $pagination;?></div>
			<div class="buttons" style="padding-top: 10px !important; padding-bottom: 20px;" align="right"><a class="button blue" style="margin-right: 0;" href="<?php echo $write_url;?>" title="<?php echo $write;?>"><span><?php echo $write;?></span></a></div>
		<?php }?>

		<?php if (isset($showall_url)) { ?>
			<div class="buttons" style="padding-top: 10px !important; padding-bottom: 20px;" align="right"><a class="button blue" href="<?php echo $write_url;?>" title="<?php echo $write;?>"><span><?php echo $write;?></span></a> &nbsp;<a class="button blue" href="<?php echo $showall_url;?>" title="<?php echo $showall;?>"><span><?php echo $showall;?></span></a></div>
		<?php }?>
    <?php } ?>
  <div class="bottom">
    <div class="left"></div>
    <div class="right"></div>
    <div class="center"></div>
	</div>
	
	
	
	
	
	
	
	</section>	
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $footer; ?> 