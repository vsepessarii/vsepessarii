<?php
	require '../connect.php';
	$qu_product = '
		SELECT	product.product_id as id,
						product_description.name as name,
						manufacturer.name as manufacturer,
						product.model as model,
						product.price as price
		FROM		product_description,
						product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
		WHERE		product.product_id = product_description.product_id
	';
	$words = explode(' ',$_GET['term']);
	foreach ($words as $word) {
		if (mb_strlen($word) >=2) $qu_product .= ' && CONCAT(product_description.name," ",manufacturer.name," ",product.model) LIKE "%'.mysqli_escape_string($word).'%"';
	}
	$qu_product .= '
		ORDER BY	product_description.name, manufacturer.name, product.model
	';
	$re_product = @mysqli_query($ddb, $qu_product);
	echo mysqli_error($ddb);
	unset ($tmp);
	while ($ro_product = @mysqli_fetch_array($re_product)) $tmp[] = '{"id":"'.$ro_product['id'].'","label":"'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' ('.floatval($ro_product['price']).' руб.)","value":"'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].'","price":"'.floatval($ro_product['price']).'"}';
	echo '['.@implode(',',$tmp).']';
?>