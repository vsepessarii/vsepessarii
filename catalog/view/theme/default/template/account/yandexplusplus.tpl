<?php echo $header; ?>
<div class="content-container">
	<section class="box">
		<h1 class="box-heading"><?php echo $heading_title; ?></h1>
		<nav class="breadcrumb">
	    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	    <?php } ?>
    </nav>
		<div class="product-compare">
			<a href="https://vsepessarii.ru/index.php?route=product/compare" id="compare-total">Сравнение товаров (0)</a>
		</div>						
		
		<div class="box-content" style="margin-top: 30px !important;">
 			<div class="yandexplusplus">
 				<p><?php
	 				if(isset($hrefpage_text)){
	 					echo $hrefpage_text;
	 				}
	 				else{
			  		echo $send_text . $inv_id . $send_text2 . $out_summ ;
			  	} ?>
				</p>
				<div class="buttons">
					<table>
						<tr>
							<td align="left"><a onclick="location='<?php echo $back; ?>'" class="button blue"><span><?php echo $button_back; ?></span></a></td>
							<?php if ($paystat != 1){ ?><td align="right"><form method="POST" action="<?php echo $merchant_url;?>"><input type="hidden" name="nesyandexa" value="nesyandexa"><input type="submit" value="<?php echo $button_pay; ?>" class="button"></form></td> <?php } ?>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<?php echo $footer; ?>