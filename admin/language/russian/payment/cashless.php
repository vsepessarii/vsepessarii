<?php
// Heading
$_['heading_title']         = 'Безналичный расчет';

// Text
$_['text_payment_field']    = 'Оплата';
$_['text_update_success']   = 'Товары обновлены';
$_['text_tax_class']        = 'Данный способ оплаты предполагает НДС, который нужно выставить в <em>Система -> Локализация -> Налоги</em>. Если у вас облагаются соответствующим налогом не все товары, то Вы можете это исправить: обновите товары, выбрав нужный налоговый класс';
$_['text_success']          = 'Настройки модуля обновлены!';
$_['text_cashless']         = '<img src="view/image/payment/cashless.png" alt="безнал" title="Безналичный расчет" style="border: 1px solid #EEEEEE;" />';

$_['button_update']         = 'Обновить';
$_['atten_update']          = 'Убедитесь, что вы сделали резервную копию БД!';

// Entry
$_['entry_status']          = 'Статус:';
$_['entry_sort_order']      = 'Порядок сортировки:';
$_['entry_supplier_info']   = 'Поставщик:';
$_['entry_supplier_placeholder'] = 'Реквизиты банка';
$_['entry_VAT']             = 'Размер НДС:';
$_['entry_order_status']    = 'Статус заказа:';
$_['entry_stamp']           = 'Печать:';
$_['entry_who_write_bill']  = 'Кто выписывает счет';

// Error
$_['error_permission']      = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info']   = 'Это поле обязательно для заполнения!';