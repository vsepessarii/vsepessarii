<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
    <meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/reset.css">
    <link rel="stylesheet/less" type="text/css" href="catalog/view/theme/brief/less/stylesheet.less">
    <?php foreach ($styles as $style) { ?>
    <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <!-- <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/slideshow.css" media="screen" /> -->
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery.ui.all.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/search_suggestion.css" media="screen"/>
    <link href='http://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery.tinycarousel.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="catalog/view/theme/brief/js/brief.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/mask.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/search_suggestion.js"></script>
    <!--[if lt IE 10]>
    <script type="text/javascript" src="js/PIE.js"></script>
    <![endif]-->
    <script type="text/javascript" src="catalog/view/javascript/less-1.3.1.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
    <?php foreach ($scripts as $script) { ?>
    <script type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
</head>
<body>
<div class="page-wrapper">
<div class="page-container">
<nav class="top-line">
    <div class="layout-wrapper">
        <ul class="secondary-menu align-left">
            <li><?php if (!$logged) { ?>
            <?php echo $text_welcome; ?>
            <?php } else { ?>
            <?php echo $text_logged; ?>
            <?php } ?></li>
        </ul>

        <ul class="secondary-menu align-right">
           <?php echo $link_to_compare; ?>
            <!--
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
            <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
            -->
        </ul>
    </div>
</nav>

<header class="header">
    <div class="layout-wrapper">
        <?php if ($logo) { ?>
        <!--21:06:13--><div class="logo align-left"><a href="/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" ></a></div>
        <?php } ?>

        <div class="align-right">
            <?php echo $cart; ?>
            <?php //if ($telephone) { ?>
            <aside class="contacts-wrapper">
            <a href="<?php echo $contact; ?>">
            <p class="title">Наши <em>телефоны:</em></p>
            <p class="phones"><?php echo $telephone;?></p>
            <p class="phones"><?php echo $fax;?></p>
            </a>
            </aside>
            <?php //} ?>
        </div>
    </div>
</header>

<nav class="menu-wrapper">
    <div class="layout-wrapper">
        <ul class="main-menu">
            <li><a href="/">Главная</a></li>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
       <!-- <li><a href="<?php echo $about; ?>">О нас</a></li>
            <li><a href="#">Вопрос-Ответ</a></li>
            <li><a href="#">Доставка</a></li>-->
            <li><a href="<?php echo $faq; ?>">Статьи</a></li>
            <li class="action"><a href="<?php echo $special; ?>">Акции</a></li>
            <li><a href="<?php echo $contact; ?>">Контакты</a></li>
        </ul>

        <div id="search">
            <form action="" method="get">
            <input type="hidden" name="route" value="product/search">
            <?php if ($filter_name) { ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
            <?php } else { ?>
            <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#000000';"/>
            <?php } ?>
            <input type="submit" class="button-search" value="Поиск"></form>
        </div>
    </div>
</nav>

<div class="content-wrapper sidebar-left">
<div class="layout-wrapper"><div id="notification"></div>