<?php
	require '../connect.php';

	if ($ro_user['user_group_id'] != 1) die('Access denied');
?>
<style>
	table.products {
		border-collapse: collapse;
		border: 1px solid gray;
		margin: 10px;
	}

	table.products tr.head td {
		background-color: #333;
		color: #FFF;
		font-weight: bold;
	}
</style>
<table class="products" border="1" style="display: inline-block; vertical-align: top;">
	<tr class="head">
		<td width="300">Товар</td>
		<td width="80" align="center">Цена (вх.)</td>
		<td width="80" align="center">Кол-во</td>
		<td width="80" align="center">Цена (исх.)</td>
		<td width="80" align="center">Сумма</td>
	</tr>
<?php
	$qu_product = 'SELECT * FROM order_product WHERE order_id="'.intval($_GET['order_id']).'"';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT product.price_in, manufacturer.name FROM manufacturer, product WHERE product.product_id = "'.$ro_product['product_id'].'" && product.manufacturer_id = manufacturer.manufacturer_id'));
		$option = '';
		$qu_option = 'SELECT * FROM order_option WHERE order_product_id="'.$ro_product['order_product_id'].'"';
		$re_option = @mysqli_query($ddb, $qu_option);
		while ($ro_option = @mysqli_fetch_array($re_option)) {
			//$tmp = Result($ddb, 'SELECT * FROM product_option_value WHERE product_option_value_id='.$ro_option['product_option_value_id']);
			$option .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$ro_option['name'].': '.$ro_option['value'].(($ro_option['price'] > 0) ? ' ('.floatval($ro_option['price']).' руб.)' : '');

		}
		echo '
			<tr>
				<td><a href="/index.php?route=product/product&product_id='.$ro_product['product_id'].'" target="_blank">'.$ro_product['name'].' '.$ro_manufacturer['name'].' '.$ro_product['model'].$option.'</a></td>
				<td align="center">'.round($ro_product['price_in'],2).'</td>
				<td align="center">'.round($ro_product['quantity']).'</td>
				<td align="center">'.round($ro_product['price'],2).'</td>
				<td align="center">'.round($ro_product['total'],2).'</td>
			</tr>
		';
	}
?>	
</table>
<table class="products" border="1" style="display: inline-block; vertical-align: top;">
	<tr class="head">
		<td width="100" align="center">Дата</td>
		<td width="120" align="center">Статус</td>
		<td width="300">Комментарий</td>
		<td width="120" align="center">Пользователь</td>
	</tr>
<?php
	$qu_comment = 'SELECT * FROM order_history WHERE order_id="'.intval($_GET['order_id']).'" ORDER BY date_added';
	//echo $qu_comment;
	$re_comment = @mysqli_query($ddb, $qu_comment);
	echo mysqli_error($ddb);
	while ($ro_comment = @mysqli_fetch_array($re_comment)) {
		$ro_status = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM order_status WHERE order_status_id="'.$ro_comment['order_status_id'].'"'));
		$user = Result($ddb, 'SELECT * FROM user WHERE user_id = '.$ro_comment['user_id']);
		echo '
			<tr>
				<td align="center">'.date('d.m.Y H:i:s',strtotime($ro_comment['date_added'])).'</td>
				<td>'.$ro_status['name'].'</td>
				<td>'.nl2br($ro_comment['comment']).'</td>
				<td>'.(($user['user_id']) ? $user['username'].' ('.$user['user_id'].')' : 'Гость').'</td>
			</tr>
		';
	}
?>	
</table>