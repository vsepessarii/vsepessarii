<?php
					$LastModified_unix = strtotime($date_modified); 
					$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
					$IfModifiedSince = false; if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))	
					$IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));  
					if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))	
					$IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
					if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix)
						{	
						header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');	
						exit;
						}
					header('Last-Modified: '. $LastModified); ?>

<?php echo $header; ?>

<div class="content-container">
<?php
    if (!empty($this->request->get['path'])) {
        $parts = explode('_', $this->request->get['path']);
        $category_id = end($parts);
    }?>
<script type="text/javascript">
    rrApiOnReady.push(function() {
        try { rrApi.categoryView(<?php echo $category_id;?>); } catch(e) {}
    })
</script>
<section class="box">
<div class="box-heading">
    <!--21:01:03--><h1 class="align-left"><?php echo $heading_title; ?></h1>
    <div class="display">
        <span class="displayText"><?php echo $text_display; ?></span>
        <div class="gridListActive"></div>
        <a onclick="display('grid');">
            <div class="listView"></div>
        </a>
    </div>

    <div class="sort"><!--16:19:18--><div style="font-weight: bold; display: inline-block;"><?php echo $text_sort; ?></div>
        <select onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </div>
</div>


<nav class="breadcrumb">
    <?php
			$tmp = count($breadcrumbs);
			$i = 0; 
			foreach ($breadcrumbs as $breadcrumb) {
				$i++;
				echo $breadcrumb['separator'].(($i == $tmp) ? $breadcrumb['text'] : '<a href="'.$breadcrumb['href'].'">'.$breadcrumb['text'].'</a>' );
	    }
		?>
</nav>
<?php echo $_SESSION['text_compare'];?>

<div class="product-list">
    <?php $i=0; foreach ($products as $product) { $i++; ?>
    <div class="one-third<?php echo ($i % 3 == 0)? " last":"";?>">
    <article class="box-content product-item<?php echo ($product['special'])?" action":"";?>">

    <div class="left">
        <?php if ($product['thumb']) { ?>
        <div class="image">
            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>"/>
            </a>
        </div>
        <?php } ?>

        <div class="name" style="height: 40px;">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        </div>
        <div class="description"><?php echo $product['description']; ?></div>
    </div>

    <div class="right">
        <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
        <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="comp_<?php echo $product['product_id']; ?> <?php if($product['in_compare']) echo 'active'; ?>"><?php echo $button_compare; ?></a></div>

        <?php if (!$product['special']) { ?>
        <div class="price"> <?php echo $product['price']; ?> </div>
        <?php } else { ?>
        <div class="price-old"><?php echo intval($product['price']); ?></div>
        <div class="price"> <?php echo $product['special']; ?></div>
        <?php } ?>

        <?php /*if ($product['tax']) { ?>
        <br/>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } */?>
        <?php /*if ($product['rating']) { ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>"/></div>
        <?php } */?>
        <div class="cart"><a href="<?php echo $product['href']; ?>" class="button <?php if($this->session->data['cart'][$product['product_id']]>0) echo 'incart'; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>'); return false;" id="addtocart_<?php echo $product['product_id']; ?>">В корзину</a>
        </div>
    </div>
    </article>
    </div>
<?php } ?>
</div>

<div class="clear"></div>
<div class="pagination"><?php echo $pagination; ?></div>
<div class="clear"></div>  <br >

<?php
	if ($page < 2) {
?>
<div id="tabs" class="htabs">
    <a href="#tab-description">Описание</a>
    <a href="#tab-review">Отзывы (<?php echo $rev_num; ?>)</a>
</div>

<div id="tab-description" class="box-content" style="border-top: none;"><?php echo $description; ?></div>
<div id="tab-review" class="box-content" style="border-top: none;"><?php echo $review; ?></div>
<?php
	}
?>
<!--
<br /><br />
<?php if ($description) { ?>
<div class="box-content">
    <?php echo $description; ?>
</div>
<?php } ?>
-->

</section>
</div>

<?php echo $column_left; ?>
<?php echo $footer; ?>
