<!--div style="background: #f7f7f7; border: 1px solid #dddddd; padding: 10px; margin-bottom: 10px;"-->
<!--style>
	.yandex_money_button {
		opacity: 0.65;
	}	
	.yandex_money_button:hover {
		opacity: 1;
	}	
</style-->
<table width="100%" style="margin:0; padding:0;">
	<tr>
		<td width="70%" valign="top">
			<ol style="padding-left:20px; margin-bottom: 0 !important;">
				<li>Для перехода к оплате нажмите на кнопку «Оплатить Яндекс.Деньгами» справа (откроется в новом окне);</li>
				<li>Произведите платёж, затем <strong>нажмите кнопку в конце данной страницы "Подтверждение заказа"</strong>;</li>
				<li>Поступление средств контролируется оператором Интернет-магазина;</li>
				<li>Следите за изменением статуса после оплаты "История заказов >> Ваш заказ".</li>
			</ol>
		</td>
		<td width="30%" align="right" valign="middle" style="text-align:right !important; padding-top:23px;">
			<!--a href="index.php?route=payment/yandex_transfer/printpay" style="text-decoration:none;" target="_blank"><img class="yandex_money_button" src="/image/yandex.money.png" border="0" width="126" height="61"/></a-->
			<a href="index.php?route=payment/yandex_transfer/printpay" class="button red" style="text-decoration:none;" target="_blank"><span>Оплатить Яндекс.Деньгами</span></a>		
		</td>
	</tr>
</table>

<!--
<?php echo $text_instruction; ?><br /><br />

<a href="index.php?route=payment/yandex_transfer/printpay" class="button red" style="text-decoration:none;" target="_blank"><span><?php echo $text_printpay; ?></span></a>

  <br /><br />
  <?php echo $text_payment; ?>
-->	
	<!--/div-->

<div class="buttons" style="width:100% !important; float: right;"><a id="button-confirm" style="display:inline-block; float: right;" class="button"><span><?php echo $button_confirm; ?></span></a></div>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
	$.ajax({
		type: 'GET',
		url: 'index.php?route=payment/yandex_transfer/confirm',
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
//--></script>
