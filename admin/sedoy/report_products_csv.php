<?php
	//отчет по товарам
	session_start();
	
	require 'connect.php';

	if ($_GET['p'] == '') $_GET['p'] = 1;

	if ($ro_user['user_group_id'] == 1) {
		$br = chr(13).chr(10);
		$ret = '"ID";"Название";"Производитель";"Модель";"Артикул";"Категория";"Подкатегория";"Просмотров";"% от всех";"Вх. цена";"Исх. цена";"Количество (купленных)";"Итого";"Прибыль";"%"'.$br;
		header('Content-Type: csv/plain; charset=cp-1251');
		header('Content-Disposition: attachment; filename="report_products_'.date('Y-m-d H-i-s').'.csv"');
		$qu_product = '
			SELECT	*
			FROM 		tmp_report_product
			WHERE		1
							'.(($_SESSION['report_products_id'] != '') ? ' && `ID` LIKE "%'.$_SESSION['report_products_id'].'%"' : '').'
							'.(($_SESSION['report_products_name'] != '') ? ' && `NAME` LIKE "%'.$_SESSION['report_products_name'].'%"' : '').'
							'.(($_SESSION['report_products_manufacturer'] != '') ? ' && `MANUFACTURER` LIKE "%'.$_SESSION[''].'%"' : '').'
							'.(($_SESSION['report_products_model'] != '') ? ' && `MODEL` LIKE "%'.$_SESSION['report_products_model'].'%"' : '').'
							'.(($_SESSION['report_products_articul'] != '') ? ' && `ARTICUL` LIKE "%'.$_SESSION['report_products_articul'].'%"' : '').'
							'.(($_SESSION['report_products_category'] != '') ? ' && `CATEGORY`="'.$_SESSION['report_products_category'].'"' : '').'
							'.(($_SESSION['report_products_subcategory'] != '') ? ' && `SUBCATEGORY`="'.$_SESSION['report_products_subcategory'].'"' : '').'
							'.(($_SESSION['report_products_view_from'] != '') ? ' && `VIEW`>="'.$_SESSION['report_products_view_from'].'"' : '').'
							'.(($_SESSION['report_products_view_to'] != '') ? ' && `VIEW`<="'.$_SESSION['report_products_view_to'].'"' : '').'
							'.(($_SESSION['report_products_view_percent_from'] != '') ? ' && `VIEW_PERCENT`<="'.$_SESSION['report_products_view_percent_from'].'"' : '').'
							'.(($_SESSION['report_products_view_percent_to'] != '') ? ' && `VIEW_PERCENT`>="'.$_SESSION['report_products_view_percent_to'].'"' : '').'
							'.(($_SESSION['report_products_price_in_from'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_products_price_in_from'].'"' : '').'
							'.(($_SESSION['report_products_price_in_to'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_products_price_in_to'].'"' : '').'
							'.(($_SESSION['report_products_price_out_from'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_products_price_out_from'].'"' : '').'
							'.(($_SESSION['report_products_price_out_to'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_products_price_out_to'].'"' : '').'
							'.(($_SESSION['report_products_quantity_from'] != '') ? ' && `QUANTITY`>="'.$_SESSION['report_products_quantity_from'].'"' : '').'
							'.(($_SESSION['report_products_quantity_to'] != '') ? ' && `QUANTITY`<="'.$_SESSION['report_products_quantity_to'].'"' : '').'
							'.(($_SESSION['report_products_total_from'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_products_total_from'].'"' : '').'
							'.(($_SESSION['report_products_total_to'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_products_total_to'].'"' : '').'
							'.(($_SESSION['report_products_profit_from'] != '') ? ' && `PROFIT`>="'.$_SESSION['report_products_profit_from'].'"' : '').'
							'.(($_SESSION['report_products_profit_to'] != '') ? ' && `PROFIT`<="'.$_SESSION['report_products_profit_to'].'"' : '').'
							'.(($_SESSION['report_products_profit_percent_from'] != '') ? ' && `PROFIT_PERCENT`>="'.$_SESSION['report_products_profit_percent_from'].'"' : '').'
							'.(($_SESSION['report_products_profit_percent_to'] != '') ? ' && `PROFIT_PERCENT`<="'.$_SESSION['report_products_profit_percent_to'].'"' : '').'
			ORDER	BY	ID ASC
		';

		$re_product = @mysqli_query($ddb, $qu_product);
		$summ['quantity'] = 0;
		$summ['total'] = 0;
		$summ['profit'] = 0;
		$summ['profit_percent'] = 0;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$summ['quantity'] += $ro_product['QUANTITY'];
			$summ['total'] += $ro_product['TOTAL'];
			$summ['profit'] += $ro_product['PROFIT'];
			$summ['profit_percent'] += $ro_product['PROFIT_PERCENT'];
			$ret .= '"'.$ro_product['ID'].'";"'.$ro_product['NAME'].'";"'.$ro_product['MANUFACTURER'].'";"'.$ro_product['MODEL'].'";"'.$ro_product['ARTICUL'].'";"'.
							$ro_product['CATEGORY'].'";"'.$ro_product['SUBCATEGORY'].'";"'.str_replace('.',',',$ro_product['VIEW']).'";"'.str_replace('.',',',$ro_product['VIEW_PERCENT']).'";"'.
							str_replace('.',',',$ro_product['PRICE_IN']).'";"'.str_replace('.',',',$ro_product['PRICE_OUT']).'";"'.str_replace('.',',',$ro_product['QUANTITY']).'";"'.
							str_replace('.',',',$ro_product['TOTAL']).'";"'.str_replace('.',',',$ro_product['PROFIT']).'";"'.str_replace('.',',',$ro_product['PROFIT_PERCENT']).'"'.$br;			
		}
		//$summ['profit_percent'] = @round($summ['profit_percent']/$nu_order_total,2);
		echo iconv('utf-8','cp1251',$ret);
	} else {
		header('Location: /');
	}

