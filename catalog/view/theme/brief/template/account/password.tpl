<?php echo $header; ?>

				<?php //echo $column_right; ?>
				<div class="content-container">

					<section class="box">
						<!--20:51:49--><div class="box-heading"><?php echo $heading_title; ?></div>

						<nav class="breadcrumb">
						    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
						    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
						    <?php } ?>
    					</nav>


						<div class="box-content" style="margin-top: 30px !important;">
						  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						    <h2><?php echo $text_password; ?></h2>
						    <div class="content">
						      <table class="form">
						        <tr>
						          <td><span class="required">*</span> <?php echo $entry_password; ?></td>
						          <td><input type="password" name="password" value="<?php echo $password; ?>" />
						            <?php if ($error_password) { ?>
						            <span class="error"><?php echo $error_password; ?></span>
						            <?php } ?></td>
						        </tr>
						        <tr>
						          <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
						          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
						            <?php if ($error_confirm) { ?>
						            <span class="error"><?php echo $error_confirm; ?></span>
						            <?php } ?></td>
						        </tr>
						      </table>
						    </div>
						    <div class="buttons">
						      <div class="left"><a href="<?php echo $back; ?>" class="button gray align-left"><?php echo $button_back; ?></a></div>
						      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button blue align-left" /></div>
						    </div>
						    <div class="clear"></div>
						  </form>
  						</div>
					</section>

				<?php //echo $content_top; ?>
				<?php //echo $content_bottom; ?>
				</div>
				<?php echo $column_left; ?><?php echo $column_right; ?>

<?php echo $footer; ?>

<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--20:55:31--><div><?php echo $heading_title; ?></div>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2><?php echo $text_password; ?></h2>
    <div class="content">
      <table class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_password; ?></td>
          <td><input type="password" name="password" value="<?php echo $password; ?>" />
            <?php if ($error_password) { ?>
            <span class="error"><?php echo $error_password; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_confirm; ?></td>
          <td><input type="password" name="confirm" value="<?php echo $confirm; ?>" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */?>