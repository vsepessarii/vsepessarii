<?
	header('Content-Type: text/xml; charset=utf-8');
	$ddb = @mysqli_connect('localhost','briefmag_brief','brief2013');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	@mysqli_select_db('briefmag_db');
	echo '<?xml version="1.0" encoding="UTF-8"?>';	
?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?=date('Y-m-d H:i')?>">
	<shop>
		<name>Brief Medicine</name>
		<company>Brief Medicine</company>
		<url>https://vsepessarii.ru/</url>
		<phone>+7 495 6603423</phone>
		<platform>ocStore</platform>
		<version>1.5.4.1</version>
		<currencies>
			<currency id="RUB" rate="1"/>
			<currency id="EUR" rate="40"/>
			<currency id="USD" rate="30.7692"/>
		</currencies>
		<categories>
			<category id="1" parentId="0">Кондиционеры</category>		
<?
	$qu_category = 'SELECT category_description.category_id as id, category_description.name as name, category.parent_id as parent FROM category, category_description WHERE category.category_id=category_description.category_id ORDER BY category.category_id';
	$re_category = @mysqli_query($ddb, $qu_category);
	while ($ro_category = @mysqli_fetch_array($re_category)) {
?>
			<category id="<?=$ro_category['id']?>" parentId="<?=$ro_category['parent']?>"><?=$ro_category['name']?></category>	
<?
	}
?>		
		</categories>
		<offers>
<?
	$qu_product = 'SELECT product_id, model, sku, image, manufacturer_id, price FROM product WHERE status="1" ORDER BY product_id';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_product_add = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name, description FROM product_description WHERE product_id="'.$ro_product['product_id'].'"'));
		$ro_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM manufacturer WHERE manufacturer_id="'.$ro_product['manufacturer_id'].'"'));
		$ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_product['product_id'].'" && main_category="0"'));
		if ($ro_category['category_id'] < 1) $ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_product['product_id'].'" && main_category="1"')); 
		
		$ro_product_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="product_id='.$ro_product['product_id'].'"'));
		$url = $ro_product_url['keyword'].'.html';
		if ($ro_category['category_id'] > 0) {
			$ro_url_cat_1 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_category['category_id'].'"'));
			$url = $ro_url_cat_1['keyword'].'/'.$url;
			$ro_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_category['category_id'].'"'));
			if ($ro_cat_2['parent_id'] > 0) {
				$ro_url_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_2['parent_id'].'"'));
				$url = $ro_url_cat_2['keyword'].'/'.$url;
				$ro_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_cat_2['parent_id'].'"'));
				if ($ro_cat_3['parent_id'] > 0) {
					$ro_url_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_3['parent_id'].'"'));
					$url = $ro_url_cat_3['keyword'].'/'.$url;
				}
			}
		}
		$url = 'https://vsepessarii.ru/'.$url;

		$id = $ro_product['product_id'];
		$price = $ro_product['price'];
		$mpc = round($price/100,2);
		$category_id = $ro_category['category_id'];
		$picture = 'https://vsepessarii.ru/image/'.$ro_product['image'];
		$name = $ro_product_add['name'].' '.$ro_manufacturer['name'].' '.$ro_product['model'];
		$vendor = $ro_manufacturer['name'];
		$vendor_code = $ro_product['model'];
		$description = htmlspecialchars(strip_tags(htmlspecialchars_decode($ro_product_add['description'])));

?>		
			<offer id="<?=$id?>" available="true">
				<url><?=$url;?></url>
				<price><?=$price;?></price>
				<currencyId>RUB</currencyId>
				<categoryId><?=$category_id;?></categoryId>
				<picture><?=$picture;?></picture>
				<delivery>true</delivery>
				<name><?=$name?></name>
				<vendor><?=$vendor;?></vendor>
				<vendorCode><?=$vendor_code;?></vendorCode>
				<description><?=$description?></description>
			</offer>
<?
	}
?>
		</offers>
	</shop>
</yml_catalog>