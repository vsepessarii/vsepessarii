<?php
	require 'connect.php';
	require 'functions.php';
	error_reporting(0);
	mb_internal_encoding('utf-8');
	ini_set('display_errors', FALSE);
	ini_set('display_startup_errors', FALSE);
	
	if ($ro_user['user_group_id'] != 1) die('Access denied');
	
	/*
	var_dump($_POST['selected']);
	die();
	*/


	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	date_default_timezone_set('Europe/London');

	/** PHPExcel_IOFactory */
	require_once 'class/PHPExcel/IOFactory.php';

	//echo date('H:i:s') , " Load from Excel5 template" , EOL;

	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("templates/ans_express.xls");

//	die('123');




	//$objPHPExcel->setActiveSheetIndex(0);

	//$objPHPExcel->getActiveSheet()->setCellValue('BC40', '77.20');
	$i = 3;
	foreach ($_POST['selected'] as $order_id) {
		$order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($order_id).'"');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, date('d.m.Y', strtotime($order['date_shipped'])));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $order['time_shipped_from']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $order['time_shipped_to']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'P-'.$order['order_id']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $order['shipping_address_1']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $order['shipping_firstname']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $order['telephone']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $order['total']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $order['pack']);
		$i++;
	}
	
	/*	
	$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');
	
	$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
	$nu_product = @mysqli_num_rows(@mysqli_query($ddb, $qu_product));
	$re_product = @mysqli_query($ddb, $qu_product);

	$order_date = date('d').' '.$MonthRod[date('m')].' '.date('Y').' г.';
	$client = $ro_order['firstname'].' '.$ro_order['lastname'].' '.$ro_order['telephone'].' '.$ro_order['shipping_city'].', '.$ro_order['shipping_address_1']; 

	$objPHPExcel->getActiveSheet()->setCellValue('AG28', $ro_order['order_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('AY21', $ro_order['order_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('AY22', $order_date);
	$objPHPExcel->getActiveSheet()->setCellValue('AL28', $order_date);
	$objPHPExcel->getActiveSheet()->setCellValue('I12', $client);
	$objPHPExcel->getActiveSheet()->setCellValue('I18', 'тот же');

	//увеличиваем количество товарных строк, если надо
	if ($nu_product >1) {
		$objPHPExcel->getActiveSheet()->insertNewRowBefore(35,$nu_product-1);
	
		for ($i=35;$i<35+$nu_product;$i++) {
			$objPHPExcel->getActiveSheet()->mergeCells('BC'.$i.':BF'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AY'.$i.':BB'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AV'.$i.':AX'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AS'.$i.':AU'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AP'.$i.':AR'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AM'.$i.':AO'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AJ'.$i.':AL'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AH'.$i.':AI'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AE'.$i.':AG'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AB'.$i.':AD'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('Y'.$i.':AA'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('V'.$i.':X'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('S'.$i.':U'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':R'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$i.':D'.$i);
			//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle ('BC34'), 'BC'.$i);
			//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle ('B34:BC34'), 'B'.$i.':BC'.$i);
		}
	}
	
	//заполняем товарные строки
	$i = 34;
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_prod = Result($ddb, 'SELECT * FROM product WHERE product_id = "'.$ro_product['product_id'].'"');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $i-33); //номер по порядку
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, ProductTitle($ro_product['product_id'])); //наименование товара
		//$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $ro_prod['sku']); //код товара
		$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, '—'); //код товара
		$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, 'шт'); //наименование ЕИ
		$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, '796'); //код ЕИ
		$objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, '—'); //вид упаковки
		$objPHPExcel->getActiveSheet()->setCellValue('AE'.$i, '—'); //кол-во в одном месте
		$objPHPExcel->getActiveSheet()->setCellValue('AH'.$i, '—'); //кол-во мест
		$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$i, '—'); //масса брутто
		$objPHPExcel->getActiveSheet()->setCellValue('AM'.$i, $ro_product['quantity']); //масса нетто
		$objPHPExcel->getActiveSheet()->setCellValue('AP'.$i, $ro_product['price']); //цена
		//$objPHPExcel->getActiveSheet()->setCellValue('AS'.$i, 2400.10); //сумма без НДС
		$objPHPExcel->getActiveSheet()->setCellValue('AS'.$i, '=AP'.$i.'*AM'.$i); //сумма без НДС
		$objPHPExcel->getActiveSheet()->setCellValue('AV'.$i, 'Без НДС'); //ставка НДС
		$objPHPExcel->getActiveSheet()->setCellValue('AY'.$i, 0); //сумма НДС
		$objPHPExcel->getActiveSheet()->setCellValue('BC'.$i, '=AS'.$i); //сумма с НДС
		$i++;
	}

	//echo date('H:i:s') , " Add new data to the template" , EOL;	
	
	//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle ('B34:BC34'), 'B35:BC39');


	//считаем суммы
	$objPHPExcel->getActiveSheet()->setCellValue('AH'.(34+$nu_product), '=SUM(AH34:AI'.(33+$nu_product).')'); //мест
	$objPHPExcel->getActiveSheet()->setCellValue('AJ'.(34+$nu_product), '=SUM(AJ34:AL'.(33+$nu_product).')'); //масса брутто
	$objPHPExcel->getActiveSheet()->setCellValue('AM'.(34+$nu_product), '=SUM(AM34:AO'.(33+$nu_product).')'); //масса нетто

	$objPHPExcel->getActiveSheet()->setCellValue('AS'.(34+$nu_product), '=SUM(AS34:AU'.(33+$nu_product).')'); //сумма без НДС
	$objPHPExcel->getActiveSheet()->setCellValue('AY'.(34+$nu_product), '=SUM(AY34:BB'.(33+$nu_product).')'); //сумма  НДС
	$objPHPExcel->getActiveSheet()->setCellValue('BC'.(34+$nu_product), '=SUM(BC34:BF'.(33+$nu_product).')'); //сумма c НДС

	//сумма прописью
	$objPHPExcel->getActiveSheet()->setCellValue('K'.(46+$nu_product), mb_ucfirst(Summ2Str($objPHPExcel->getActiveSheet()->getCell('BC'.(34+$nu_product))->getCalculatedValue())).'. Без НДС.'); //сумма c НДС
	//$objPHPExcel->getActiveSheet()->setCellValue('AA'.(39+$nu_product), mb_ucfirst(Weight2Str($objPHPExcel->getActiveSheet()->getCell('AM'.(34+$nu_product))->getCalculatedValue()))); //масса нетто
	//$objPHPExcel->getActiveSheet()->setCellValue('AA'.(41+$nu_product), mb_ucfirst(Weight2Str($objPHPExcel->getActiveSheet()->getCell('AJ'.(34+$nu_product))->getCalculatedValue()))); //масса брутто

	$objPHPExcel->getActiveSheet()->setCellValue('R'.(36+$nu_product), mb_ucfirst($NumPred['1'])); //количество листов
	$objPHPExcel->getActiveSheet()->setCellValue('G'.(37+$nu_product), mb_ucfirst(Num2Str($nu_product))); //количество строк
	$objPHPExcel->getActiveSheet()->setCellValue('G'.(41+$nu_product), mb_ucfirst(Num2Str($objPHPExcel->getActiveSheet()->getCell('AM'.(34+$nu_product))->getCalculatedValue()))); //количество мест

	$objPHPExcel->getActiveSheet()->setCellValue('I'.(56+$nu_product), date('d/m/Y'));
	*/

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="vsepessarii_ans_express_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	//echo date('H:i:s') , " Write to Excel5 format" , EOL;
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('tmp/tmp.xls');
	echo file_get_contents('tmp/tmp.xls');
	unlink('tmp/tmp.xls');
	/*
	$objWriter->save('torg12_'.date('H-i-s').'.xls');
	echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
	echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
	echo date('H:i:s') , " Done writing file" , EOL;
	echo 'File has been created in ' , getcwd() , EOL;
	*/

