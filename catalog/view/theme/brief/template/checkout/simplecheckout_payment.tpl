<div class="simplecheckout-block-heading box-heading"><?php echo $text_checkout_payment_method ?></div>
<?php if ($simple_create_order && $error_warning) { ?>
	<div class="simplecheckout-warning-block"><?php echo $error_warning ?></div>
<?php } ?>  
<div class="simplecheckout-block-content">
	<?php if (!empty($disabled_methods)) { ?>
		<table class="simplecheckout-methods-table" style="margin-bottom:0px;">
			<?php foreach ($disabled_methods as $key => $value) { ?>
				<tr>
					<td class="code">
						<input type="radio" name="disabled_payment_method" disabled="disabled" value="<?php echo $key; ?>" id="<?php echo $key; ?>" />
					</td>
					<td class="title">
						<label for="<?php echo $key; ?>">
							<?php echo $value['title']; ?>
						</label>
					</td>
				</tr>
				<?php if (!empty($value['description'])) { ?>
					<tr>
						<td class="code">
						</td>
						<td class="title">
							<label for="<?php echo $key; ?>"><?php echo $value['description']; ?></label>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		</table>
	<?php } ?>
	<?php if (!empty($payment_methods)) { ?>
				<table class="simplecheckout-methods-table">
			<?php foreach ($payment_methods as $payment_method) {
				/*
				#$country = $_SESSION['simple']['checkout_customer']['main_country_id'];
				$country = $_SESSION['sedoy_country'];
				$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT country_id FROM country WHERE name="'.mysqli_escape_string($country).'"'));
				$country_id = $tmp['country_id'];
				#$city = $_SESSION['simple']['checkout_customer']['main_city'];
				$city = $_SESSION['sedoy_city'];
				if (@mysqli_num_rows(@mysqli_query($ddb, 'SELECT city_id FROM city WHERE country_id="'.$country_id.'" && city="'.mysqli_escape_string($city).'"')) == 0) $city = ''; //костылик для проверки соответствия города стране, ибо очистить город не получается
				$pay_code = $payment_method['code'];
							
				if ($pay_code == 'cod') {
					if ($country != 'Россия') continue;
					if ($city != 'Москва' && $city != 'Санкт-Петербург') continue;
				} elseif ($pay_code == 'cashless') {
					if ($country != 'Россия' || $city == '') continue;
				} elseif ($pay_code == 'yandexplusplus') {
					if ($country == '' || $city == '') continue;
				} elseif ($pay_code == 'yandexplusplus_card') {
					if ($country != 'Россия' || $city == '') continue;								
				} elseif ($pay_code == 'sberbank_transfer') {
					if ($country == '' || $city == '') continue;								
				} else {
					continue;
				}
				*/
			?>
				<tr>
					<td class="code">
						<input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" <?php if ($payment_method['code'] == $code) { ?>checked="checked"<?php } ?> onchange="simplecheckout_reload('payment_changed')" />
					</td>
					<td class="title">
						<label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label>
					</td>
				</tr>
				<?php if (!empty($payment_method['description'])) { ?>
					<tr>
						<td class="code">
						</td>
						<td class="title">
							<label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['description']; ?></label>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
				<?php
					/*
					if ($country == '' || $city == '') echo '
						<tr>
							<td style="text-align:center;">Выбор способа оплаты будет доступен<br />после выбора страны и города доставки.</td>
						</tr>
					';
					*/
				?>
			
		</table>
		<input type="hidden" name="payment_method_current" value="<?php echo $code ?>" />
		<input type="hidden" name="payment_method_checked" value="<?php echo $checked_code ?>" />
	<?php } ?>
	<?php if (empty($payment_methods) && $address_empty && $simple_payment_view_address_empty) { ?>
		<div class="simplecheckout-warning-text"><?php echo $text_payment_address; ?></div>
	<?php } ?>
	<?php if (empty($payment_methods) && !$address_empty) { ?>
		<div class="simplecheckout-warning-text"><?php echo $error_no_payment; ?></div>
	<?php } ?>
</div>
<?php if ($simple_debug) print_r($address); ?>