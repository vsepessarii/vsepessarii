<?php
	//отчет по товарам с привязкой к заказам
	/*
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	//var_dump($_SESSION);
	$ddb = @mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmed_brief');
	
	function Result($ddb, $qu) {
		return @mysqli_fetch_array(@mysqli_query($ddb, $qu));
	}
	
	$ro_user = Result($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"');
	*/

	require 'connect.php';
	
	if ($ro_user['user_group_id'] == 1) {
		if (!isset($_SESSION['report_orders_per_page']) || $_SESSION['report_orders_per_page'] == '') {
			$_SESSION['report_products_per_page'] = 25;
		}
		if (isset($_GET['s']) && ($_GET['s'] == '')) $_GET['s'] = '`OID` DESC';
		if ($_SESSION['courier_price'] == '') $_SESSION['courier_price'] = 250;
		if (isset($_SESSION['report_orders_products_per_page']) && ($_SESSION['report_orders_products_per_page'] == '')) $_SESSION['report_orders_products_per_page'] = 25;
		
		if ($_POST['act'] == 'clear_filter') {
			$_SESSION['report_orders_products_pid'] = ''; //идентификатор товара
			$_SESSION['report_orders_products_oid'] = ''; //идентификатор заказа 
			$_SESSION['report_orders_products_date_min'] = ''; //дата начала периода 
			$_SESSION['report_orders_products_date_max'] = ''; //дата окончания периода
			$_SESSION['report_orders_products_art'] = ''; //артикул						
			$_SESSION['report_orders_products_name'] = ''; //название
			$_SESSION['report_orders_products_manufacturer'] = ''; //производитель
			$_SESSION['report_orders_products_model'] = ''; //модель
			$_SESSION['report_orders_products_option_1'] = ''; //опция 1
			$_SESSION['report_orders_products_option_2'] = ''; //опция 2
			$_SESSION['report_orders_products_option_3'] = ''; //опция 3
			$_SESSION['report_orders_products_category'] = ''; //категория
			$_SESSION['report_orders_products_subcategory'] = ''; //подкатегория
			$_SESSION['report_orders_products_price_in_min'] = ''; //цена входящая от
			$_SESSION['report_orders_products_price_in_max'] = ''; //цена входящая до
			$_SESSION['report_orders_products_price_out_min'] = ''; //цена продажи от
			$_SESSION['report_orders_products_price_out_max'] = ''; //цена продажи до
			$_SESSION['report_orders_products_quantity_min'] = ''; //количество от
			$_SESSION['report_orders_products_quantity_max'] = ''; //количество до
			$_SESSION['report_orders_products_total_min'] = ''; //итого от
			$_SESSION['report_orders_products_total_max'] = ''; //итого до
			$_SESSION['report_orders_products_margin_min'] = ''; //прибыль от
			$_SESSION['report_orders_products_margin_max'] = ''; //прибыль до
			$_SESSION['report_orders_products_margin_percent_min'] = ''; //прибыль в процентах от
			$_SESSION['report_orders_products_margin_percent_max'] = ''; //прибыль в процентах до
			$_SESSION['report_orders_products_city'] = ''; //город
		}
		
		if ($_POST['act'] == 'setting') {
			/*
			$_SESSION['report_products_date_from'] = $_POST['report_products_date_from'];
			$_SESSION['report_products_date_to'] = $_POST['report_products_date_to'];
			*/
			$_SESSION['report_orders_products_status'] = $_POST['report_orders_products_status'];
			$_SESSION['report_orders_products_per_page'] = $_POST['report_orders_products_per_page'];
		}

		if ($_POST['act'] == 'filter') {
			$_SESSION['report_orders_products_pid'] = isset($_POST['report_orders_products_pid']) ? $_POST['report_orders_products_pid'] : ''; //идентификатор товара
			$_SESSION['report_orders_products_oid'] = isset($_POST['report_orders_products_oid']) ? $_POST['report_orders_products_oid'] : ''; //идентификатор заказа
			$_SESSION['report_orders_products_date_min'] = isset($_POST['report_orders_products_date_min']) ? $_POST['report_orders_products_date_min'] : ''; //дата начала периода
			$_SESSION['report_orders_products_date_max'] = isset($_POST['report_orders_products_date_max']) ? $_POST['report_orders_products_date_max'] : ''; //дата окончания периода
			$_SESSION['report_orders_products_art'] = isset($_POST['report_orders_products_art']) ? $_POST['report_orders_products_art'] : ''; //артикул
			$_SESSION['report_orders_products_name'] = isset($_POST['report_orders_products_name']) ? $_POST['report_orders_products_name'] : ''; //название
			$_SESSION['report_orders_products_manufacturer'] = isset($_POST['report_orders_products_manufacturer']) ? $_POST['report_orders_products_manufacturer'] : ''; //производитель
			$_SESSION['report_orders_products_model'] = isset($_POST['report_orders_products_model']) ? $_POST['report_orders_products_model'] : ''; //модель
			$_SESSION['report_orders_products_option_1'] = isset($_POST['report_orders_products_option_1']) ? $_POST['report_orders_products_option_1'] : ''; //опция 1
			$_SESSION['report_orders_products_option_2'] = isset($_POST['report_orders_products_option_2']) ? $_POST['report_orders_products_option_2'] : ''; //опция 2
			$_SESSION['report_orders_products_option_3'] = isset($_POST['report_orders_products_option_3']) ? $_POST['report_orders_products_option_3'] : ''; //опция 3
			$_SESSION['report_orders_products_category'] = isset($_POST['report_orders_products_category']) ? $_POST['report_orders_products_category'] : ''; //категория
			$_SESSION['report_orders_products_subcategory'] = isset($_POST['report_orders_products_subcategory']) ? $_POST['report_orders_products_subcategory'] : ''; //подкатегория
			$_SESSION['report_orders_products_price_in_min'] = isset($_POST['report_orders_products_price_in_min']) ? $_POST['report_orders_products_price_in_min'] : ''; //цена входящая от
			$_SESSION['report_orders_products_price_in_max'] = isset($_POST['report_orders_products_price_in_max']) ? $_POST['report_orders_products_price_in_max'] : ''; //цена входящая до
			$_SESSION['report_orders_products_price_out_min'] = isset($_POST['report_orders_products_price_out_min']) ? $_POST['report_orders_products_price_out_min'] : ''; //цена продажи от
			$_SESSION['report_orders_products_price_out_max'] = isset($_POST['report_orders_products_price_out_max']) ? $_POST['report_orders_products_price_out_max'] : ''; //цена продажи до
			$_SESSION['report_orders_products_quantity_min'] = isset($_POST['report_orders_products_quantity_min']) ? $_POST['report_orders_products_quantity_min'] : ''; //количество от
			$_SESSION['report_orders_products_quantity_max'] = isset($_POST['report_orders_products_quantity_max']) ? $_POST['report_orders_products_quantity_max'] : ''; //количество до
			$_SESSION['report_orders_products_total_min'] = isset($_POST['report_orders_products_total_min']) ? $_POST['report_orders_products_total_min'] : ''; //итого от
			$_SESSION['report_orders_products_total_max'] = isset($_POST['report_orders_products_total_max']) ? $_POST['report_orders_products_total_max'] : ''; //итого до
			$_SESSION['report_orders_products_margin_min'] = isset($_POST['report_orders_products_margin_min']) ? $_POST['report_orders_products_margin_min'] :''; //прибыль от
			$_SESSION['report_orders_products_margin_max'] = isset($_POST['report_orders_products_margin_max']) ? $_POST['report_orders_products_margin_max'] : ''; //прибыль до
			$_SESSION['report_orders_products_margin_percent_min'] = isset($_POST['report_orders_products_margin_percent_min']) ? $_POST['report_orders_products_margin_percent_min'] : ''; //прибыль в процентах от
			$_SESSION['report_orders_products_margin_percent_max'] = isset($_POST['report_orders_products_margin_percent_max']) ? $_POST['report_orders_products_margin_percent_max'] :''; //прибыль в процентах до
			$_SESSION['report_orders_products_city'] = isset($_POST['report_orders_products_city']) ? $_POST['report_orders_products_city'] : ''; //артикул
			$_GET['p'] = 1;
		}

		if (isset($_POST['db_not_update']) && $_POST['db_not_update'] == '') {

		@mysqli_query($ddb, 'TRUNCATE TABLE tmp_report_orders_products');
		
		$qu_product = '
			SELECT		`product`.`product_id` as `PID`,
								`order`.`order_id` as `OID`,
								`order_product`.`order_product_id` as `OPID`,
								`order`.`date_added` as `DATE`,
								`order`.`payment_city` as `CITY`,
								`product`.`sku` as `ART`,
								`product_description`.`name` as `NAME`,
								`manufacturer`.`name` as `MANUFACTURER`,
								`product`.`model` as `MODEL`,
								`order_product`.`price_in` as `PRICE_IN`,
								`order_product`.`price` as `PRICE_OUT`,
								`order_product`.`quantity` as `QUANTITY`,
								`order_product`.`price`*`order_product`.`quantity` as `TOTAL`,
								`order_product`.`price`*`order_product`.`quantity`*0.94-`order_product`.`price_in`*`order_product`.`quantity` as `MARGIN`,
								(`order_product`.`price`*`order_product`.`quantity`*0.94-`order_product`.`price_in`*`order_product`.`quantity`)*100/(`order_product`.`price`*`order_product`.`quantity`) as `MARGIN_PERCENT`,
								`order`.`order_status_id` AS `STATUS`
			FROM			`order`,
								`order_product`,
								`product_description`,
								`product` LEFT JOIN `manufacturer` ON `product`.`manufacturer_id` = `manufacturer`.`manufacturer_id`
			WHERE			`order`.`order_id` = `order_product`.`order_id` &&
								`order_product`.`product_id` = `product`.`product_id` &&
								`product`.`product_id` = `product_description`.`product_id`
								/*
								'.((isset($_SESSION['report_orders_products_pid']) && $_SESSION['report_orders_products_pid'] != '') ? ' && `product`.`product_id` LIKE "%'.$_SESSION['report_orders_products_pid'].'%"' : '').' 	
								'.((isset($_SESSION['report_orders_products_oid']) && $_SESSION['report_orders_products_oid'] != '') ? ' && `order`.`order_id` LIKE "%'.$_SESSION['report_orders_products_oid'].'%"' : '').' 	
								'.((isset($_SESSION['report_orders_products_date_min']) && $_SESSION['report_orders_products_date_min'] != '') ? ' && `order`.`date_added`>="'.$_SESSION['report_orders_products_date_min'].' 00:00:00"' : '').' 	
								'.((isset($_SESSION['report_orders_products_date_max']) && $_SESSION['report_orders_products_date_max'] != '') ? ' && `order`.`date_added`<="'.$_SESSION['report_orders_products_date_max'].' 23:59:59"' : '').' 	
								'.((isset($_SESSION['report_orders_products_art']) && $_SESSION['report_orders_products_art'] != '') ? ' && `product`.`sku` LIKE "%'.$_SESSION['report_orders_products_art'].'%"' : '').' 	
								'.((isset($_SESSION['report_orders_products_name']) && $_SESSION['report_orders_products_name'] != '') ? ' && `product_description`.`name` LIKE "%'.$_SESSION['report_orders_products_name'].'%"' : '').' 	
								'.((isset($_SESSION['report_orders_products_manufacturer']) && $_SESSION['report_orders_products_manufacturer'] != '') ? ' && `manufacturer`.`name`="'.$_SESSION['report_orders_products_manufacturer'].'"' : '').' 	
								'.((isset($_SESSION['report_orders_products_model']) && $_SESSION['report_orders_products_model'] != '') ? ' && `product`.`model` LIKE "%'.$_SESSION['report_orders_products_model'].'%"' : '').' 	
								'.((isset($_SESSION['report_orders_products_price_in_min']) && $_SESSION['report_orders_products_price_in_min'] != '') ? ' && `order_product`.`price_in`>="'.$_SESSION['report_orders_products_price_in_min'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_price_in_max']) && $_SESSION['report_orders_products_price_in_max'] != '') ? ' && `order_product`.`price_in`<="'.$_SESSION['report_orders_products_price_in_max'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_price_out_min']) && $_SESSION['report_orders_products_price_out_min'] != '') ? ' && `order_product`.`price`>="'.$_SESSION['report_orders_products_price_out_min'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_price_out_max']) && $_SESSION['report_orders_products_price_out_max'] != '') ? ' && `order_product`.`price`<="'.$_SESSION['report_orders_products_price_out_max'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_quantity_min']) && $_SESSION['report_orders_products_quantity_min'] != '') ? ' && `order_product`.`quantity`>="'.$_SESSION['report_orders_products_quantity_min'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_quantity_max']) && $_SESSION['report_orders_products_quantity_max'] != '') ? ' && `order_product`.`quantity`<="'.$_SESSION['report_orders_products_quantity_max'].'"' : '').'
								'.((isset($_SESSION['report_orders_products_status']) && $_SESSION['report_orders_products_status']) ? ' && `order`.`order_status_id`="'.$_SESSION['report_orders_products_status'].'"' : '').' 	
								'.((isset($_SESSION['report_orders_products_city']) && $_SESSION['report_orders_products_city'] != '') ? ' && `order`.`payment_city`="'.$_SESSION['report_orders_products_city'].'"' : '').'
								*/
			ORDER BY	`order`.`date_added` ASC,
								`order`.`order_id` ASC
		';
		
		$re_product = @mysqli_query($ddb, $qu_product);

		//echo $qu_product;
		echo mysqli_error($ddb);
		$view_total = 0;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$ro_sub = Result($ddb, '
				SELECT	category_description.category_id,
								category_description.name,
								category.parent_id
				FROM		category,
								category_description,
								product_to_category
				WHERE		category.category_id = category_description.category_id &&
								category.parent_id > 0 &&
								category.category_id = product_to_category.category_id &&
								product_to_category.product_id="'.$ro_product['PID'].'"
			');
			if ($ro_sub['category_id'] > 0) {
				$subcategory_id = $ro_sub['category_id'];
				$subcategory = $ro_sub['name'];
				$ro_cat = Result($ddb, '
					SELECT	category_id,
									name
					FROM 		category_description
					WHERE		category_id = "'.$ro_sub['parent_id'].'"
				');
				$category_id = $ro_cat['category_id'];
				$category = $ro_cat['name'];
			} else {
				$subcategory_id = '';
				$subcategory = '';
				$ro_cat = Result($ddb, '
					SELECT	category_description.category_id,
									category_description.name
					FROM		category,
									category_description,
									product_to_category
					WHERE		category.category_id = category_description.category_id &&
									category.parent_id = 0 &&
									category.category_id = product_to_category.category_id &&
									product_to_category.product_id="'.$ro_product['PID'].'"
				');
				$category_id = $ro_cat['category_id'];
				$category = $ro_cat['name'];
			}
			
			$option_1 = '';
			$option_2 = '';
			$option_3 = '';
			$qu_option = 'SELECT * FROM `order_option` WHERE `order_product_id`="'.$ro_product['OPID'].'" ORDER BY `name`';
			$re_option = @mysqli_query($ddb, $qu_option);
			$ro_option = @mysqli_fetch_array($re_option); $option_1 = ($ro_option['name']) ? $ro_option['name'].': '.$ro_option['value'] : '';
			$ro_option = @mysqli_fetch_array($re_option); $option_2 = ($ro_option['name']) ? $ro_option['name'].': '.$ro_option['value'] : '';
			$ro_option = @mysqli_fetch_array($re_option); $option_3 = ($ro_option['name']) ? $ro_option['name'].': '.$ro_option['value'] : '';
			/*
			$quantity = $ro_product['quantity'];
			$total = $ro_product['total'];
			$profit = $total * 0.94 - $quantity * $price_in;
			$profit_percent = round($profit*100/$total,2);
			*/
			@mysqli_query($ddb, '
				INSERT INTO	`tmp_report_orders_products`
				SET					`PID`="'.$ro_product['PID'].'",
										`OID`="'.$ro_product['OID'].'",
										`DATE`="'.$ro_product['DATE'].'",
										`ART`="'.$ro_product['ART'].'",
										`NAME`="'.$ro_product['NAME'].'",
										`MANUFACTURER`="'.$ro_product['MANUFACTURER'].'",
										`MODEL`="'.$ro_product['MODEL'].'",
										`OPTION_1`="'.$option_1.'",
										`OPTION_2`="'.$option_2.'",
										`OPTION_3`="'.$option_3.'",
										`CATEGORY`="'.$category.'",
										`SUBCATEGORY`="'.$subcategory.'",
										`PRICE_IN`="'.$ro_product['PRICE_IN'].'",
										`PRICE_OUT`="'.$ro_product['PRICE_OUT'].'",
										`QUANTITY`="'.$ro_product['QUANTITY'].'",
										`TOTAL`="'.$ro_product['TOTAL'].'",
										`MARGIN`="'.$ro_product['MARGIN'].'",
										`MARGIN_PERCENT`="'.round($ro_product['MARGIN_PERCENT'],2).'",
										`CITY`="'.$ro_product['CITY'].'",
										`STATUS`="'.$ro_product['STATUS'].'"
			');
		}
	}
?>
<html>
	<head>
		<title>Отчет по товарам в заказах без группировки (SedEdition)</title>
		<link type="text/css" href="<?php echo $app_url?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<!--
		<script language="javascript" src="js/right.js"></script>
		<script language="javascript" src="js/right-calendar.js"></script>
		<script language="javascript" src="js/ru.js"></script>
		-->
	</head>
	<body>
		<script language="javascript">
			$(function() {
	  	  $( "#report_orders_products_date_min" ).datepicker();
		    $( "#report_orders_products_date_max" ).datepicker();
		  });
		</script>
		<div align="right">
			<br />
			<a href="/index.php?route=common/home&token=<?php echo $_SESSION['token']?>">Вернуться в панель управления</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="report_orders_products_csv.php">Скачать в CSV</a>
			&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				<input type="hidden" name="act" value="clear_filter" />
				<input type="hidden" name="db_not_update" value="on" />
				<input type="submit" value="Сбросить фильтры" />
			</form>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				<input type="hidden" name="act" value="setting" />
				Статус заказа:
				<select name="report_orders_products_status">
					<option value=""></option>				
					<?php
						$qu_status = 'SELECT * FROM order_status ORDER BY order_status_id';
						$re_status = @mysqli_query($ddb, $qu_status);
						while ($ro_status = @mysqli_fetch_array($re_status)) echo '<option value="'.$ro_status['order_status_id'].'"'.(($ro_status['order_status_id'] == $_SESSION['report_orders_products_status']) ? ' selected' : '').'>'.$ro_status['name'].'</option>';
					?>
				</select>
				&nbsp;&nbsp;&nbsp;
				Выводить по:
				<select name="report_orders_products_per_page">
					<option value="25"<?php echo ($_SESSION['report_orders_per_page'] == '25') ? ' selected' : ''?>>25</option>
					<option value="50"<?php echo ($_SESSION['report_orders_per_page'] == '50') ? ' selected' : ''?>>50</option>
					<option value="100"<?php echo ($_SESSION['report_orders_per_page'] == '100') ? ' selected' : ''?>>100</option>
					<option value="200"<?php echo ($_SESSION['report_orders_per_page'] == '200') ? ' selected' : ''?>>200</option>
					<option value="500"<?php echo ($_SESSION['report_orders_per_page'] == '500') ? ' selected' : ''?>>500</option>
					<option value="1000"<?php echo ($_SESSION['report_orders_per_page'] == '1000') ? ' selected' : ''?>>1000</option>
					<option value="5000"<?php echo ($_SESSION['report_orders_per_page'] == '5000') ? ' selected' : ''?>>5000</option>
					<option value="10000"<?php echo ($_SESSION['report_orders_per_page'] == '10000') ? ' selected' : ''?>>10000</option>
					<option value="50000"<?php echo ($_SESSION['report_orders_per_page'] == '50000') ? ' selected' : ''?>>50000</option>
				</select>
				&nbsp;&nbsp;&nbsp;
				Не обновлять сводную БД:
				<input type="checkbox" name="db_not_update" checked="checked" />
				&nbsp;&nbsp;&nbsp;
				<input type="submit" value="Сохранить" />
			</form>
			<br /><br />
		</div>
		<table class="list" width="">
			<thead>
				<tr>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && ($_GET['s'] == '`OID` ASC')) ? '`OID` DESC' : '`OID` ASC';?>" class="<?php echo ((isset($_GET['s']) && ($_GET['s'] == '`OID` ASC')) ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`OID` DESC') ? 'desc' : '')?>" title="ID заказа">№</a></td>
					<td class="left" width="">
						<a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && ($_GET['s'] == '`DATE` ASC')) ? '`DATE` DESC' : '`DATE` ASC';?>" class="<?php echo ((isset($_GET['s']) && ($_GET['s'] == '`DATE` ASC')) ? 'asc' : '').((isset($_GET['s']) && ($_GET['s'] == '`DATE` DESC')) ? 'desc' : '')?>" title="Дата заказа">Дата</a>
						&nbsp;&nbsp;&nbsp;
						<a href="" onclick="$('#report_orders_products_date_min').val('<?php echo date('Y-m-d');?>'); $('#report_orders_products_date_max').val('<?php echo date('Y-m-d');?>'); return false" style="color: red;">Сегодня</a>
					</td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PID` ASC') ? '`PID` DESC' : '`PID` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PID` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PID` DESC') ? 'desc' : '')?>" title="ID товара">ID</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`ART` ASC') ? '`ART` DESC' : '`ART` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`ART` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`ART` DESC') ? 'desc' : '')?>" title="Артикул товара">Артикул</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`NAME` ASC') ? '`NAME` DESC' : '`NAME` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`NAME` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`NAME` DESC') ? 'desc' : '')?>" title="Название товара">Название</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` ASC') ? '`MANUFACTURER` DESC' : '`MANUFACTURER` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` DESC') ? 'desc' : '')?>" title="Производитель товара">Производитель</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MODEL` ASC') ? '`MODEL` DESC' : '`MODEL` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MODEL` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MODEL` DESC') ? 'desc' : '')?>" title="Модель товара">Модель</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`OPTION_1` ASC') ? '`OPTION_1` DESC' : '`OPTION_1` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`OPTION_1` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`OPTION_1` DESC') ? 'desc' : '')?>" title="1-я опция товара">Опция 1</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`OPTION_2` ASC') ? '`OPTION_2` DESC' : '`OPTION_2` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`OPTION_2` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`OPTION_2` DESC') ? 'desc' : '')?>" title="2-я опция товара">Опция 2</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`OPTION_3` ASC') ? '`OPTION_3` DESC' : '`OPTION_3` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`OPTION_3` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`OPTION_3` DESC') ? 'desc' : '')?>" title="3-я опция товара">Опция 3</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`CATEGORY` ASC') ? '`CATEGORY` DESC' : '`CATEGORY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`CATEGORY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`CATEGORY` DESC') ? 'desc' : '')?>" title="Категория товара">Категория</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` ASC') ? '`SUBCATEGORY` DESC' : '`SUBCATEGORY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` DESC') ? 'desc' : '')?>" title="Подкатегория товара">Подкатегория</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` ASC') ? '`PRICE_IN` DESC' : '`PRICE_IN` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` DESC') ? 'desc' : '')?>" title="Входящая цена товара">Цена (вх.)</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` ASC') ? '`PRICE_OUT` DESC' : '`PRICE_OUT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` DESC') ? 'desc' : '')?>" title="Исходящая цена товара">Цена (исх.)</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`QUANTITY` ASC') ? '`QUANTITY` DESC' : '`QUANTITY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`QUANTITY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`QUANTITY` DESC') ? 'desc' : '')?>" title="Количество товара в заказе">Количество</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`TOTAL` ASC') ? '`TOTAL` DESC' : '`TOTAL` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`TOTAL` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`TOTAL` DESC') ? 'desc' : '')?>" title="Сумма за товар в заказе">Итого</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MARGIN` ASC') ? '`MARGIN` DESC' : '`MARGIN` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MARGIN` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MARGIN` DESC') ? 'desc' : '')?>" title="Прибыль за товар в заказе">Прибыль</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MARGIN_PERCENT` ASC') ? '`MARGIN_PERCENT` DESC' : '`MARGIN_PERCENT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MARGIN_PERCENT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MARGIN_PERCENT` DESC') ? 'desc' : '')?>" title="Процент прибыли за товар в заказе">%</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`CITY` ASC') ? '`CITY` DESC' : '`CITY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`CITY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`CITY` DESC') ? 'desc' : '')?>" title="Город">Город</a></td>
					<td class="left" width="">Действие</td>
				</tr>
			</thead>
			<!-- ТУТ ЗАКОНЧИЛ -->
			<tbody>
				<form method="post" id="filter">
				<input type="hidden" name="act" value="filter" />
				<input type="hidden" name="db_not_update" value="on" />
				<tr class="filter">
					<td><input type="text" name="report_orders_products_oid" value="<?php echo $_SESSION['report_orders_products_oid'];?>" style="width: 100%;"></td>
					<td>
						<input type="text" name="report_orders_products_date_min" id="report_orders_products_date_min" value="<?php echo $_SESSION['report_orders_products_date_min'];?>" style="width: 100%;">
						<input type="text" name="report_orders_products_date_max" id="report_orders_products_date_max" value="<?php echo $_SESSION['report_orders_products_date_max'];?>" style="width: 100%;">
					</td>
					<td><input type="text" name="report_orders_products_pid" value="<?php echo $_SESSION['report_orders_products_pid'];?>" style="width: 100%;"></td>
					<td><input type="text" name="report_orders_products_art" value="<?php echo $_SESSION['report_orders_products_art'];?>" style="width: 100%;"></td>
					<td><input type="text" name="report_orders_products_name" value="<?php echo $_SESSION['report_orders_products_name'];?>" style="width: 100%;"></td>
					<td>
						<select name="report_orders_products_manufacturer" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT MANUFACTURER FROM tmp_report_orders_products GROUP BY MANUFACTURER ORDER BY MANUFACTURER';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['MANUFACTURER'].'"'.(($ro['MANUFACTURER'] == $_SESSION['report_orders_products_manufacturer']) ? ' selected' : '').'>'.$ro['MANUFACTURER'].'</option>';
							?>							
						</select>
					</td>					
					<td><input type="text" name="report_orders_products_model" value="<?php echo $_SESSION['report_orders_products_model'];?>" style="width: 100%;"></td>					
					<td><input type="text" name="report_orders_products_option_1" value="<?php echo $_SESSION['report_orders_products_option_1'];?>" style="width: 100%;"></td>
					<td><input type="text" name="report_orders_products_option_2" value="<?php echo $_SESSION['report_orders_products_option_2'];?>" style="width: 100%;"></td>
					<td><input type="text" name="report_orders_products_option_3" value="<?php echo $_SESSION['report_orders_products_option_3'];?>" style="width: 100%;"></td>
					<td>
						<select name="report_orders_products_category" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT CATEGORY FROM tmp_report_orders_products GROUP BY CATEGORY ORDER BY CATEGORY';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['CATEGORY'].'"'.(($ro['CATEGORY'] == $_SESSION['report_orders_products_category']) ? ' selected' : '').'>'.$ro['CATEGORY'].'</option>';
							?>							
						</select>
					</td>
					<td>
						<select name="report_orders_products_subcategory" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT SUBCATEGORY FROM tmp_report_orders_products GROUP BY SUBCATEGORY ORDER BY SUBCATEGORY';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['SUBCATEGORY'].'"'.(($ro['SUBCATEGORY'] == $_SESSION['report_orders_products_subcategory']) ? ' selected' : '').'>'.$ro['SUBCATEGORY'].'</option>';
							?>							
						</select>
					</td>
					<td>
						<input type="text" name="report_orders_products_price_in_min" value="<?php echo isset($_SESSION['report_orders_products_price_in_min']) ? $_SESSION['report_orders_products_price_in_min'] : '';?>" style="width: 100%;">
						<input type="text" name="report_orders_products_price_in_max" value="<?php echo isset($_SESSION['report_orders_products_price_in_max']) ? $_SESSION['report_orders_products_price_in_max'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_orders_products_price_out_min" value="<?php echo isset($_SESSION['report_orders_products_price_out_min']) ? $_SESSION['report_orders_products_price_out_min'] : '';?>" style="width: 100%;">
						<input type="text" name="report_orders_products_price_out_max" value="<?php echo isset($_SESSION['report_orders_products_price_out_max']) ? $_SESSION['report_orders_products_price_out_max'] : ''?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_orders_products_quantity_min" value="<?php echo isset($_SESSION['report_orders_products_quantity_min']) ? $_SESSION['report_orders_products_quantity_min'] : '';?>" style="width: 100%;">
						<input type="text" name="report_orders_products_quantity_max" value="<?php echo isset($_SESSION['report_orders_products_quantity_max']) ? $_SESSION['report_orders_products_quantity_max'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_orders_products_total_min" value="<?php echo isset($_SESSION['report_orders_products_total_min']) ? $_SESSION['report_orders_products_total_min'] : '';?>" style="width: 100%;">
						<input type="text" name="report_orders_products_total_max" value="<?php echo isset($_SESSION['report_orders_products_total_max']) ? $_SESSION['report_orders_products_total_max'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_orders_products_margin_min" value="<?php echo isset($_SESSION['report_orders_products_profit_min']) ? $_SESSION['report_orders_products_profit_min'] : '';?>" style="width: 100%;">
						<input type="text" name="report_orders_products_margin_max" value="<?php echo isset($_SESSION['report_orders_products_profit_max']) ? $_SESSION['report_orders_products_profit_max'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_orders_products_margin_percent_min" value="<?php echo isset($_SESSION['report_orders_products_margin_percent_min']) ? $_SESSION['report_orders_products_margin_percent_min'] : ''?>" style="width: 100%;">
						<input type="text" name="report_orders_products_margin_percent_max" value="<?php echo isset($_SESSION['report_orders_products_margin_percent_max']) ? $_SESSION['report_orders_products_margin_percent_max'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<select name="report_orders_products_city" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT CITY FROM tmp_report_orders_products GROUP BY CITY ORDER BY CITY';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['CITY'].'"'.(($ro['CITY'] == $_SESSION['report_orders_products_city']) ? ' selected' : '').'>'.$ro['CITY'].'</option>';
							?>							
						</select>
					</td>
					<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
				</tr>
				</form>
				<?php
					$qu_product = '
						SELECT	*
						FROM 		tmp_report_orders_products
						WHERE		1
									'.((isset($_SESSION['report_orders_products_pid']) && $_SESSION['report_orders_products_pid'] != '') ? ' && `PID` LIKE "%'.$_SESSION['report_orders_products_pid'].'%"' : '').' 	
									'.((isset($_SESSION['report_orders_products_oid']) && $_SESSION['report_orders_products_oid'] != '') ? ' && `OID` LIKE "%'.$_SESSION['report_orders_products_oid'].'%"' : '').' 	
									'.((isset($_SESSION['report_orders_products_date_min']) && $_SESSION['report_orders_products_date_min'] != '') ? ' && `DATE`>="'.$_SESSION['report_orders_products_date_min'].' 00:00:00"' : '').' 	
									'.((isset($_SESSION['report_orders_products_date_max']) && $_SESSION['report_orders_products_date_max'] != '') ? ' && `DATE`<="'.$_SESSION['report_orders_products_date_max'].' 23:59:59"' : '').' 	
									'.((isset($_SESSION['report_orders_products_art']) && $_SESSION['report_orders_products_art'] != '') ? ' && `ART` LIKE "%'.$_SESSION['report_orders_products_art'].'%"' : '').' 	
									'.((isset($_SESSION['report_orders_products_name']) && $_SESSION['report_orders_products_name'] != '') ? ' && `NAME`.`name` LIKE "%'.$_SESSION['report_orders_products_name'].'%"' : '').' 	
									'.((isset($_SESSION['report_orders_products_manufacturer']) && $_SESSION['report_orders_products_manufacturer'] != '') ? ' && `MANUFACTURER`="'.$_SESSION['report_orders_products_manufacturer'].'"' : '').' 	
									'.((isset($_SESSION['report_orders_products_model']) && $_SESSION['report_orders_products_model'] != '') ? ' && `MODEL` LIKE "%'.$_SESSION['report_orders_products_model'].'%"' : '').' 	
									'.((isset($_SESSION['report_orders_products_price_in_min']) && $_SESSION['report_orders_products_price_in_min'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_orders_products_price_in_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_price_in_max']) && $_SESSION['report_orders_products_price_in_max'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_orders_products_price_in_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_price_out_min']) && $_SESSION['report_orders_products_price_out_min'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_orders_products_price_out_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_price_out_max']) && $_SESSION['report_orders_products_price_out_max'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_orders_products_price_out_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_quantity_min']) && $_SESSION['report_orders_products_quantity_min'] != '') ? ' && `QUANTITY`>="'.$_SESSION['report_orders_products_quantity_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_quantity_max']) && $_SESSION['report_orders_products_quantity_max'] != '') ? ' && `QUANTITY`<="'.$_SESSION['report_orders_products_quantity_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_status']) && $_SESSION['report_orders_products_status']) ? ' && `order`.`order_status_id`="'.$_SESSION['report_orders_products_status'].'"' : '').' 	
									'.((isset($_SESSION['report_orders_products_option_1']) && $_SESSION['report_orders_products_option_1'] != '') ? ' && `OPTION_1` LIKE "%'.$_SESSION['report_orders_products_option_1'].'%"' : '').'
									'.((isset($_SESSION['report_orders_products_option_2']) && $_SESSION['report_orders_products_option_2'] != '') ? ' && `OPTION_2` LIKE "%'.$_SESSION['report_orders_products_option_2'].'%"' : '').'
									'.((isset($_SESSION['report_orders_products_option_3']) && $_SESSION['report_orders_products_option_3'] != '') ? ' && `OPTION_3` LIKE "%'.$_SESSION['report_orders_products_option_3'].'%"' : '').'
									'.((isset($_SESSION['report_orders_products_category']) && $_SESSION['report_orders_products_category'] != '') ? ' && `CATEGORY`="'.$_SESSION['report_orders_products_category'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_subcategory']) && $_SESSION['report_orders_products_subcategory'] != '') ? ' && `SUBCATEGORY`="'.$_SESSION['report_orders_products_subcategory'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_total_min']) && $_SESSION['report_orders_products_total_min'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_orders_products_total_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_total_max']) && $_SESSION['report_orders_products_total_max'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_orders_products_total_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_margin_min']) && $_SESSION['report_orders_products_margin_min'] != '') ? ' && `MARGIN`>="'.$_SESSION['report_orders_products_margin_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_margin_max']) && $_SESSION['report_orders_products_margin_max'] != '') ? ' && `MARGIN`<="'.$_SESSION['report_orders_products_margin_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_margin_percent_min']) && $_SESSION['report_orders_products_margin_percent_min'] != '') ? ' && `MARGIN_PERCENT`>="'.$_SESSION['report_orders_products_margin_percent_min'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_margin_percent_max']) && $_SESSION['report_orders_products_margin_percent_max'] != '') ? ' && `MARGIN_PERCENT`<="'.$_SESSION['report_orders_products_margin_percent_max'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_city']) && $_SESSION['report_orders_products_city'] != '') ? ' && `CITY`="'.$_SESSION['report_orders_products_city'].'"' : '').'
									'.((isset($_SESSION['report_orders_products_status']) && $_SESSION['report_orders_products_status']) ? ' && `STATUS`="'.$_SESSION['report_orders_products_status'].'"' : '').' 	
						';
						if(isset($_GET['s'])){
							$qu_product .= 'ORDER BY '.$_GET['s'];
                        }
				//echo $qu_product;
				$re_product = @mysqli_query($ddb, $qu_product);
				//echo mysqli_error($ddb);
				$nu_product_total = @mysqli_num_rows($re_product);
				$summ['quantity'] = 0;
				$summ['total'] = 0;
				$summ['margin'] = 0;
				//$summ['profit_percent'] = 0;
				while ($ro_product = @mysqli_fetch_array($re_product)) {
					$summ['quantity'] += $ro_product['QUANTITY'];
					$summ['total'] += $ro_product['TOTAL'];
					$summ['margin'] += $ro_product['MARGIN'];
				}
				$summ_page['quantity'] = 0;
				$summ_page['total'] = 0;
				$summ_page['margin'] = 0;
				$summ_page['margin_percent'] = 0;
				$nu_pages = ceil($nu_product_total/$_SESSION['report_orders_per_page']);
				$qu_product .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_orders_per_page']).','.$_SESSION['report_orders_per_page'];

				$re_product = @mysqli_query($ddb, $qu_product);
				while ($ro_product = @mysqli_fetch_array($re_product)) {
					$ro_key = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM url_alias WHERE query="product_id='.$ro_product['PID'].'"'));
					$summ_page['quantity'] += $ro_product['QUANTITY'];
					$summ_page['total'] += $ro_product['TOTAL'];
					$summ_page['margin'] += $ro_product['MARGIN'];
					//$summ_page['profit_percent'] += $ro_product['PROFIT_PERCENT'];
					?>
				<tr >
					<td><?php echo $ro_product['OID']?></td>
					<td><?php echo $ro_product['DATE']?></td>
					<td><?php echo $ro_product['PID']?></td>
					<td><?php echo $ro_product['ART']?></td>
					<td><?php echo $ro_product['NAME']?></td>
					<td><?php echo $ro_product['MANUFACTURER']?></td>
					<td><?php echo $ro_product['MODEL']?></td>
					<td><?php echo $ro_product['OPTION_1']?></td>
					<td><?php echo $ro_product['OPTION_2']?></td>
					<td><?php echo $ro_product['OPTION_3']?></td>
					<td><?php echo $ro_product['CATEGORY']?></td>
					<td><?php echo $ro_product['SUBCATEGORY']?></td>
					<td><?php echo $ro_product['PRICE_IN']?></td>
					<td><?php echo $ro_product['PRICE_OUT']?></td>
					<td><?php echo $ro_product['QUANTITY']?></td>
					<td><?php echo $ro_product['TOTAL']?></td>
					<td><?php echo $ro_product['MARGIN']?></td>
					<td><?php echo $ro_product['MARGIN_PERCENT']?></td>
					<td><?php echo $ro_product['CITY']?></td>
					<td class="right">
						[<a href="/index.php?route=catalog/product/update&token=<?php echo $_SESSION['token'];?>&product_id=<?php echo $ro_product['PID']?>" target="_blank">Изменить</a>]<br />
						[<a href="/<?php echo $ro_key['keyword']?>.html" target="_blank">Посмотреть</a>]
						
					</td>
					<?php
				}
				//$summ_page['profit_percent'] = @round($summ_page['profit_percent']/@mysqli_num_rows($re_product),2);

				?>
				<tr>
					<td colspan="14"><strong>Итог по странице</strong></td>
					<td><strong><?php echo $summ_page['quantity']?></strong></td>
					<td><strong><?php echo $summ_page['total']?></strong></td>
					<td><strong><?php echo $summ_page['margin']?></strong></td>
					<td><strong><?php echo @round($summ_page['margin']/$summ_page['total']*100,2)?></strong></td>
					<td colspan="4"></td>
				</tr>			
				<tr>
					<td colspan="14"><strong>Итог по всем страницам</strong></td>
					<td><strong><?php echo $summ['quantity']?></strong></td>
					<td><strong><?php echo $summ['total']?></strong></td>
					<td><strong><?php echo $summ['margin']?></strong></td>
					<td><strong><?php echo @round($summ['margin']/$summ['total']*100,2);?></strong></td>
					<td colspan="4"></td>
				</tr>			
			</tbody>
		</table>
		<div class="pagination">
			<div class="links">
<?php
		for ($i=1;$i<=$nu_pages;$i++) {
			echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.$_GET['s'].'">'.$i.'</a> ';
		}
?>
			</div>
			<div class="results">Показано с <?php echo (($_GET['p']-1)*$_SESSION['report_orders_per_page']+1);?> по <?php echo min($_GET['p']*$_SESSION['report_orders_per_page'],$nu_product_total);?> из <?php echo $nu_product_total;?> (всего страниц: <?php echo $nu_pages;?>)</div>
		</div>
	</body>
</html>
<?php
	}else{
		header('Location: /');
	}
?>