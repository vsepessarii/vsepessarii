<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
		
    
    
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td valign="top"><span class="required">*</span> <?php echo $entry_name; ?></td>
            <td><?php foreach ($languages as $language) { ?>
              <input type="text" name="order_status[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($order_status[$language['language_id']]) ? $order_status[$language['language_id']]['name'] : ''; ?>" />
              <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
              <?php if (isset($error_name[$language['language_id']])) { ?>
              <span class="error"><?php echo $error_name[$language['language_id']]; ?></span><br />
              <?php } ?>
              <?php } ?>
              <br />
							<?php
								$alert = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM order_status_alert WHERE id="'.$_GET['order_status_id'].'"'));
							?>
							<table width="1000">
								<tr>
									<td width="150" valign="center">Отправлять SMS:</td>
									<td width="850">
										<input type="radio" name="sms_send" value="1"<?php echo ($alert['sms_send'] == 1) ? ' checked="checked"' : ''?> />Да
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="sms_send" value="0"<?php echo ($alert['sms_send'] == 0) ? ' checked="checked"' : ''?> />Нет
									</td>								
								</tr>
								<tr>
									<td valign="top">Текст SMS (RU):</td>
									<td valign="top"><textarea cols="70" rows="5" name="sms_text_ru"><?php echo $alert['sms_text_ru']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="top">Текст SMS (BY):</td>
									<td valign="top"><textarea cols="70" rows="5" name="sms_text_by"><?php echo $alert['sms_text_by']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="top">Текст SMS (KZ):</td>
									<td valign="top"><textarea cols="70" rows="5" name="sms_text_kz"><?php echo $alert['sms_text_kz']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="center">Отправлять e-mail:</td>
									<td>
										<input type="radio" name="email_send" value="1"<?php echo ($alert['email_send'] == 1) ? ' checked="checked"' : ''?> />Да
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" name="email_send" value="0"<?php echo ($alert['email_send'] == 0) ? ' checked="checked"' : ''?> />Нет
									</td>								
								</tr>
								<tr>
									<td valign="center">Тема e-mail (RU):</td>
									<td><input type="text" name="email_topic_ru" size="100" value="<?php echo $alert['email_topic_ru']; ?>" /></td>								
								</tr>
								<tr>
									<td valign="top">Текст email (RU):</td>
									<td valign="top"><textarea cols="150" rows="5" name="email_text_ru"><?php echo $alert['email_text_ru']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="center">Тема e-mail (BY):</td>
									<td><input type="text" name="email_topic_by" size="100" value="<?php echo $alert['email_topic_by']; ?>" /></td>								
								</tr>
								<tr>
									<td valign="top">Текст email (BY):</td>
									<td valign="top"><textarea cols="150" rows="5" name="email_text_by"><?php echo $alert['email_text_by']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="center">Тема e-mail (KZ):</td>
									<td><input type="text" name="email_topic_kz" size="100" value="<?php echo $alert['email_topic_kz']; ?>" /></td>								
								</tr>
								<tr>
									<td valign="top">Текст email (KZ):</td>
									<td valign="top"><textarea cols="150" rows="5" name="email_text_kz"><?php echo $alert['email_text_kz']; ?></textarea></td>
								</tr>
								<tr>
									<td valign="top">Переменные</td>
									<td>
										<strong>[ORDER_ID]</strong> - номер заказа<br />
										<strong>[DECL]</strong> - номер декларации<br />
										<strong>[ORDER_DATE]</strong> - дата заказа<br />
										<strong>[SHIP_DATE]</strong> - дата доставки<br />
										<strong>[ORDER_SUM]</strong> - сумма заказа<br />
										<strong>[COURIER_NAME]</strong> - имя курьера<br />
										<strong>[COURIER_PHONE]</strong> - телефон курьера<br />
										
										
									</td>
								</tr>
							</table>
							</td>
          </tr>
        </table>
      </form>
      
		<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
		<script type="text/javascript">
			CKEDITOR.replace('email_text_ru', {
				filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
			});
			CKEDITOR.replace('email_text_by', {
				filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
			});
			CKEDITOR.replace('email_text_kz', {
				filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
				filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
			});
		</script> 
		<script type="text/javascript">
			function image_upload(field, thumb) {
				$('#dialog').remove();
				$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
				$('#dialog').dialog({
					title: '<?php echo $text_image_manager; ?>',
					close: function (event, ui) {
						if ($('#' + field).attr('value')) {
							$.ajax({
								url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
								dataType: 'text',
								success: function(data) {
									$('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
								}
							});
						}
					},	
					bgiframe: false,
					width: 800,
					height: 400,
					resizable: false,
					modal: false
				});
			};
			</script>       
      
    </div>
  </div>
</div>
<?php echo $footer; ?>