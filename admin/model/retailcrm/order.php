<?php

class ModelRetailcrmOrder extends Model {

    public function sendToCrm($order_data, $order_id)
    {
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('retailcrm');

        if(!empty($settings['retailcrm_url']) && !empty($settings['retailcrm_apikey'])) {
            require_once DIR_SYSTEM . 'library/retailcrm/bootstrap.php';

            $this->retailcrm = new RetailcrmProxy(
                $settings['retailcrm_url'],
                $settings['retailcrm_apikey'],
                DIR_SYSTEM . 'logs/retailcrm.log'
            );

            $order = array();

            $order_data['order_id'] = $order_id;
            $altTotals = isset($order_data['order_total']) ? $order_data['order_total'] : "";
            $order_data['order_total']= isset($order_data['totals']) ? $order_data['totals'] : $altTotals ;

            $order = $this->process($order_data);

            $order = $this->prepareOrders(array($order));
            $order = reset($order);
/* 
            $customers = $this->retailcrm->customersList(
                array(
                    'name' => $order_data['telephone'],
                    'email' => $order_data['email']
                ),
                1,
                100
            );

            foreach($customers['customers'] as $customer) {
                $order['customer']['id'] = $customer['id'];
            }

            unset($customers);

			$number = $order_id . (($order_data['quick']) ? 'Б' : '');
            $order['externalId'] = $order_id;
			$order['number'] = $number;
            $order['firstName'] = $order_data['firstname'];
            $order['lastName'] = $order_data['lastname'];
            $order['email'] = $order_data['email'];
            $order['phone'] = $order_data['telephone'];
            $order['customerComment'] = $order_data['comment'];

            $deliveryCost = 0;
            $altTotals = isset($order_data['order_total']) ? $order_data['order_total'] : "";
            $orderTotals = isset($order_data['totals']) ? $order_data['totals'] : $altTotals ;

            if (!empty($orderTotals)) {
                foreach ($orderTotals as $totals) {
                    if ($totals['code'] == 'shipping') {
                        $deliveryCost = $totals['value'];
                    }
                }
            }

            $order['createdAt'] = date('Y-m-d H:i:s');

            $payment_code = $order_data['payment_code'];
            $order['paymentType'] = $settings['retailcrm_payment'][$payment_code];

            $delivery_code = $order_data['shipping_code'];
            $order['delivery'] = array(
                'code' => $settings['retailcrm_delivery'][$delivery_code],
                'cost' => $deliveryCost,
                'address' => array(
                    'index' => $order_data['shipping_postcode'],
                    'city' => $order_data['shipping_city'],
                    'countryIso' => $order_data['shipping_iso_code_2'],
                    'text' => trim(implode(', ', array(
                        $order_data['shipping_address_1'],
                        $order_data['shipping_address_2']
                    )), ', ')
                )
            );

            $orderProducts = isset($order_data['products']) ? $order_data['products'] : $order_data['order_product'];
            $offerOptions = array('select', 'radio');

            foreach ($orderProducts as $product) {
                $offerId = '';
                if(!empty($product['option'])) {
                    $options = array();

                    foreach($product['option'] as $option) {
                        if(!in_array($option['type'], $offerOptions)) continue;
                        $options[$option['product_option_id']] = $option['option_value_id'];
                    }

                    ksort($options);

                    $offerId = array();
                    foreach($options as $optionKey => $optionValue) {
                        $offerId[] = $optionKey.'-'.$optionValue;
                    }
                    $offerId = implode('_', $offerId);
                }


                $order['items'][] = array(
                    'productId' => !empty($offerId) ? $product['product_id'].'#'.$offerId : $product['product_id'],
                    'productName' => $product['name'],
                    'initialPrice' => $product['price'],
                    'quantity' => $product['quantity'],
                );
            }

            if (isset($order_data['order_status_id']) && $order_data['order_status_id'] > 0) {
                $order['status'] = $settings['retailcrm_status'][$order_data['order_status_id']];
            } */

            $this->retailcrm->ordersCreate($order);
        }
    }

    /**
     * @param $order_data
     * @param $order_id
     */
    public function changeInCrm($order_data, $order_id)
    {
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('retailcrm');

        if(!empty($settings['retailcrm_url']) && !empty($settings['retailcrm_apikey'])) {
            require_once DIR_SYSTEM . 'library/retailcrm/bootstrap.php';

            $this->retailcrm = new RetailcrmProxy(
                $settings['retailcrm_url'],
                $settings['retailcrm_apikey'],
                DIR_SYSTEM . 'logs/retailcrm.log'
            );

            $order = array();

            $payment_code = $order_data['payment_code'];
            $delivery_code = $order_data['shipping_code'];

            $order['externalId'] = $order_id;
            $order['firstName'] = $order_data['firstname'];
            $order['lastName'] = $order_data['lastname'];
            $order['email'] = $order_data['email'];
            $order['phone'] = $order_data['telephone'];
            $order['customerComment'] = $order_data['comment'];

            $deliveryCost = 0;
            $orderTotals = isset($order_data['totals']) ? $order_data['totals'] : $order_data['order_total'] ;

            foreach ($orderTotals as $totals) {
                if ($totals['code'] == 'shipping') {
                    $deliveryCost = $totals['value'];
                }
            }

            $order['createdAt'] = date('Y-m-d H:i:s');
            $order['paymentType'] = $settings['retailcrm_payment'][$payment_code];

            $country = (isset($order_data['shipping_country'])) ? $order_data['shipping_country'] : '' ;

            $order['delivery'] = array(
                'code' => $settings['retailcrm_delivery'][$delivery_code],
                'cost' => $deliveryCost,
                'address' => array(
                    'index' => $order_data['shipping_postcode'],
                    'city' => $order_data['shipping_city'],
                    'country' => $order_data['shipping_country_id'],
                    'region' => $order_data['shipping_zone_id'],
                    'text' => implode(', ', array(
                        $order_data['shipping_postcode'],
                        $country,
                        $order_data['shipping_city'],
                        $order_data['shipping_address_1'],
                        $order_data['shipping_address_2']
                    ))
                )
            );

            $orderProducts = isset($order_data['products']) ? $order_data['products'] : $order_data['order_product'];
            $offerOptions = array('select', 'radio');

            foreach ($orderProducts as $product) {
                if(!empty($product['order_option'])) {
                    $options = array();

                    $productOptions = $this->model_catalog_product->getProductOptions($product['product_id']);
                    foreach($productOptions as $key=>$productOption) {
                        $productOptionValues[$productOption['product_option_id']] = array();

                        foreach($productOption['product_option_value'] as $productOptionValue) {
                            $productOptionValues[$productOption['product_option_id']][$productOptionValue['product_option_value_id']] = $productOptionValue['option_value_id'];
                        }
                    }

                    foreach($product['order_option'] as $option) {
                        if(!in_array($option['type'], $offerOptions)) continue;
                        $options[$option['product_option_id']] = $productOptionValues[$option['product_option_id']][$option['product_option_value_id']];
                    }

                    ksort($options);

                    $offerId = array();
                    foreach($options as $optionKey => $optionValue) {
                        $offerId[] = $optionKey.'-'.$optionValue;
                    }
                    $offerId = implode('_', $offerId);
                }

                $order['items'][] = array(
                    'productId' => !empty($offerId) ? $product['product_id'].'#'.$offerId : $product['product_id'],
                    'productName' => $product['name'],
                    'initialPrice' => $product['price'],
                    'purchasePrice' => $product['price_in'],
                    'quantity' => $product['quantity'],
                );
            }

            // Сделаем порядок товраных позиций в срм как и в магазине
            $order['items'] = array_reverse($order['items']);

            if (isset($order_data['order_status_id']) && $order_data['order_status_id'] > 0) {
                $order['status'] = $settings['retailcrm_status'][$order_data['order_status_id']];
            }

            $this->retailcrm->ordersEdit($order);
        }
    }
    
    
    public function uploadToCrm($orders) {
        $this->load->model('catalog/product');
        $this->load->model('setting/setting');
        $this->settings = $this->model_setting_setting->getSetting('retailcrm');
        $this->retailcrm = new RetailcrmProxy(
            $this->settings['retailcrm_url'],
            $this->settings['retailcrm_apikey'],
            DIR_SYSTEM . 'logs/retailcrm.log'
            );

        $ordersToCrm = array();
        foreach($orders as $order) {
            $ordersToCrm[] = $this->process($order);
        }
 
        $chunkedOrders = array_chunk($ordersToCrm, 50);
        foreach($chunkedOrders as $ordersPart) {
            
            $ordersPart = $this->prepareOrders($ordersPart);
            $this->retailcrm->ordersUpload($ordersPart);
        }
    }
    
    private function Result($ddb, $q) {
        return @mysqli_fetch_array(@mysqli_query($ddb, $q));
    }
    
    private function prepareOrders($orders)
    {
        foreach ($orders as $idx => $order) {
            /* if (isset($order['customer'])) {
             continue;
             } */
            
            $customer = array();
            $customer['externalId'] = isset($order['customer']) ? $order['customer']['externalId'] : uniqid('', true);
            $customer['createdAt'] = $order['createdAt'];
            if (isset($order['firstName'])) {
                $customer['firstName'] = $order['firstName'];
            }
            if (isset($order['lastName'])) {
                $customer['lastName'] = $order['lastName'];
            }
            if (isset($order['patronymic'])) {
                $customer['patronymic'] = $order['patronymic'];
            }
            if (!empty($order['delivery']['address'])) {
                $customer['address'] = $order['delivery']['address'];
            }
            if (isset($order['phone'])) {
                $customer['phones'][]['number'] = $order['phone'];
            }
            if (isset($order['email']) && $order['email'] != 'empty@localhost') {
                $customer['email'] = $order['email'];
            }

            $checkResult = $this->checkCustomers($customer);
            if ($checkResult !== false) {
                $orders[$idx]["customer"]['id'] = $checkResult;
            }
        }
        return $orders;
    }
    
    private function checkCustomers($customer, $searchEdit = false)
    {
        $criteria = array(
            'name' => (isset($customer['phones'][0]['number'])) ? $customer['phones'][0]['number'] : $customer['lastName']
        );
        $search = $this->retailcrm->customersList($criteria);
        //var_dump($search);die();
        if (!is_null($search)) {
            if(empty($search['customers'])) {
                if(!is_null($r = $this->retailcrm->customersCreate($customer))) {
                    return $r["id"];
                } else {
                    return false;
                }
            } else {
                $_externalId = null;
                foreach ($search['customers'] as $_customer) {
                    if (!empty($_customer['externalId'])) {
                        $_externalId = $_customer['externalId'];
                        break;
                    }
                }
                
                if (is_null($_externalId)) {
                    $customerFix = array(
                        'id' => $search['customers'][0]['id'],
                        'externalId' => $customer['externalId']
                    );
                    $response = $this->retailcrm->customersFixExternalIds(
                        array($customerFix)
                    );
                    $_externalId = $customer['externalId'];
                };
                if ($searchEdit) {
                    $customer['externalId'] = $_externalId;
                    $this->retailcrm->customersEdit($customer);
                }

                return $search['customers'][0]['id'];
            }
        } else {
            return false;
        }
    }
    
    private function process($order_data) {

        $ro_order = $this->Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($order_data['order_id']).'"');
        $order = array();
        $payment_code = $order_data['payment_code'];
        $delivery_code = $order_data['shipping_code'];
        $order['externalId'] = $order_data['order_id'];
        $order['number'] = $order_data['order_id'] . (($ro_order['quick']) ? 'Б' : '');
        $order['orderMethod'] = (($ro_order['quick']) ? 'one-click' : '');
        $order['firstName'] = $order_data['firstname'];
        //$order['lastName'] = $order_data['lastname'];

        if (empty($order_data['lastname'])) {
            $order = array_merge($order, self::explodeFIO($order_data['firstname']));
        }

        $order['phone'] = $order_data['telephone'];
        $order['customerComment'] = $order_data['comment'];
        if(!empty($order_data['email']) && $order_data['email'] != 'empty@localhost') {
            $order['email'] = $order_data['email'];
        }
        if (((int) $order_data['customer_id']) > 0) {
            $order['customer']['externalId'] = $order_data['customer_id'];
        }
        $deliveryCost = 0;
        $orderTotals = isset($order_data['totals']) ? $order_data['totals'] : $order_data['order_total'] ;
        foreach ($orderTotals as $totals) {
            if ($totals['code'] == 'shipping') {
                $deliveryCost = $totals['value'];
            }
        }
        $order['createdAt'] = $order_data['date_added'];
        $order['paymentType'] = $this->settings['retailcrm_payment'][$payment_code];
        if (!empty($order_data['shipping_city'])) {
            if (stripos($order_data['shipping_city'], '(РФ)') !== false) {
                $order['countryIso']= 'RU';
            } else if (stripos($order_data['shipping_city'], '(Казахстан)') !== false) {
                $order['countryIso']= 'KZ';
            } else if (stripos($order_data['shipping_city'], '(Беларусь)') !== false) {
                $order['countryIso']= 'BY';
            }
        }

        $country = (isset($order_data['shipping_country'])) ? $order_data['shipping_country'] : '' ;
        $order['delivery'] = array(
            'code' => !empty($delivery_code) ? $this->settings['retailcrm_delivery'][$delivery_code] : '',
            'cost' => $deliveryCost,
            'date' => ($ro_order['date_shipped'] != '0000-00-00 00:00:00') ? date('Y-m-d', strtotime($ro_order['date_shipped'])) : '',
            'address' => array(
                'index' => $order_data['shipping_postcode'],
                'city' => $order_data['shipping_city'],
                'text' => trim(implode(', ', array(
                    $order_data['shipping_address_1'],
                    $order_data['shipping_address_2']
                )), ' ,'),
                'notes' => $order_data['shipping_method']
            )
        );
        if (empty($order['delivery']['code'])) {
            unset($order['delivery']['code']);
        }
        $orderProducts = isset($order_data['products']) ? $order_data['products'] : $order_data['order_product'];
        $offerOptions = array('select', 'radio');
        foreach ($orderProducts as $product) {
            $offerId = '';
            if(!empty($product['option'])) {
                $options = array();
                $productOptions = $this->model_catalog_product->getProductOptions($product['product_id']);
                foreach($product['option'] as $option) {
                    if(!in_array($option['type'], $offerOptions)) continue;
                    foreach($productOptions as $productOption) {
                        if($productOption['product_option_id'] = $option['product_option_id']) {
                            foreach($productOption['product_option_value'] as $productOptionValue) {
                                if($productOptionValue['product_option_value_id'] == $option['product_option_value_id']) {
                                    $options[$option['product_option_id']] = $productOptionValue['option_value_id'];
                                }
                            }
                        }
                    }
                }
                ksort($options);
                $offerId = array();
                foreach($options as $optionKey => $optionValue) {
                    $offerId[] = $optionKey.'-'.$optionValue;
                }
                $offerId = implode('_', $offerId);
            }
            $order['items'][] = array(
                'productId' => !empty($offerId) ? $product['product_id'].'#'.$offerId : $product['product_id'],
                'productName' => $product['name'],
                'initialPrice' => $product['price'],
                'quantity' => $product['quantity'],
            );
        }
        
        if (isset($order_data['order_status_id']) && $order_data['order_status_id'] > 0) {
            $order['status'] = $this->settings['retailcrm_status'][$order_data['order_status_id']];
        }
        if (in_array($order['status'], array('completesdelka', 'prepayed'))) {
            $order['paymentStatus'] = 'paid';
        }

        return self::filterRecursive($order);
    }
    
    
    public static function explodeFIO($string)
    {
        $result = array();
        $parse = (!$string) ? false : explode(" ", $string, 3);
        switch (count($parse)) {
            case 1:
                $result['firstName'] = $parse[0];
                $result['lastName'] = '';
                $result['patronymic'] = '';
                break;
            case 2:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = '';
                break;
            case 3:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = $parse[2];
                break;
            default:
                return false;
        }
        return $result;
    }
    
    public static function filterRecursive($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::filterRecursive($haystack[$key]);
            }
            if (is_null($haystack[$key]) || $haystack[$key] === '' || count($haystack[$key]) == 0) {
                unset($haystack[$key]);
            } elseif (!is_array($value)) {
                $haystack[$key] = trim($value);
            }
        }
        return $haystack;
    }
}
