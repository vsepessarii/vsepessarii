<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">    
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
    <?php $keygen = rtrim(DIR_APPLICATION, 'admin/'); if (@file_exists($keygen . '/vqmod/xml/yandexpluspluskeygen.xml')) { ?><h1 style="color:red">Ай-я-Яй! Вы нарушаете лицензию данного модуля. У нас есть адрес Вашего сайта. Пожалуйста приобритите лицензию.</h1> <?php } ?>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
      	<tr>
        <td width="25%"><span class="required">*</span> <?php echo $entry_license; ?></td>
        <td><input type="text" name="yandexplusplus_card_license" value="<?php if (isset($yandexplusplus_card_license)){ echo $yandexplusplus_card_license; }?>" />
          <br />
          <?php if ($error_license) { ?>
          <span class="error"><?php echo $error_license; ?></span>
          <?php } ?></td>
        </tr>
      	<tr>
        <td width="25%"><span class="required">*</span> <?php echo $entry_login; ?></td>
        <td><input type="text" name="yandexplusplus_card_login" value="<?php if (isset($yandexplusplus_card_login)){ echo $yandexplusplus_card_login; }?>" />
          <br />
          <?php if ($error_login) { ?>
          <span class="error"><?php echo $error_login; ?></span>
          <?php } ?></td>
      	</tr>
      	<tr>
        <td><span class="required">*</span> <?php echo $entry_password; ?></td>
        <td><input type="password" name="yandexplusplus_card_password" value="<?php if (isset($yandexplusplus_card_login)){ echo $yandexplusplus_card_password; }?>" />
          <br />
          <?php if ($error_password) { ?>
          <span class="error"><?php echo $error_password; ?></span>
          <?php } ?></td>
      	</tr>
        <tr>
          <td><span class="required">*</span> Адрес HTTP-уведомления:</td>
          <td><?php echo $copy_result_url; ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_name_tab; ?></td>
          <td><?php if ($yandexplusplus_card_name_attach) { ?>
            <input type="radio" name="yandexplusplus_card_name_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_name_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_name_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_name_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_name; ?></td>
          <td><textarea name="yandexplusplus_card_name" cols="50" rows="1"><?php echo isset($yandexplusplus_card_name) ? $yandexplusplus_card_name : ''; ?></textarea><br /></td>
        </tr>
        <tr>
          <td><?php echo $entry_komis; ?></td>
          <td><input type="text" name="yandexplusplus_card_komis" value="<?php echo isset($yandexplusplus_card_komis) ? $yandexplusplus_card_komis : ''; ?>" >%</td>
        </tr>
        <tr>
          <td><?php echo $entry_maxpay; ?></td>
          <td><input type="text" name="yandexplusplus_card_maxpay" value="<?php echo isset($yandexplusplus_card_maxpay) ? $yandexplusplus_card_maxpay : ''; ?>" >руб.</td>
        </tr>
        <tr>
          <td><?php echo $entry_style; ?></td>
          <td><?php if ($yandexplusplus_card_style) { ?>
            <input type="radio" name="yandexplusplus_card_style" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_style" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_style" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_style" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_later; ?></td>
          <td><?php if ($yandexplusplus_card_button_later) { ?>
            <input type="radio" name="yandexplusplus_card_button_later" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_button_later" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_button_later" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_button_later" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_alert_admin_tab; ?></td>
          <td><?php if ($yandexplusplus_card_success_alert_admin) { ?>
            <input type="radio" name="yandexplusplus_card_success_alert_admin" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_alert_admin" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_success_alert_admin" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_alert_admin" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_alert_customer_tab; ?></td>
          <td><?php if ($yandexplusplus_card_success_alert_customer) { ?>
            <input type="radio" name="yandexplusplus_card_success_alert_customer" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_alert_customer" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_success_alert_customer" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_alert_customer" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_instruction_tab; ?></td>
          <td><?php if ($yandexplusplus_card_instruction_attach) { ?>
            <input type="radio" name="yandexplusplus_card_instruction_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_instruction_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_instruction_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_instruction_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_instruction; ?></td>
          <td><textarea name="yandexplusplus_card_instruction" cols="50" rows="3"><?php echo isset($yandexplusplus_card_instruction) ? $yandexplusplus_card_instruction : ''; ?></textarea><br /></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_mail_instruction_tab; ?></td>
          <td><?php if ($yandexplusplus_card_mail_instruction_attach) { ?>
            <input type="radio" name="yandexplusplus_card_mail_instruction_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_mail_instruction_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_mail_instruction_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_mail_instruction_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_mail_instruction; ?></td>
          <td><textarea name="yandexplusplus_card_mail_instruction" cols="50" rows="3"><?php echo isset($yandexplusplus_card_mail_instruction) ? $yandexplusplus_card_mail_instruction : ''; ?></textarea><br /></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_comment_tab; ?></td>
          <td><?php if ($yandexplusplus_card_success_comment_attach) { ?>
            <input type="radio" name="yandexplusplus_card_success_comment_attach" value="1" checked="checked" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_comment_attach" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_success_comment_attach" value="1" />
            <?php echo $text_yes; ?>
            <input type="radio" name="yandexplusplus_card_success_comment_attach" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_comment; ?></td>
          <td><textarea name="yandexplusplus_card_success_comment" cols="50" rows="3"><?php echo isset($yandexplusplus_card_success_comment) ? $yandexplusplus_card_success_comment : ''; ?></textarea><br /></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_hrefpage_tab; ?></td>
          <td><?php if ($yandexplusplus_card_hrefpage_text_attach) { ?>
            <input type="radio" name="yandexplusplus_card_hrefpage_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_hrefpage_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_hrefpage_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_hrefpage_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_hrefpage; ?></td>
          <td><textarea name="yandexplusplus_card_hrefpage_text" cols="50" rows="3"><?php echo isset($yandexplusplus_card_hrefpage_text) ? $yandexplusplus_card_hrefpage_text : ''; ?></textarea><br /></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_page_tab; ?></td>
          <td><?php if ($yandexplusplus_card_success_page_text_attach) { ?>
            <input type="radio" name="yandexplusplus_card_success_page_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_success_page_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_success_page_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_success_page_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_success_page_text; ?></td>
          <td><textarea name="yandexplusplus_card_success_page_text" cols="50" rows="3"><?php echo isset($yandexplusplus_card_success_page_text) ? $yandexplusplus_card_success_page_text : ''; ?></textarea><br /></td>
        </tr>
         <tr>
          <td><?php echo $entry_yandexplusplus_card_waiting_page_tab; ?></td>
          <td><?php if ($yandexplusplus_card_waiting_page_text_attach) { ?>
            <input type="radio" name="yandexplusplus_card_waiting_page_text_attach" value="1" checked="checked" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_waiting_page_text_attach" value="0" />
            <?php echo $text_default; ?>
            <?php } else { ?>
            <input type="radio" name="yandexplusplus_card_waiting_page_text_attach" value="1" />
            <?php echo $text_my; ?>
            <input type="radio" name="yandexplusplus_card_waiting_page_text_attach" value="0" checked="checked" />
            <?php echo $text_default; ?>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_yandexplusplus_card_waiting_page_text; ?></td>
          <td><textarea name="yandexplusplus_card_waiting_page_text" cols="50" rows="3"><?php echo isset($yandexplusplus_card_waiting_page_text) ? $yandexplusplus_card_waiting_page_text : ''; ?></textarea><br /></td>
        </tr>
        <tr>
        <td><?php echo $entry_on_status; ?></td>
        <td><select name="yandexplusplus_card_on_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $yandexplusplus_card_on_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_order_status; ?></td>
        <td><select name="yandexplusplus_card_order_status_id">
            <?php foreach ($order_statuses as $order_status) { ?>
            <?php if ($order_status['order_status_id'] == $yandexplusplus_card_order_status_id) { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_geo_zone; ?></td>
        <td><select name="yandexplusplus_card_geo_zone_id">
            <option value="0"><?php echo $text_all_zones; ?></option>
            <?php foreach ($geo_zones as $geo_zone) { ?>
            <?php if ($geo_zone['geo_zone_id'] == $yandexplusplus_card_geo_zone_id) { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      	</tr>
      	<tr>
        <td><?php echo $entry_status; ?></td>
        <td><select name="yandexplusplus_card_status">
            <?php if ($yandexplusplus_card_status) { ?>
            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
            <option value="0"><?php echo $text_disabled; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_enabled; ?></option>
            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
            <?php } ?>
          </select></td>
      	</tr>
      	 <tr>
          <td><?php echo $entry_sort_order; ?></td>
          <td><input type="text" name="yandexplusplus_card_sort_order" value="<?php echo $yandexplusplus_card_sort_order; ?>" size="1" /></td>
        </tr>
      </table>

        </div>
      </form>
    </div>
      <p style="text-align:center;">YandexPlusPlus Версия <?php echo $version ?></p>
  </div>
</div>
<?php echo $footer; ?> 