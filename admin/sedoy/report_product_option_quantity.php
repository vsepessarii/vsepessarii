<?php
	//отчет по остаткам товарам в разрезе опций
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	
	//var_dump($_SESSION);

	require 'connect.php';
	
	//if ($_GET['p'] == '') $_GET['p'] = 1;
	

	$ro_user = Result($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"');
	if ($ro_user['user_group_id'] == 1) {
		if ($_POST['act'] == 'filter') {
			$_SESSION['report_product_option_quantity_status'] = $_POST['report_product_option_quantity_status'];
		}
?>
<html>
	<head>
		<title>Отчет по товарам в заказах без группировки (SedEdition)</title>
		<link type="text/css" href="/admin/index.php" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<!--
		<script language="javascript" src="js/right.js"></script>
		<script language="javascript" src="js/right-calendar.js"></script>
		<script language="javascript" src="js/ru.js"></script>
		-->
	</head>
	<body>
		<style>
			.hover-gray:hover {
				background-color: #EEE;
			}
		</style>
	
		<form action="" method="post">
			<input type="hidden" name="act" value="filter" />
			Статуc:
			<select name="report_product_option_quantity_status">
				<option value=""></option>
				<option value="1"<?=($_SESSION['report_product_option_quantity_status'] === '1') ? ' selected' : ''?>>включен</option>
				<option value="0"<?=($_SESSION['report_product_option_quantity_status'] === '0') ? ' selected' : ''?>>отключен</option>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" value="Фильтр" />
		</form>

		<ul>
<?php
		$qu_product = '
			SELECT		product.product_id as id,
								product.model as model,
								product_description.name as name,
								manufacturer.name as manufacturer,
								product.status as status,
								product.quantity as quantity
			FROM			product_description,
								product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
			WHERE			product.product_id = product_description.product_id
								'.(($_SESSION['report_product_option_quantity_status'] !== '') ? ' && product.status="'.$_SESSION['report_product_option_quantity_status'].'"' : '').'
			ORDER BY	product_description.name,
								manufacturer.name,
								product.model
		';
		$re_product = @mysqli_query($ddb, $qu_product);
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$qu_option = '
				SELECT	option_description.name as name,
								option_value_description.name as value,
								product_option_value.quantity as quantity
				FROM		option_description,
								option_value_description,
								product_option_value
				WHERE		product_option_value.product_id="'.$ro_product['id'].'" &&
								product_option_value.option_id = option_description.option_id &&
								product_option_value.option_value_id = option_value_description.option_value_id
			';
			$re_option = @mysqli_query($ddb, $qu_option);
			echo '<li><span'.((@mysqli_num_rows($re_option)) ? ' onClick="$(\'#option-'.$ro_product['id'].'\').toggle();" style="cursor:pointer; text-decoration: underline; display:inline-block; width:450px;"' : ' style="display:inline-block; width:450px;"').' class="hover-gray">'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' ('.$ro_product['quantity'].')</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/index.php?route=catalog/product/update&token='.$_SESSION['token'].'&product_id='.$ro_product['id'].'" target="_blank">edit</a>';
			if (@mysqli_num_rows($re_option)) {
				echo '<ul id="option-'.$ro_product['id'].'" style="display:none;">';
				while ($ro_option = @mysqli_fetch_array($re_option)) echo '<li><span style="width:200px; display: inline-block;">'.$ro_option['name'].': '.$ro_option['value'].'</span>'.$ro_option['quantity'].'</li>';
				echo '</ul>';
			}
			echo '</li>';	
		}
?>		
		</ul>
	</body>
</html>
<?php
	} else {
		header('Location: /');
	}

