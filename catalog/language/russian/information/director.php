<?php
// Heading
$_['heading_title']  = 'Написать директору';

// Text 
$_['text_location']  = 'Наше местонахождение';
$_['text_contact']   = 'Написать нам';
$_['text_address']   = 'Адрес:';
$_['text_email']     = 'E-Mail:';
$_['text_telephone'] = 'Телефон:';
$_['text_fax']       = 'Факс:';
$_['text_message']   = '<p>Ваш запрос был успешно отправлен директору магазина!</p>';

// Entry Fields
$_['entry_name']     = 'Ваше имя:';
$_['entry_email']    = 'Ваш E-Mail:';
$_['entry_phone']    = 'Ваш телефон:';
$_['entry_enquiry']  = 'Ваш вопрос:';
$_['entry_captcha']  = 'Введите код, указанный на картинке:';
$_['entry_button_send']  = 'Отправить';
$_['entry_button_main']  = 'На главную';

// Email
$_['email_subject']  = 'Написать директору: %s';

// Errors
$_['error_subject']     = 'Тема сообщения должна быть указана!';
$_['error_publish']     = 'Поле должно быть заполнено!';
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-mail адрес введён неверно!';
$_['error_phone']    = 'Телефон введён неверно (от 10 до 32 символов)!';
$_['error_enquiry']  = 'Длина текста должна быть от 10 до 3000 символов!';
$_['error_captcha']  = 'Проверочный код не совпадает с изображением!';
?>
