<?php if ($products) {?>
<section class="box bestseller">
<!--21:02:23--><div class="box-heading"><?php echo $heading_title; ?></div>
<?php $i=0; foreach ($products as $product) { $i++; ?>
<div class="one-third<?php echo ($i % 3 == 0)? " last":"";?>" >
	<article class="box-content product-item hit<?php echo $product['sale'].$product['gift'];?>">
<?php if ($product['thumb']) { ?>
<div class="image">
    <a href="<?php echo $product['href']; ?>">
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"/>
    </a>
</div>
<?php } ?>
<div class="name" style="height: 40px;">
    <a href="<?php echo $product['href']; ?>" style="font-size:110%;"><?php echo $product['name']; ?></a>
</div>
<?php if ($product['price']) { ?>
<div class="price">
    <?php if (!$product['special']) { ?>
    <?php echo $product['price']; ?>
    <?php } else { ?>
    <span class="price-old"><?php echo intval($product['price']); ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
    <?php } ?>
</div>
<?php } ?>
    <div class="cart">
        <?php 
				
					//if($this->session->data['cart'][$product['product_id']]>0) {
					$incart = '';
					$tmp_cart_var_names  = array();
					foreach ($_SESSION['cart'] as $cart_name => $cart_value) {
						$tmp = explode(':',$cart_name);
						if ($product['product_id'] == $tmp[0]) $incart = 'incart';
					}
					// *********** SEDOY конец проверки есть ли в корзине
					if ($incart) {
				
				?>
        <a href="<?php echo $product['href']; ?>" class="button " onclick="addToCart('<?php echo $product['product_id']; ?>','1','mainfeat'); return false;" id="addtocart_<?php echo $product['product_id']; ?>"><?php echo $button_cart; ?></a>
				<a href="/index.php?route=checkout/simplecheckout" class="button incart" id="incart_mainfeat_<?php echo $product['product_id']?>" style="padding: 6px 12px 4px 16px;" title="В корзине"></a>
        <?php } else { ?>
        <a href="<?php echo $product['href']; ?>" class="button " onclick="addToCart('<?php echo $product['product_id']; ?>','1','mainfeat'); return false;" id="addtocart_<?php echo $product['product_id']; ?>"><?php echo $button_cart; ?></a>
				<a href="/index.php?route=checkout/simplecheckout" class="button incart" id="incart_mainfeat_<?php echo $product['product_id'];?>" style="padding: 6px 12px 4px 16px; display: none;" title="В корзине"></a>
        <?php } ?>
    </div>
</article>
</div>
<?php } ?>
</section>
<?php } ?>
