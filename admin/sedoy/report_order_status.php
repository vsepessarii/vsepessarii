﻿<?php
//отчет по статусам заказов 2015-03-14
error_reporting(2047);
require 'connect.php';

$qu = 'SELECT * FROM order_status ORDER BY order_status_id';
$re = @mysqli_query($ddb, $qu);
while ($ro = @mysqli_fetch_assoc($re)) $order_status[$ro['order_status_id']] = $ro['name'];

if ($ro_user['user_group_id'] == 1) {
	if (isset($_GET['s']) && ($_GET['s'] == '')) $_GET['s'] = '`ID` DESC';
	if ($_POST['act'] == 'clear_filter') {
		$_SESSION['report_order_status_id'] = '';

		$_SESSION['report_order_status_sum_from'] = '';		
		$_SESSION['report_order_status_sum_to'] = '';		

		$_SESSION['report_order_status_delay_from'] = '';		
		$_SESSION['report_order_status_delay_to'] = '';		

		$_SESSION['report_order_status_date_in_from'] = '';
		$_SESSION['report_order_status_date_in_to'] = '';
		$_SESSION['report_order_status_time_in_from'] = '';
		$_SESSION['report_order_status_time_in_to'] = '';

		$_SESSION['report_order_status_date_fc_from'] = '';
		$_SESSION['report_order_status_date_fc_to'] = '';
		$_SESSION['report_order_status_time_fc_from'] = '';
		$_SESSION['report_order_status_time_fc_to'] = '';

		$_SESSION['report_order_status_status_fc'] = '';		
		$_SESSION['report_order_status_status'] = '';		

		$_SESSION['report_order_status_email'] = '';		
	}

	if (isset($_SESSION['report_order_status_per_page']) && $_SESSION['report_order_status_per_page'] == '') $_SESSION['report_order_status_per_page'] = 25;
	if ($_POST['act'] == 'setting') {
		$_SESSION['report_order_status_per_page'] = $_POST['report_order_status_per_page'];
		$_GET['p'] = 1;		
	}

	if ($_POST['act'] == 'filter') {
		$_SESSION['report_order_status_id'] = isset($_POST['report_order_status_id']) ? $_POST['report_order_status_id'] : '';

		$_SESSION['report_order_status_sum_from'] = isset($_POST['report_order_status_sum_from']) ? $_POST['report_order_status_sum_from'] : '';
		$_SESSION['report_order_status_sum_to'] = isset($_POST['report_order_status_sum_to']) ? $_POST['report_order_status_sum_to'] : '';

		$_SESSION['report_order_status_delay_from'] = isset($_POST['report_order_status_delay_from']) ? $_POST['report_order_status_delay_from'] : '';
		$_SESSION['report_order_status_delay_to'] = isset($_POST['report_order_status_delay_to']) ? $_POST['report_order_status_delay_to'] : '';

		$_SESSION['report_order_status_date_in_from'] = isset($_POST['report_order_status_date_in_from']) ? $_POST['report_order_status_date_in_from'] : '';
		$_SESSION['report_order_status_date_in_to'] = isset($_POST['report_order_status_date_in_to']) ? $_POST['report_order_status_date_in_to'] : '';
		$_SESSION['report_order_status_time_in_from'] = isset($_POST['report_order_status_time_in_from']) ? $_POST['report_order_status_time_in_from'] : '';
		$_SESSION['report_order_status_time_in_to'] = isset($_POST['report_order_status_time_in_to']) ? $_POST['report_order_status_time_in_to'] :'';

		$_SESSION['report_order_status_date_fc_from'] = isset($_POST['report_order_status_date_fc_from']) ? $_POST['report_order_status_date_fc_from'] : '';
		$_SESSION['report_order_status_date_fc_to'] = isset($_POST['report_order_status_date_fc_to']) ? $_POST['report_order_status_date_fc_to'] :'';
		$_SESSION['report_order_status_time_fc_from'] = isset($_POST['report_order_status_time_fc_from']) ? $_POST['report_order_status_time_fc_from'] : '';
		$_SESSION['report_order_status_time_fc_to'] = isset($_POST['report_order_status_time_fc_to']) ? $_POST['report_order_status_time_fc_to'] : '';

		$_SESSION['report_order_status_status_fc'] = isset($_POST['report_order_status_status_fc']) ? $_POST['report_order_status_status_fc'] : '';
		$_SESSION['report_order_status_status'] = isset($_POST['report_order_status_status']) ? $_POST['report_order_status_status'] : '';

		$_SESSION['report_order_status_email'] = isset($_POST['report_order_status_email']) ? $_POST['report_order_status_email'] : '';

		$_GET['p'] = 1;
	}

	@mysqli_query($ddb, 'TRUNCATE TABLE tmp_report_order_status');


	$qu_order = '
		SELECT		*
		FROM		`order`
		WHERE		order_status_id>0
					'.(($_SESSION['report_order_status_date_in_from']) ? '  && date_added>="'.$_SESSION['report_order_status_date_in_from'].' 00:00:00"' : '').(($_SESSION['report_order_status_date_in_to']) ? '  && date_added<="'.$_SESSION['report_order_status_date_in_to'].' 23:59:59"' : '').'
		ORDER BY	order_id
	';
	//echo $qu_order;
	$re_order = @mysqli_query($ddb, $qu_order);
	while ($ro_order = @mysqli_fetch_array($re_order)) {
		$fc = Result($ddb, 'SELECT * FROM order_history WHERE order_id='.$ro_order['order_id'].' ORDER BY date_added ASC LIMIT 1,1'); //первое изменение
		$id = $ro_order['order_id'];
		$sum = $ro_order['total'];
		$date_in = date('Y-m-d',strtotime($ro_order['date_added']));
		$time_in = date('H:i:s', strtotime($ro_order['date_added']));
		$date_fc = ($fc['order_history_id']) ? date('Y-m-d',strtotime($fc['date_added'])) : '';
		$time_fc = ($fc['order_history_id']) ? date('H:i:s', strtotime($fc['date_added'])) : '';
		$status = $ro_order['order_status_id'];
		$status_fc = $fc['order_status_id'];
		$comment_fc = $fc['comment'];
		$delay = floor((strtotime($fc['date_added'])-strtotime($ro_order['date_added']))/60);
		$email = $ro_order['email'];

		@mysqli_query($ddb, '
				INSERT INTO	`tmp_report_order_status`
				SET			`ID`="'.$id.'",
							`SUM`="'.$sum.'",
							`DATE_IN`="'.$date_in.'",
							`TIME_IN`="'.$time_in.'",
							`DATE_FC`="'.$date_fc.'",
							`TIME_FC`="'.$time_fc.'",
							`STATUS_FC`="'.$status_fc.'",
							`COMMENT_FC`="'.mysqli_escape_string($comment_fc).'",
							`STATUS`="'.$status.'",
							`DELAY`="'.$delay.'",
							`EMAIL`="'.$email.'"
			');
		//echo mysqli_error($ddb);
	}
	//echo microtime_float().'<br />';

	?>
	<html>
	<head>
		<title>Отчет по статусам заказов (SedEdition)</title>
		<link type="text/css" href="<?php echo $app_url?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
		<script language="javascript">
			$(function() {
				$( "#report_order_status_date_in_from" ).datepicker();
				$( "#report_order_status_date_in_to" ).datepicker();
				$( "#report_order_status_date_fc_from" ).datepicker();
				$( "#report_order_status_date_fc_to" ).datepicker();
			});

			function ShowHideProducts(order_id) {
				var products = $('#products_'+order_id);
				if (products.css('display') == 'none') {
					$.get('ajax/order_products.php', { order_id: order_id }).done(function(data) {
						products.html(data);
					});
					products.css('display', 'block');
				} else {
					products.html('');
					products.css('display', 'none');
				}
			}
		</script>
	</head>
	<body>
	<style>
		td { word-wrap: break-word;}
	</style>
	<div align="right">
		<br />
		<a href="/index.php?route=common/home&token=<?php echo  $_SESSION['token']?>">Вернуться в панель управления</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="clear_filter" />
			<input type="submit" value="Сбросить фильтры" />
		</form>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<form method="post" style="display: inline-block;">
			<input type="hidden" name="act" value="setting" />
			Выводить по:
			<select name="report_order_status_per_page">
				<option value="25"<?php echo  (isset($_SESSION['report_order_status_per_page']) && $_SESSION['report_order_status_per_page'] == '25') ? ' selected' : ''?>>25</option>
				<option value="50"<?php echo  (isset($_SESSION['report_order_status_per_page']) && $_SESSION['report_order_status_per_page'] == '50') ? ' selected' : ''?>>50</option>
				<option value="100"<?php echo  (isset($_SESSION['report_order_status_per_page']) && $_SESSION['report_order_status_per_page'] == '100') ? ' selected' : ''?>>100</option>
				<option value="200"<?php echo  (isset($_SESSION['report_order_status_per_page']) && $_SESSION['report_order_status_per_page'] == '200') ? ' selected' : ''?>>200</option>
			</select>
			<input type="submit" value="Сохранить" />
		</form>
		<br /><br />
	</div>
	<table class="list">
		<thead>
		<tr>
			<td class="left" width="2%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`ID` ASC')) ? '`ID` DESC' : '`ID` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`ID` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`ID` DESC') ? 'desc' : '')?>">№</a></td>
			<td class="left" width="4%">
				<a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`DATE_IN` ASC') ? '`DATE_IN` DESC' : '`DATE_IN` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`DATE_IN` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`DATE_IN` DESC') ? 'desc' : '')?>">Дата</a>
				&nbsp;&nbsp;&nbsp;
				<a href="" onclick="$('#report_order_status_date_in_from').val('<?php echo  date('Y-m-d');?>'); $('#report_order_status_date_in_to').val('<?php echo  date('Y-m-d');?>'); return false" style="color: red;">Сег.</a>
			</td>
			<td class="left" width="2%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`TIME_IN` ASC')) ? '`TIME_IN` DESC' : '`TIME_IN` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`TIME_IN` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`TIME_IN` DESC') ? 'desc' : '')?>">Время</a></td>
			<td class="left" width="4%">
				<a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`DATE_FC` ASC') ? '`DATE_FC` DESC' : '`DATE_FC` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`DATE_FC` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`DATE_FC` DESC') ? 'desc' : '')?>">Дата п/и.</a>
				&nbsp;&nbsp;&nbsp;
				<a href="" onclick="$('#report_order_status_date_fc_from').val('<?php echo  date('Y-m-d');?>'); $('#report_order_status_date_fc_to').val('<?php echo  date('Y-m-d');?>'); return false" style="color: red;">Сег.</a>
			</td>
			<td class="left" width="2%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`TIME_FC` ASC') ? '`TIME_FC` DESC' : '`TIME_FC` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`TIME_FC` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`TIME_FC` DESC') ? 'desc' : '')?>">Время п/и</a></td>
			<td class="left" width="5%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`STATUS_FC` ASC') ? '`STATUS_FC` DESC' : '`STATUS_FC` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`STATUS_FC` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`STATUS_FC` DESC') ? 'desc' : '')?>">Статус п/и</a></td>
			<td class="left" width="30%">Комментарий перв.изм.</td>
			<td class="left" width="5%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`STATUS` ASC') ? '`STATUS` DESC' : '`STATUS` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`STATUS` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`STATUS` DESC') ? 'desc' : '')?>">Статус текущий</a></td>
			<td class="left" width="2%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`SUM` ASC') ? '`SUM` DESC' : '`SUM` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`SUM` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`SUM` DESC') ? 'desc' : '')?>">Сумма заказа</a></td>
			<td class="left" width="2%"><a href="?p=<?php echo  $_GET['p']?>&s=<?php echo  (isset($_GET['s']) && $_GET['s'] == '`DELAY` ASC') ? '`DELAY` DESC' : '`DELAY` ASC';?>" class="<?php echo  (isset($_GET['s']) && ($_GET['s'] == '`DELAY` ASC') ? 'asc' : '').(isset($_GET['s']) && ($_GET['s'] == '`DELAY` DESC') ? 'desc' : '')?>">Задержка (минут)</a></td>
			<td class="left" width="2%">E-mail</td>
			<td class="left" width="2%">Действие</td>
		</tr>
		</thead>
		<tbody>
		<form method="post" id="filter">
			<input type="hidden" name="act" value="filter" />
			<tr class="filter">
				<td><input type="text" name="report_order_status_id" value="<?php echo  $_SESSION['report_order_status_id'];?>" style="width: 100%;"></td>
				<td>
					<input type="text" name="report_order_status_date_in_from" id="report_order_status_date_in_from" value="<?php echo  $_SESSION['report_order_status_date_in_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_date_in_to" id="report_order_status_date_in_to" value="<?php echo  $_SESSION['report_order_status_date_in_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_order_status_time_in_from" id="report_order_status_time_in_from" value="<?php echo  $_SESSION['report_order_status_time_in_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_time_in_to" id="report_order_status_time_in_to" value="<?php echo  $_SESSION['report_order_status_time_in_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_order_status_date_fc_from" id="report_order_status_date_fc_from" value="<?php echo  $_SESSION['report_order_status_date_fc_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_date_fc_to" id="report_order_status_date_fc_to" value="<?php echo  $_SESSION['report_order_status_date_fc_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_order_status_time_fc_from" id="report_order_status_time_fc_from" value="<?php echo  $_SESSION['report_order_status_time_fc_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_time_fc_to" id="report_order_status_time_fc_to" value="<?php echo  $_SESSION['report_order_status_time_fc_to'];?>" style="width: 100%;">
				</td>
				<td>
					<select name="report_order_status_status_fc">
						<option value=""></option>
						<?php
							reset($order_status);
							foreach ($order_status as $key => $value) echo '<option value="'.$key.'"'.(($key == $_SESSION['report_order_status_status_fc']) ? ' selected' : '').'>'.$value.'</option>';
						?>
					</select>
				</td>
				<td></td>
				<td>
					<select name="report_order_status_status">
						<option value=""></option>
						<?php
							reset($order_status);
							foreach ($order_status as $key => $value) echo '<option value="'.$key.'"'.(($key == $_SESSION['report_order_status_status']) ? ' selected' : '').'>'.$value.'</option>';
						?>
					</select>
				</td>
				<td>
					<input type="text" name="report_order_status_sum_from" value="<?php echo  $_SESSION['report_order_status_sum_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_sum_to" value="<?php echo  $_SESSION['report_order_status_sum_to'];?>" style="width: 100%;">
				</td>
				<td>
					<input type="text" name="report_order_status_delay_from" value="<?php echo  $_SESSION['report_order_status_delay_from'];?>" style="width: 100%;">
					<input type="text" name="report_order_status_delay_to" value="<?php echo  $_SESSION['report_order_status_delay_to'];?>" style="width: 100%;">
				</td>
				<td align="right"><input type="text" name="report_order_status_email" value="<?php echo  $_SESSION['report_order_status_email'];?>" style="width: 100%;"></td>
				<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
			</tr>
		</form>
		<?php
		$qu_order = '
						SELECT	*
						FROM 		tmp_report_order_status
						WHERE		1
										'.(($_SESSION['report_order_status_id'] != '') ? ' && `ID` LIKE "%'.$_SESSION['report_order_status_id'].'%"' : '').'
										'.(($_SESSION['report_order_status_date_in_from'] != '') ? ' && `DATE_IN`>="'.$_SESSION['report_order_status_date_in_from'].'"' : '').'
										'.(($_SESSION['report_order_status_date_in_to'] != '') ? ' && `DATE_IN`<="'.$_SESSION['report_order_status_date_in_to'].'"' : '').'
										'.(($_SESSION['report_order_status_time_in_from'] != '') ? ' && `TIME_IN`>="'.$_SESSION['report_order_status_time_in_from'].'"' : '').'
										'.(($_SESSION['report_order_status_time_in_to'] != '') ? ' && `TIME_IN`<="'.$_SESSION['report_order_status_time_in_to'].'"' : '').'
										'.(($_SESSION['report_order_status_date_fc_from'] != '') ? ' && `DATE_FC`>="'.$_SESSION['report_order_status_date_fc_from'].'"' : '').'
										'.(($_SESSION['report_order_status_date_fc_to'] != '') ? ' && `DATE_FC`<="'.$_SESSION['report_order_status_date_fc_to'].'"' : '').'
										'.(($_SESSION['report_order_status_time_fc_from'] != '') ? ' && `TIME_FC`>="'.$_SESSION['report_order_status_time_fc_from'].'"' : '').'
										'.(($_SESSION['report_order_status_time_fc_to'] != '') ? ' && `TIME_FC`<="'.$_SESSION['report_order_status_time_fc_to'].'"' : '').'
										'.(($_SESSION['report_order_status_status_fc'] != '') ? ' && `STATUS_FC`="'.$_SESSION['report_order_status_status_fc'].'"' : '').'										
										'.(($_SESSION['report_order_status_status'] != '') ? ' && `STATUS`="'.$_SESSION['report_order_status_status'].'"' : '').'										
										'.(($_SESSION['report_order_status_email'] != '') ? ' && `EMAIL` LIKE "%'.$_SESSION['report_order_status_email'].'%"' : '').'										
										'.(($_SESSION['report_order_status_sum_from'] != '') ? ' && `SUM`>="'.$_SESSION['report_order_status_sum_from'].'"' : '').'
										'.(($_SESSION['report_order_status_sum_to'] != '') ? ' && `SUM`<="'.$_SESSION['report_order_status_sum_to'].'"' : '').'
										'.(($_SESSION['report_order_status_delay_from'] != '') ? ' && `DELAY`>="'.$_SESSION['report_order_status_delay_from'].'"' : '').'
										'.(($_SESSION['report_order_status_delay_to'] != '') ? ' && `DELAY`<="'.$_SESSION['report_order_status_delay_to'].'"' : '').'
							';
		if(isset($_GET['s'])){
			$qu_order.= 'ORDER	BY '.$_GET['s'];
        }
		$re_order = @mysqli_query($ddb, $qu_order);
		if(isset($_SESSION['report_order_status_per_page'])){
			while ($ro_order = @mysqli_fetch_assoc($re_order)) $sum_total += $ro_order['SUM'];
			$nu_order_total = @mysqli_num_rows($re_order);
			$nu_pages = ceil($nu_order_total/$_SESSION['report_order_status_per_page']);
			$qu_order .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_order_status_per_page']).','.$_SESSION['report_order_status_per_page'];
			$re_order = @mysqli_query($ddb, $qu_order);
        }
		$sum_page = 0;
		while ($ro_order = @mysqli_fetch_array($re_order)) {
			$sum_page += $ro_order['SUM'];
			/*
			$summ_page['price_in'] += $ro_order['PRICE_IN'];
			$summ_page['price_out'] += $ro_order['PRICE_OUT'];
			$summ_page['price_delivery'] += $ro_order['PRICE_DELIVERY'];
			$summ_page['total'] += $ro_order['TOTAL'];
			$summ_page['margin'] += $ro_order['MARGIN'];
			$summ_page['tax'] += $ro_order['TAX'];
			$summ_page['courier_price'] += $ro_order['COURIER_PRICE'];
			$summ_page['profit'] += $ro_order['PROFIT'];
			*/
			?>
			<tr >
				<td><a href="#" onclick="ShowHideProducts(<?php echo  $ro_order['ID'];?>); return false;"><?php echo  $ro_order['ID']?></a></td>
				<td><?php echo  $ro_order['DATE_IN']?></td>
				<td><?php echo  $ro_order['TIME_IN']?></td>
				<td><?php echo  $ro_order['DATE_FC']?></td>
				<td><?php echo  $ro_order['TIME_FC']?></td>
				<td><?php echo  $order_status[$ro_order['STATUS_FC']]?></td>
				<td><?php echo  $ro_order['COMMENT_FC']?></td>
				<td><?php echo  $order_status[$ro_order['STATUS']]?></td>
				<td><?php echo  $ro_order['SUM']?></td>
				<td><?php echo  ($ro_order['DELAY'] >= 0) ? floor($ro_order['DELAY']/60).':'.sprintf('%02d',$ro_order['DELAY']-floor($ro_order['DELAY']/60)*60) : ''?></td>
				<td><?php echo  $ro_order['EMAIL']?></td>
				<td class="right">
					[<a href="/sedoy/order_new.php?id=<?php echo  $ro_order['ID']?>" target="_blank">New Edit</a>]<br />
				</td>
			</tr>

			<tr>
				<td colspan="10" style="display: none;" class="order_products" id="products_<?php echo  $ro_order['ID'];?>">
				</td>
			</tr>
		<?php
		}
		?>
		<tr>
			<td colspan="8"><strong>Итог по странице</strong></td>
			<td><strong><?php echo  isset($sum_page) ? $sum_page : ''?></strong></td>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="8"><strong>Итог по всем страницам</strong></td>
			<td><strong><?php echo  isset($sum_page) ? $sum_page : ''?></strong></td>
			<td colspan="3"></td>
		</tr>
		</tbody>
	</table>
	<div class="pagination">
		<div class="links">
			<?php
            if(isset($nu_pages)){
	            for ($i=max(1,$_GET['p']-10);$i<=min($nu_pages,$_GET['p']+10);$i++) {
		            echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.$_GET['s'].'">'.$i.'</a> ';
	            }
            }
			?>
		</div>
		<div class="results">Показано с <?php echo isset($_SESSION['report_order_status_per_page']) ? (($_GET['p']-1)*$_SESSION['report_order_status_per_page']+1) : '';?> по <?php echo isset($_SESSION['report_order_status_per_page']) && isset($nu_order_total) ? min($_GET['p']*$_SESSION['report_order_status_per_page'],$nu_order_total) : '';?> из <?php echo  isset($nu_order_total) ? $nu_order_total : '';?> (всего страниц: <?php echo  isset($nu_pages) ? $nu_pages : '';?>)</div>
	</div>
	</body>
	</html>
<?php
} else {
	header('Location: /');
}

