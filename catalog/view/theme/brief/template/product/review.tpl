<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review-list">
  <?php
		$ro_product = @mysqli_fetch_array(@mysqli_query($ddb, '
			SELECT	product_description.name as name,
							product.model as model,
							manufacturer.name as manufacturer
			FROM		product,
							product_description,
							manufacturer
			WHERE		product.product_id = product_description.product_id &&
							product.product_id = "'.$review['product_id'].'" &&
							product.manufacturer_id = manufacturer.manufacturer_id
		'));
		//echo mysqli_Error();
		//var_dump($ro_product);
	?>
	<h2 style="margin-bottom: 4px; display: inline-block;"><ins><?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model']; ?></ins></h2> отзыв:
	<div class="author"><b><?php echo $review['author']; ?></b> <?php echo $review['date_added']; ?></div>
  <div class="rating"><img src="catalog/view/theme/brief/image/stars-<?php echo $review['rating'] . '.png'; ?>" alt="<?php echo $review['reviews']; ?>" /></div>
  <div class="text"><?php echo $review['text']; ?></div><br />
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_reviews; ?></div>
<?php } ?>
