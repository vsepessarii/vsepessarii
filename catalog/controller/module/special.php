<?php
class ControllerModuleSpecial extends Controller
{
    protected function index($setting)
    {
        $this->language->load('module/special');
        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['button_cart'] = $this->language->get('button_cart');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->data['products'] = array();
        $data = array(
            'sort' => 'pd.name',
            'order' => 'ASC',
            'start' => 0
//            'limit' => $setting['limit']
        );

        $results = $this->model_catalog_product->getProductSpecials($data);
        shuffle($results); $results = array_slice($results,0,$setting['limit']); //SEDOY перемешали товары в акциях в левой колонке
//				echo '<!--'.count($results).'-->';
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $setting['image_width'], $setting['image_height']);
            } else {
                $image = false;
            }

           if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $special = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = $result['rating'];
            } else {
                $rating = false;
            }

            $this->data['products'][] = array(
                'product_id' => $result['product_id'],
                'thumb' => $image,
                'name' => $result['name'].' '.$result['manufacturer'].' '.($result['model']!=$result['name']?$result['model']:''),
                'price' => $price,
                'special' => $special,
                'rating' => $rating,
                'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                'manufacturer' => $result['manufacturer'],
                'sale' => (@mysqli_num_rows(@mysqli_query($ddb, 'SELECT product_discount_id FROM product_discount WHERE product_id="'.$result['product_id'].'" && (date_start<=NOW() || date_start="0000-00-00 00:00:00") && (date_end>=NOW() || date_end="0000-00-00")')) ? ' sale' : ''),
				'gift' => (@mysqli_num_rows(@mysqli_query($ddb, 'SELECT product_option_value_id FROM product_option_value WHERE product_id="'.$result['product_id'].'" && option_id=24')) ? ' gift' : '')
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/module/special.tpl';
        } else {
            $this->template = 'default/template/module/special.tpl';
        }

        $this->render();
    }
}
?>