<?php
	require 'connect.php';
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	if ($ro_user['user_group_id'] != 1) die();
	$order = Result($ddb, 'SELECT * FROM `order` WHERE `order_id`='.intval($_GET['id']));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Квитанция СБ РФ (ПД-4)</title>

<style type="text/css">
body {font-family:Arial, Helvetica, sans-serif;/*font-size:14px;*/}
a {color:#006400;}
p {padding: 5px 0px 0px 5px;}
.vas ul {padding: 0px 10px 0px 15px;}
.vas li {list-style-type:circle;}
h3 {padding:0px 0px 0px 5px;font-size:100%;}
h1 {color:#006400;padding:0px 0px 0px 5px;font-size:120%;}
li {list-style-type: none;padding-bottom:5px;padding: 6px 0px 0px 5px;}
.main {font-size:12px;}
.list {font-size:12px;padding: 6px 15px 0px 5px;}
.main input {font-size:12px;background-color:#CCFFCC;}
.text14 {font-family:"Times New Roman", Times, serif;font-size:14px;}
.text14 strong {font-family:"Times New Roman", Times, serif;font-size:11px;}
.link {font-size:12px;}
.link a {text-decoration:none;color:#006400;}
.link_u {font-size:12px;}
.link_u a {color:#006400;}
</style>

</head>

<body>
<div class="text14">
  <table width="720" bordercolor="#000000" style="border:#000000 1px solid;" cellpadding="0" cellspacing="0">
    <tr>
      <td width="220" valign="top" height="250" align="center" style="border-bottom:#000000 1px solid; border-right:#000000 1px solid;">&nbsp;<strong>Платеж</strong></td>
      <td valign="top" style="border-bottom:#000000 1px solid; border-right:#000000 1px solid;">
      <li><strong>Получатель: </strong> <font style="font-size:90%"> <?php echo GetSetting($ddb, 'sberbank_transfer_bank_1');?></font>&nbsp;&nbsp;&nbsp;<br />
        <li><strong>ИНН:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_inn_1');?>&nbsp;&nbsp;<font style="font-size:11px"> &nbsp;</font> <strong>P/сч.:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_rs_1');?>&nbsp;&nbsp;<br />
        <li> <strong>в:</strong> <font style="font-size:90%"> <?php echo GetSetting($ddb,'sberbank_transfer_bankuser_1');?> </font><br />
        <li><strong>БИК:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_bik_1');?>&nbsp; <strong>К/сч.:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_ks_1');?> <br />
        <li>
					<strong>Платеж:</strong> <font style="font-size:90%">Оплата заказа № <?php echo $order['order_id']?></font>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Номер отделения:</strong> <font style="font-size:90%"><?php echo GetSetting($ddb, 'sberbank_transfer_office_1');?></font>
				<br />
        <li><strong>Плательщик:</strong> <?php echo $order['firstname']?>  <br />
        <li><strong>Адрес плательщика:</strong> <font style="font-size:90%"><?php echo $order['payment_country'].', '.$order['payment_city'].', '.$order['payment_address_1'];?></font><br />
        <li><strong>ИНН плательщика:</strong> ____________&nbsp;&nbsp;&nbsp;&nbsp; <strong>№ л/сч. плательщика:</strong> ______________
        <li><strong>Сумма:</strong> <?php echo ($order['total']-$order['delta']);?> руб. &nbsp;&nbsp;&nbsp;&nbsp;<strong>Сумма оплаты услуг банка:</strong> ______ руб. __ коп.<br />
          <br />
          Подпись:________________________        Дата: &quot; __ &quot;&nbsp;_______&nbsp;&nbsp;20&nbsp;&nbsp; г. <br />
          <br />
      </td>
    </tr>
    <tr>
      <td width="220" valign="top" height="250" align="center" style="border-bottom:#000000 1px solid; border-right:#000000 1px solid;">&nbsp;<strong>Платеж</strong></td>
      <td valign="top" style="border-bottom:#000000 1px solid; border-right:#000000 1px solid;">
      <li><strong>Получатель: </strong> <font style="font-size:90%"> <?php echo GetSetting($ddb,'sberbank_transfer_bank_1');?></font>&nbsp;&nbsp;&nbsp;<br />
        <li><strong>ИНН:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_inn_1');?>&nbsp;&nbsp;<font style="font-size:11px"> &nbsp;</font> <strong>P/сч.:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_rs_1');?>&nbsp;&nbsp;<br />
        <li> <strong>в:</strong> <font style="font-size:90%"> <?php echo GetSetting($ddb,'sberbank_transfer_bankuser_1');?> </font><br />
        <li><strong>БИК:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_bik_1');?>&nbsp; <strong>К/сч.:</strong> <?php echo GetSetting($ddb,'sberbank_transfer_ks_1');?> <br />
        <li>
					<strong>Платеж:</strong> <font style="font-size:90%">Оплата заказа № <?php echo $order['order_id']?></font>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Номер отделения:</strong> <font style="font-size:90%"><?php echo GetSetting($ddb,'sberbank_transfer_office_1');?></font>
				<br />
        <li><strong>Плательщик:</strong> <?php echo $order['firstname']?>  <br />
        <li><strong>Адрес плательщика:</strong> <font style="font-size:90%"><?php echo $order['payment_country'].', '.$order['payment_city'].', '.$order['payment_address_1'];?></font><br />
        <li><strong>ИНН плательщика:</strong> ____________&nbsp;&nbsp;&nbsp;&nbsp; <strong>№ л/сч. плательщика:</strong> ______________
        <li><strong>Сумма:</strong> <?php echo ($order['total']-$order['delta']);?> руб. &nbsp;&nbsp;&nbsp;&nbsp;<strong>Сумма оплаты услуг банка:</strong> ______ руб. __ коп.<br />
          <br />
          Подпись:________________________        Дата: &quot; __ &quot;&nbsp;_______&nbsp;&nbsp;20&nbsp;&nbsp; г. <br />
          <br />
      </td>
    </tr>
  </table>
</div>
</body>
</html>