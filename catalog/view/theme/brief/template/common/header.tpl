<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
    <meta http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta name="yandex-verification" content="5b191dd7241c0ca1" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1020<?php if (strpos($_SERVER['HTTP_USER_AGENT'],'iPad') === false && 0) echo ', initial-scale=1'; ?>" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon" />
    <?php } ?>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/reset.css">
    <link rel="stylesheet/less" type="text/css" href="catalog/view/theme/brief/less/stylesheet.less">
    <?php foreach ($styles as $style) { ?>
    <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <!-- <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/slideshow.css" media="screen" /> -->
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery.ui.all.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/brief/stylesheet/search_suggestion.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="/catalog/view/theme/brief/stylesheet/jcarousel.css" media="screen"/>
    <link href='https://fonts.googleapis.com/css?family=Cuprum:400,400italic,700,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shiv -->
    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
    
		<!-- JV_Quick_Order -->
		<script type="text/javascript" src="catalog/view/javascript/jv_quickorder/jquery.validate.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/jv_quickorder/jquery.maskedinput-1.3.min.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/jv_quickorder/jv_quickorder.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/jv_bootstrap/bootstrap.min.js"></script>
		<!-- JV_Quick_Order -->
    
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery.tinycarousel.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript" src="catalog/view/theme/brief/js/brief.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/mask.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/search_suggestion.js"></script>
    <script type="text/javascript" src="/catalog/view/javascript/jquery.jcarousel.js"></script>
    
    <!--[if lt IE 10]>
    <script type="text/javascript" src="js/PIE.js"></script>
    <![endif]-->
    <script type="text/javascript" src="catalog/view/javascript/less-1.3.1.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
    <?php foreach ($scripts as $script) { ?>
    <script type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
	<script>
	    var rrPartnerId = "5548af9d1e99472698173ad5";
	    var rrApi = {};
	    var rrApiOnReady = rrApiOnReady || [];
	    rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
        	rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
    	(function(d) {
	        var ref = d.getElementsByTagName('script')[0];
        	var apiJs, apiJsId = 'rrApi-jssdk';
        	if (d.getElementById(apiJsId)) return;
        	apiJs = d.createElement('script');
        	apiJs.id = apiJsId;
        	apiJs.async = true;
        	apiJs.src = "//cdn.retailrocket.ru/content/javascript/api.js";
        	ref.parentNode.insertBefore(apiJs, ref);
    	}(document));
	</script>    
</head>
<body>

<div style="width: 1020px !important; margin: 0 auto !important;">
<div class="page-wrapper">
<div class="page-container">
<nav class="top-line">
    <div class="layout-wrapper">
        <ul class="secondary-menu align-left">
            <li><?php if (!$logged) { ?>
            <?php echo $text_welcome; ?>
            <?php } else { ?>
            <?php echo $text_logged; ?>
            <?php } ?></li>
        </ul>
				
				<style>
					a.social {
						display: inline-block;
						width: 24px;
						height: 25px;
						vertical-align: middle;
						background-image: url(/image/social.png);
						margin-top: 3px !important;
					}

					a#social_fb {background-position: 0px 0px;}
					a#social_fb:hover {background-position: 0px -25px;}
					a#social_fb:active {background-position: 0px -50px;}

					a#social_twitter {background-position: -25px 0px;}
					a#social_twitter:hover {background-position: -25px -25px;}
					a#social_twitter:active {background-position: -25px -50px;}

					a#social_vk {background-position: -50px 0px;}
					a#social_vk:hover {background-position: -50px -25px;}
					a#social_vk:active {background-position: -50px -50px;}

					a#social_ok {background-position: -75px 0px;}
					a#social_ok:hover {background-position: -75px -25px;}
					a#social_ok:active {background-position: -75px -50px;}

					a#social_youtube {background-position: -100px 0px;}
					a#social_youtube:hover {background-position: -100px -25px;}
					a#social_youtube:active {background-position: -100px -50px;}
				</style>
				
        <ul class="secondary-menu align-right" style="padding: 5px 0 0 0;">
            <li style="padding-left: 0px; padding-top: 8px;">Мы в соцсетях:</li>
        	<li style="padding-right: 110px;">
				<a href="https://vk.com/gynecology_help" onclick="yaCounter23350270.reachGoal('vk'); return true;" target="_blank" title="Vkontakte" id="social_vk" class="social"></a>&nbsp;
				<a href="https://www.facebook.com/vsepessarii" onclick="yaCounter23350270.reachGoal('fb'); return true;" target="_blank" title="Facebook" id="social_fb" class="social"></a>&nbsp;
				<a href="https://www.youtube.com/channel/UCgbOB6snd4MebX711drqcSw" onclick="yaCounter23350270.reachGoal('youtube'); return true;" target="_blank" title="Youtube" id="social_youtube" class="social"></a>
			</li>
        	
        	<!--
					<li style=""><a href="https://vsepessarii.ru/"><img src="/image/flags/24/ru.png" /></a></li>
					-->
			<li class="director" style="padding-top: 5px;"><a href="/director.html"><img src="/image/director.png">&nbsp; Написать директору</a></li>


<!--        	<li style="">-->
<!--						<a href="https://vsepessarii.com/" target="_blank">-->
<!--							<img src="/image/flags/24/ua.png" align="middle" />-->
<!--							+38 (098) 388-45-63 (Украина)-->
<!--						</a>-->
<!--					</li>-->

        	<li style="padding-top: 5px;">
						<img src="/image/flags/24/kz.png" align="middle" />
						+7 (717) 272-70-68 (Казахстан)
						</a>
					</li>
					
					<li style="width: 20px;"></li>

					<li style="padding-top: 5px;">
					<a href="https://vsepessarii.ru/" onclick="yaCounter23350270.reachGoal('icon_ru'); return true;"><img src="/image/flags/24/ru.png" /></a>
					</li>
					<li style="padding-top: 5px;">
					<a href="https://vsepessarii.com/" target="_blank" onclick="yaCounter23350270.reachGoal('icon_kz'); return true;"><img src="/image/flags/24/kz.png" /></a>
					</li>




				<!--        
				    <li style="padding-left: 0px; padding-top: 8px;">Мы в соцсетях:</li>
        	<li style="padding-right: 25px;">
						<a href="https://vk.com/briefmed" target="_blank" title="Vkontakte" id="social_vk" class="social"></a>
						<a href="https://www.facebook.com/brief.ru" target="_blank" title="Facebook" id="social_fb" class="social"></a>
						<a href="https://www.odnoklassniki.ru/group/56794892795965" target="_blank" title="Одноклассники" id="social_ok" class="social"></a>
					</li>
        	<li style=""><a href="https://market.yandex.ru/shop/152023/reviews" target="_blank"><img src="/image/banner_otzyvy.jpg" onmouseover="this.src='/image/banner_otzyvy_hover.jpg';" onmouseout="this.src='/image/banner_otzyvy.jpg'" /></a></li>
        	<li style=""><a href="https://market.yandex.ru/shop/152023/reviews/add?retpath=http%3A%2F%2Fmarket.yandex.ru%2Fshop%2F152023%2Freviews%3Fcpc%3DoblBbesuZeXcedXAXtaTMA%26cmid%3D1975678158" target="_blank"><img src="/image/banner_ostav_otzyv.jpg" onmouseover="this.src='/image/banner_ostav_otzyv_hover.jpg';" onmouseout="this.src='/image/banner_ostav_otzyv.jpg'" /></a></li>
				-->
           
					 <?php //echo $link_to_compare; ?>
            <!--
            <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
            <li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
            -->
				</ul>
    </div>
    
</nav>

<header class="header">
    <div class="layout-wrapper">
        <?php if ($logo) { ?>
        <!--20:58:03--><div class="logo align-left"><a href="/" title="Пессарий цена, пессарий купить"><img src="<?php echo $logo; ?>" title="<?php echo $title; ?>" alt="<?php echo $title; ?>" ></a></div>
        <?php } ?>
        

<style type="text/css">
	#recall_form {
		background: none repeat scroll 0 0 #FFFFFF;
		box-shadow: 0 2px 13px rgba(0, 0, 0, 0.5);
		display: none;
		left: 50%;
		padding: 3px;
		position: fixed;
		top: 50%;
		width: 330px;
		z-index: 99999;
	}
	.green_big_title {
		color: #FFF;
		/*font-family: Georgia; */
		font-size: 15px;
		/* font-style: italic; */
		line-height: 24px;
		padding: 2px 0 2px 10px;
	}
	.recall_header {
		background: none repeat scroll 0 0 #0081e5;
		margin: 5px;
		width: 320px;
		padding: 2px 0 2px 10px;
		
	}
	
	.recall_header {
		cursor: move;
	}
	
	.recall_input {
		width: 210px;
	}
	.success_block {
		/*
		background: none repeat scroll 0 0 #9BFF9D;
		border: 1px solid #3ABC3D;
		border-radius: 4px 4px 4px 4px;
		*/
		margin: 20px 10px;
		text-align: center;
	}
	.form_table_recall .input_error{
		border-color: #7A0404;
	}

	.form_table_recall td {
		padding: 3px !important;
	} 	
	
	.form_table_recall {
		margin-bottom: 0px !important;
	} 	
	
	.form_table_recall input[type=text], .form_table_recall textarea {
		border: 1px solid #999;
		font-family: Arial;	
	}
	
	.error_message {
		color: #7A0404;
		/*display: block;*/
		font-family: tahoma;
		font-size: 11px;
		font-weight: normal;
		line-height: 11px;
		padding: 10px 10px 10px 0;
		display: none;
	}
	
	a.recall_close {
		display: inline-block;
		margin-top: 5px;
	}


</style>
<div id="recall_form" class="ui-draggable" style="border-radius: 10px 10px 10px 10px; display: none;">

	<table cellpadding="0" border="0" class="recall_header " colspan="0" style="border-radius: 5px 5px 5px 5px;">
		<tbody><tr>
			<td valign="center" align="center">
				<div class="green_big_title">Заказать обратный звонок</div>
			</td>
			<td valign="center" align="right">
				<a onclick="recall_close();" class="recall_close" href="javascript:void(0);"><img title="Закрыть окно" src="catalog/view/theme/brief/image/cancel.png"></a>
			</td>
		</tr>
	</tbody></table>

	<div id="recall_message2" style="display:none;" class="error_block">
		<span id="recall_message" class="error_message" style="text-align:center"></span>
	</div>

	<div style="display:none;" id="recall_success" class="success_block">
		<span class="success_message" style="text-align:center">
			Ваша заявка успешно отправлена!		</span>
			<br /><br /><br />
			<a onclick="recall_close();" class="button blue" href="javascript:void(0);">Закрыть</a>			
			
	</div>

	<form id="recall_ajax_form" onsubmit="return recall_ajax();" method="POST">
		<input type="hidden" value="yes" name="recall">
		<table cellspacing="5" class="form_table_recall" cellpadding="3">
			<tbody>
			<tr>
				<td class="td_recall_caption">Ваше имя:</td>
				<td colspan="2"><input type="text" class="recall_input" value="" id="user_name" name="user_name"><div id="user_name_error" class="error_message"></div></td>
			</tr>
			<tr>
				<td class="td_recall_caption"><span class="required">*</span>&nbsp;Ваш телефон:</td>
				<td colspan="2"><input type="text" class="recall_input" value="" id="user_phone" name="user_phone"><div id="user_phone_error" class="error_message"></div></td>
			</tr>
<!--			<tr>-->
<!--				<td class="td_recall_caption">Ваш e-mail:</td>-->
<!--				<td colspan="2"><input type="text" class="recall_input" value="" id="user_email" name="user_email"><div id="user_email_error" class="error_message"></div></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="td_recall_caption"><span class="required">*</span>&nbsp;Город:</td>-->
<!--				<td colspan="2"><input type="text" class="recall_input" value="" id="user_city" name="user_city"><div id="user_city_error" class="error_message"></div></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="td_recall_caption">Время звонка:</td>-->
<!--				<td colspan="2"><input type="text" class="recall_input" value="" id="recommend_to_call" name="recommend_to_call" /><div id="recommend_to_call_error" class="error_message"></div></td>-->
<!--			</tr>-->
<!--			<tr>-->
<!--				<td class="td_recall_caption">Комментарий:</td>-->
<!--				<td colspan="2"><textarea class="recall_input" rows="5" cols="20" id="user_comment" name="user_comment"></textarea><div id="user_comment_error" class="error_message"></div></td>-->
<!--			</tr>-->
			<tr>
			<td align="center" colspan="3">
				<img style="display:none;" id="load_recall" src="catalog/view/theme/default/image/loading.gif">
				<button class="button  blue" id="submit_recall" type="submit">Оставить заявку</button>
			</td>
		</tr>
		</tbody></table>
	</form>
</div>
<script>
	function recall_close(){
		$('#recall_form').hide();
		return false;
	}

	function recall_show(){
        margin_top = -$('#recall_form').height()/2;
        margin_left= -$('#recall_form').width()/2;
        $('#recall_form').css({'margin-left': margin_left, 'margin-top': margin_top });
		$('#recall_form').show();

		$('#recall_ajax_form').show();
		$('#recall_success').hide();

		$('#user_name').val('');
		$('#user_phone').val('');
		$('#user_city').val('');
		$('#recommend_to_call').val('');
		$('#user_comment').val('');
		$('#recall_code').val('');
		return false;
	}

	function show_message_recall(id_message, message){
		$('#'+id_message+'_error').html(message).show();
		$("#"+id_message).focus();
		$("#"+id_message).addClass('input_error');
		return false;
	}

	function recall_ajax(){
		var vars = $('#recall_ajax_form').serialize();
		$('#load_recall').show();
		$('#submit_recall').hide();
		$.ajax({
			type: "POST",
			data: 'recall=yes&'+vars,
			url:'index.php?route=module/recall/ajax',
			dataType:'json',
			success: function(json){
				$('#load_recall').hide();
				$('#submit_recall').show();
				$('.recall_input').removeClass('input_error');
				$('.error_message').html('').hide();
				switch (json['result']) {
					case 'success':
						$('#recall_message2').hide();
						$('#recall_ajax_form').hide();
						$('#recall_success').show();
					break;
					case 'error':
					    $.each(json['errors'], 
						function(index, value){
							show_message_recall(index, value);
						});

					break;
				}
			}
			});
		return false;
	}

	/*
	jQuery(function($){
		$.mask.definitions['~']='[0-6,9]';
			$("#user_phone").mask("+7 (~99) 999-99-99");
	});
	*/		
</script>
        
        
        
        

        <div class="align-right">
            <?php echo $cart; ?>
            
            <aside class="schedule-wrapper">
            	<a href="/contacts.html" onclick="yaCounter23350270.reachGoal('header_phones'); return true;">Наши операторы работают <red>КРУГЛОСУТОЧНО</red><br>Самовывоз: пн-пт 10:00-18:00, сб-вс 10:00-17:00</a>
            </aside>

            <?php //if ($telephone) { ?>
            <aside class="contacts-wrapper">
						<div onclick="recall_show();" style="
							display: inline-block; 
							position: absolute; 
							top: 7px;
							padding-top: 1px; 
							left: 803px; 
							width:173px; 
							height:16px; 
							background-color: #0081e5;
							color: #FFF; 	
							-moz-border-radius: 10px; 
							-webkit-border-radius: 10px; 
							border-radius: 10px;
							font-size: 11px;
							text-align: center;
							cursor: pointer;
						">Заказать обратный звонок</div>
						<!--
						<div style="
							display: inline-block; 
							position: absolute; 
							top: 3px;
							padding-top: 5px; 
							left: 775px; 
							width:203px; 
							height:18px; 
							background-color: red;
							color: #FFF; 	
							-moz-border-radius: 10px; 
							-webkit-border-radius: 10px; 
							border-radius: 10px;
							font-size: 11px;
							text-align: center;
							cursor: pointer;
							font-weight: bold;
						" onclick="window.location='/index.php?route=information/news&news_id=16'">График работы&nbsp; 31.12.14-12.01.15</div>
						-->						 
            
            <!--a href="<?php echo $contact ?>"-->
            <a href="/contacts.html">
            <p class="title" style="margin-top: 6px;">Наши <em>телефоны:</em></p>
            <p class="phones"><span>Москва:&nbsp;</span> <?php echo $config_telephone_msk; ?></p>
            <p class="phones"><span>Санкт-Петербург:&nbsp;</span> <?php echo $config_telephone_spb; ?></p>
            <p class="phones"><span>Россия:&nbsp;</span> <?php echo $config_telephone_rus; ?></p>
            </a>
            </aside>
            <?php //} ?>
        </div>
    </div>
</header>

<nav class="menu-wrapper">
    <div class="layout-wrapper">
        <ul class="main-menu">
<?php
	/*
		старое меню	
            <li><a href="/">Главная</a></li>
            <?php foreach ($informations as $information) { ?>
            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php } ?>
       <!-- <li><a href="<?php echo $about; ?>">О нас</a></li>
            <li><a href="#">Вопрос-Ответ</a></li>
            <li><a href="#">Доставка</a></li>-->
            <li><a href="<?php echo $faq; ?>">Статьи</a></li>
            <li class="action"><a href="<?php echo $special; ?>">Акции</a></li>
            <!--li><a href="<?php echo $contact; ?>">Контакты</a></li-->
            <li><a href="/contacts.html">Контакты</a></li>
	
	*/
?>
					<li><a href="/" title="Пессарий купить, пессарий цена."><img src="/image/design/home-icon.png" height="20" alt="Купить акушерские и маточные пессарии, низкие цены на пессарии." /></a></li>
					<!--li><a href="/about_us.html">О нас</a></li-->
					<li><a href="/konsultatsija-ginekologa/">Консультация гинеколога <sup style="color: red; font-weight: bold;">NEW</sup></a></li>
					<li><a href="/index.php?route=information/faq">FAQ</a></li>
					<li><a href="/statji.html">Статьи</a></li>
					<!--li><a href="/index.php?route=product/special">Акции</a></li-->
					<li><a href="/dostavka.html">Доставка и оплата</a></li>
					<li><a href="/reviews.html">Отзывы</a></li>
					<li><a href="/contacts.html">Контакты</a></li>
        </ul>

        <div id="search">
            <form action="/index.php" method="get">
            <input type="hidden" name="route" value="product/search">
            <?php if ($filter_name) { ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
            <?php } else { ?>
            <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#000000';"/>
            <?php } ?>
            <input type="submit" class="button-search" value="Найти"></form>
        </div>
    </div>
</nav>

<div class="content-wrapper sidebar-left">
<div class="layout-wrapper"><div id="notification"></div>