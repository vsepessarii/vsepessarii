<?php 
	class ControllerInformationPodbor extends Controller {
		private $error = array(); 
		public function index() {
			$this->document->setTitle('Подбор пессария');  
			
			//хлебные крошки
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),        	
				'separator' => false
			);

			$this->data['breadcrumbs'][] = array(
				'text'      => 'Подбор пессария',
				'href'      => '/podbor.html',
				'separator' => $this->language->get('text_separator')
			);	
			
    	$this->data['heading_title'] = 'Подбор типа пессария по медицинским показаниям';
    	$this->data['pobor_description'] = htmlspecialchars_decode($this->config->get('config_pobor_description'));
    	
    	$this->data['type']= array(
				'Истмико-цервикальная недостаточность' => array(441,440,1954),
				'I степень - шейка матки опускается не больше,чем до половины длины влагалища' => array(449,1909,1944),
				'I Степень и сопутствующее недержание мочи ' => array(458,1946),
				'II степень - шейка матки и/или стенки влагалища опускаются до входа во влагалище' => array(449,456,1928,1911,1909,439,460,1944,1945,1949,1948),
				'II степень и сопутствующее недержание мочи' => array(462,1947),
				'III степень - шейка матки и/или стенки влагалища опускаются за пределы входа во влагалище,а тело матки располагается выше него ' => array(1928,1911,439,460,457,443,1948,1949,1951,1952),
				'III степень и сопутствующее недержание мочи ' => array(462,1928,1911,439,460,457,443,1947,1948,1949,1951,1952),
				'IV степень - вся матка и/или стенки влагалища находятся за пределами входа во влагалище' => array(1928,1911,457,443,1951,1952),
				'IV степень и сопутствующее недержание мочи' => array(462,1928,1911,457,443,1951,1952,1947)
			);

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/podbor.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/podbor.tpl';
			} else {
				$this->template = 'default/template/information/podbor.tpl';
			}
		
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
				
			$this->response->setOutput($this->render());		
  	}

  	public function success() {
		}
	
  	private function validate() {
  	}

		public function captcha() {
		}	
	}
?>