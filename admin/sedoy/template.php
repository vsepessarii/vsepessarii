<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?php echo $tpl['meta_title'];?></title>
		<link rel="stylesheet" href="style.css"></li>
		<link type="text/css" href="/admin/index.php" rel="stylesheet" />
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>
	</head>
	<body>
		<h1><?php echo $tpl['body_title'];?></h1>
		<?php echo $tpl['body_content'];?>
	</body>
</html>