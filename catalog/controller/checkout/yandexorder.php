<?php 
define('SIMPLE_CHECKOUT', true); //Если используете быстрый заказ - true, иначе false

class ControllerCheckoutYandexorder extends Controller {
	private $userdata = array(
			'firstname' => '',
			'lastname' => '',
			'email' => '',
			'telephone' => '',
			'fax' => ''
		);
	private $addressdata  = array(
			//'company' => '',
			'address_1' => '',
			//'address_2' => '',
			'postcode' => '',
			'city' => '',
			'country_id' => '',
			//'zone_id' => ''
		);

	public function index() {
		$stage = $_COOKIE['yandex_stage'];
		$url_part = '';
		if (isset($this->request->post['operation_id'])) {
			$this->session->data['yandex_operation_id'] = $this->request->post['operation_id'];
		}
		elseif (isset($this->session->data['yandex_post']['operation_id'])) {
			$this->session->data['yandex_operation_id'] = $this->session->data['yandex_post']['operation_id'];
			$this->request->post = $this->session->data['yandex_post'];
			unset($this->session->data['yandex_post']);
		}

		$this->setData();
		if (!$this->customer->isLogged()) {
			if ($stage == 'payment') {
				$this->session->data['guest'] = $this->userdata;
				$this->session->data['guest']['payment'] = $this->addressdata;
				$this->session->data['temp_session'] = array_merge($this->userdata, $this->addressdata);
			}
			elseif ($stage == 'register') {
				foreach ($this->userdata as $key=>$value) {
					$this->session->data['shipping_'.$key] = $value;
				}
				foreach ($this->addressdata as $key=>$value) {
					$this->session->data['shipping_'.$key] = $value;
				}
				$this->session->data['temp_session'] = array_merge($this->userdata, $this->addressdata);
			}
			elseif ($stage == 'shipping') {
				$this->session->data['guest']['shipping'] = array_merge($this->userdata, $this->addressdata);
				$this->session->data['temp_session'] = $this->session->data['guest']['shipping'];
			}
		}
		else {
			if ($stage == 'payment') {
				foreach ($this->userdata as $key=>$value) {
					$this->session->data['payment_'.$key] = $value;
				}
				foreach ($this->addressdata as $key=>$value) {
					$this->session->data['payment_'.$key] = $value;
				}
				$this->session->data['temp_session'] = array_merge($this->userdata, $this->addressdata);
			}
			elseif ($stage == 'shipping') {
				foreach ($this->userdata as $key=>$value) {
					$this->session->data['shipping_'.$key] = $value;
				}
				foreach ($this->addressdata as $key=>$value) {
					$this->session->data['shipping_'.$key] = $value;
				}
				if (isset($this->session->data['payment_address_id'])) {
					if (SIMPLE_CHECKOUT)
						$url_part.= '&customer_address_id='.$this->session->data['payment_address_id'];
					else
						$url_part.= '&payment_address_id='.$this->session->data['payment_address_id'];
				}
				$this->session->data['temp_session'] = array_merge($this->userdata, $this->addressdata);
			}
		}
		if (SIMPLE_CHECKOUT)
			$this->redirect($this->url->link('checkout/simplecheckout', 'stage='.$stage).$url_part);
		else
			$this->redirect($this->url->link('checkout/checkout', 'stage='.$stage).$url_part);
	}
	
	public function temp_data() {
		$json = array();
		if (isset($this->session->data['temp_session'])) {
			$json = $this->session->data['temp_session'];
			//unset($this->session->data['temp_session']);
		}
		$this->response->setOutput(json_encode($json));		
	}
	
	protected function setData() {
		/*
		Яндекс POST-запросом присылает данные:
		    operation_id — уникальный идентификатор операции передачи адреса (строка).
		    id — идентификатор адреса (целое число).
		    title — название адреса (строка).
		    street — улица (строка).
		    building — номер дома (целое число).
		    suite — корпус (строка).
		    flat — квартира (целое число).
		    entrance — подъезд (целое число).
		    floor — этаж (целое число).
		    intercom — домофон (строка).
		    city — город (строка).
		    country — страна (строка).
		    zip — индекс (целое число).
		    metro — станция метро (строка).
		    cargolift — наличие грузового лифта (значение «yes» при наличии, значение «no» при отсутствии).
		    firstname — имя (строка).
		    lastname — фамилия (строка).
		    fathersname — отчество (строка).
		    phone — телефон (строка).
		    phone-extra — дополнительный телефон (строка).
		    email — электронный адрес для связи (строка).
		    comment — комментарий к адресу (строка).
		*/
		
		$data = array(
			'firstname' => $this->request->post['firstname'],
			'lastname' => $this->request->post['lastname'],
			'email' => $this->request->post['email'],
			'telephone' => $this->request->post['phone'],
			'fax' => $this->request->post['phone-extra'],
			//'company' => '',
			'address_1' => ($this->request->post['street'] ? 'ул.'.$this->request->post['street'] : '').', '.
				($this->request->post['building'] ? ' д.'.$this->request->post['building'] : '').
				($this->request->post['suite'] ? ' корп.'.$this->request->post['suite'] : '').
				($this->request->post['flat'] ? ', кв.'.$this->request->post['flat'] : ''),
			//'address_2' => '',
			'postcode' => $this->request->post['zip'],
			'city' => $this->request->post['city'],
			'country_id' => $this->detectCountry($this->request->post['country']),
			//'zone_id' => ''
		);
		
		foreach ($this->userdata as $key=>$item) {
			$this->userdata[$key] = (isset($data[$key]) ? $data[$key] : '');
		}
		foreach ($this->addressdata as $key=>$item) {
			$this->addressdata[$key] = (isset($data[$key]) ? $data[$key] : '');
		}
	}
	
	private function detectCountry($country) {
		if ($country == 'Россия') return 176;
		$query = $this->db->query("SELECT country_id FROM " . DB_PREFIX . "country WHERE name = '" . $this->db->escape(utf8_strtolower($country)) . "' LIMIT 1");
	
		if ($query->num_rows)
			return $query->row['country_id'];
		return false;
	}
}
?>
