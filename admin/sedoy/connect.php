<?php
mb_internal_encoding("UTF-8");
header('Content-Type: text/html; charset=utf-8');
session_start();
//	$ddb = @mysqli_connect('localhost','vsepessarii_sql','tqekmwkg');
//$ddb = @mysqli_connect('localhost', 'homestead', 'secret');
$ddb = @mysqli_connect('localhost', 'homestead', 'secret', 'vsepessarii');
mysqli_set_charset( $ddb, 'utf8');
require __DIR__.'/../../vendor/autoload.php';
//@mysqli_query($ddb, "set names utf8");

$app_url = strtolower(explode('/', $_SERVER['SERVER_PROTOCOL'])[0]) .'://'.$_SERVER['HTTP_HOST'];

if (! $ddb) {
    echo "Don't connect to database";
    exit;
}
if (!isset($_GET['p']) || $_GET['p'] == '') {
    $_GET['p'] = 1;
}
//@mysqli_select_db($ddb, 'vsepessarii');

$ro_user = @mysqli_fetch_array(@mysqli_query(
    $ddb,
    'SELECT user_group_id FROM user WHERE user_id="' . intval($_SESSION['user_id']) . '"'
));

function Result($ddb, $q, $field = '')
{
    $tmp = @mysqli_fetch_array(@mysqli_query($ddb, $q));
    if ($field != '') {
        return $tmp[$field];
    } else {
        return $tmp;
    }
}

//значение value из таблицы setting по ключу
function GetSetting($ddb, $key)
{
    $tmp = Result($ddb, $ddb, 'SELECT * FROM setting WHERE `key`="' . mysqli_escape_string($ddb, $key) . '"');

    return $tmp['value'];
}

//возвращает список ID категорий (текущей и всех родителей)
function ParentCategories($ddb, $id)
{
    $ret[] = $id;
    do {
        $ro = @mysqli_fetch_array(@mysqli_query(
            $ddb,
            'SELECT parent_id FROM category WHERE category_id="' . $id . '"'
        ));
        $id = $ro['parent_id'];
        if ($id > 0) {
            $ret[] = $id;
        }
    } while ($id > 0);

    return $ret;
}

function SedReplace($text, $replace)
{
    foreach ($replace as $key => $value) {
        $text = str_replace($key, $value, $text);
    }

    return $text;
}

//возвращает текущую и все подчиненные категории
function ChildCategories($ddb, $id)
{
    $tmp[] = $id;
    $qu_1  = 'SELECT category_id FROM category WHERE parent_id="' . $id . '"';
    $re_1  = @mysqli_query($ddb, $qu_1);
    while ($ro_1 = @mysqli_fetch_array($re_1)) {
        $tmp[] = $ro_1['category_id'];
        $qu_2  = 'SELECT category_id FROM category WHERE parent_id="' . $ro_1['category_id'] . '"';
        $re_2  = @mysqli_query($ddb, $qu_2);
        while ($ro_2 = @mysqli_fetch_array($re_2)) {
            $tmp[] = $ro_2['category_id'];
            $qu_3  = 'SELECT category_id FROM category WHERE parent_id="' . $ro_2['category_id'] . '"';
            $re_3  = @mysqli_query($ddb, $qu_3);
            while ($ro_3 = @mysqli_fetch_array($re_3)) {
                $tmp[] = $ro_3['category_id'];
                $qu_4  = 'SELECT category_id FROM category WHERE parent_id="' . $ro_3['category_id'] . '"';
                $re_4  = @mysqli_query($ddb, $qu_4);
                while ($ro_4 = @mysqli_fetch_array($re_4)) {
                    $tmp[] = $ro_4['category_id'];
                }
            }
        }
    }

    return $tmp;
}

//возвращает ID пользователей, входящих в группу
function GroupUsers($ddb, $id)
{
    //echo '---'.$id.'---';
    $qu = 'SELECT user_id FROM user WHERE user_group_id="' . $id . '"';
    $re = @mysqli_query($ddb, $qu);
    while ($ro = @mysqli_fetch_array($re)) {
        $ret[] = $ro['user_id'];
    }

    return $ret;
}

//дерево категорий в <option>
function CategoryOptions($ddb, $parent = 0, $curr = '')
{
    $qu_1 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id = category_description.category_id && category.parent_id="' . $parent . '" ORDER BY category.sort_order';
    $re_1 = @mysqli_query($ddb, $qu_1);
    //echo $qu_1;
    while ($ro_1 = @mysqli_fetch_array($re_1)) {
        $ret  .= '<option value="' . $ro_1['category_id'] . '"' . (($ro_1['category_id'] == $curr) ? ' selected' : '') . '>' . $ro_1['name'] . '</option>';
        $qu_2 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id = category_description.category_id && category.parent_id="' . $ro_1['category_id'] . '" ORDER BY category.sort_order';
        $re_2 = @mysqli_query($ddb, $qu_2);
        while ($ro_2 = @mysqli_fetch_array($re_2)) {
            $ret  .= '<option value="' . $ro_2['category_id'] . '"' . (($ro_2['category_id'] == $curr) ? ' selected' : '') . '>&nbsp;&nbsp;&nbsp;' . $ro_2['name'] . '</option>';
            $qu_3 = 'SELECT category.category_id, category_description.name FROM category, category_description WHERE category.category_id = category_description.category_id && category.parent_id="' . $ro_2['category_id'] . '" ORDER BY category.sort_order';
            $re_3 = @mysqli_query($ddb, $qu_3);
            while ($ro_3 = @mysqli_fetch_array($re_3)) {
                $ret .= '<option value="' . $ro_3['category_id'] . '"' . (($ro_3['category_id'] == $curr) ? ' selected' : '') . '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $ro_3['name'] . '</option>';
            }
        }
    }

    return $ret;
}

//дерево групп и пользователей в <option>
function GroupUserOptions($ddb, $curr = '')
{
    $qu_group = 'SELECT user_group_id, name FROM user_group ORDER BY name';
    $re_group = @mysqli_query($ddb, $qu_group);
    while ($ro_group = @mysqli_fetch_array($re_group)) {
        $ret     .= '<option value="g' . $ro_group['user_group_id'] . '"' . (('g' . $ro_group['user_group_id'] == $curr) ? ' selected' : '') . '>' . $ro_group['name'] . '</option>';
        $qu_user = 'SELECT user_id, firstname, lastname FROM user WHERE user_group_id="' . $ro_group['user_group_id'] . '" ORDER BY lastname, firstname';
        $re_user = @mysqli_query($ddb, $qu_user);
        //echo $qu_user.'<br>';
        while ($ro_user = @mysqli_fetch_array($re_user)) {
            $ret .= '<option value="u' . $ro_user['user_id'] . '"' . (('u' . $ro_user['user_id'] == $curr) ? ' selected' : '') . '>&nbsp;&nbsp;&nbsp;' . $ro_user['lastname'] . ' ' . $ro_user['firstname'] . '</option>';
        }
    }

    return $ret;
}

//список производителей в <option>
function ManufacturerOptions($ddb, $curr = '')
{
    $ret             = '';
    $qu_manufacturer = 'SELECT * FROM manufacturer ORDER BY sort_order, name';
    $re_manufacturer = @mysqli_query($ddb, $qu_manufacturer);
    while ($ro_manufacturer = @mysqli_fetch_array($re_manufacturer)) {
        $ret .= '<option value="' . $ro_manufacturer['manufacturer_id'] . '"' . (($ro_manufacturer['manufacturer_id'] == $curr) ? ' selected' : '') . '>' . $ro_manufacturer['name'] . '</option>';
    }

    return $ret;
}

//пользователи в options
function UserOptions($ddb, $current)
{
    $qu = 'SELECT user_id, firstname, lastname FROM user ORDER BY lastname, firstname';
    $re = @mysqli_query($ddb, $qu);
    while ($ro = @mysqli_fetch_array($re)) {
        $ret .= '<option value="' . $ro['user_id'] . '"' . (($ro['user_id'] == $current) ? ' selected' : '') . '>' . $ro['lastname'] . ' ' . $ro['firstname'] . '</option>';
    }

    return $ret;
}

//числа в options
function NumberOptions($current, $min, $max, $step = 1)
{
    $ret = '';
    for ($i = $min; $i <= $max; $i = $i + $step) {
        $ret .= '<option value="' . $i . '"' . (($i == $current) ? ' selected' : '') . '>' . $i . '</option>';
    }

    return $ret;
}

//название товара
function ProductTitle($ddb, $id)
{
    $tmp = Result($ddb, '
			SELECT	product_description.NAME AS NAME,
							manufacturer.name as MANUFACTURER,
							product.model AS MODEL
			FROM		product_description,
							product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
			WHERE		product.product_id="' . $id . '" &&
							product.product_id = product_description.product_id
		');
    return trim($tmp['NAME'] . ' ' . $tmp['MANUFACTURER'] . ' ' . $tmp['MODEL']);
}

function AlertOrderStatusChange($ddb, $order_id, $new_status_id, $post = false)
{
    $order   = Result($ddb, $ddb, 'SELECT * FROM `order` WHERE order_id=' . intval($order_id));
    $courier = Result(
        $ddb,
        $ddb,
        'SELECT name FROM courier WHERE courier_id=' . intval(($post) ? $post['courier_id'] : $order['courier_id'])
    );

    if ($post) {
        switch (intval($post['country_id'])) {
            case '176':
                $country_suffix = 'ru';
                break;
            case '20':
                $country_suffix = 'by';
                break;
            case '109':
                $country_suffix = 'kz';
                break;
        }
    } else {
        switch ($order['shipping_country_id']) {
            case '176':
                $country_suffix = 'ru';
                break;
            case '20':
                $country_suffix = 'by';
                break;
            case '109':
                $country_suffix = 'kz';
                break;
        }
    }

    if ($order['order_status_id'] != $new_status_id) {
        $replace['[ORDER_ID]']      = $order['order_id'];
        $replace['[DECL]']          = ($post) ? $post['decl'] : $order['decl'];
        $replace['[ORDER_DATE]']    = date('d.m.Y', strtotime($order['date_added']));
        $replace['[SHIP_DATE]']     = date('d.m.Y', strtotime(($post) ? $post['ship_date'] : $order['date_shipped']));
        $replace['[ORDER_SUM]']     = ($post) ? $_POST['summ_after_ship'] : $order['total'];
        $replace['[COURIER_NAME]']  = $courier['name'];
        $replace['[COURIER_PHONE]'] = $courier['phone'];

        $alert = Result($ddb, $ddb, 'SELECT * FROM order_status_alert WHERE id=' . $new_status_id);
        if ($alert['sms_send']) {
            $phone = preg_replace('~\D+~', '', $order['telephone']);
            $text  = $alert['sms_text_' . $country_suffix];
            $text  = SedReplace($text, $replace);
            @mysqli_query(
                $ddb,
                'INSERT INTO sms SET PHONE="' . $phone . '", TEXT="' . mysqli_escape_string($ddb, $text) . '"'
            );
        }
        if ($alert['email_send']) {
            $topic = $alert['email_topic_' . $country_suffix];
            $topic = SedReplace($topic, $replace);
            $text  = $alert['email_text_' . $country_suffix];
            $text  = SedReplace($text, $replace);
            @mysqli_query($ddb, '
					INSERT INTO	mail
					SET			EMAIL_TO = "' . $order['email'] . '",
								NAME_TO = "' . $order['firstname'] . '",
								EMAIL_FROM = "' . GetSetting($ddb, 'config_email') . '",
								NAME_FROM = "' . GetSetting($ddb, 'config_name') . '",
								SUBJECT = "' . $topic . '",
								BODY_PLAIN = "Для чтения сообщения используйте HTML-просмотр!",
								BODY_HTML = "' . mysqli_escape_string($ddb, html_entity_decode($text)) . '"
				');
        }
    }
}
