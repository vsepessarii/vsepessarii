<?php echo $header; ?>

				<?php //echo $column_right; ?>
				<div class="content-container">

					<section class="box">
						<!--20:52:26--><div class="box-heading"><?php echo $heading_title; ?></div>

						<nav class="breadcrumb">
						    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
						    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
						    <?php } ?>
    					</nav>


						<div class="box-content" style="margin-top: 30px !important;">
						  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
						    <div class="content">
						      <table class="form">
						        <tr>
						          <td><?php echo $entry_newsletter; ?></td>
						          <td><?php if ($newsletter) { ?>
						            <input type="radio" name="newsletter" value="1" checked="checked" />
						            <?php echo $text_yes; ?>&nbsp;
						            <input type="radio" name="newsletter" value="0" />
						            <?php echo $text_no; ?>
						            <?php } else { ?>
						            <input type="radio" name="newsletter" value="1" />
						            <?php echo $text_yes; ?>&nbsp;
						            <input type="radio" name="newsletter" value="0" checked="checked" />
						            <?php echo $text_no; ?>
						            <?php } ?></td>
						        </tr>
						      </table>
						    </div>

						    <div class="buttons">
						      <div class="left"><a href="<?php echo $back; ?>" class="button gray align-left"><?php echo $button_back; ?></a></div>
						      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button blue align-left" /></div>
						    </div>
						  </form>
 							<div class="clear"></div>
  						</div>
					</section>

				<?php //echo $content_top; ?>
				<?php //echo $content_bottom; ?>
				</div>
				<?php echo $column_left; ?><?php echo $column_right; ?>

<?php echo $footer; ?>

<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--20:56:09--><div><?php echo $heading_title; ?></div>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="content">
      <table class="form">
        <tr>
          <td><?php echo $entry_newsletter; ?></td>
          <td><?php if ($newsletter) { ?>
            <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?>&nbsp;
            <input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?>
            <?php } else { ?>
            <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?>&nbsp;
            <input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?>
            <?php } ?></td>
        </tr>
      </table>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */?>