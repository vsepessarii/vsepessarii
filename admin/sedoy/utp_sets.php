<?php
	//предложения УТП
	/*header('Content-Type: text/html; charset=utf-8');
	session_start();*/

	require 'connect.php';	
	
	if ($_GET['p'] == '') $_GET['p'] = 1;

	if (isset($ro_user['user_group_id']) && $ro_user['user_group_id'] == 1) {
		if (isset($_GET['act']) && ($_GET['act'] == 'del')) {
			$_GET['id'] = intval($_GET['id']);
			$used = @mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM product WHERE utp_set_id='.intval($_GET['id'])));
			if ($used == 0) {
				@mysqli_query($ddb, 'DELETE FROM utp_set WHERE utp_set_id='.intval($_GET['id']));
				@mysqli_query($ddb, 'DELETE FROM utp_set_item WHERE utp_set_id='.intval($_GET['id']));
				$tpl['msg'] = 'Предложение УТП удалено.';
			} else {
				$tpl['msg'] = 'Предложение УТП изпользуется в '.$used.' товарах и не может быть удалено.';
			}
			$_GET['act'] = '';
			$_GET['id'] = '';

		}

		if(isset($_POST['act'])){
			switch ($_POST['act']) {
				case 'add':
					@mysqli_query($ddb, '
					INSERT INTO	utp_set
					SET			title="'.mysqli_escape_string($ddb,$_POST['title']).'",
								status="1"
				');
					$_GET['act'] = 'edit';
					$_GET['id'] = @mysqli_insert_id($ddb);
					#foreach ($_POST['item'] as $item_id => $sort) if ($sort > 0) @mysqli_query($ddb, 'INSERT INTO utp_set_item SET utp_set_id='.$_GET['id'].', utp_item_id='.$item_id.', sort='.$sort);
					$tpl['msg'] = 'Предложение УТП <b>"'.$_POST['title'].'"</b> добавлено.';
					break;

				case 'edit':
					#var_dump($_POST);
					$_POST['id'] = intval($_POST['id']);
					@mysqli_query($ddb, '
					UPDATE	utp_set
					SET		title="'.htmlspecialchars($_POST['title']).'",
							status="1"
					WHERE	utp_set_id="'.$_POST['id'].'"
				');
					$_GET['act'] = 'edit';
					$_GET['id'] = $_POST['id'];
					@mysqli_query($ddb, 'DELETE FROM utp_set_item WHERE utp_set_id='.$_GET['id']);
					#foreach ($_POST['item'] as $item_id => $sort) if ($sort > 0) @mysqli_query($ddb, 'INSERT INTO utp_set_item SET utp_set_id='.$_GET['id'].', utp_item_id='.$item_id.', sort='.$sort);
					$tpl['msg'] = 'Предложение УТП <b>"'.$_POST['title'].'"</b> изменено.';
					break;
			}
		}

		if (isset($_POST['item']) && isset($_POST['act']) && in_array($_POST['act'], array('add', 'edit'))) {
			foreach ($_POST['item'] as $item_id => $sort) if ($sort > 0) @mysqli_query($ddb, 'INSERT INTO utp_set_item SET utp_set_id='.$_GET['id'].', utp_item_id='.$item_id.', sort='.$sort);
			$qu_prod = '
				SELECT		p.product_id,
							usp.utp_set_id
				FROM		product p LEFT JOIN utp_set_product usp ON p.product_id = usp.product_id
				GROUP BY	p.product_id
			';
			$re_prod = @mysqli_query($ddb, $qu_prod);
			while ($ro_prod = @mysqli_fetch_assoc($re_prod)) if ($_POST['product'][$ro_prod['product_id']]) {
				@mysqli_query($ddb, 'DELETE FROM utp_set_product WHERE product_id='.$ro_prod['product_id']);
				@mysqli_query($ddb, 'INSERT INTO utp_set_product SET utp_set_id = '.$_GET['id'].', product_id = '.$ro_prod['product_id']);
			} elseif ($_POST['product'][$ro_prod['product_id']] < 1 AND $ro_prod['utp_set_id'] == $_GET['id']) {
				@mysqli_query($ddb, 'DELETE FROM utp_set_product WHERE product_id='.$ro_prod['product_id']);
			}
		}

		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"><a href="'.$_SERVER['PHP_SELF'].'?act=add" class="button">Новое предложение УТП</a></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br>
			'.((isset($tpl) && ($tpl['msg'] != '')) ? '<div class="msg">'.$tpl['msg'].'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="15%">
						<table class="list">
							<thead>
								<tr>
									<td width="5%"></td>
									<td width="5%"></td>
									<td width="90%">Предложение УТП</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM utp_set ORDER BY title, utp_set_id';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.(isset($_GET['page']) ? $_GET['page'] : '').'&id='.$ro['utp_set_id'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td align="center"><a href="?act=copy&id='.$ro['utp_set_id'].'"><img src="img/copy.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['utp_set_id'].'" style="'.(isset($_GET['id']) && ($ro['utp_set_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['title'].'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="85%">
		';
		if (isset($_GET['act']) && ($_GET['act'] == 'add' || $_GET['act'] == 'edit' || $_GET['act'] == 'copy')) {
			if ($_GET['act'] == 'add') {
				$tpl['body_title'] = 'Добавить предложение УТП';
			} elseif ($_GET['act'] == 'edit') {
				$tpl['body_title'] = 'Обновить предложение УТП';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM utp_set WHERE utp_set_id="'.intval($_GET['id']).'"'));
			} elseif ($_GET['act'] == 'copy') {
				$tpl['body_title'] = 'Копировать элемент УТП';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM utp_set WHERE utp_set_id="'.intval($_GET['id']).'"'));
				$_GET['id'] = '';
				$_GET['act'] = 'add';
			}
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.title.value == "") msg = msg + br + " - не заполнено наименование;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.$_GET['act'].'">
				<input type="hidden" name="id" value="'.(isset($_GET['id']) ? $_GET['id'] : '').'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Наименование (внутр.):</td>
						<td width="85%" class="txt" align="left"><input name="title" id="title" title="Наименование внутреннее (на сайте не отображается)" class="txt" style="width:350px;" value="'.$ro['title'].'"></td>
					</tr>
					<!--tr>
						<td class="txt" align="right">Текст:</td>
						<td class="txt" align="left"><textarea name="text" id="text" class="txt" style="width:350px; height: 200px;">'.(isset($ro['text']) ? $ro['text'] : '').'</textarea></td>
					</tr-->
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="'.(($_GET['act'] == 'add') ? 'Добавить' : 'Обновить').' предложение УТП"></td>
					</tr>
				</table>			
				<br><br>
				<h2>Элементы УТП в предложении</h2>
				<table width="60%" style="border-color: gray; border-collapse: collapse;" border="1">
					<tr>
						<td width="10%" align="center"><b>Порядок</b></td>
						<td width="90%"><b>Элемент УТП</b></td>
					</tr>
			';
			$qu_item = 'SELECT utp_item.*, CAST(utp_set_item.sort AS SIGNED) as sort FROM utp_item LEFT JOIN utp_set_item ON utp_item.utp_item_id = utp_set_item.utp_item_id AND utp_set_item.utp_set_id = '.$ro['utp_set_id'].' ORDER BY IF(sort IS NULL, 999999999, sort)';
			$re_item = @mysqli_query($ddb, $qu_item);
			#echo mysqli_error($ddb);
			while ($ro_item = @mysqli_fetch_assoc($re_item)) {
				$tpl['body_content'] .= '
					<tr'.(($ro_item['sort'] < 1) ? ' class="gray"' : '').'>
						<td align="center">
							<select name="item['.$ro_item['utp_item_id'].']">
								<option value=""></option>
				';
				for ($i=1;$i<=50;$i++) $tpl['body_content'] .= '<option value="'.$i.'"'.(($i == $ro_item['sort']) ? ' selected' : '').'>'.$i.'</option>';
				$tpl['body_content'] .= '	
							</select>
						</td>
						<td>'.$ro_item['name'].' / '.$ro_item['title'].'</td>
					</tr>
				';
			}
			$tpl['body_content'] .= '
				</table>
				<br><br>
				<h2>Товары, к которым привязано это предложение УТП</h2>
				<table width="100%" style="border-color: gray; border-collapse: collapse;" border="1">
					<tr>
						<td width="5%" align="center"><b>Привязка</b></td>
						<td width="5%" align="center"><b>ID</b></td>
						<td width="25%"><b>Категории</b></td>
						<td width="45%"><b>Товар</b></td>
						<td width="20%"><b>Текущая привязка</b></td>
					</tr>
			';
			$qu_product = '
				SELECT		p.product_id as product_id,
							p.model as product_model,
							pd.name as product_name,
							m.name as product_manufacturer,
							us.title as utp_set_title,
							us.utp_set_id as utp_set_id,
							(
								SELECT		GROUP_CONCAT(cd.name SEPARATOR "<br>")
								FROM		category_description cd, product_to_category ptc
								WHERE		cd.category_id = ptc.category_id AND ptc.product_id = p.product_id
								ORDER BY	ptc.main_category DESC
							) as cat
				FROM		product p
							LEFT JOIN product_description pd ON p.product_id = pd.product_id
							LEFT JOIN manufacturer m ON m.manufacturer_id = p.manufacturer_id
							LEFT JOIN utp_set_product usp ON p.product_id = usp.product_id
							LEFT JOIN utp_set us ON us.utp_set_id = usp.utp_set_id
				ORDER BY	cat,
							product_name,
							product_manufacturer,
							product_model,
							product_id
			';
			$re_product = @mysqli_query($ddb, $qu_product);
			while ($ro_product = @mysqli_fetch_assoc($re_product)) $tpl['body_content'] .= '
					<tr'.(isset($_GET['id']) && ($ro_product['utp_set_id'] != $_GET['id']) ? ' class="gray"' : '').'>
						<td align="center"><input type="checkbox" name=product['.$ro_product['product_id'].']" value="1"'.(($ro_product['utp_set_id'] == $ro['utp_set_id']) ? ' checked' : '').' /></td>
						<td align="center">'.$ro_product['product_id'].'</td>
						<td>'.$ro_product['cat'].'</td>
						<td>'.$ro_product['product_name'].' '.$ro_product['product_manufacturer'].' '.$ro_product['product_model'].'</td>
						<td>'.$ro_product['utp_set_title'].'</td>
					</tr>
			';
			$tpl['body_content'] .= '
				</table>

			';
		} else {
			$tpl['body_title'] = 'Предложения УТП';
		}
		$tpl['body_content'] .= '
				</form>
				</td>
			</tr>
		</table>
		';
		$tpl['meta_title'] = 'Предложения УТП';
		require 'template.php';	
	} else {
		header('Location: /');
	}

