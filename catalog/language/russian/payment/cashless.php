<?php
// Text
$_['text_title']         = 'Оплата по безналичному расчету (для юридических лиц)';
$_['text_instruction']   = '<center style="font-size:130%;">Для печати Счета нажмите на кнопку <strong>Печать Счета</strong> (откроется в новом окне)!</center><br />';
$_['text_instruction_mail']   = '<center>Для печати Счета нажмите на ссылку <strong>Печать Счета</strong> (откроется в новом окне)!<br>';
$_['text_payment']       = '<br /><center style="font-size:130%;">Распечатайте счет, затем нажмите кнопку в конце данной странице "Подтверждение заказа".</center>';
$_['text_payment_mail']  = 'После осуществления оплаты <strong>необходимо уведомить по телефону менеджера магазина</strong> с указанием номера заказа и <strong>выслать скан платежки/оплаченной квитанции</strong> через <a href="/contacts.html" target="_blank">форму обратной связи</a> или по электронной почте <a href="mailto:info@vsepessarii.ru" target="_blank">info@vsepessarii.ru</a> (с темой "Оплата заказа "№%s"). Ваш заказ будет обработан <strong>после того, как деньги поступят на наш расчетный счёт</strong>, и мы получим <strong>подтверждение, что это оплата именно Вашего заказа</strong>.</center>';
$_['text_order_history'] = 'Счет хранится в <a href="{href}" target="_blank">Истории заказов</a>';
/* $_['text_printpay']      = '<a href="{href}" class="button" style="text-decoration:none;" target="_blank"><span style="font-size:16px !important;">Печать Счета</span></a>'; */
$_['text_printpay']      = '<center><a class="button red" href="{href}" style="margin-right: 0;" target="_blank"><span style="font-size:14px !important;">Печать Счета</span></a></center>';
$_['text_printpay_mail']      = '<a class="button red" href="{href}" style="margin-right: 0;" target="_blank"><span style="font-size:13px !important;">Печать Счета</span></a><br />';


$_['text_month_1']       = 'Січня';
$_['text_month_2']       = 'Лютого';
$_['text_month_3']       = 'Березня';
$_['text_month_4']       = 'Квітня';
$_['text_month_5']       = 'Травня';
$_['text_month_6']       = 'Червня';
$_['text_month_7']       = 'Липня';
$_['text_month_8']       = 'Серпня';
$_['text_month_9']       = 'Вересня';
$_['text_month_10']      = 'Жовтня';
$_['text_month_11']      = 'Листопада';
$_['text_month_12']      = 'Грудня';

$_['text_month_1']       = 'Января';
$_['text_month_2']       = 'Ферваля';
$_['text_month_3']       = 'Марта';
$_['text_month_4']       = 'Апреля';
$_['text_month_5']       = 'Мая';
$_['text_month_6']       = 'Июня';
$_['text_month_7']       = 'Июля';
$_['text_month_8']       = 'Августа';
$_['text_month_9']       = 'Сентября';
$_['text_month_10']      = 'Октября';
$_['text_month_11']      = 'Ноября';
$_['text_month_12']      = 'Декабря';

?>