<?php
// Heading
$_['heading_title']    = 'Maintenance';

// Text
$_['text_maintenance'] = 'Maintenance';
$_['text_message']     = '<!--21:12:30--><div style="text-align:center;">We are currently performing some scheduled maintenance. <br/>We will be back as soon as possible. Please check back soon.</div>';
?>