<?php

class ModelRetailcrmCustomer extends Model {

    public function uploadToCrm($customers) {
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('retailcrm');

        if(empty($customers))
            return false;
        if(empty($settings['retailcrm_url']) || empty($settings['retailcrm_apikey']))
            return false;

        require_once DIR_SYSTEM . 'library/retailcrm/bootstrap.php';

        $this->retailcrmApi = new RetailcrmProxy(
            $settings['retailcrm_url'],
            $settings['retailcrm_apikey'],
            DIR_SYSTEM . 'logs/retailcrm.log'
        );

        $customersToCrm = array();

        foreach($customers as $customer) {
            $customersToCrm[] = $this->process($customer);
        }

        $chunkedCustomers = array_chunk($customersToCrm, 50);

        foreach($chunkedCustomers as $customersPart) {
            $this->retailcrmApi->customersUpload($customersPart);
        }
    }

    private function process($customer) {
        $customerToCrm = array(
            'externalId' => $customer['customer_id'],
            'firstName' => $customer['firstname'],
            'lastName' => $customer['lastname'],
            'email' => $customer['email'],
            'phones' => array(
                array(
                    'number' => $customer['telephone']
                )
            ),
            'createdAt' => $customer['date_added'],
            'commentary' => $customer['description']
        );

        $this->load->model('sale/customer');
        $address = $this->model_sale_customer->getAddress($customer['address_id']);

        if (!is_null($address)) {
            if (!empty($address['address_1'])) {
                $customerToCrm['address']['text'] = $address['address_1'];
            }
            if (!empty($address['postcode'])) {
                $customerToCrm['address']['index'] = $address['postcode'];
            }
            if (!empty($address['city'])) {
                $customerToCrm['address']['city'] = $address['city'];
                if (stripos($address['city'], '(РФ)') !== false) {
                    $customerToCrm['address']['countryIso'] = 'RU';
                } else if (stripos($address['city'], '(Казахстан)') !== false) {
                    $customerToCrm['address']['countryIso'] = 'KZ';
                } else if (stripos($address['city'], '(Беларусь)') !== false) {
                    $customerToCrm['address']['countryIso'] = 'BY';
                }
            }
        }

        if (empty($customer['lastname'])) {
            $customerToCrm = array_merge($customerToCrm, self::explodeFIO($customer['firstname']));
        }

        return self::filterRecursive($customerToCrm);
    }

    public static function explodeFIO($string)
    {
        $result = array();
        $parse = (!$string) ? false : explode(" ", $string, 3);
        switch (count($parse)) {
            case 1:
                $result['firstName'] = $parse[0];
                $result['lastName'] = '';
                $result['patronymic'] = '';
                break;
            case 2:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = '';
                break;
            case 3:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = $parse[2];
                break;
            default:
                return false;
        }
        return $result;
    }
    
    public static function filterRecursive($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::filterRecursive($haystack[$key]);
            }
            if (is_null($haystack[$key]) || $haystack[$key] === '' || count($haystack[$key]) == 0) {
                unset($haystack[$key]);
            } elseif (!is_array($value)) {
                $haystack[$key] = trim($value);
            }
        }
        return $haystack;
    }
}
