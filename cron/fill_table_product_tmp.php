<?php
	//заполнение таблицы для подбора товара в новой форме заказа
	mysqli_query('DELETE FROM product_tmp WHERE 1'.(($_SERVER['REMOTE_ADDR'] == '195.225.144.21200') ? ' AND id = 1190' : ''));
	$qu_product = '
		SELECT	product.product_id as id,
						product_description.name as name,
						manufacturer.name as manufacturer,
						product.model as model,
						product.sku as sku,
						product.price_in as price_in,
						product.price as price
		FROM		product_description,
						product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
		WHERE		product.product_id = product_description.product_id
					'.(($_SERVER['REMOTE_ADDR'] == '195.225.144.21200') ? ' AND product.product_id = 1190' : '').'
	';
	$re_product = mysqli_query($qu_product);
	while ($ro_product = mysqli_fetch_array($re_product)) {

		########	Fix Special Price (2016-08-07)
		/*
		$ro_special = Result($ddb, 'SELECT * FROM product_special WHERE product_id='.$ro_product['id'].' AND (date_start >= "'.date('Y-m-d').'" OR date_start = "0000-00-00") AND (date_end <= "'.date('Y-m-d').'" OR date_end = "0000-00-00") ORDER BY priority ASC LIMIT 1');
		if ($ro_special['price'] > 0) $ro_product['price'] = $ro_special['price'];
		*/

		unset($options); unset($options_txt); $oid = 0;
		unset($values); unset($values_txt); $vid = 0;
		$qu_option = '
			SELECT		`product_option`.*, `option_description`.`name`
			FROM			`product_option`, `option`, `option_description`
			WHERE			`product_option`.`product_id` = "'.$ro_product['id'].'" &&
								`product_option`.`option_id` = `option`.`option_id` &&
								`option`.`option_id` = `option_description`.`option_id`
			ORDER BY	`option`.`sort_order`
		';
		$re_option = mysqli_query($qu_option);
		while ($ro_option = mysqli_fetch_array($re_option)) {
			$options[$oid] = $ro_option['product_option_id'];
			$options_txt[$oid] = $ro_option['name'];
			$qu_value = '
				SELECT		`product_option_value`.*, `option_value_description`.`name`
				FROM			`product_option_value`, `option_value`, `option_value_description`
				WHERE			`product_option_value`.`product_option_id`="'.$ro_option['product_option_id'].'" &&
									`product_option_value`.`option_value_id`=`option_value`.`option_value_id` &&
									`option_value`.`option_value_id` = `option_value_description`.`option_value_id`
				ORDER BY	`option_value`.`sort_order`
			';
			$re_value = mysqli_query($qu_value);
			//echo mysqli_error();
			while ($ro_value = mysqli_fetch_array($re_value)) {
				$values[$oid][$vid] = $ro_value['product_option_value_id'];
				$values_txt[$oid][$vid] = $ro_value['name'];
				$values_price[$oid][$vid] = ($ro_value['price_prefix'] == '+') ? $ro_value['price'] : '-1'*$ro_value['price'];  //поправка цены
				$vid++;
			}
			$vid = 0;
			$oid++;
		}
		#echo print_r($options,1).'<br>';
		#echo print_r($options_txt,1).'<br>';
		#echo print_r($values,1).'<br>';
		#echo print_r($values_txt,1).'<br>';
		if (@count($options) > 0) {
			//расчитываем максимальное количество вариантов
			$n = 1; for ($i=0;$i<count($options);$i++) {
				$n = $n * count($values[$i]);
				#echo count($values[$i]).'<br>';
			}
			#echo $n.'<br>';
			//проходим по всем вариантам
			for ($i=0;$i<$n;$i++) {
				$d_price = 0; //поправка цены
				if (count($options) == 1) {
					//одно свойство
					$option = $options[0].':'.$values[0][$i];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i].')';
					$d_price = $d_price + $values_price[0][$i];
				} elseif (count($options) == 2) {
					//два свойства
					$i_0 = floor($i/count($values[1]));
					$i_1 = $i - $i_0*count($values[1]);
					$option = $options[0].':'.$values[0][$i_0].','.$options[1].':'.$values[1][$i_1];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i_0].'; '.$options_txt[1].': '.$values_txt[1][$i_1].')';
					$d_price = $d_price + $values_price[0][$i_0] + $values_price[1][$i_1];
				} elseif (count($options) == 3) {
					//три свойства
					$i_0 = floor($i/count($values[1])/count($values[2]));
					$i_1 = floor(($i-$i_0*count($values[1])*count($values[2]))/count($values[2]));
					$i_2 = $i - $i_0*count($values[0])*count($values[1]) - $i_1*count($values[2]);
					$option = $options[0].':'.$values[0][$i_0].','.$options[1].':'.$values[1][$i_1].','.$options[2].':'.$values[2][$i_2];
					$option_label = ' ('.$options_txt[0].': '.$values_txt[0][$i_0].'; '.$options_txt[1].': '.$values_txt[1][$i_1].'; '.$options_txt[2].': '.$values_txt[2][$i_2].')';
					$d_price = $d_price + $values_price[0][$i_0] + $values_price[1][$i_1] + $values_price[2][$i_2];
					if ($_SERVER['REMOTE_ADDR'] == '195.225.144.21200') echo $i.'-->'.$i_0.'*'.$i_1.'*'.$i_2.'<br>';
					if ($_SERVER['REMOTE_ADDR'] == '195.225.144.21200') echo $option_label.'<br>';
				}
				mysqli_query('
					INSERT INTO	`product_tmp`
					SET					`id`="'.$ro_product['id'].'",
											`label`="'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' '.$option_label.' '.$ro_product['sku'].' ('.floatval($ro_product['price']+$d_price).' руб.)",
											`name`="'.$ro_product['name'].'",
											`manufacturer`="'.$ro_product['manufacturer'].'",
											`model`="'.$ro_product['model'].'",
											`sku`="'.$ro_product['sku'].'",
											`price`="'.floatval($ro_product['price']).'",
											`price_in`="'.floatval($ro_product['price_in']+$d_price).'",
											`option`="'.$option.'"
				');
			}
		} else {
			mysqli_query('
			INSERT INTO	`product_tmp`
			SET					`id`="'.$ro_product['id'].'",
									`label`="'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].' '.$option_label.' '.$ro_product['sku'].' ('.floatval($ro_product['price']+$d_price).' руб.)",
									`name`="'.$ro_product['name'].'",
									`manufacturer`="'.$ro_product['manufacturer'].'",
									`model`="'.$ro_product['model'].'",
									`sku`="'.$ro_product['sku'].'",
									`price`="'.floatval($ro_product['price']).'",
									`price_in`="'.floatval($ro_product['price_in']).'",
									`option`=""
			');
		}						
		
	}
?>