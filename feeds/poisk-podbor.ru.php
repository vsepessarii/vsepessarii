<?
	header('Content-Type: text/xml; charset=windows-1251');
	$ddb = @mysqli_connect('localhost','briefmag_brief','brief2013');
	@mysqli_query($ddb, "set names cp1251");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	@mysqli_select_db('briefmag_db');
	@mysqli_query($ddb, 'SET NAMES cp1251');
	echo '<?xml version="1.0" encoding="windows-1251"?>';	
?>

<yml_catalog date="<?=date('Y-m-d H:i')?>">
	<shop>
		<name>Brief Medicine</name>
		<company>Brief Medicine</company>
		<url>https://vsepessarii.ru</url>
		<currencies>
			<currency id="RUR" rate="1"/>
		</currencies>
		<offers>
<?
	$qu_product = 'SELECT product_id, model, sku, image, manufacturer_id, price FROM product WHERE status="1" ORDER BY product_id';
	$re_product = @mysqli_query($ddb, $qu_product);
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_product_add = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name, description FROM product_description WHERE product_id="'.$ro_product['product_id'].'"'));
		$ro_manufacturer = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM manufacturer WHERE manufacturer_id="'.$ro_product['manufacturer_id'].'"'));
		
		$category = '';
		$ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_product['product_id'].'" && main_category="0"'));
		if ($ro_category['category_id'] < 1) $ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_product['product_id'].'" && main_category="1"')); 
		
		$ro_product_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="product_id='.$ro_product['product_id'].'"'));
		$url = $ro_product_url['keyword'].'.html';
		if ($ro_category['category_id'] > 0) {
			//////////////////////
			$ro_cat1_descr = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM category_description WHERE category_id="'.$ro_category['category_id'].'"'));
			$category = $ro_cat1_descr['name'];
			//////////////////////
			
			$ro_url_cat_1 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_category['category_id'].'"'));
			$url = $ro_url_cat_1['keyword'].'/'.$url;
			$ro_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_category['category_id'].'"'));
			if ($ro_cat_2['parent_id'] > 0) {
				//////////////////////
				$ro_cat2_descr = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM category_description WHERE category_id="'.$ro_cat_2['parent_id'].'"'));
				$category = $ro_cat2_descr['name'].'-->'.$category;
				//////////////////////
				$ro_url_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_2['parent_id'].'"'));
				$url = $ro_url_cat_2['keyword'].'/'.$url;
				$ro_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_cat_2['parent_id'].'"'));
				if ($ro_cat_3['parent_id'] > 0) {
					//////////////////////
					$ro_cat3_descr = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM category_description WHERE category_id="'.$ro_cat_3['parent_id'].'"'));
					$category = $ro_cat3_descr['name'].'-->'.$category;
					//////////////////////
					$ro_url_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_3['parent_id'].'"'));
					$url = $ro_url_cat_3['keyword'].'/'.$url;
				}
			}
		}
		$url = 'https://vsepessarii.ru/'.$url;

		$id = $ro_product['product_id'];
		$price = $ro_product['price'];
		$category_id = $ro_category['category_id'];
		$picture = 'https://vsepessarii.ru/image/'.$ro_product['image'];
		$name = $ro_product_add['name'].' '.$ro_manufacturer['name'].' '.$ro_product['model'];
		$vendor = $ro_manufacturer['name'];
		$description = htmlspecialchars(strip_tags(htmlspecialchars_decode($ro_product_add['description'])));

?>		
			<offer id="<?=$id?>">
				<category><?=$category;?></category>
				<title><?=$name?></title>
				<price><?=$price;?></price>
				<currencyId>RUR</currencyId>
				<url><?=$url;?></url>
				<img><?=$picture;?></img>
				<produceby><?=$vendor;?></produceby>
				<descript><?=$description?></descript>
			</offer>
<?
	}
?>
		</offers>
	</shop>
</yml_catalog>