<?php if (!$ajax) { ?>
<?php 
	
$simple_page = 'simplecheckout';
$heading_title .= $simple_show_weight ? '&nbsp;(<span id="weight">'. $weight . '</span>)' : '';
include $simple->tpl_header();
include $simple->tpl_static();
?>
<div class="simple-content">
<?php } ?>
    <div class="simplecheckout" id="simplecheckout_form">
    <!-- simplecheckout form -->
        <?php 
            $replace = array(
                '{three_column}'  => '<div class="simplecheckout-three-column">',
                '{/three_column}' => '</div>',
                '{left_column}'   => '<div class="simplecheckout-left-column">',
                '{/left_column}'  => '</div>',
                '{right_column}'  => '<div class="simplecheckout-right-column">',
                '{/right_column}' => '</div>',
                '{customer}'      => '<div class="simplecheckout-block" id="simplecheckout_customer"'.($simple_customer_hide_if_logged ? ' style="display:none;"' : '').'>'. $simplecheckout_customer .'</div>',
                '{cart}'          => '<div class="simplecheckout-block" id="simplecheckout_cart">' . $simplecheckout_cart . '</div><br><br>',
                '{login}'          => '<div class="simplecheckout-block" id="simplecheckout_login">' . $simplecheckout_login . '</div>',
                '{shipping}'      => $has_shipping ? '<div class="simplecheckout-block" id="simplecheckout_shipping"'.($simple_shipping_methods_hide ? ' style="display:none;"' : '').'>' . $simplecheckout_shipping . '</div>' : '',
                '{payment}'       => '<div class="simplecheckout-block" id="simplecheckout_payment"'.($simple_payment_methods_hide ? ' style="display:none;"' : '').'>' . $simplecheckout_payment . '</div>',
                '{agreement}'     => $simple_common_view_agreement_text ? '<div class="simplecheckout-block" id="simplecheckout_agreement"></div>' : '',
                '{help}'          => $simple_common_view_help_text ? '<div class="simplecheckout-block" id="simplecheckout_help"></div>' : '',
                '{payment_form}'  => '',
			);
            
            if ($simple_common_view_agreement_text && isset($information_title) && isset($information_text)) { 
                $replace['{agreement}'] = '<div class="simplecheckout-block" id="simplecheckout_agreement">';
                $replace['{agreement}'] .= '<div class="simplecheckout-block-heading">' . $information_title . '</div>';
                $replace['{agreement}'] .= '<div class="simplecheckout-block-content simplecheckout-scroll">' . $information_text . '</div>';
                $replace['{agreement}'] .= '</div>';
            }
            
            if ($simple_common_view_help_text && isset($help_title) && isset($help_text)) { 
                $replace['{help}'] = '<div class="simplecheckout-block" id="simplecheckout_help">';
                $replace['{help}'] .= '<div class="simplecheckout-block-heading">' . $help_title . '</div>';
                $replace['{help}'] .= '<div class="simplecheckout-block-content simplecheckout-scroll">' . $help_text . '</div>';
                $replace['{help}'] .= '</div>';
            }
            
            if ($payment_form) {
                $replace['{payment_form}'] = '<div class="simplecheckout-block" id="simplecheckout_payment_form">';
                $replace['{payment_form}'] .= '<div class="simplecheckout-block-heading">' . $text_payment_form_title . '</div>';
                $replace['{payment_form}'] .= '<div class="simplecheckout-block-content">' . $payment_form . '</div>';
                $replace['{payment_form}'] .= '</div>';
            }
            
            $find = array(
	  			'{three_column}',
	  			'{/three_column}',
	  			'{left_column}',
	  			'{/left_column}',
	  			'{right_column}',
      			'{/right_column}',
      			'{customer}',
     			'{cart}',
     			'{login}',
      			'{shipping}',
      			'{payment}',
                '{agreement}',
                '{help}',
                '{payment_form}'
			);	

            if (!empty($modules)) {
                foreach ($modules as $key => $value) {
                    $find[] = $key;
                    $replace[$key] = $value;
                }
            }

            echo trim(str_replace($find, $replace, $simple_common_template));
        ?>
    <input type="hidden" name="simple_create_order" id="simple_create_order" value="">
    <span style="display:none" id="need_save_changes"><?php echo $text_need_save_changes ?></span>
    <span style="display:none" id="saving_changes"><?php echo $text_saving_changes ?></span>
    <span style="display:none" id="save_changes"><?php echo $button_save_changes ?></span>
    <span style="display:none" id="default_button"><?php echo $button_order; ?></span>
    <!--div style="width:100%;height:1px;clear:both;"></div-->
    <div style="display: block; width:440px; text-align: left !important; margin-left: 15px; clear:both; padding-right:0px;" class="simplecheckout-button-block" id="buttons" <?php if ($block_order) { ?>style="display:none;"<?php } ?>>
        <?php if ($simple_common_view_agreement_checkbox) { ?><label><input type="checkbox" id="agree" name="agree" value="1" <?php if ($agree == 1) { ?>checked="checked"<?php } ?> />
            Нажимая на данную кнопку, я подтверждаю, что ознакомлен с <a href="/publichnaja-oferta.html" target="_blank">“Публичной офертой”</a>, и даю свое согласие на обработку персональных данных.

            </label>&nbsp;<?php } ?><a class="button blue" style="display: inline-block !important; margin-top:20px;" onclick="simplecheckout_submit();" style="margin-right: 0;" id="simplecheckout_button_confirm"><span><?php echo $button_order; ?></span></a>
            <?php if ($simple_show_back) { ?>
            <div class="simplecheckout-button-left">
                <a class="button" style="float:right !important" onclick="history.back()"><span><?php echo $button_back; ?></span></a>
            </div>
            <?php } ?>        
        </div>
        <style>
            .simplecheckout-button-block .button{
                float: right !important;
            }
        </style>

        <!-- <div style="display: block; text-align: right; width:326px; margin-left: 15px; float:right; clear:none; padding-right:0px;" class="simplecheckout-button-block" id="buttons" <?php if ($block_order) { ?>style="display:none;"<?php } ?>>
        <?php if ($simple_common_view_agreement_checkbox) { ?><label><input type="checkbox" id="agree" name="agree" value="1" <?php if ($agree == 1) { ?>checked="checked"<?php } ?> /><?php echo $text_agree; ?></label>&nbsp;<?php } ?><a class="button blue" onclick="simplecheckout_submit();" style="margin-right: 0;" id="simplecheckout_button_confirm"><span><?php echo $button_order; ?></span></a>
            <?php if ($simple_show_back) { ?>
            <div class="simplecheckout-button-left">
                <a class="button" onclick="history.back()"><span><?php echo $button_back; ?></span></a>
            </div>
            <?php } ?>        
        </div> -->







    <!-- <div style="display: block; text-align: right; width:326px; margin-left: 15px; float:right; clear:none; padding-right:0px;" class="simplecheckout-button-block" id="buttons" >
        <label>
            <input type="checkbox" id="agree" name="agree" value="1"  />Я ознакомился/ась и согласен/на с <a class="colorbox fancybox" href="index.php?route=account/return/insert">условиями возврата и обмена</a>, а также <a class="colorbox fancybox" href="index.php?route=information/information/info&amp;information_id=6">условиями доставки и оплаты товара</a>
        </label>
    </div> -->

    
    
    <!--div style="width:100%;height:1px;clear:both;"></div-->
    
    <div class="simplecheckout-proceed-payment" id="simplecheckout_proceed_payment" style="display:none;"><?php echo $text_proceed_payment ?></div>
    <!-- order button block -->
    <?php if ($error_warning_agree && $simple_create_order) { ?>
        <div class="simplecheckout-warning-block agree-warning"><?php echo $error_warning_agree ?></div>
    <?php } elseif ($agree_warning) { ?>
        <div class="simplecheckout-warning-block agree-warning" style="display:none"><?php echo $agree_warning ?></div>
    <?php } ?> 

    </div>
    


<?php
function YandexOrderLink($stage) {
	/*return '<div style="float: right; margin-left: 10px; margin-right:5px;" class="yandex-addr"><a href="yandex_order.php?stage='.$stage.'"><img class="b-image_block" border="0" align="" src="/image/profile-yandex.png" alt="image" /></a></div>';*/
}
?>
<script type="text/javascript"><!--
var done = false;
$('#simplecheckout_customer').find('.simplecheckout-block-heading:first').prepend('<?php echo YandexOrderLink('payment'); ?>');
$('.yandex-addr a').click(function(e) {
	e.preventDefault();
	$.cookie('yandex_checkout_state',
		JSON.stringify({ same: ($('#shipping_address_same').attr('checked') == 'checked'),
			caddr_id: $('#customer_address_id').val() }));
	document.location.href = $(this).attr('href');
});

<?php if (isset($_REQUEST['stage'])) { ?>
	var ycookie = $.cookie('yandex_checkout_state');
	if (ycookie) {
		var state = JSON.parse(ycookie);
		//$.cookie('yandex_checkout_state',null);
		var reload = false;
		if (state.same == false) {
			$('#shipping_address_same').attr('checked', false);
			reload = true;
		}
		if (state.caddr_id != $('#customer_address_id').val()) {
			$('#customer_address_id').val(state.caddr_id);
			simplecheckout_reload.field = 'customer_address_id';
			reload = true;
        }
		if (reload) {
			simplecheckout_reload('from_customer');
		}
		$('body').ajaxComplete(function(e, xhr, settings) {
			if (settings.url.match(/.*checkout\/simplecheckout.*/i)) {
				setYandexData();
			}
		})
		$.cookie('yandex_checkout_state',null);
	}
function setYandexData() {
<?php if ($_REQUEST['stage'] == 'payment') { ?>
$.getJSON('index.php?route=checkout/yandexorder/temp_data', function(json) {
	for(var key in json) {
		//$('#simplecheckout_customer').find('#customer_main_' + key).val(json[key]);
		$('#checkout_customer_main_' + key).val(json[key]);
		//alert(key + ' = ' + json[key]);
	}
	done = true;
})
<?php } ?>
}

setYandexData();
<?php } ?>
//--></script>  


    
<?php if (!$ajax) { ?>
</div>
<?php include $simple->tpl_footer() ?>
<?php } ?>

<script>
//$(document).ready(function(){
    //$(':radio[name=shipping_method]').trigger('click');
    //if ($(':radio[name=shipping_method]').attr( 'checked' ) != 'checked'){
    //    $(':radio[name=shipping_method]').trigger('click');
    //}
//});
</script>

<script type="text/javascript"><!--
$('div.sidebar-left').removeClass('sidebar-left');
$('div.sidebar').remove();
//--></script>
