<?php
// Text
$_['text_home']           = 'Главная';
$_['text_wishlist']       = 'Закладки (%s)';
$_['text_shopping_cart']  = 'Корзина покупок';
//$_['text_search']         = 'Я ищу';
$_['text_search']         = '';
$_['text_welcome']        = '<a href="%s">Вход</a> | <a href="%s">Регистрация</a>';
//$_['text_logged']         = 'Вы вошли как <a href="%s" style="font-weight:bold;">%s</a> | <a href="%s">Выход</a>';
$_['text_logged']         = '<a href="%s" style="font-weight:bold;">%s</a> | <a href="%s">Выход</a>';
$_['text_account']        = 'Постоянный покупатель';
$_['text_checkout']       = 'Оформление заказа';
?>