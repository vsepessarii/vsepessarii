<?php
	require 'connect.php';
	require 'functions.php';
	error_reporting(0);
	mb_internal_encoding('utf-8');
	ini_set('display_errors', FALSE);
	ini_set('display_startup_errors', FALSE);
	
	if ($ro_user['user_group_id'] != 1) die('Access denied');

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	date_default_timezone_set('Europe/London');

	/** PHPExcel_IOFactory */
	require_once 'class/PHPExcel/IOFactory.php';

	//echo date('H:i:s') , " Load from Excel5 template" , EOL;

	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("templates/ponyexpress.xls");

	//$objPHPExcel->setActiveSheetIndex(0);

	//$objPHPExcel->getActiveSheet()->setCellValue('BC40', '77.20');
	$i = 2;
	foreach ($_POST['selected'] as $order_id) {
		$order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($order_id).'"');
		unset($product);
		$qu = 'SELECT name FROM order_product WHERE order_id="'.$order['order_id'].'" GROUP BY name ORDER BY name';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) $product[] = $ro['name'];
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '12');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '30247');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'ИП Васильев Максим Александрович');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Васильев Максим');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '(495)660-34-23');		
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'Москва');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, 'Ленинский проспект');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '33, стр 5, оф 204');
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, 'частное лицо');
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $order['shipping_firstname']);
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $order['telephone']);
		$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $order['shipping_city']);
		$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $order['shipping_address_1']);
		$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, $order['shipping_address_1']);
		$objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, implode(',',$product));
		$objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, '1');
		$objPHPExcel->getActiveSheet()->setCellValue('AD'.$i, '0,3');
		$objPHPExcel->getActiveSheet()->setCellValue('AE'.$i, '2');
		$objPHPExcel->getActiveSheet()->setCellValue('AF'.$i, '2');
		$objPHPExcel->getActiveSheet()->setCellValue('AH'.$i, '0');
		$objPHPExcel->getActiveSheet()->setCellValue('AI'.$i, '1');
		$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$i, '12');
		$objPHPExcel->getActiveSheet()->setCellValue('AK'.$i, '30247');
		$objPHPExcel->getActiveSheet()->setCellValue('AL'.$i, '1');
		$i++;
	}

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="vsepessarii_ponyexpress_'.date('Y-m-d').'.xls"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('tmp/tmp.xls');
	echo file_get_contents('tmp/tmp.xls');
	unlink('tmp/tmp.xls');

