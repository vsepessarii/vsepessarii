<?php

class ControllerProductCategory extends Controller
{

    public function index()
    {



        $this->language->load('product/category');


        $this->load->model('catalog/category');


        $this->load->model('catalog/product');


        $this->load->model('tool/image');


        if (isset($this->request->get['sort'])) {

            $sort = $this->request->get['sort'];

        } else {

            $sort = 'p.sort_order';

        }


        if (isset($this->request->get['order'])) {

            $order = $this->request->get['order'];

        } else {

            $order = 'ASC';

        }


        if (isset($this->request->get['page'])) {

            $page = $this->request->get['page'];

        } else {

            $page = 1;

        }


        if (isset($this->request->get['limit'])) {

            $limit = $this->request->get['limit'];

        } else {

            $limit = $this->config->get('config_catalog_limit');

        }


        $this->data['breadcrumbs'] = array();


        $this->data['breadcrumbs'][] = array(

            'text' => $this->language->get('text_home'),

            'href' => $this->url->link('common/home'),

            'separator' => false

        );


        if (isset($this->request->get['path'])) {

            $path = '';


            $parts = explode('_', (string)$this->request->get['path']);


            foreach ($parts as $path_id) {

                if (!$path) {

                    $path = (int)$path_id;

                } else {

                    $path .= '_' . (int)$path_id;

                }


                $category_info = $this->model_catalog_category->getCategory($path_id);


                if ($category_info) {

                    $this->data['breadcrumbs'][] = array(

                        'text' => $category_info['name'],

                        'href' => $this->url->link('product/category', 'path=' . $path),

                        'separator' => $this->language->get('text_separator')

                    );

                }

            }


            $category_id = (int)array_pop($parts);

        } else {

            $category_id = 0;

        }


        $category_info = $this->model_catalog_category->getCategory($category_id);


        if ($category_info) {

            if ($category_info['seo_title']) {

                $this->document->setTitle($category_info['seo_title']);

            } else {

                $this->document->setTitle($category_info['name']);

            }


            $this->document->setDescription($category_info['meta_description']);

            $this->document->setKeywords($category_info['meta_keyword']);


            $this->data['seo_h1'] = $category_info['seo_h1'];
            $this->data['date_modified'] = $category_info['date_modified']; //SEDOY

            $this->data['heading_title'] = ($category_info['seo_h1'] != '') ? $category_info['seo_h1'] : $category_info['name']; //SEDOY


            $this->data['text_refine'] = $this->language->get('text_refine');

            $this->data['text_empty'] = $this->language->get('text_empty');

            $this->data['text_quantity'] = $this->language->get('text_quantity');

            $this->data['text_manufacturer'] = $this->language->get('text_manufacturer');

            $this->data['text_model'] = $this->language->get('text_model');

            $this->data['text_price'] = $this->language->get('text_price');

            $this->data['text_tax'] = $this->language->get('text_tax');

            $this->data['text_points'] = $this->language->get('text_points');

            $this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));

            $this->data['text_display'] = $this->language->get('text_display');

            $this->data['text_list'] = $this->language->get('text_list');

            $this->data['text_grid'] = $this->language->get('text_grid');

            $this->data['text_sort'] = $this->language->get('text_sort');

            $this->data['text_limit'] = $this->language->get('text_limit');


            $this->data['button_cart'] = $this->language->get('button_cart');
            
            $this->data['button_wishlist'] = $this->language->get('button_wishlist');

            $this->data['button_compare'] = $this->language->get('button_compare');

            $this->data['button_continue'] = $this->language->get('button_continue');

            if ($category_info['image']) {

                $this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));

            } else {

                $this->data['thumb'] = '';

            }


            $this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
            $this->data['page'] = $_GET['page'];            
            
						//SEDOY собираем отзывы
						$rev = '';
						$rev_num = 0;
						$qu_prod = 'SELECT product.product_id, product.model, product_description.name, manufacturer.name as manufacturer FROM product_to_category, product, product_description, manufacturer WHERE product_to_category.category_id="'.$category_id.'" && product.product_id=product_to_category.product_id && product.product_id=product_description.product_id && product.manufacturer_id=manufacturer.manufacturer_id && product.status="1" GROUP BY product.product_id';
						$re_prod = @mysqli_query($ddb, $qu_prod);
						//if ($_SERVER['REMOTE_ADDR'] == '187.244.180.3') echo mysqli_error();
						while ($ro_prod = @mysqli_fetch_array($re_prod)) {
							$qu_rev = 'SELECT * FROM review WHERE product_id="'.$ro_prod['product_id'].'" && status="1"';
							//if ($_SERVER['REMOTE_ADDR'] == '87.244.180.3') echo $qu_rev.'<br>';
							$re_rev = @mysqli_query($ddb, $qu_rev);
							if (@mysqli_num_rows($re_rev) > 0) {
								//получаем URL
								$ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_prod['product_id'].'" && main_category="0"'));
								if ($ro_category['category_id'] < 1) $ro_category = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT category_id FROM product_to_category WHERE product_id="'.$ro_prod['product_id'].'" && main_category="1"')); 
								$ro_product_url = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="product_id='.$ro_prod['product_id'].'"'));
								$url = $ro_product_url['keyword'].'.html';
								if ($ro_category['category_id'] > 0) {
									$ro_url_cat_1 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_category['category_id'].'"'));
									$url = $ro_url_cat_1['keyword'].'/'.$url;
									$ro_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_category['category_id'].'"'));
									if ($ro_cat_2['parent_id'] > 0) {
										$ro_url_cat_2 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_2['parent_id'].'"'));
										$url = $ro_url_cat_2['keyword'].'/'.$url;
										$ro_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT parent_id FROM category WHERE category_id="'.$ro_cat_2['parent_id'].'"'));
										if ($ro_cat_3['parent_id'] > 0) {
											$ro_url_cat_3 = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT keyword FROM url_alias WHERE query="category_id='.$ro_cat_3['parent_id'].'"'));
											$url = $ro_url_cat_3['keyword'].'/'.$url;
										}
									}
								}
								$url = '/'.$url;
								//получили URL

								while ($ro_rev = @mysqli_fetch_array($re_rev)) {
									$rev_num++;
									$rev .= '<a href="'.$url.'"><b>'.$ro_prod['name'].' '.$ro_prod['manufacturer'].' '.(($ro_prod['model'] != '') ? ' '.$ro_prod['model'] : '').'</b></a> отзыв:<ul>';
									$rev .= '
									<li>
										<b>'.$ro_rev['author'].'</b>&nbsp;&nbsp;&nbsp;&nbsp;'.
										date('d.m.Y', strtotime($ro_rev['date_added'])).'&nbsp;&nbsp;&nbsp;&nbsp;'.
										'<div class="rating" style="display:block;"><img src="/catalog/view/theme/brief/image/stars-'.$ro_rev['rating'].'.png"></div>'.
										''.$ro_rev['text'].'</li>';
									$rev .= '</ul><br>';

								}
								
								
							} 
						}
						//SEDOY отзывы
						$this->data['review'] = ($rev != '') ? $rev : 'В данной категории пока нет отзывов.';
						$this->data['rev_num'] = $rev_num;


						
            $this->data['compare'] = $this->url->link('product/compare');


            $url = '';


            if (isset($this->request->get['sort'])) {

                $url .= '&sort=' . $this->request->get['sort'];

            }


            if (isset($this->request->get['order'])) {

                $url .= '&order=' . $this->request->get['order'];

            }


            if (isset($this->request->get['limit'])) {

                $url .= '&limit=' . $this->request->get['limit'];

            }


            $this->data['categories'] = array();


            $results = $this->model_catalog_category->getCategories($category_id);


            foreach ($results as $result) {

                $data = array(

                    'filter_category_id' => $result['category_id'],

                    'filter_sub_category' => true

                );


                $product_total = $this->model_catalog_product->getTotalProducts($data);


                $this->data['categories'][] = array(

                    'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),

                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)

                );

            }


            $this->data['products'] = array();


            $data = array(

                'filter_category_id' => $category_id,

                'sort' => $sort,

                'order' => $order,

                'start' => ($page - 1) * $limit,

                'limit' => $limit

            );


            $product_total = $this->model_catalog_product->getTotalProducts($data);


            $results = $this->model_catalog_product->getProducts($data);


						foreach ($results as $result) {

                if ($result['image']) {

                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

                } else {

                    $image = false;

                }


                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {

                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));

                } else {

                    $price = false;

                }


                if ((float)$result['special']) {

                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));

                } else {

                    $special = false;

                }


                if ($this->config->get('config_tax')) {

                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);

                } else {

                    $tax = false;

                }


                if ($this->config->get('config_review_status')) {

                    $rating = (int)$result['rating'];

                } else {

                    $rating = false;

                }


								
								

                $this->data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'].' '.$result['manufacturer'].' '.($result['model']!=$result['name']?$result['model']:''),
                    'description' => ($result['short_description'] != '') ? $result['short_description']  : utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 150) . '..',
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $result['rating'],
                    'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
                    'manufacturer' => $result['manufacturer'],
                    'in_compare' => (in_array($result['product_id'],$this->session->data['compare'])?1:0)
                );

            }
            


            $url = '';


            if (isset($this->request->get['limit'])) {

                $url .= '&limit=' . $this->request->get['limit'];

            }


            $this->data['sorts'] = array();


            $this->data['sorts'][] = array(

                'text' => $this->language->get('text_default'),

                'value' => 'p.sort_order-ASC',

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)

            );


            $this->data['sorts'][] = array(

                'text' => $this->language->get('text_name_asc'),

                'value' => 'pd.name-ASC',

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)

            );


            $this->data['sorts'][] = array(

                'text' => $this->language->get('text_name_desc'),

                'value' => 'pd.name-DESC',

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)

            );


            $this->data['sorts'][] = array(

                'text' => $this->language->get('text_price_asc'),

                'value' => 'p.price-ASC',

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)

            );


            $this->data['sorts'][] = array(

                'text' => $this->language->get('text_price_desc'),

                'value' => 'p.price-DESC',

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)

            );


            if ($this->config->get('config_review_status')) {

                $this->data['sorts'][] = array(

                    'text' => $this->language->get('text_rating_desc'),

                    'value' => 'rating-DESC',

                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)

                );


                $this->data['sorts'][] = array(

                    'text' => $this->language->get('text_rating_asc'),

                    'value' => 'rating-ASC',

                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)

                );

            }
/*
            $this->data['sorts'][] = array(
                'text' => $this->language->get('text_model_asc'),
                'value' => 'p.model-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
            );


            $this->data['sorts'][] = array(
                'text' => $this->language->get('text_model_desc'),
                'value' => 'p.model-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
            );
*/

            $url = '';


            if (isset($this->request->get['sort'])) {

                $url .= '&sort=' . $this->request->get['sort'];

            }


            if (isset($this->request->get['order'])) {

                $url .= '&order=' . $this->request->get['order'];

            }


            $this->data['limits'] = array();


            $this->data['limits'][] = array(

                'text' => $this->config->get('config_catalog_limit'),

                'value' => $this->config->get('config_catalog_limit'),

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $this->config->get('config_catalog_limit'))

            );


            $this->data['limits'][] = array(

                'text' => 25,

                'value' => 25,

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=25')

            );


            $this->data['limits'][] = array(

                'text' => 50,

                'value' => 50,

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=50')

            );


            $this->data['limits'][] = array(

                'text' => 75,

                'value' => 75,

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=75')

            );


            $this->data['limits'][] = array(

                'text' => 100,

                'value' => 100,

                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=100')

            );


            $url = '';


            if (isset($this->request->get['sort'])) {

                $url .= '&sort=' . $this->request->get['sort'];

            }


            if (isset($this->request->get['order'])) {

                $url .= '&order=' . $this->request->get['order'];

            }


            if (isset($this->request->get['limit'])) {

                $url .= '&limit=' . $this->request->get['limit'];

            }


            $pagination = new Pagination();

            $pagination->total = $product_total;

            $pagination->page = $page;

            $pagination->limit = $limit;

            $pagination->text = $this->language->get('text_pagination');

            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');


            $this->data['pagination'] = $pagination->render();


            $this->data['sort'] = $sort;

            $this->data['order'] = $order;

            $this->data['limit'] = $limit;


            $this->data['continue'] = $this->url->link('common/home');


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {

                $this->template = $this->config->get('config_template') . '/template/product/category.tpl';

            } else {

                $this->template = 'default/template/product/category.tpl';

            }


            $this->children = array(

                'common/column_left',

                'common/column_right',

                'common/content_top',

                'common/content_bottom',

                'common/footer',

                'common/header'

            );


            $this->response->setOutput($this->render());

        } else {

            $url = '';


            if (isset($this->request->get['path'])) {

                $url .= '&path=' . $this->request->get['path'];

            }


            if (isset($this->request->get['sort'])) {

                $url .= '&sort=' . $this->request->get['sort'];

            }


            if (isset($this->request->get['order'])) {

                $url .= '&order=' . $this->request->get['order'];

            }


            if (isset($this->request->get['page'])) {

                $url .= '&page=' . $this->request->get['page'];

            }


            if (isset($this->request->get['limit'])) {

                $url .= '&limit=' . $this->request->get['limit'];

            }


            $this->data['breadcrumbs'][] = array(

                'text' => $this->language->get('text_error'),

                'href' => $this->url->link('product/category', $url),

                'separator' => $this->language->get('text_separator')

            );


            $this->document->setTitle($this->language->get('text_error'));


            $this->data['heading_title'] = $this->language->get('text_error');


            $this->data['text_error'] = $this->language->get('text_error');


            $this->data['button_continue'] = $this->language->get('button_continue');


            $this->data['continue'] = $this->url->link('common/home');


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {

                $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';

            } else {

                $this->template = 'default/template/error/not_found.tpl';

            }


            $this->children = array(

                'common/column_left',

                'common/column_right',

                'common/content_top',

                'common/content_bottom',

                'common/footer',

                'common/header'

            );


            $this->response->setOutput($this->render());

        }

    }

}

?>