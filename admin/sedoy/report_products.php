<?php
	//отчет по товарам в заказах
	/*header('Content-Type: text/html; charset=utf-8');
	session_start();*/
	//var_dump($_SESSION);

	require 'connect.php';

	if ($_GET['p'] == '') $_GET['p'] = 1;

	if ($ro_user['user_group_id'] == 1) {

		if (isset($_GET['s']) && $_GET['s'] == '') {
			$_GET['s'] = '`QUANTITY` DESC';
        }

		if (!isset($_SESSION['courier_price']) || $_SESSION['courier_price'] == '') {
			$_SESSION['courier_price'] = 250;
        }
		if (!isset($_SESSION['report_products_per_page']) || $_SESSION['report_products_per_page'] == '') {
			$_SESSION['report_products_per_page'] = 25;
        }
		
		if (isset($_POST['act']) && $_POST['act'] == 'clear_filter') {
			$_SESSION['report_products_id'] = '';
			$_SESSION['report_products_name'] = '';
			$_SESSION['report_products_manufacturer'] = '';
			$_SESSION['report_products_model'] = '';
			$_SESSION['report_products_articul'] = '';
			$_SESSION['report_products_category'] = '';
			$_SESSION['report_products_subcategory'] = '';
			$_SESSION['report_products_view_from'] = '';
			$_SESSION['report_products_view_to'] = '';
			$_SESSION['report_products_view_percent_from'] = '';
			$_SESSION['report_products_view_percent_to'] = '';
			$_SESSION['report_products_price_in_from'] = '';
			$_SESSION['report_products_price_in_to'] = '';
			$_SESSION['report_products_price_out_from'] = '';
			$_SESSION['report_products_price_out_to'] = '';
			$_SESSION['report_products_quantity_from'] = '';
			$_SESSION['report_products_quantity_to'] = '';
			$_SESSION['report_products_total_from'] = '';
			$_SESSION['report_products_total_to'] = '';
			$_SESSION['report_products_profit_from'] = '';
			$_SESSION['report_products_profit_to'] = '';
			$_SESSION['report_products_profit_percent_from'] = '';
			$_SESSION['report_products_profit_percent_to'] = '';
		}
		
		if (isset($_POST['act']) && $_POST['act'] == 'setting') {
			$_SESSION['report_products_date_from'] = $_POST['report_products_date_from'];
			$_SESSION['report_products_date_to'] = $_POST['report_products_date_to'];
			$_SESSION['report_products_status'] = $_POST['report_products_status'];
			$_SESSION['report_products_per_page'] = $_POST['report_products_per_page'];
		}
		
		if (isset($_POST['act']) && $_POST['act'] == 'filter') {
			$_SESSION['report_products_id'] = $_POST['report_products_id'];
			$_SESSION['report_products_name'] = $_POST['report_products_name'];
			$_SESSION['report_products_manufacturer'] = $_POST['report_products_manufacturer'];
			$_SESSION['report_products_model'] = $_POST['report_products_model'];
			$_SESSION['report_products_articul'] = $_POST['report_products_articul'];
			$_SESSION['report_products_category'] = $_POST['report_products_category'];
			$_SESSION['report_products_subcategory'] = $_POST['report_products_subcategory'];
			$_SESSION['report_products_view_from'] = $_POST['report_products_view_from'];
			$_SESSION['report_products_view_to'] = $_POST['report_products_view_to'];
			$_SESSION['report_products_view_percent_from'] = $_POST['report_products_view_percent_from'];
			$_SESSION['report_products_view_percent_to'] = $_POST['report_products_view_percent_to'];
			$_SESSION['report_products_price_in_from'] = $_POST['report_products_price_in_from'];
			$_SESSION['report_products_price_in_to'] = $_POST['report_products_price_in_to'];
			$_SESSION['report_products_price_out_from'] = $_POST['report_products_price_out_from'];
			$_SESSION['report_products_price_out_to'] = $_POST['report_products_price_out_to'];
			$_SESSION['report_products_quantity_from'] = $_POST['report_products_quantity_from'];
			$_SESSION['report_products_quantity_to'] = $_POST['report_products_quantity_to'];
			$_SESSION['report_products_total_from'] = $_POST['report_products_total_from'];
			$_SESSION['report_products_total_to'] = $_POST['report_products_total_to'];
			$_SESSION['report_products_profit_from'] = $_POST['report_products_profit_from'];
			$_SESSION['report_products_profit_to'] = $_POST['report_products_profit_to'];
			$_SESSION['report_products_profit_percent_from'] = $_POST['report_products_profit_percent_from'];
			$_SESSION['report_products_profit_percent_to'] = $_POST['report_products_profit_percent_to'];
			$_GET['p'] = 1;
		}
		@mysqli_query($ddb, 'TRUNCATE TABLE tmp_report_product');
		
		$qu_product = '
			SELECT		order_product.product_id as product_id,
								SUM(order_product.quantity) as quantity,
								SUM(order_product.total) as total
			FROM 			`order`, order_product
			WHERE			`order`.order_id = order_product.order_id
								'.(isset($_SESSION['report_products_date_from']) ? ' && `order`.date_added>="'.$_SESSION['report_products_date_from'].' 00:00:00"' : '').' 	
								'.(isset($_SESSION['report_products_date_to']) ? ' && `order`.date_added<="'.$_SESSION['report_products_date_to'].' 23:59:59"' : '').' 	
								'.(isset($_SESSION['report_products_status']) ? ' && `order`.order_status_id="'.$_SESSION['report_products_status'].'"' : '').' 	
			GROUP BY	order_product.product_id;		
		';
		$re_product = @mysqli_query($ddb, $qu_product);
		//echo mysqli_error($ddb);
		$view_total = 0;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$id = $ro_product['product_id'];
			$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT model, sku, price_in, price, manufacturer_id, viewed FROM product WHERE product_id="'.$id.'"'));
			echo mysqli_error($ddb);
			$model = $tmp['model'];
			$articul = $tmp['sku'];
			$price_in = $tmp['price_in'];
			$price_out = $tmp['price'];
			$manufacturer_id = $tmp['manufacturer_id'];
			$view = $tmp['viewed'];
			$view_total += $view;
			$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM product_description WHERE product_id="'.$id.'"'));
			$name = $tmp['name'];
			$tmp = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT name FROM manufacturer WHERE manufacturer_id="'.$manufacturer_id.'"'));
			$manufacturer = $tmp['name'];
			$qu_special = 'SELECT price FROM product_special WHERE product_id="'.$ro_product['product_id'].'"';
			$re_special = @mysqli_query($ddb, $qu_special);
			unset($tmp);
			while ($ro_special = @mysqli_fetch_array($re_special)) $tmp[] = round($ro_special['price'],0);
			$price_special = @implode(' , ',$tmp);
			
			if (@mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM product_to_category WHERE product_id="'.$id.'"')) == 1) {
				$tmp = @mysqli_fetch_array(@mysqli_query($ddb, '
					SELECT	category_description.name
					FROM 		category_description, 
									product_to_category 
					WHERE 	category_description.category_id = product_to_category.category_id &&
									product_to_category.main_category = "0" &&
									product_to_category.product_id = "'.$id.'"
				'));
				$category = $tmp['name'];
				$tmp = @mysqli_fetch_array(@mysqli_query($ddb, '
					SELECT	category_description.name
					FROM 		category_description, 
									product_to_category 
					WHERE 	category_description.category_id = product_to_category.category_id &&
									product_to_category.main_category = "1" &&
									product_to_category.product_id = "'.$id.'"
				'));
				$category = $tmp['name'];
				$subcategory = '';
			} else {
				$tmp = @mysqli_fetch_array(@mysqli_query($ddb, '
					SELECT	category_description.category_id,
									category_description.name
					FROM 		category_description, 
									product_to_category 
					WHERE 	category_description.category_id = product_to_category.category_id &&
									product_to_category.main_category = "0" &&
									product_to_category.product_id = "'.$id.'"
				'));
				$subcategory = $tmp['name'];
				$subcategory_id = $tmp['category_id'];
				$tmp = @mysqli_fetch_array(@mysqli_query($ddb, '
					SELECT	category_description.category_id,
									category_description.name
					FROM 		category_description, 
									product_to_category 
					WHERE 	category_description.category_id = product_to_category.category_id &&
									product_to_category.main_category = "1" &&
									product_to_category.product_id = "'.$id.'"
				'));
				 $category = $tmp['name'];
				 $category_id = $tmp['category_id'];
				 if (@mysqli_num_rows(@mysqli_query($ddb, 'SELECT category_id FROM category WHERE category_id="'.$category_id.'" && parent_id="'.$subcategory_id.'"')) > 0) {
				 	//если категория и субкатегория перепутаны
				 	$tmp = $subcategory;
				 	$subcategory = $category;
				 	$category = $tmp;
				 }
			}
			$quantity = $ro_product['quantity'];
			$total = $ro_product['total'];
			$profit = $total * 0.94 - $quantity * $price_in;
			$profit_percent = round($profit*100/$total,2);
			@mysqli_query($ddb, '
				INSERT INTO	`tmp_report_product`
				SET					`ID`="'.$id.'",
										`NAME`="'.$name.'",
										`MANUFACTURER`="'.$manufacturer.'",
										`MODEL`="'.$model.'",
										`ARTICUL`="'.$articul.'",
										`CATEGORY`="'.$category.'",
										`SUBCATEGORY`="'.$subcategory.'",
										`VIEW`="'.$view.'",
										`VIEW_PERCENT`="'.$view.'",
										`PRICE_IN`="'.$price_in.'",
										`PRICE_OUT`="'.$price_out.'",
										`PRICE_SPECIAL`="'.$price_special.'",
										`QUANTITY`="'.$quantity.'",
										`TOTAL`="'.$total.'",
										`PROFIT`="'.$profit.'",
										`PROFIT_PERCENT`="'.$profit_percent.'"
			');
			//echo mysqli_error($ddb);
			//echo 1;
		}
		@mysqli_query($ddb, 'UPDATE tmp_report_product SET VIEW_PERCENT=ROUND(VIEW_PERCENT*100/'.$view_total.',2)');
?>
<html>
	<head>
		<title>Отчет по товарам (SedEdition)</title>
		<link type="text/css" href="<?php $app_url ?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon">
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon">
		<script language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
		<!--script language="javascript" src="js/right.js"></script>
		<script language="javascript" src="js/right-calendar.js"></script>
		<script language="javascript" src="js/ru.js"></script-->
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.ui.datepicker-ru.js"></script>

	</head>
	<body>
		<div align="right">
			<br />
			<a href="/index.php?route=common/home&token=<?php echo $_SESSION['token']?>">Вернуться в панель управления</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="report_products_csv.php">Скачать в CSV</a>
			&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				<input type="hidden" name="act" value="clear_filter" />
				<input type="submit" value="Сбросить фильтры" />
			</form>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<form method="post" style="display: inline-block;">
				<input type="hidden" name="act" value="setting" />
				Период:
				<input name="report_products_date_from" id="report_products_date_from" value="<?php echo isset($_SESSION['report_products_date_from']) ? $_SESSION['report_products_date_from'] : ''?>" size="10" />
				-
				<input name="report_products_date_to" id="report_products_date_to" value="<?php echo isset($_SESSION['report_products_date_to']) ? $_SESSION['report_products_date_to'] : ''?>" size="10" />
				&nbsp;&nbsp;
				<a href="" onclick="$('#report_products_date_from').val('<?php echo date('Y-m-d');?>'); $('#report_products_date_to').val('<?php echo date('Y-m-d');?>'); return false" style="color: red;">Сегодня</a>
				&nbsp;&nbsp;&nbsp;
				Статус:
				<select name="report_products_status">
					<option value=""></option>				
					<?
						$qu_status = 'SELECT * FROM order_status ORDER BY order_status_id';
						$re_status = @mysqli_query($ddb, $qu_status);
						while ($ro_status = @mysqli_fetch_array($re_status)) echo '<option value="'.$ro_status['order_status_id'].'"'.(($ro_status['order_status_id'] == $_SESSION['report_products_status']) ? ' selected' : '').'>'.$ro_status['name'].'</option>';
					?>
				</select>
				&nbsp;&nbsp;&nbsp;
				Выводить по:
				<select name="report_products_per_page">
					<option value="25"<?php echo ($_SESSION['report_products_per_page'] == '25') ? ' selected' : ''?>>25</option>
					<option value="50"<?php echo ($_SESSION['report_products_per_page'] == '50') ? ' selected' : ''?>>50</option>
					<option value="100"<?php echo ($_SESSION['report_products_per_page'] == '100') ? ' selected' : ''?>>100</option>
					<option value="200"<?php echo ($_SESSION['report_products_per_page'] == '200') ? ' selected' : ''?>>200</option>
				</select>
				
				<input type="submit" value="Сохранить" />
			</form>
			<br /><br />
		</div>
		<table class="list" width="">
			<thead>
				<tr>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`ID` ASC') ? '`ID` DESC' : '`ID` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`ID` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`ID` DESC') ? 'desc' : '')?>">№</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && ($_GET['s'] == '`NAME` ASC')) ? '`NAME` DESC' : '`NAME` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`NAME` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`NAME` DESC') ? 'desc' : '')?>">Название</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` ASC') ? '`MANUFACTURER` DESC' : '`MANUFACTURER` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MANUFACTURER` DESC') ? 'desc' : '')?>">Производитель</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`MODEL` ASC') ? '`MODEL` DESC' : '`MODEL` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`MODEL` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`MODEL` DESC') ? 'desc' : '')?>">Модель</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`ARTICUL` ASC') ? '`ARTICUL` DESC' : '`ARTICUL` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`ARTICUL` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`ARTICUL` DESC') ? 'desc' : '')?>">Артикул</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`CATEGORY` ASC') ? '`CATEGORY` DESC' : '`CATEGORY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`CATEGORY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`CATEGORY` DESC') ? 'desc' : '')?>">Категория</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` ASC') ? '`SUBCATEGORY` DESC' : '`SUBCATEGORY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`SUBCATEGORY` DESC') ? 'desc' : '')?>">Подкатегория</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`VIEW` ASC') ? '`VIEW` DESC' : '`VIEW` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`VIEW` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`VIEW` DESC') ? 'desc' : '')?>">Просмотров</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`VIEW_PERCENT` ASC') ? '`VIEW_PERCENT` DESC' : '`VIEW_PERCENT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`VIEW_PERCENT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`VIEW_PERCENT` DESC') ? 'desc' : '')?>">% от всех</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` ASC') ? '`PRICE_IN` DESC' : '`PRICE_IN` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PRICE_IN` DESC') ? 'desc' : '')?>">Цена (вх.)</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` ASC') ? '`PRICE_OUT` DESC' : '`PRICE_OUT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PRICE_OUT` DESC') ? 'desc' : '')?>">Цена (исх.)</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`QUANTITY` ASC') ? '`QUANTITY` DESC' : '`QUANTITY` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`QUANTITY` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`QUANTITY` DESC') ? 'desc' : '')?>">Количество</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`TOTAL` ASC') ? '`TOTAL` DESC' : '`TOTAL` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`TOTAL` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`TOTAL` DESC') ? 'desc' : '')?>">Итого</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PROFIT` ASC') ? '`PROFIT` DESC' : '`PROFIT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PROFIT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PROFIT` DESC') ? 'desc' : '')?>">Прибыль</a></td>
					<td class="left" width=""><a href="?p=<?php echo $_GET['p']?>&s=<?php echo (isset($_GET['s']) && $_GET['s'] == '`PROFIT_PERCENT` ASC') ? '`PROFIT_PERCENT` DESC' : '`PROFIT_PERCENT` ASC';?>" class="<?php echo ((isset($_GET['s']) && $_GET['s'] == '`PROFIT_PERCENT` ASC') ? 'asc' : '').((isset($_GET['s']) && $_GET['s'] == '`PROFIT_PERCENT` DESC') ? 'desc' : '')?>">%</a></td>
					<td class="left" width="">Действие</td>
				</tr>
			</thead>
			<tbody>
				<form method="post" id="filter">
				<input type="hidden" name="act" value="filter" />
				<tr class="filter">
					<td><input type="text" name="report_products_id" value="<?php echo isset($_SESSION['report_products_id']) ? $_SESSION['report_products_id'] : '';?>" style="width: 100%;"></td>
					<td><input type="text" name="report_products_name" value="<?php echo isset($_SESSION['report_products_name']) ? $_SESSION['report_products_name'] : '';?>" style="width: 100%;"></td>
					<!--td><input type="text" name="report_products_manufacturer" value="<?php echo $_SESSION['report_products_manufacturer'];?>" style="width: 100%;"></td-->
					<td>
						<select name="report_products_manufacturer" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT MANUFACTURER FROM tmp_report_product GROUP BY MANUFACTURER ORDER BY MANUFACTURER';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.(isset($ro['MANUFACTURER']) ? $ro['MANUFACTURER'] : '').'"'.((isset($_SESSION['report_products_manufacturer']) && isset($ro['MANUFACTURER']) && $ro['MANUFACTURER'] == $_SESSION['report_products_manufacturer']) ? ' selected' : '').'>'.(isset($ro['MANUFACTURER']) ? $ro['MANUFACTURER'] : '').'</option>';
							?>							
						</select>
					</td>					
					<td><input type="text" name="report_products_model" value="<?php echo isset($_SESSION['report_products_model']) ? $_SESSION['report_products_model'] : '';?>" style="width: 100%;"></td>
					<td><input type="text" name="report_products_articul" value="<?php echo isset($_SESSION['report_products_articul']) ? $_SESSION['report_products_articul'] : ''?>" style="width: 100%;"></td>
					<td>
						<select name="report_products_category" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT CATEGORY FROM tmp_report_product GROUP BY CATEGORY ORDER BY CATEGORY';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['CATEGORY'].'"'.((isset($_SESSION['report_products_category']) && $ro['CATEGORY'] == $_SESSION['report_products_category']) ? ' selected' : '').'>'.$ro['CATEGORY'].'</option>';
							?>							
						</select>
					</td>
					<td>
						<select name="report_products_subcategory" style="width: 100%;">
							<option value=""></option>
							<?php
								$qu = 'SELECT SUBCATEGORY FROM tmp_report_product GROUP BY SUBCATEGORY ORDER BY SUBCATEGORY';
								$re = @mysqli_query($ddb, $qu);
								while ($ro = @mysqli_fetch_array($re)) echo '<option value="'.$ro['SUBCATEGORY'].'"'.((isset($_SESSION['report_products_subcategory']) && $ro['SUBCATEGORY'] == $_SESSION['report_products_subcategory']) ? ' selected' : '').'>'.$ro['SUBCATEGORY'].'</option>';
							?>							
						</select>
					</td>
					<td>
						<input type="text" name="report_products_view_from" value="<?php echo isset($_SESSION['report_products_view_from']) ? $_SESSION['report_products_view_from'] : '';?>" style="width: 100%;">
						<input type="text" name="report_products_view_to" value="<?php echo isset($_SESSION['report_products_view_to']) ? $_SESSION['report_products_view_to'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_view_percent_from" value="<?php echo isset($_SESSION['report_products_view_percent_from']) ? $_SESSION['report_products_view_percent_from'] : '';?>" style="width: 100%;">
						<input type="text" name="report_products_view_percent_to" value="<?php echo isset($_SESSION['report_products_view_percent_to']) ? $_SESSION['report_products_view_percent_to'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_price_in_from" value="<?php echo isset($_SESSION['report_products_price_in_from']) ? $_SESSION['report_products_price_in_from'] : ''?>" style="width: 100%;">
						<input type="text" name="report_products_price_in_to" value="<?php echo isset($_SESSION['report_products_price_in_to']) ? $_SESSION['report_products_price_in_to'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_price_out_from" value="<?php echo isset($_SESSION['report_products_price_out_from']) ? $_SESSION['report_products_price_out_from'] : '';?>" style="width: 100%;">
						<input type="text" name="report_products_price_out_to" value="<?php echo isset($_SESSION['report_products_price_out_to']) ? $_SESSION['report_products_price_out_to'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_quantity_from" value="<?php echo isset($_SESSION['report_products_quantity_from']) ? $_SESSION['report_products_quantity_from'] : ''?>" style="width: 100%;">
						<input type="text" name="report_products_quantity_to" value="<?php echo isset($_SESSION['report_products_quantity_to']) ? $_SESSION['report_products_quantity_to'] : '';?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_total_from" value="<?php echo isset($_SESSION['report_products_total_from']) ? $_SESSION['report_products_total_from'] : ''?>" style="width: 100%;">
						<input type="text" name="report_products_total_to" value="<?php echo isset($_SESSION['report_products_total_to']) ? $_SESSION['report_products_total_to'] : ''?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_profit_from" value="<?php echo isset($_SESSION['report_products_profit_from']) ? $_SESSION['report_products_profit_from'] : '';?>" style="width: 100%;">
						<input type="text" name="report_products_profit_to" value="<?php echo isset( $_SESSION['report_products_profit_to']) ?  $_SESSION['report_products_profit_to'] : ''?>" style="width: 100%;">
					</td>
					<td>
						<input type="text" name="report_products_profit_percent_from" value="<?php echo isset($_SESSION['report_products_profit_percent_from']) ? $_SESSION['report_products_profit_percent_from'] : '';?>" style="width: 100%;">
						<input type="text" name="report_products_profit_percent_to" value="<?php echo isset($_SESSION['report_products_profit_percent_to']) ? $_SESSION['report_products_profit_percent_to'] : '';?>" style="width: 100%;">
					</td>
					<td align="right"><input type="submit" class="button" value="Фильтр" /></td>
				</tr>
				</form>
				<?php
					$qu_product = '
						SELECT	*
						FROM 		tmp_report_product
						WHERE		1
										'.((isset($_SESSION['report_products_id']) && $_SESSION['report_products_id'] != '') ? ' && `ID` LIKE "%'.$_SESSION['report_products_id'].'%"' : '').'
										'.((isset($_SESSION['report_products_name']) && $_SESSION['report_products_name'] != '') ? ' && `NAME` LIKE "%'.$_SESSION['report_products_name'].'%"' : '').'
										'.((isset($_SESSION['report_products_manufacturer']) && $_SESSION['report_products_manufacturer'] != '') ? ' && `MANUFACTURER` ="'.$_SESSION['report_products_manufacturer'].'"' : '').'
										'.((isset($_SESSION['report_products_model']) && $_SESSION['report_products_model'] != '') ? ' && `MODEL` LIKE "%'.$_SESSION['report_products_model'].'%"' : '').'
										'.((isset($_SESSION['report_products_articul']) && $_SESSION['report_products_articul'] != '') ? ' && `ARTICUL` LIKE "%'.$_SESSION['report_products_articul'].'%"' : '').'
										'.((isset($_SESSION['report_products_category']) && $_SESSION['report_products_category'] != '') ? ' && `CATEGORY`="'.$_SESSION['report_products_category'].'"' : '').'
										'.((isset($_SESSION['report_products_subcategory']) && $_SESSION['report_products_subcategory'] != '') ? ' && `SUBCATEGORY`="'.$_SESSION['report_products_subcategory'].'"' : '').'
										'.((isset($_SESSION['report_products_view_from']) && $_SESSION['report_products_view_from'] != '') ? ' && `VIEW`>="'.$_SESSION['report_products_view_from'].'"' : '').'
										'.((isset($_SESSION['report_products_view_to']) && $_SESSION['report_products_view_to'] != '') ? ' && `VIEW`<="'.$_SESSION['report_products_view_to'].'"' : '').'
										'.((isset($_SESSION['report_products_view_percent_from']) && $_SESSION['report_products_view_percent_from'] != '') ? ' && `VIEW_PERCENT`<="'.$_SESSION['report_products_view_percent_from'].'"' : '').'
										'.((isset($_SESSION['report_products_view_percent_to']) && $_SESSION['report_products_view_percent_to'] != '') ? ' && `VIEW_PERCENT`>="'.$_SESSION['report_products_view_percent_to'].'"' : '').'
										'.((isset($_SESSION['report_products_price_in_from']) && $_SESSION['report_products_price_in_from'] != '') ? ' && `PRICE_IN`>="'.$_SESSION['report_products_price_in_from'].'"' : '').'
										'.((isset($_SESSION['report_products_price_in_to']) && $_SESSION['report_products_price_in_to'] != '') ? ' && `PRICE_IN`<="'.$_SESSION['report_products_price_in_to'].'"' : '').'
										'.((isset($_SESSION['report_products_price_out_from']) && $_SESSION['report_products_price_out_from'] != '') ? ' && `PRICE_OUT`>="'.$_SESSION['report_products_price_out_from'].'"' : '').'
										'.((isset($_SESSION['report_products_price_out_to']) && $_SESSION['report_products_price_out_to'] != '') ? ' && `PRICE_OUT`<="'.$_SESSION['report_products_price_out_to'].'"' : '').'
										'.((isset($_SESSION['report_products_quantity_from']) && $_SESSION['report_products_quantity_from'] != '') ? ' && `QUANTITY`>="'.$_SESSION['report_products_quantity_from'].'"' : '').'
										'.((isset($_SESSION['report_products_quantity_to']) && $_SESSION['report_products_quantity_to'] != '') ? ' && `QUANTITY`<="'.$_SESSION['report_products_quantity_to'].'"' : '').'
										'.((isset($_SESSION['report_products_total_from']) && $_SESSION['report_products_total_from'] != '') ? ' && `TOTAL`>="'.$_SESSION['report_products_total_from'].'"' : '').'
										'.((isset($_SESSION['report_products_total_to']) && $_SESSION['report_products_total_to'] != '') ? ' && `TOTAL`<="'.$_SESSION['report_products_total_to'].'"' : '').'
										'.((isset($_SESSION['report_products_profit_from']) && $_SESSION['report_products_profit_from'] != '') ? ' && `PROFIT`>="'.$_SESSION['report_products_profit_from'].'"' : '').'
										'.((isset($_SESSION['report_products_profit_to']) && $_SESSION['report_products_profit_to'] != '') ? ' && `PROFIT`<="'.$_SESSION['report_products_profit_to'].'"' : '').'
										'.((isset($_SESSION['report_products_profit_percent_from']) && $_SESSION['report_products_profit_percent_from'] != '') ? ' && `PROFIT_PERCENT`>="'.$_SESSION['report_products_profit_percent_from'].'"' : '').'
										'.((isset($_SESSION['report_products_profit_percent_to']) && $_SESSION['report_products_profit_percent_to'] != '') ? ' && `PROFIT_PERCENT`<="'.$_SESSION['report_products_profit_percent_to'].'"' : '').'
							';
						if(isset($_GET['s'])){
							$qu_product .= 'ORDER BY '.$_GET['s'];
                        }
				//echo $qu_product;
				$re_product = @mysqli_query($ddb, $qu_product);
				$nu_product_total = @mysqli_num_rows($re_product);
				$summ['quantity'] = 0;
				$summ['total'] = 0;
				$summ['profit'] = 0;
				//$summ['profit_percent'] = 0;
				while ($ro_product = @mysqli_fetch_array($re_product)) {
					$summ['quantity'] += $ro_product['QUANTITY'];
					$summ['total'] += $ro_product['TOTAL'];
					$summ['profit'] += $ro_product['PROFIT'];
					//$summ['profit_percent'] += $ro_product['PROFIT_PERCENT'];
				}
				//$summ['profit_percent'] = @round($summ['profit_percent']/$nu_product_total,2);
				$summ_page['quantity'] = 0;
				$summ_page['total'] = 0;
				$summ_page['profit'] = 0;
				$summ_page['profit_percent'] = 0;
				$nu_pages = ceil($nu_product_total/$_SESSION['report_products_per_page']);
				$qu_product .= ' LIMIT '.(($_GET['p']-1)*$_SESSION['report_products_per_page']).','.$_SESSION['report_products_per_page'];
				$re_product = @mysqli_query($ddb, $qu_product);
				while ($ro_product = @mysqli_fetch_array($re_product)) {
					$ro_key = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM url_alias WHERE query="product_id='.$ro_product['ID'].'"'));
					$summ_page['quantity'] += $ro_product['QUANTITY'];
					$summ_page['total'] += $ro_product['TOTAL'];
					$summ_page['profit'] += $ro_product['PROFIT'];
					//$summ_page['profit_percent'] += $ro_product['PROFIT_PERCENT'];
					?>
				<tr >
					<td><?php echo $ro_product['ID']?></td>
					<td><?php echo $ro_product['NAME']?></td>
					<td><?php echo $ro_product['MANUFACTURER']?></td>
					<td><?php echo $ro_product['MODEL']?></td>
					<td><?php echo $ro_product['ARTICUL']?></td>
					<td><?php echo $ro_product['CATEGORY']?></td>
					<td><?php echo $ro_product['SUBCATEGORY']?></td>
					<td><?php echo $ro_product['VIEW']?></td>
					<td><?php echo $ro_product['VIEW_PERCENT']?></td>
					<td><?php echo $ro_product['PRICE_IN']?></td>
					<td><?php echo $ro_product['PRICE_OUT'].(($ro_product['PRICE_SPECIAL']) ? '<br><span style="font-size:80%; font-weight:bold; color:red;">'.$ro_product['PRICE_SPECIAL'].'</span>' : '')?></td>
					<td><?php echo $ro_product['QUANTITY']?></td>
					<td><?php echo $ro_product['TOTAL']?></td>
					<td><?php echo $ro_product['PROFIT']?></td>
					<td><?php echo $ro_product['PROFIT_PERCENT']?></td>
					<td class="right">
						[<a href="/index.php?route=catalog/product/update&token=<?php echo $_SESSION['token'];?>&product_id=<?php echo $ro_product['ID']?>" target="_blank">Изменить</a>]<br />
						[<a href="/<?php echo $ro_key['keyword']?>.html" target="_blank">Посмотреть</a>]
						
					</td>
					<?php
				}
				//$summ_page['profit_percent'] = @round($summ_page['profit_percent']/@mysqli_num_rows($re_product),2);

				?>
				<tr>
					<td colspan="11"><strong>Итог по странице</strong></td>
					<td><strong><?php echo $summ_page['quantity']?></strong></td>
					<td><strong><?php echo $summ_page['total']?></strong></td>
					<td><strong><?php echo $summ_page['profit']?></strong></td>
					<td><strong><?php echo @round($summ_page['profit']/$summ_page['total']*100,2)?></strong></td>
					<td colspan="4"></td>
				</tr>			
				<tr>
					<td colspan="11"><strong>Итог по всем страницам</strong></td>
					<td><strong><?php echo $summ['quantity']?></strong></td>
					<td><strong><?php echo $summ['total']?></strong></td>
					<td><strong><?php echo $summ['profit']?></strong></td>
					<td><strong><?php echo @round($summ['profit']/$summ['total']*100,2);?></strong></td>
					<td colspan="4"></td>
				</tr>			
			</tbody>
		</table>
		<div class="pagination">
			<div class="links">
<?php
		for ($i=1;$i<=$nu_pages;$i++) {
			echo ($i == $_GET['p']) ? '<b>'.$i.'</b> ' : '<a href="?p='.$i.'&s='.(isset($_GET['s']) ? $_GET['s'] : '').'">'.$i.'</a> ';
		}
?>
			</div>
			<div class="results">Показано с <?php echo (($_GET['p']-1)*$_SESSION['report_products_per_page']+1);?> по <?php echo min($_GET['p']*$_SESSION['report_products_per_page'],$nu_product_total);?> из <?php echo $nu_product_total;?> (всего страниц: <?php echo $nu_pages;?>)</div>
		</div>
					<script>
						$(function() {
				  	  $( "#report_products_date_from" ).datepicker();
					    $( "#report_products_date_to" ).datepicker();
					  });


				//new Calendar({format: "%Y-%m-%d"}).assignTo('report_products_date_from');
				//new Calendar({format: "%Y-%m-%d"}).assignTo('report_products_date_to');
			</script>
		
	</body>
</html>
<?php	
	} else {
		header('Location: /');
	}
?>