<?php
class ControllerPaymentCashless extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/cashless');
		$this->language->load('setting/setting');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('cashless', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');

		$this->data['supplier_info'] = $this->language->get('entry_supplier_info');
		$this->data['supplier_placeholder'] = $this->language->get('entry_supplier_placeholder');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_stamp'] = $this->language->get('entry_stamp');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['atten_update'] = $this->language->get('atten_update');
		$this->data['entry_tax_class'] = $this->db->query('SELECT tax_class_id, title FROM '.DB_PREFIX.'tax_class')->rows;


		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_update'] = $this->language->get('button_update');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (isset($this->error['supplier_info_' . $language['language_id']])) {
				$this->data['error_supplier_info_' . $language['language_id']] = $this->error['supplier_info_' . $language['language_id']];
			} else {
				$this->data['error_supplier_info_' . $language['language_id']] = '';
			}
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment_field'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/cashless', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = $this->url->link('payment/cashless', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['update'] = $this->url->link('payment/cashless/update_tax', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		foreach ($languages as $language) {
			if (isset($this->request->post['supplier_info_' . $language['language_id']])) {
				$this->data['supplier_info_' . $language['language_id']] = $this->request->post['supplier_info_' . $language['language_id']];
			} else {
				$this->data['supplier_info_' . $language['language_id']] = $this->config->get('supplier_info_' . $language['language_id']);
			}
		}

		$this->data['languages'] = $languages;

		if (isset($this->request->post['cashless_order_status_id'])) {
			$this->data['cashless_order_status_id'] = $this->request->post['cashless_order_status_id'];
		} else {
			$this->data['cashless_order_status_id'] = $this->config->get('cashless_order_status_id');
		}

		$this->load->model('localisation/order_status');
		$this->load->model('tool/image');

		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['cashless_status'])) {
			$this->data['cashless_status'] = $this->request->post['cashless_status'];
		} else {
			$this->data['cashless_status'] = $this->config->get('cashless_status');
		}

		if (isset($this->request->post['who_write_bill'])) {
			$this->data['who_write_bill'] = $this->request->post['who_write_bill'];
		} else {
			$this->data['who_write_bill'] = $this->config->get('who_write_bill');
		}

		if (isset($this->request->post['cashless_sort_order'])) {
			$this->data['cashless_sort_order'] = $this->request->post['cashless_sort_order'];
		} else {
			$this->data['cashless_sort_order'] = $this->config->get('cashless_sort_order');
		}

		if (isset($this->request->post['stamp_img'])) {
			$this->data['stamp_img'] = $this->request->post['stamp_img'];
		} else {
			$this->data['stamp_img'] = $this->config->get('stamp_img');
		}

		if ($this->config->get('stamp_img') && file_exists(DIR_IMAGE . $this->config->get('stamp_img')) && is_file(DIR_IMAGE . $this->config->get('stamp_img'))) {
			$this->data['stamp'] = $this->model_tool_image->resize($this->config->get('stamp_img'), 100, 100);
		} else {
			$this->data['stamp'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}


		$this->template = 'payment/cashless.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function update_tax()
	{
		if (($this->request->server['REQUEST_METHOD'] == 'POST'))
		{
			$this->load->language('payment/cashless');

			$this->db->query('UPDATE '.DB_PREFIX.'product SET tax_class_id = '.(int) $this->request->post['tax_class_id']);

			$this->session->data['success'] = $this->language->get('text_update_success');

			$this->redirect($this->url->link('payment/cashless', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	// Используется в методе index()
	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/cashless')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		// Если "прилетел" неверный элемент POST, то, если есть ошибка в
		// языковом файле - заносим сообщение об ошибке в $this->error,
		// которая в свою очередь даст методу index() знать, что есть ошибки.
		foreach ($languages as $language)
		{
			if (!$this->request->post['supplier_info_' . $language['language_id']]) {
				$this->error['supplier_info_' .  $language['language_id']] = $this->language->get('error_supplier_info');
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>