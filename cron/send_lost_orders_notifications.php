<?php
	//отсылаем письма админу о заказах, которые уже 20 минут висят в потерянных
	
	$qu_order = 'SELECT order_id FROM `order` WHERE lost_order_notify="0" && order_status_id="0" && date_added < ADDDATE(NOW(), INTERVAL -30 MINUTE) ORDER BY order_id DESC LIMIT 1';
	$re_order = mysqli_query($qu_order);
	$ro_order = mysqli_fetch_array($re_order);
	$order_id = $ro_order['order_id'];	
	unset($id);
	if ($order_id > 0) {
		//$order_id = $ro_order['order_id'];
		//$order_id = $ro_order['order_id'];
		//echo $order_id;
		//$text = 'Номера потерянных заказов: '.implode(',',$id);

		$config_email  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_email"'));
		$config_name  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_name"'));
		$config_mail_protocol  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_mail_protocol"'));
		$config_smtp_host  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_host"'));
		$config_smtp_username  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_username"'));
		$config_smtp_password  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_password"'));
		$config_smtp_port  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_port"'));
		$config_smtp_timeout  = mysqli_fetch_array(mysqli_query('SELECT `value` FROM `setting` WHERE `key`="config_smtp_timeout"'));
			
		$mail = new PHPMailer();
		$mail -> CharSet = "UTF-8";
		$mail->ContentType = 'text/html';
		if ($config_mail_protocol == 'mail') {
			$mail->IsSendmail();
		} else {
			$mail->IsSMTP();
			$mail->SMTPAuth = true; 
			$mail->Host = $config_smtp_host['value'];
			$mail->Port = $config_smtp_port['value'];
			$mail->Username = $config_smtp_username['value'];
			$mail->Password = $config_smtp_password['value'];  	
			
		}
		
		//формируем письмо
		$subject = sprintf('Потерянный заказ '.$order_id);
		$ro_order = mysqli_fetch_array(mysqli_query('SELECT * FROM `order` WHERE order_id="'.$order_id.'"'));
		$br = '<br>';
		$text = 'Вы получили новый заказ'.$br.$br;
		$text = 'Номер: '.$ro_order['order_id'].$br.
						'Дата добавления: '.date('d.m.Y H:i:s', strtotime($ro_order['date_added'])).$br.
						'Имя: '.$ro_order['firstname'].$br.
						'Фамилия: '.$ro_order['lastname'].$br.
						'E-mail: '.$ro_order['email'].$br.
						'Телефон: '.$ro_order['telephone'].$br.
						'Страна: '.$ro_order['shipping_country'].$br.
						'Город: '.$ro_order['shipping_city'].$br.
						'Адрес: '.$ro_order['shipping_address_1'].$br.
						'Способ доставки: '.$ro_order['shipping_method'].$br.
						'Способ оплаты: '.$ro_order['payment_method'].$br.
						'Комментарий заказчика: '.$ro_order['comment'].$br.$br
		;
				
		$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$order_id.'"';
		$re_product = mysqli_query($qu_product);
		while ($ro_product = mysqli_fetch_array($re_product)) {
			$ro_manufacturer = mysqli_fetch_array(mysqli_query('SELECT manufacturer.name FROM manufacturer, product WHERE manufacturer.manufacturer_id=product.manufacturer_id && product.product_id="'.$ro_product['product_id'].'"'));
			$ro_keyword = mysqli_fetch_array(mysqli_query('SELECT `keyword` FROM url_alias WHERE `query`="product_id='.$ro_product['product_id'].'"'));
			$text .= $ro_product['quantity'] . 'x ' . $ro_product['name'] . ' ' . $ro_manufacturer['name'] . ' ' . $ro_product['model'] . ' (https://vsepessarii.ru/'.$ro_keyword['keyword'].'.html) ' . html_entity_decode($ro_product['total']) . $br;
			
			$qu_product_option = 'SELECT * FROM order_option WHERE order_id = "'.$order_id.'" && order_product_id = "'.$ro_product['product_id'].'"';
			$re_product_option = mysqli_query($qu_product_option);
			while ($ro_product_option = mysqli_fetch_array($re_product_option)) {
				if ($ro_product_option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$value = utf8_substr($ro_product_option['value'], 0, utf8_strrpos($ro_product_option['value'], '.'));
				}
									
				$text .= chr(9) . '-' . $ro_product_option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . $br;
			}
		}
		
		$text .= "Заказ итого:".$br;
		$qu_total = 'SELECT * FROM order_total WHERE order_id="'.$order_id.'" ORDER BY sort_order';
		$re_total = mysqli_query($qu_total);
		while ($ro_total = mysqli_fetch_array($re_total)) {
			$text .= $ro_total['title'] . ': ' . html_entity_decode($ro_total['text'], ENT_NOQUOTES, 'UTF-8') . $br;
		}	


		 
		//$mail->IsHTML(false);
		$mail->SetFrom($config_email['value'], $config_name['value']);
		$mail->AddReplyTo($config_email['value'], $config_name['value']);
		$mail->AddAddress($config_email['value'], $config_name['value']);
		$mail->Subject = $subject;
		$mail->AltBody = $text;
		$mail->Body = $text;
		//$mail->MsgHTML($text);
		if($mail->Send()) mysqli_query('UPDATE `order` SET lost_order_notify="1" WHERE order_id='.$order_id);
		echo $mail->ErrorInfo;
	}
?>