<?php
class ControllerAccountyandexplusplus extends Controller {
    private $error = array();
    public function index() {
      $this->load->language('account/yandexplusplus');
      $this->data['button_pay'] = $this->language->get('button_pay');
  		$this->data['button_back'] = $this->language->get('button_back');
      $this->data['heading_title'] = $this->language->get('heading_title');
      if (isset($this->request->post['order_id']) || isset($this->request->get['order_id'])){
        if (isset($this->request->post['order_id'])){$inv_id = $this->request->post['order_id'];}
        if (isset($this->request->get['order_id'])){$inv_id = $this->request->get['order_id'];}
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($inv_id);
        if ($order_info['order_id'] == 0){$this->redirect($this->url->link('error/not_found'));}
        $out_summ = $order_info['total'];
        $out_summ = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], false);
        if (!$this->customer->isLogged()) {
          $this->data['back'] = $this->url->link('common/home');
        }
        else{
          $this->data['back'] = HTTPS_SERVER . 'index.php?route=account/order';
        }
	      $action = HTTPS_SERVER . 'index.php?route=account/yandexplusplus/pay';
    
    		$this->data['merchant_url'] = $action .

							'&order_id=' 		. $inv_id .
              '&paymentType=' . $order_info['payment_code'];

        $this->load->model('account/yandexplusplus');
        $paystat = $this->model_account_yandexplusplus->getPaymentStatus($inv_id);
        if (!isset($paystat['status'])){$paystat['status'] = 0;}
        $this->data['paystat'] = $paystat['status'];
        if ($paystat['status'] != 1){
	        if ($order_info['payment_code'] == 'yandexplusplus'){
		        if ($this->config->get('yandexplusplus_hrefpage_text_attach')) {
		          $this->data['hrefpage_text'] = '';

		          $instros = explode('$', ($this->config->get('yandexplusplus_hrefpage_text')));
		                  $instroz = "";
		                  foreach ($instros as $instro) {
		                    if ($instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
		                      
		                        if ($instro == 'orderid'){
		                            $instro_other = $order_info['order_id'];
		                      }
		                      if ($instro == 'itogo'){
		                          $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		                      }
		                      if ($instro == 'komis'){
		                        if($this->config->get('yandexplusplus_komis')){
		                            $instro_other = $this->config->get('yandexplusplus_komis') . '%';
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'total-komis'){
		                        if($this->config->get('yandexplusplus_komis')){
		                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'plus-komis'){
		                        if($this->config->get('yandexplusplus_komis')){
		                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                    }
		                    else {
		                      $instro_other = nl2br(htmlspecialchars_decode($instro));
		                    }
		                    $instroz .=  $instro_other;
		                  }

		          $this->data['hrefpage_text'] .= $instroz;
		        }
		        else{        
		        $this->data['send_text'] = $this->language->get('send_text');
		        $this->data['send_text2'] = $this->language->get('send_text2');
		        $this->data['inv_id'] = $inv_id;
		        $this->data['out_summ'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		        }
		    }
		    if ($order_info['payment_code'] == 'yandexplusplus_card'){
		    	if ($this->config->get('yandexplusplus_card_hrefpage_text_attach')) {
		          $this->data['hrefpage_text'] = '';

		          $instros = explode('$', ($this->config->get('yandexplusplus_card_hrefpage_text')));
		                  $instroz = "";
		                  foreach ($instros as $instro) {
		                    if ($instro == 'orderid' ||  $instro == 'itogo' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
		                      
		                        if ($instro == 'orderid'){
		                            $instro_other = $order_info['order_id'];
		                      }
		                      if ($instro == 'itogo'){
		                          $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		                      }
		                      if ($instro == 'komis'){
		                        if($this->config->get('yandexplusplus_card_komis')){
		                            $instro_other = $this->config->get('yandexplusplus_card_komis') . '%';
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'total-komis'){
		                        if($this->config->get('yandexplusplus_card_komis')){
		                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                      if ($instro == 'plus-komis'){
		                        if($this->config->get('yandexplusplus_card_komis')){
		                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
		                        }
		                        else{$instro_other = '';}
		                      }
		                    }
		                    else {
		                      $instro_other = nl2br(htmlspecialchars_decode($instro));
		                    }
		                    $instroz .=  $instro_other;
		                  }

		          $this->data['hrefpage_text'] .= $instroz;
		        }
		        else{        
		        $this->data['send_text'] = $this->language->get('send_text');
		        $this->data['send_text2'] = $this->language->get('send_text2');
		        $this->data['inv_id'] = $inv_id;
		        $this->data['out_summ'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		        }
		    }
		    else if ($order_info['payment_code'] != 'yandexplusplus_card' && $order_info['payment_code'] != 'yandexplusplus'){
		    	$this->redirect($this->url->link('error/not_found'));
		    }
		}
		else{
			$this->data['hrefpage_text'] = $this->language->get('oplachen');
		}


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') .
           '/template/account/yandexplusplus.tpl')) {

            $this->template = $this->config->get('config_template') .
              '/template/account/yandexplusplus.tpl';
        } else {
            $this->template = 'default/template/account/yandexplusplus.tpl';
        }

        $this->children = array(
            'common/column_left',
            'common/column_right',
            'common/content_top',
            'common/content_bottom',
            'common/footer',
            'common/header'
        );

        $this->response->setOutput($this->render());
    
      }
      else{
        echo "No data";
      }
    }

  public function pay() {
  	  if (isset($this->session->data['order_id'])){
  	  	$sesionord = $this->session->data['order_id'];
  	  }
  	  else{
  	  	$sesionord = '';
  	  }

      if ($this->request->get['order_id'] != $sesionord){
        if(!isset($this->request->post['nesyandexa'])){
        	if(isset($this->request->get['paymentType'])){
        		if(isset($this->request->get['first'])){$first = '&first=1';}else{$first = '';}
        		if($this->request->get['paymentType'] == 'yandexplusplus'){
        			$this->redirect($this->url->link('account/yandexplusplus/status&order_id='.$this->request->get['order_id'].$first));
        		}
        		if($this->request->get['paymentType'] == 'yandexplusplus_card'){
        			$this->redirect($this->url->link('account/yandexplusplus/status&order_id='.$this->request->get['order_id'].$first));
        		}
        	}
        	else{
        		$this->redirect($this->url->link('common/home'));
        	}
        }
      }
      
      if ($this->request->get['paymentType'] == 'yandexplusplus' || $this->request->get['paymentType'] == 'yandexplusplus_card'){
        if ($this->request->get['paymentType'] == 'yandexplusplus'){
          $this->data['paymentType'] = 'PC';
          $this->data['receiver'] = $this->config->get('yandexplusplus_login');
          if ($this->config->get('yandexplusplus_komis')){$proc=$this->config->get('yandexplusplus_komis');}
        }
        if ($this->request->get['paymentType'] == 'yandexplusplus_card'){
          $this->data['paymentType'] = 'AC';
          $this->data['receiver'] = $this->config->get('yandexplusplus_card_login');
          if ($this->config->get('yandexplusplus_card_komis')){$proc=$this->config->get('yandexplusplus_card_komis');}
        }
      }
      else{
        echo 'error: no payment method';
        exit();
      }
      $this->load->model('checkout/order');
      $order_info = $this->model_checkout_order->getOrder($this->request->get['order_id']);
      if (is_numeric($order_info['total'])){
        if ($this->currency->has('RUB')){
          $totalrub = $order_info['total'];
          if (isset($proc)){$this->data['total'] = $this->currency->format(($totalrub*$proc/100)+$totalrub, 'RUB', $this->currency->getValue('RUB'), false);}
          else{$this->data['total'] = $this->currency->format($totalrub, 'RUB', $this->currency->getValue('RUB'), false);}
        }
        else{echo 'No currency RUB'; exit();}
      }
      else{
        echo 'error: no total sum';
        exit();
      }

      if (is_numeric($this->request->get['order_id'])){
        $this->data['order_id'] = $this->request->get['order_id'];
      }
      else{
        echo 'error: no order id';
        exit();
      }


      if (isset($this->session->data['order_id'])){
        if ($this->request->get['order_id'] == $this->session->data['order_id']){
            $this->cart->clear();
            
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['guest']);
            unset($this->session->data['comment']);
            unset($this->session->data['order_id']);  
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);
        }
      }



      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/yandexplusplus_pay.tpl')) {
      $this->template = $this->config->get('config_template') . '/template/account/yandexplusplus_pay.tpl';
    } else {
      $this->template = 'default/template/account/yandexplusplus_pay.tpl';
    }
    $this->response->setOutput($this->render());
    }

  public function callback() {
  if (isset($this->request->post['operation_id']) && isset($this->request->post['amount']) && ($this->request->post['codepro'] == 'false')){

  	$this->load->model('checkout/order');

	$order_info = $this->model_checkout_order->getOrder($this->request->post['label']);

	$totalrub = $order_info['total'];

	if ($this->request->post['notification_type'] == 'p2p-incoming'){
		$secret_key = $this->config->get('yandexplusplus_password');
		if ($this->config->get('yandexplusplus_komis')){
			$youpayment = $this->currency->format(($totalrub * $this->config->get('yandexplusplus_komis')/100) + $totalrub, 'RUB', $this->currency->getValue('RUB'), false);
		}
		else{
			$youpayment = $this->currency->format($totalrub, 'RUB', $this->currency->getValue('RUB'), false);
		}
	}
	if ($this->request->post['notification_type'] == 'card-incoming'){
		$secret_key = $this->config->get('yandexplusplus_card_password');
		if ($this->config->get('yandexplusplus_card_komis')){
			$youpayment = $this->currency->format(($totalrub * $this->config->get('yandexplusplus_card_komis')/100) + $totalrub, 'RUB', $this->currency->getValue('RUB'), false);
		}
		else{
			$youpayment = $this->currency->format($totalrub, 'RUB', $this->currency->getValue('RUB'), false);
		}
	}

	$payments = number_format($this->request->post['withdraw_amount'],2);
	$youpayment = number_format($youpayment,2);
	if($youpayment == $payments){
		$yahash = $this->request->post['sha1_hash'];
		$myhash = $this->request->post['notification_type'].'&'.$this->request->post['operation_id'].'&'.$this->request->post['amount'].'&'.$this->request->post['currency'].'&'.$this->request->post['datetime'].'&'.$this->request->post['sender'].'&'.$this->request->post['codepro'].'&'.$this->encryption->decrypt($secret_key).'&'.$this->request->post['label'];
	  	$myhash = hash("sha1", $myhash);
		

		if($yahash == $myhash) {
		  

			echo "HTTP 200 OK";
		    
			$query = $this->db->query ("INSERT INTO `" . DB_PREFIX . "yandexplusplus` SET `num_order` = '".$order_info['order_id']."' , `sum` = '".$this->request->post['withdraw_amount']."' , `date_enroled` = '".$this->request->post['datetime']."', `date_created` = '".$order_info['date_added']."', `user` = '".$order_info['payment_firstname']." ".$order_info['payment_lastname']."', `email` = '".$order_info['email']."', `status` = '1', `sender` = '".$this->request->post['operation_id']."' ");
			if ($this->request->post['notification_type'] == 'p2p-incoming'){
				$yandexpay_status_id = $this->config->get('yandexplusplus_order_status_id');
		    	if ($this->config->get('yandexplusplus_success_alert_customer')){
		        	if ($this->config->get('yandexplusplus_success_comment_attach')) {
		          		$instros = explode('$', ($this->config->get('yandexplusplus_success_comment')));
		              	$instroz = "";
		              	foreach ($instros as $instro) {
		                	if ($instro == 'orderid' ||  $instro == 'itogo'){
		                    	if ($instro == 'orderid'){
		                    		$instro_other = $order_info['order_id'];
		                  		}
		                  		if ($instro == 'itogo'){
		                      		$instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		                  		}
		                	}
		                	else {
		                  		$instro_other = nl2br(htmlspecialchars_decode($instro));
		                	}
		                	$instroz .=  $instro_other;
		              	}
		          		$message = $instroz;
		          		$this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_order_status_id'), $message, true);
		        	}
		        	else{
		          		$message = '';
		          		$this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_order_status_id'), $message, true);
		        	}
		    	}
		    	else{
		      		$this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_order_status_id'), false);
		    	}
			

		    	if ($this->config->get('yandexplusplus_success_alert_admin')) {
		      
		       		$subject = sprintf(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_info['order_id']);
		        
		        	// Text 
			        $this->load->language('account/yandexplusplus');
			        $text = sprintf($this->language->get('success_admin_alert'), $order_info['order_id']) . "\n";
			        
			        
			      
			        $mail = new Mail(); 
			        $mail->protocol = $this->config->get('config_mail_protocol');
			        $mail->parameter = $this->config->get('config_mail_parameter');
			        $mail->hostname = $this->config->get('config_smtp_host');
			        $mail->username = $this->config->get('config_smtp_username');
			        $mail->password = $this->config->get('config_smtp_password');
			        $mail->port = $this->config->get('config_smtp_port');
			        $mail->timeout = $this->config->get('config_smtp_timeout');
			        $mail->setTo($this->config->get('config_email'));
			        $mail->setFrom($this->config->get('config_email'));
			        $mail->setSender($order_info['store_name']);
			        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			        $mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
			        $mail->send();
		        
		        	// Send to additional alert emails
		        	$emails = explode(',', $this->config->get('config_alert_emails'));
		        
		        	foreach ($emails as $email) {
		          		if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
		            		$mail->setTo($email);
		            		$mail->send();
		          		}
		        	}
		    	}
		    }
		    if ($this->request->post['notification_type'] == 'card-incoming'){
		    	$yandexpay_status_id = $this->config->get('yandexplusplus_card_order_status_id');
		    	if ($this->config->get('yandexplusplus_card_success_alert_customer')){
		        	if ($this->config->get('yandexplusplus_card_success_comment_attach')) {
		          		$instros = explode('$', ($this->config->get('yandexplusplus_card_success_comment')));
		              	$instroz = "";
		              	foreach ($instros as $instro) {
		                	if ($instro == 'orderid' ||  $instro == 'itogo'){
		                    	if ($instro == 'orderid'){
		                    		$instro_other = $order_info['order_id'];
		                  		}
		                  		if ($instro == 'itogo'){
		                      		$instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
		                  		}
		                	}
		                	else {
		                  		$instro_other = nl2br(htmlspecialchars_decode($instro));
		                	}
		                	$instroz .=  $instro_other;
		              	}
		          		$message = $instroz;
		          		$this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_card_order_status_id'), $message, true);
		        	}
		        else{
		          $message = '';
		          $this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_card_order_status_id'), $message, true);
		        }
		    }
		    else{
		      $this->model_checkout_order->update($order_info['order_id'], $this->config->get('yandexplusplus_card_order_status_id'), false);
		    }

		    if ($this->config->get('yandexplusplus_card_success_alert_admin')) {
		      
		        $subject = sprintf(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_info['order_id']);
		        
		        // Text 
		        $this->load->language('account/yandexplusplus_card');
		        $text = sprintf($this->language->get('success_admin_alert'), $order_info['order_id']) . "\n";
		        
		        
		      
		        $mail = new Mail(); 
		        $mail->protocol = $this->config->get('config_mail_protocol');
		        $mail->parameter = $this->config->get('config_mail_parameter');
		        $mail->hostname = $this->config->get('config_smtp_host');
		        $mail->username = $this->config->get('config_smtp_username');
		        $mail->password = $this->config->get('config_smtp_password');
		        $mail->port = $this->config->get('config_smtp_port');
		        $mail->timeout = $this->config->get('config_smtp_timeout');
		        $mail->setTo($this->config->get('config_email'));
		        $mail->setFrom($this->config->get('config_email'));
		        $mail->setSender($order_info['store_name']);
		        $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		        $mail->setText(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
		        $mail->send();
		        
		        // Send to additional alert emails
		        $emails = explode(',', $this->config->get('config_alert_emails'));
		        
		        foreach ($emails as $email) {
		          if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
		            $mail->setTo($email);
		            $mail->send();
		          }
		        }
		   	} 
		  }
		}
		  else{
		  	echo "bad sign\n";
		  exit();
		  } 
		}
		else{
		echo "Order sum false\n";
	  exit();
	}
  }
  else{
  echo "No data";
  }
}
  public function status() {
  	if (isset($this->request->get['order_id'])){
   		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($this->request->get['order_id']);
  	}
  	else{
   		echo 'No order';
   		exit();
  	}

  	if ($this->request->get['order_id'] == $order_info['order_id']){
      $out_summ = $order_info['total'];
      $inv_id = $order_info['order_id'];
      $this->load->language('account/yandexplusplus');
      $this->data['heading_title'] = $this->language->get('heading_title');
      $this->document->setTitle($this->language->get('heading_title'));
      $this->data['button_ok'] = $this->language->get('button_ok');
      $this->data['inv_id'] = $order_info['order_id'];

      $action= HTTPS_SERVER . 'index.php?route=account/yandexplusplus';
				$online_url = $action .
				'&order_id=' . $order_info['order_id'];

      $this->data['success_text'] = '';
      if (isset($this->request->get['first'])) {
        $this->data['success_text'] .=  $this->language->get('success_text_first');
      }

      
      if($order_info['payment_code'] == 'yandexplusplus'){
      	if ($order_info['order_status_id'] == $this->config->get('yandexplusplus_order_status_id')) {
	      if ($this->config->get('yandexplusplus_success_page_text_attach')) {

	      $instros = explode('$', ($this->config->get('yandexplusplus_success_page_text')));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->config->get('yandexplusplus_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $this->data['success_text'] .= $instroz;
	      }
	      else{
	          $this->data['success_text'] .=  sprintf($this->language->get('success_text'), $inv_id);
	      }
	    }
	    else{
	    	if ($this->config->get('yandexplusplus_waiting_page_text_attach')) {

	      $instros = explode('$', ($this->config->get('yandexplusplus_waiting_page_text')));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->config->get('yandexplusplus_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get('yandexplusplus_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $this->data['success_text'] .= $instroz;
	      }
	      else{
	          $this->data['success_text'] .=  sprintf($this->language->get('success_text_wait'), $inv_id, $online_url);
	      }
	    }
	  }
	  if($order_info['payment_code'] == 'yandexplusplus_card'){
	      if ($order_info['order_status_id'] == $this->config->get('yandexplusplus_card_order_status_id')) {
	      if ($this->config->get('yandexplusplus_card_success_page_text_attach')) {

	      $instros = explode('$', ($this->config->get('yandexplusplus_card_success_page_text')));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->config->get('yandexplusplus_card_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $this->data['success_text'] .= $instroz;
	      }
	      else{
	          $this->data['success_text'] .=  sprintf($this->language->get('success_text'), $inv_id);
	      }
	    }
	    else{
	    	if ($this->config->get('yandexplusplus_card_waiting_page_text_attach')) {

	      $instros = explode('$', ($this->config->get('yandexplusplus_card_waiting_page_text')));
	              $instroz = "";
	              foreach ($instros as $instro) {
	                if ($instro == 'orderid' ||  $instro == 'itogo' || $instro== 'href' || $instro == 'komis' || $instro == 'total-komis' || $instro == 'plus-komis'){
	                  if ($instro == 'orderid'){
	                    $instro_other = $inv_id;
	                  }
	                  if ($instro == 'itogo'){
	                      $instro_other = $this->currency->format($out_summ, $order_info['currency_code'], $order_info['currency_value'], true);
	                  }
	                  if ($instro == 'href'){
	                      $instro_other = $online_url;
	                  }
	                  if ($instro == 'komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->config->get('yandexplusplus_card_komis') . '%';
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'total-komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100, $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                      if ($instro == 'plus-komis'){
	                        if($this->config->get('yandexplusplus_card_komis')){
	                            $instro_other = $this->currency->format($order_info['total'] + ($order_info['total'] * $this->config->get('yandexplusplus_card_komis')/100), $order_info['currency_code'], $order_info['currency_value'], true);
	                        }
	                        else{$instro_other = '';}
	                      }
	                }
	                else {
	                  $instro_other = nl2br(htmlspecialchars_decode($instro));
	                }
	                $instroz .=  $instro_other;
	              }

	      $this->data['success_text'] .= $instroz;
	      }
	      else{
	          $this->data['success_text'] .=  sprintf($this->language->get('success_text_wait'), $inv_id, $online_url);
	      }
	    }
	  }
	  
      if ($this->customer->isLogged()) {
        $this->data['success_text'] .=  sprintf($this->language->get('success_text_loged'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/order/info&order_id=' . $inv_id, '', 'SSL'));
        if($order_info['payment_code'] == 'yandexplusplus'){ 
	        if ($order_info['order_status_id'] != $this->config->get('yandexplusplus_order_status_id')) {
	        	 $this->data['success_text'] .=  sprintf($this->language->get('waiting_text_loged'), $this->url->link('account/order', '', 'SSL'));
	        }
	    }
	    if($order_info['payment_code'] == 'yandexplusplus_card'){
	    	if ($order_info['order_status_id'] != $this->config->get('yandexplusplus_card_order_status_id')) {
	        	 $this->data['success_text'] .=  sprintf($this->language->get('waiting_text_loged'), $this->url->link('account/order', '', 'SSL'));
	        }
	    }
      }
      
      $this->data['breadcrumbs'] = array();

      $this->data['breadcrumbs'][] = array(
        'text'      => $this->language->get('text_home'),
        'href'      => $this->url->link('common/home'),
        'separator' => false
      );
      
      if (isset($this->request->get['first'])) {
        $this->language->load('checkout/success');
        $this->data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/cart'),
          'text'      => $this->language->get('text_basket'),
          'separator' => $this->language->get('text_separator')
        );
        
        $this->data['breadcrumbs'][] = array(
          'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
          'text'      => $this->language->get('text_checkout'),
          'separator' => $this->language->get('text_separator')
        );
        $this->data['button_ok_url'] = $this->url->link('common/home');
      }
      else{
        if ($this->customer->isLogged()) {
          $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('lich'),
            'href'      => $this->url->link('account/account', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
          );

          $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('history'),
            'href'      => $this->url->link('account/order', '', 'SSL'),
            'separator' => $this->language->get('text_separator')
          );
          $this->data['button_ok_url'] = $this->url->link('account/order', '', 'SSL');
        }
        else{
          $this->data['button_ok_url'] = $this->url->link('common/home');
        }
      }

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') .
          '/template/account/yandexplusplus_success.tpl')) {

          $this->template = $this->config->get('config_template') .
          '/template/account/yandexplusplus_success.tpl';
      } else {
          $this->template = 'default/template/account/yandexplusplus_status.tpl';
      }

      $this->children = array(
          'common/column_left',
          'common/column_right',
          'common/content_top',
          'common/content_bottom',
          'common/footer',
          'common/header'
      );
      $this->response->setOutput($this->render());
    }
    else{
      echo "No data";
    }
  }
}
?>