<?php
	//отчет по товарам
	require 'connect.php';
	@mysqli_select_db($ddb, 'briefmed_brief');
	if ($ro_user['user_group_id'] == 1) {
		$br = chr(13).chr(10);
		$ret = '"Дата";Сумма (вх.)";"Сумма (исх.)";"Сумма (дост.)";"ИТОГО";"Налог 6%";"Курьеру";"МАРЖА";"Брутто МАРЖА";"Рентабельность %";"Заказов";"Arabin ASQ":"Другие Arabin";"Акушерский Юнона";"Маточный Юнона";"Арабины";"Юноны";"Портекс";"Альфа-пластик";"ИТОГО Пессарии"'.$br;
		header('Content-Type: csv/plain; charset=cp-1251');
		header('Content-Disposition: attachment; filename="report_summ_daily_'.date('Y-m-d H-i-s').'.csv"');
		$qu_day = '
						SELECT	*
						FROM 		tmp_report_summ_daily
						WHERE		1
										'.(($_SESSION['report_summ_daily_date_from'] != '') ? ' && `date`>="'.$_SESSION['report_summ_daily_date_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_date_to'] != '') ? ' && `date`<="'.$_SESSION['report_summ_daily_date_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_summ_in_from'] != '') ? ' && `summ_in`>="'.$_SESSION['report_summ_daily_summ_in_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_summ_in_to'] != '') ? ' && `summ_in`<="'.$_SESSION['report_summ_daily_summ_in_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_summ_out_from'] != '') ? ' && `summ_out`>="'.$_SESSION['report_summ_daily_summ_out_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_summ_out_to'] != '') ? ' && `summ_out`<="'.$_SESSION['report_summ_daily_summ_out_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_price_delivery_from'] != '') ? ' && `price_delivery`>="'.$_SESSION['report_summ_daily_price_delivery_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_price_delivery_to'] != '') ? ' && `price_delivery`<="'.$_SESSION['report_summ_daily_price_delivery_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_total_from'] != '') ? ' && `total`>="'.$_SESSION['report_summ_daily_total_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_total_to'] != '') ? ' && `total`<="'.$_SESSION['report_summ_daily_total_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_tax_from'] != '') ? ' && `tax`>="'.$_SESSION['report_summ_daily_tax_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_tax_to'] != '') ? ' && `tax`<="'.$_SESSION['report_summ_daily_tax_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_courier_price_from'] != '') ? ' && `courier_price`>="'.$_SESSION['report_summ_daily_courier_price_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_courier_price_to'] != '') ? ' && `courier_price`<="'.$_SESSION['report_summ_daily_courier_price_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_margin_netto_from'] != '') ? ' && `margin_netto`>="'.$_SESSION['report_summ_daily_margin_netto_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_margin_netto_to'] != '') ? ' && `margin_netto`<="'.$_SESSION['report_summ_daily_margin_netto_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_margin_brutto_from'] != '') ? ' && `margin_brutto`>="'.$_SESSION['report_summ_daily_margin_brutto_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_margin_brutto_to'] != '') ? ' && `margin_brutto`<="'.$_SESSION['report_summ_daily_margin_brutto_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_profitability_from'] != '') ? ' && `profitability`>="'.$_SESSION['report_summ_daily_profitability_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_profitability_to'] != '') ? ' && `profitability`<="'.$_SESSION['report_summ_daily_profitability_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_orders_from'] != '') ? ' && `orders`>="'.$_SESSION['report_summ_daily_orders_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_orders_to'] != '') ? ' && `orders`<="'.$_SESSION['report_summ_daily_orders_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_asq_from'] != '') ? ' && `amount_arabin_asq`>="'.$_SESSION['report_summ_daily_amount_arabin_asq_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_asq_to'] != '') ? ' && `amount_arabin_asq`<="'.$_SESSION['report_summ_daily_amount_arabin_asq_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_other_from'] != '') ? ' && `amount_arabin_other`>="'.$_SESSION['report_summ_daily_amount_arabin_other_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_other_to'] != '') ? ' && `amount_arabin_other`<="'.$_SESSION['report_summ_daily_amount_arabin_other_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_obstetric_from'] != '') ? ' && `amount_yunona_obstetric`>="'.$_SESSION['report_summ_daily_amount_yunona_obstetric_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_obstetric_to'] != '') ? ' && `amount_yunona_obstetric`<="'.$_SESSION['report_summ_daily_amount_yunona_obstetric_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_gynecological_from'] != '') ? ' && `amount_yunona_gynecological`>="'.$_SESSION['report_summ_daily_amount_yunona_gynecological_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_gynecological_to'] != '') ? ' && `amount_yunona_gynecological`<="'.$_SESSION['report_summ_daily_amount_yunona_gynecological_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_arabin_all_from'] != '') ? ' && `amount_arabin_all`>="'.$_SESSION['report_summ_daily_amount_arabin_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_arabin_all_to'] != '') ? ' && `amount_arabin_all`<="'.$_SESSION['report_summ_daily_amount_arabin_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_yunona_all_from'] != '') ? ' && `amount_yunona_all`>="'.$_SESSION['report_summ_daily_amount_yunona_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_yunona_all_to'] != '') ? ' && `amount_yunona_all`<="'.$_SESSION['report_summ_daily_amount_yunona_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_portex_all_from'] != '') ? ' && `amount_portex_all`>="'.$_SESSION['report_summ_daily_amount_portex_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_portex_all_to'] != '') ? ' && `amount_portex_all`<="'.$_SESSION['report_summ_daily_amount_portex_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_alfa_all_from'] != '') ? ' && `amount_alfa_all`>="'.$_SESSION['report_summ_daily_amount_alfa_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_alfa_all_to'] != '') ? ' && `amount_alfa_all`<="'.$_SESSION['report_summ_daily_amount_alfa_all_to'].'"' : '').'

										'.(($_SESSION['report_summ_daily_amount_all_from'] != '') ? ' && `amount_all`>="'.$_SESSION['report_summ_daily_amount_all_from'].'"' : '').'
										'.(($_SESSION['report_summ_daily_amount_all_to'] != '') ? ' && `amount_all`<="'.$_SESSION['report_summ_daily_amount_all_to'].'"' : '').'
						ORDER	BY	'.$_GET['s'];
		$re_day = @mysqli_query($ddb, $qu_day);
		//$ret .=  $qu_day.$br;
		//$ret .= mysqli_error($ddb).$br;
		unset($summ);
		//$ret .= '-'.mysqli_num_rows($re_day).'-'.$br;
		while ($ro_day = @mysqli_fetch_array($re_day)) {
			$summ['summ_in'] += $ro_day['summ_in'];
			$summ['summ_out'] += $ro_day['summ_out'];
			$summ['price_delivery'] += $ro_day['price_delivery'];
			$summ['total'] += $ro_day['total'];
			$summ['tax'] += $ro_day['tax'];
			$summ['courier_price'] += $ro_day['courier_price'];
			$summ['margin_netto'] += $ro_day['margin_netto'];
			$summ['margin_brutto'] += $ro_day['margin_brutto'];
			$summ['orders'] += $ro_day['orders'];
			$summ['amount_arabin_asq'] += $ro_day['amount_arabin_asq'];
			$summ['amount_arabin_other'] += $ro_day['amount_arabin_other'];
			$summ['amount_yunona_obstetric'] += $ro_day['amount_yunona_obstetric'];
			$summ['amount_yunona_gynecological'] += $ro_day['amount_yunona_gynecological'];
			$summ['amount_arabin_all'] += $ro_day['amount_arabin_all'];
			$summ['amount_yunona_all'] += $ro_day['amount_yunona_all'];
			$summ['amount_portex_all'] += $ro_day['amount_portex_all'];
			$summ['amount_alfa_all'] += $ro_day['amount_alfa_all'];
			$summ['amount_all'] += $ro_day['amount_all'];
			$ret .= 	'"'.$ro_day['date'].'";"'
							 .str_replace('.',',',$ro_day['summ_in']).'";"'
							 .str_replace('.',',',$ro_day['summ_out']).'";"'
							 .str_replace('.',',',$ro_day['price_delivery']).'";"'
							 .str_replace('.',',',$ro_day['total']).'";"'
							 .str_replace('.',',',$ro_day['tax']).'";"'
							 .str_replace('.',',',$ro_day['courier_price']).'";"'
							 .str_replace('.',',',$ro_day['margin_netto']).'";"'
							 .str_replace('.',',',$ro_day['margin_brutto']).'";"'
							 .str_replace('.',',',$ro_day['profitability']).'";"'
							 .$ro_day['orders'].'";"'
							 .$ro_day['amount_arabin_asq'].'";"'
							 .$ro_day['amount_arabin_other'].'";"'
							 .$ro_day['amount_yunona_obstetric'].'";"'
							 .$ro_day['amount_yunona_gynecological'].'";"'
							 .$ro_day['amount_arabin_all'].'";"'
							 .$ro_day['amount_yunona_all'].'";"'
							 .$ro_day['amount_portex_all'].'";"'
							 .$ro_day['amount_alfa_all'].'";"'
							 .$ro_day['amount_all'].'"'.$br;			
		}
		$ret .= '"ИТОГО";"'
						 .str_replace('.',',',$summ['summ_in']).'";"'
						 .str_replace('.',',',$summ['summ_out']).'";"'
						 .str_replace('.',',',$summ['price_delivery']).'";"'
						 .str_replace('.',',',$summ['total']).'";"'
						 .str_replace('.',',',$summ['tax']).'";"'
						 .str_replace('.',',',$summ['courier_price']).'";"'
						 .str_replace('.',',',$summ['margin_netto']).'";"'
						 .str_replace('.',',',$summ['margin_brutto']).'";"'
						 .str_replace('.',',',round($summ['margin_netto']/$summ['total']*100,2)).'";"'
						 .$summ['orders'].'";"'
						 .$summ['amount_arabin_asq'].'";"'
						 .$summ['amount_arabin_other'].'";"'
						 .$summ['amount_yunona_obstetric'].'";"'
						 .$summ['amount_yunona_gynecological'].'";"'
						 .$summ['amount_arabin_all'].'";"'
						 .$summ['amount_yunona_all'].'";"'
						 .$summ['amount_portex_all'].'";"'
						 .$summ['amount_alfa_all'].'";"'
						 .$summ['amount_all'].'"';			
		
		echo iconv('utf-8','cp1251',$ret);
		//echo $ret;
	} else {
		header('Location: /');
	}

