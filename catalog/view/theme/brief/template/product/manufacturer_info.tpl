<?php echo $header; ?>

				<?php //echo $column_right; ?>
				<div class="content-container">

					<section class="box">
						<div class="box-heading">
							<!--21:05:35--><div class="align-left"><?php echo $heading_title; ?></div>
							<div class="display">
								<span class="displayText"><?php echo $text_display; ?></span>
								<div class="gridListActive"></div>
								<a onclick="display('grid');">
									<div class="listView"></div>
								</a>
							</div>
						    <div class="sort"><b><?php echo $text_sort; ?></b>
						      <select onchange="location = this.value;">
						        <?php foreach ($sorts as $sorts) { ?>
						        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
						        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
						        <?php } else { ?>
						        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
						        <?php } ?>
						        <?php } ?>
						      </select>
						    </div>
						</div>

						<nav class="breadcrumb">
						    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
						    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
						    <?php } ?>
						</nav>
						<?php echo $_SESSION['text_compare'];?>
						<div style="clear: both; height: 1px;"></div>

					  <?php if ($products) { /*?>
					  <div class="product-filter">
					    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
					    <div class="limit"><b><?php echo $text_limit; ?></b>
					      <select onchange="location = this.value;">
					        <?php foreach ($limits as $limits) { ?>
					        <?php if ($limits['value'] == $limit) { ?>
					        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
					        <?php } else { ?>
					        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
					        <?php } ?>
					        <?php } ?>
					      </select>
					    </div>
					    <div class="sort"><b><?php echo $text_sort; ?></b>
					      <select onchange="location = this.value;">
					        <?php foreach ($sorts as $sorts) { ?>
					        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
					        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
					        <?php } else { ?>
					        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
					        <?php } ?>
					        <?php } ?>
					      </select>
					    </div>
					  </div>
					  <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
					  <?php */ ?>
					  <div class="product-list">
					    <?php $i=0; foreach ($products as $product) { $i++; ?>
							<div class="one-third<?php echo ($i % 3 == 0)? " last":"";?>">
								<article class="box-content product-item<?php echo ($product['special'])?" action":"";?><?php echo $product['sale'].$product['gift'];?>">
									<div class="left">
										<?php if ($product['thumb']) { ?>
										<div class="image">
											<a href="<?php echo $product['href']; ?>">
												<img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>"/>
											</a>
										</div>
										<?php } ?>
										<div class="name" style="height: 50px;">
											<h6><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h6>
										</div>
										<div class="description"><?php echo $product['description']; ?></div>
									</div>
									<div class="right" style="margin-bottom: 0;">
										<div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
										<div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"<?php  echo ($_SESSION['compare'][0] == $product['product_id'] || $_SESSION['compare'][1] == $product['product_id'] || $_SESSION['compare'][2] == $product['product_id']) ? ' class="active"' : ''; ?>><?php echo $button_compare; ?></a></div>

								        <?php if (!$product['special']) { ?>
								        <div class="price"><?php echo $product['price']; ?></div>
								        <?php } else { ?>
								        <div class="price"><div style="display: inline-block; font-size: 14px; font-weight: bold; text-decoration: line-through; color: #000;"><?php echo intval($product['price']); ?></div> <?php echo $product['special']; ?></div>
								        <?php } ?>
								        <?php /*if ($product['tax']) { ?>
								        <br />
								        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
								        <?php } */?>

									    <?php /*if ($product['rating']) { ?>
									      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
									    <?php } */?>

										<div class="cart">
										<?
											// ******************* SEDOY проверка есть ли в корзине (в т.ч. товары со свойствами)
											$incart = '';
											$tmp_cart_var_names  = array();
											foreach ($_SESSION['cart'] as $cart_name => $cart_value) {
												$tmp = explode(':',$cart_name);
												if ($product['product_id'] == $tmp[0]) $incart = 'incart';
											}
											// ******************* SEDOY конец проверки на наличие в корзине
											if ($incart) {
												echo '<a href="'.$product['href'].'" class="button incart" id="addtocart_'.$product['product_id'].'">В корзине</a>';												
											} else {
												echo '<a href="'.$product['href'].'" class="button" onclick="addToCart(\''.$product['product_id'].'\'); return false;" id="addtocart_'.$product['product_id'].'">Купить</a>';
											}
										?>
										<!--<a href="<?php echo $product['href']; ?>" class="button" onclick="addToCart('<?php echo $product['product_id']; ?>'); return false;" id="addtocart_<?php echo $product['product_id']; ?>">Купить</a>--></div>
									</div>
								</article>
							</div>
					    <?php } ?>
					  </div>
					  <div class="clear"></div>
					  <div class="pagination"><?php echo $pagination; ?></div>
					  <?php } ?>

					</section>

				<?php //echo $content_top; ?>
				<?php //echo $content_bottom; ?>
				</div>
				<?php echo $column_left; ?><?php echo $column_right; ?>

<script type="text/javascript"><!--
// $('div.sidebar-left').removeClass('sidebar-left');

//--></script>

<?php echo $footer; ?>


<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--21:05:42--><div><?php echo $heading_title; ?></div>
  <?php if ($products) { ?>
  <div class="product-filter">
    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
    <div class="limit"><?php echo $text_limit; ?>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort"><?php echo $text_sort; ?>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
  <div class="product-list">
    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($product['rating']) { ?>
      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
      <?php } ?>
      <div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" /></div>
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a></div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php if ($description) { ?>
  <div class="manufacturer-info"><?php echo $description; ?></div>
  <?php } ?>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php }?>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');

		$('.product-list > div').each(function(index, element) {
			html  = '<div class="right">';
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
			html += '</div>';

			html += '<div class="left">';

			var image = $(element).find('.image').html();

			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}

			var price = $(element).find('.price').html();

			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}

			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

			var rating = $(element).find('.rating').html();

			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}

			html += '</div>';


			$(element).html(html);
		});

		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');

		$.cookie('display', 'list');
	} else {
		$('.product-list').attr('class', 'product-grid');

		$('.product-grid > div').each(function(index, element) {
			html = '';

			var image = $(element).find('.image').html();

			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}

			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';

			var price = $(element).find('.price').html();

			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}

			var rating = $(element).find('.rating').html();

			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}

			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';

			$(element).html(html);
		});

		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');

		$.cookie('display', 'grid');
	}
}

view = $.cookie('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script>
<?php echo $footer; */?>