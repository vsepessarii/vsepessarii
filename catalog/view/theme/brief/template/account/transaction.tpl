<?php echo $header; ?>

				<?php //echo $column_right; ?>
				<div class="content-container">

					<section class="box">
						<!--20:59:48--><div class="box-heading"><?php echo $heading_title; ?></div>

						<nav class="breadcrumb">
						    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
						    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
						    <?php } ?>
    					</nav>


						<div class="box-content" style="margin-top: 30px !important;">
						  <p><?php echo $text_total; ?><b> <?php echo $total; ?></b>.</p>
						  <table class="list">
						    <thead>
						      <tr>
						        <td class="left"><?php echo $column_date_added; ?></td>
						        <td class="left"><?php echo $column_description; ?></td>
						        <td class="right"><?php echo $column_amount; ?></td>
						      </tr>
						    </thead>
						    <tbody>
						      <?php if ($transactions) { ?>
						      <?php foreach ($transactions  as $transaction) { ?>
						      <tr>
						        <td class="left"><?php echo $transaction['date_added']; ?></td>
						        <td class="left"><?php echo $transaction['description']; ?></td>
						        <td class="right"><?php echo $transaction['amount']; ?></td>
						      </tr>
						      <?php } ?>
						      <?php } else { ?>
						      <tr>
						        <td class="center" colspan="5"><?php echo $text_empty; ?></td>
						      </tr>
						      <?php } ?>
						    </tbody>
						  </table>
						  <div class="pagination"><?php echo $pagination; ?></div>
						  <div class="buttons">
						    <div class="right"><a href="<?php echo $continue; ?>" class="button gray align-left"><?php echo $button_continue; ?></a></div>
						  </div>
 							<div class="clear"></div>
  						</div>
					</section>

				<?php //echo $content_top; ?>
				<?php //echo $content_bottom; ?>
				</div>
				<?php echo $column_left; ?><?php echo $column_right; ?>

<?php echo $footer; ?>

<?php /*echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--20:59:53--><div><?php echo $heading_title; ?></div>
  <p><?php echo $text_total; ?><b> <?php echo $total; ?></b>.</p>
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $column_date_added; ?></td>
        <td class="left"><?php echo $column_description; ?></td>
        <td class="right"><?php echo $column_amount; ?></td>
      </tr>
    </thead>
    <tbody>
      <?php if ($transactions) { ?>
      <?php foreach ($transactions  as $transaction) { ?>
      <tr>
        <td class="left"><?php echo $transaction['date_added']; ?></td>
        <td class="left"><?php echo $transaction['description']; ?></td>
        <td class="right"><?php echo $transaction['amount']; ?></td>
      </tr>
      <?php } ?>
      <?php } else { ?>
      <tr>
        <td class="center" colspan="5"><?php echo $text_empty; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
  <div class="pagination"><?php echo $pagination; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; */?>