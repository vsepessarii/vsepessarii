<?php
	//элементы УТП
	/*header('Content-Type: text/html; charset=utf-8');
	session_start();*/

	require 'connect.php';	
	
	if ($_GET['p'] == '') $_GET['p'] = 1;

	if (isset($ro_user['user_group_id']) && $ro_user['user_group_id'] == 1) {
		if (isset($_GET['act']) && ($_GET['act'] == 'del')) {
			$_GET['id'] = intval($_GET['id']);
			$used = @mysqli_num_rows(@mysqli_query($ddb, 'SELECT * FROM utp_set_item WHERE utp_item_id='.intval($_GET['id'])));
			if ($used == 0) {
				@mysqli_query($ddb, 'DELETE FROM utp_item WHERE utp_item_id='.intval($_GET['id']));
				$tpl['msg'] = 'Элемент УТП удален.';
			} else {
				$tpl['msg'] = 'Элемент УТП изпользуется в '.$used.' предложениях и не может быть удален.';
			}
			$_GET['act'] = '';
			$_GET['id'] = '';

		}
		
		switch (isset($_POST['act']) && $_POST['act']) {
			case 'add':
				@mysqli_query($ddb, '
					INSERT INTO	utp_item
					SET			`name`="'.mysqli_escape_string($ddb,$_POST['name']).'",
								`title`="'.mysqli_escape_string($ddb,$_POST['title']).'",
								`text`="'.mysqli_escape_string($ddb,$_POST['text']).'",
								`params`="'.mysqli_escape_string($ddb,json_encode($_POST['params'])).'",
								`type`="'.$_POST['type'].'"
				');
				#echo mysqli_error($ddb);
				$_GET['act'] = 'edit';
				$_GET['id'] = @mysqli_insert_id($ddb);
				$tpl['msg'] = 'Элемент УТП "'.$_POST['title'].'" добавлен.';
				header('Location: ' . $_SERVER['REQUEST_URI']);
				exit();
				break;
				
			case 'edit':
				$_POST['id'] = intval($_POST['id']);
				@mysqli_query($ddb, '
					UPDATE	utp_item
					SET		name="'.htmlspecialchars($_POST['name']).'",
							title="'.htmlspecialchars($_POST['title']).'",
							text="'.mysqli_escape_string($ddb,$_POST['text']).'",
							params="'.mysqli_escape_string($ddb,json_encode($_POST['params'])).'",
							type="'.$_POST['type'].'"
					WHERE	utp_item_id="'.$_POST['id'].'"
				');
				$_GET['act'] = 'edit';
				$_GET['id'] = $_POST['id'];
				$tpl['msg'] = 'Элемент УТП "'.$_POST['title'].'" изменен.';
				break;
		}

		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
			</style>
			<script src="/admin/view/javascript/ckeditor/ckeditor.js"></script>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"><a href="'.$_SERVER['PHP_SELF'].'?act=add" class="button">Новый элемент УТП</a></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br>
			'.((isset($tpl) && ($tpl['msg'] != '')) ? '<div class="msg">'.(isset($tpl) ? $tpl['msg'] : '').'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="50%">
						<table class="list">
							<thead>
								<tr>
									<td width="5%"></td>
									<td width="5%"></td>
									<td width="65%">Элемент УТП</td>
									<td width="25%" align="center">Тип</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM utp_item ORDER BY name, title';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.(isset($_GET['page']) ? $_GET['page'] : '').'&id='.$ro['utp_item_id'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td align="center"><a href="?act=copy&id='.$ro['utp_item_id'].'"><img src="img/copy.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['utp_item_id'].'" style="'.(isset($_GET['id']) && ($ro['utp_item_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['name'].' / '.$ro['title'].'</a></td>
									<td align="center"><a href="?act=edit&id='.$ro['utp_item_id'].'" style="'.(isset($_GET['id']) && ($ro['utp_item_id'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['type'].'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="50%">
		';
		if (isset($_GET['act']) && ($_GET['act'] == 'add' || $_GET['act'] == 'edit' || $_GET['act'] == 'copy')) {
			if ($_GET['act'] == 'add') {
				$tpl['body_title'] = 'Добавить элемент УТП';
			} elseif ($_GET['act'] == 'edit') {
				$tpl['body_title'] = 'Обновить элемент УТП';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM utp_item WHERE utp_item_id="'.intval($_GET['id']).'"'));
				$params = json_decode($ro['params'], 1);
			} elseif ($_GET['act'] == 'copy') {
				$tpl['body_title'] = 'Копировать элемент УТП';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM utp_item WHERE utp_item_id="'.intval($_GET['id']).'"'));
				$params = json_decode($ro['params'], 1);
				$_GET['id'] = '';
				$_GET['act'] = 'add';
			}
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.title.value == "") msg = msg + br + " - не заполнено наименование;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}

					function SwitchBlocks(block) {
						$("tr[rel=\'text\']").css("display", "none");
						$("tr[rel=\'last-site-feedback\']").css("display", "none");
						$("tr[rel=\'definite-article\']").css("display", "none");
						$("tr[rel=\'random-faq\']").css("display", "none");
						$("tr[rel=\'" + block + "\']").css("display", "table-row");
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.$_GET['act'].'">
				<input type="hidden" name="id" value="'.(isset($_GET['id']) ? $_GET['id'] : '').'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Наименование (внутр.):</td>
						<td width="85%" class="txt" align="left"><input name="name" id="name" title="Наименование внутреннее (на сайте не отображается)" class="txt" style="width:350px;" value="'.$ro['name'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">Наименование (сайт):</td>
						<td class="txt" align="left"><input name="title" id="title" title="Наименование, отображаемое на сайте" class="txt" style="width:350px;" value="'.$ro['title'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right">Тип:</td>
						<td class="txt" align="left">
							<select name="type" onChange="SwitchBlocks(this.value);">
								<option></option>
								<option value="text"'.(($ro['type'] == 'text') ? ' selected' : '').'>Текст (статика)</option>
								<option value="last-site-feedback"'.(($ro['type'] == 'last-site-feedback') ? ' selected' : '').'>Последние отзывы с сайта</option>
								<option value="definite-article"'.(($ro['type'] == 'definite-article') ? ' selected' : '').'>Определенные статьи</option>
								<option value="random-faq"'.(($ro['type'] == 'random-faq') ? ' selected' : '').'>Случайные FAQ</option>
							</select>
						</td>
					</tr>
					<tr rel="text"'.(($ro['type'] == 'text') ? '' : ' style="display: none;"').'>
						<td class="txt" align="right">Текст:</td>
						<td class="txt" align="left"><textarea name="text" id="text" class="txt" style="width:350px; height: 200px;">'.$ro['text'].'</textarea></td>
					</tr>
					<tr rel="last-site-feedback"'.(($ro['type'] == 'last-site-feedback') ? '' : ' style="display: none;"').'>
						<td class="txt" align="right">Кол-во отзывов:</td>
						<td class="txt" align="left"><input type="text" name="params[last_site_feedback_count]" value="'.(isset($params['last_site_feedback_count']) ? $params['last_site_feedback_count'] : '').'" /></td>
					</tr>
					<tr rel="definite-article"'.(($ro['type'] == 'definite-article') ? '' : ' style="display: none;"').'>
						<td class="txt" align="right">Кол-во статей:</td>
						<td class="txt" align="left"><input type="text" name="params[definite_article_count]" value="'.(isset($params['definite_article_count']) ? $params['definite_article_count'] : '').'" /></td>
					</tr>
					<tr rel="definite-article"'.(($ro['type'] == 'definite-article') ? '' : ' style="display: none;"').'>					
						<td class="txt" align="right">Статьи:</td>
						<td class="txt" align="left">
							<table width="100%"
			';
			$qu_art= 'SELECT information_id, title FROM information_description ORDER by title';
			$re_art = @mysqli_query($ddb, $qu_art);
			while ($ro_art = @mysqli_fetch_assoc($re_art)) $tpl['body_content'] .= '
								<tr'.((isset($params['definite_article_id'][$ro_art['information_id']])) ? '' : ' class="gray"').'>
									<td>
										<select name="params[definite_article_id]['.$ro_art['information_id'].']">
											<option></option>
											'.NumberOptions((isset($params['definite_article_id'][$ro_art['information_id']]) ? $params['definite_article_id'][$ro_art['information_id']] : 1),1,50,1).'
										</select>
									</td>
									<td>'.$ro_art['title'].'</td>
								</tr>
			';
			$tpl['body_content'] .= '
							</table>
						</td>
					</tr>
					<tr rel="random-faq"'.(($ro['type'] == 'random-faq') ? '' : ' style="display: none;"').'>
						<td class="txt" align="right">Кол-во вопр.-отв.:</td>
						<td class="txt" align="left"><input type="text" name="params[random_faq_count]" value="'.(isset($params['random_faq_count']) ? $params['random_faq_count'] : '').'" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="txt" value="'.(($_GET['act'] == 'add') ? 'Добавить' : 'Обновить').' элемент УТП"></td>
					</tr>
				</table>
				</form>
				<script>
					CKEDITOR.replace(\'text\',{toolbar : \'Minimum\'});
				</script>
			';
		} else {
			$tpl['body_title'] = 'Элементы УТП';
		}
		$tpl['body_content'] .= '
				</td>
			</tr>
		</table>
		';		
		$tpl['meta_title'] = 'Элементы УТП';
		require 'template.php';	
	} else {
		header('Location: /');
	}

