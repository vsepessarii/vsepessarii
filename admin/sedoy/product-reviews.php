<?php
	//количество отзывов о товарах
	/*
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	$ddb = @mysqli_connect('briefmed.mysql.ukraine.com.ua','briefmed_brief','azxhwwbr');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmed_brief');
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	*/

	require 'connect.php';

	if ($ro_user['user_group_id'] == 1) {
		$qu_product = '
			SELECT 		product.product_id as product_id,
								product_description.name as name,
								manufacturer.name as manufacturer,
								product.model as model,
								product.sku as sku,
								(SELECT COUNT(product_id) FROM review WHERE review.product_id = product.product_id) as reviews_all,
								(SELECT COUNT(product_id) FROM review WHERE review.product_id = product.product_id && status="1") as reviews_on,
								product.price as price,
								product.status as status
			FROM			product_description,
								product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
			WHERE			product.product_id = product_description.product_id
								'.((isset($_GET['status']) && ($_GET['status'] !== '')) ? ' && product.status="'.$_GET['status'].'"' : '').'
			GROUP BY	product.product_id
			ORDER BY	reviews_on ASC,
								reviews_all ASC,
								product.product_id ASC
								
		';
		$re_product = @mysqli_query($ddb, $qu_product);
		//echo mysqli_error($ddb);
?>
<html>
	<head>
		<title>Отзывы о товарах</title>
		<link type="text/css" href="<?php $app_url ?>/view/stylesheet/stylesheet.css" rel="stylesheet" />
		<link rel="icon" href="icon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="icon.ico" type="image/x-icon" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
		<script>
			function AddComment(product_id, n) {
				$.post("product-reviews-add.php", $("#form-"+product_id+"-"+n).serialize()).done(function( data ) {
					$("#add-comment-td-"+product_id+"-"+n ).html( data );
				});
			}
		</script>
	</head>
	<body>
		<center>
			<a href="?status=1">Включены</a>
			&nbsp;&nbsp;
			<a href="?status=0">Отключены</a>
			&nbsp;&nbsp;
			<a href="?status=">Все</a>
		</center>	
		<br />
		<table class="list">
			<thead>
				<tr>
					<td class="center" width="2%">№</td>
					<td class="center" width="5%">ID</td>
					<td width="27%">Наименование</td>
					<td class="center" width="7%">Артикул</td>
					<td class="center" width="10%">Поиск</td>
					<td class="center" width="43%">Отз.</td>
					<td class="center" width="1%">С</td>
				</tr>
			</thead>
			<tbody>
<?php
		$i=0;
		while ($ro_product = @mysqli_fetch_array($re_product)) {
			$i++
?>
				<tr>
					<td class="center"><?php echo $i;?></td>
					<td class="center"><a href="/index.php?route=product/product&product_id=<?php echo $ro_product['product_id']?>" target="_blank"><?php echo $ro_product['product_id'];?></a></td>
					<td><?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model']?></td>
					<td class="center"><?php echo $ro_product['sku']?></td>
					<td class="center">
						<a href="http://yandex.ru/yandsearch?text=<?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'];?>" target="_blank">Я</a>
						&nbsp;&nbsp;
						<a href="http://yandex.ru/yandsearch?text=<?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'];?> отзывы" target="_blank">Я (+о)</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="http://google.ru/search?num=100&newwindow=1&safe=off&espv=210&es_sm=93&q=<?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'];?>" target="_blank">G</a>
						<a href="http://google.ru/search?num=100&newwindow=1&safe=off&espv=210&es_sm=93&q=<?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'];?> отзывы" target="_blank">G (+о)</a>
					</td>
					<td class="center"><span onclick="$('#add-comment-tr-<?php echo $ro_product['product_id']?>-1').toggle('fast');$('#add-comment-tr-<?php echo $ro_product['product_id']?>-2').toggle('fast');$('#add-comment-tr-<?php echo $ro_product['product_id']?>-3').toggle('fast');" style="cursor: pointer;"><?
						echo $ro_product['reviews_on'].' из ',$ro_product['reviews_all'].'</span>';
						$qu_review = 'SELECT * FROM review WHERE product_id='.$ro_product['product_id'].' ORDER BY status DESC, date_added DESC';
						$re_review = @mysqli_query($ddb, $qu_review);
						while ($ro_review = @mysqli_fetch_array($re_review)) echo '<br><a href="/index.php?route=catalog/review/update&token='.$_SESSION['token'].'&review_id='.$ro_review['review_id'].'" target="_blank"'.(($ro_review['status'] == '0') ? ' style="text-decoration: line-through;"' : '').'>'.mb_substr($ro_review['text'],0,50,'utf-8').' ('.$ro_review['rating'].')</a>';
					?></td>
					<td class="center"><?php echo $ro_product['status'] ? '+' : ''?></td>
				</tr>
<?php
	for ($i=1;$i<=3;$i++) {
?>				
				<tr id="add-comment-tr-<?php echo $ro_product['product_id']?>-<?php echo $i;?>" style="display: none;">
					<td colspan="7" id="add-comment-td-<?php echo $ro_product['product_id'];?>-<?php echo $i;?>">
						<form method="post" action="product-reviews-add.php" target="_blank" onsubmit="AddComment(<?php echo $ro_product['product_id'];?>,<?php echo $i;?>); return false;" id="form-<?php echo $ro_product['product_id']?>-<?php echo $i;?>">
							<input type="hidden" name="product_id" value="<?php echo $ro_product['product_id'];?>" />
							Автор:
							<input type="text" name="author" value="" />
							&nbsp;&nbsp;&nbsp;&nbsp;
							Дата:
							<select name="date_type">
								<option value="0">точно</option>
								<option value="1">день</option>
								<option value="7">неделя</option>
								<option value="30">месяц</option>
								<option value="92">квартал</option>
								<option value="365">год</option>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;
							Рейтинг:
							<select name="rating">
								<option value="5">5</option>
								<option value="4">4</option>
								<option value="3">3</option>
								<option value="2">2</option>
								<option value="1">1</option>
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;
							Отзыв:
							<textarea name="text" cols="120" rows="5" style="vertical-align: top;"></textarea>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="submit" value="Разместить отзыв <?php echo $ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'];?>" style="width: 400px;" />
						</form>
					</td>
				</tr>
<?php
	}
		}
?>
			</tbody>
		</table>
	</body>
</html>
<?php
	} else {
		header('Location: /');
	}
?>