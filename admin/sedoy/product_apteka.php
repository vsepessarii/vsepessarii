<?php
	header('Content-Type: text/html; charset=utf-8');
//	session_start();
	require 'connect.php';
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
	if ($ro_user['user_group_id'] != 1) die('222');

	$product = Result($ddb, 'SELECT * FROM product WHERE product_id="'.$_GET['product_id'].'"');
	$product_id = $product['product_id'];

	//список опций по id товара
	function GetOptions($ddb, $product_id) {
		$qu = '
			SELECT		option_description.*
			FROM			option_description,
								product_option
			WHERE			product_option.product_id="'.$product_id.'" &&
								product_option.option_id=option_description.option_id
			ORDER BY	option_description.name
		';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) $return[$ro['option_id']] = $ro['name'];
		return $return;
	}
	
	//список значений опций по id товара и опции
	function GetOptionValues($ddb, $product_id, $option_id) {
		$qu	= '
			SELECT		option_value_description.*
			FROM			option_value_description,
								product_option_value
			WHERE			product_option_value.product_id="'.$product_id.'" &&
								product_option_value.option_id="'.$option_id.'" &&
								product_option_value.option_value_id=option_value_description.option_value_id
			ORDER BY	option_value_description.name
		';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) $ret[$ro['option_value_id']] = $ro['name'];
		return $ret;
	}
	
	//дельта цены по id товара и значения опции
	function GetDeltaPrice($ddb, $product_id, $option_value_id) {
		$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT price, price_prefix FROM product_option_value WHERE product_id="'.$product_id.'" && option_value_id="'.$option_value_id.'"'));
		return ($ro['price_prefix'] == '+') ? $ro['price'] : -1*$ro['price'];
	}

	switch (isset($_POST['act'])) {
		case 'save':
			//запись списка товаров для аптеки
			mysqli_query($ddb, 'DELETE FROM product_apteka WHERE product_id="'.$product_id.'"');
			//echo '<pre>'; var_dump($_POST['items']); echo '</pre>';
			foreach ($_POST['items'] as $key => $value) if ($value['del'] != 'on') {
				@mysqli_query($ddb, '
					INSERT INTO	product_apteka
					SET					product_id="'.$product_id.'",
											egk="'.mysqli_escape_string($ddb,$value['egk']).'",
											`name`="'.mysqli_escape_string($ddb,$value['name']).'",
											firm="'.mysqli_escape_string($ddb,$value['firm']).'",
											country="'.mysqli_escape_string($ddb,$value['country']).'",
											price_coeff="'.floatval(str_replace(',','.',$value['price_coeff'])).'",
											price_diff="'.floatval(str_replace(',','.',$value['price_diff'])).'",
											price="'.floatval(str_replace(',','.',$value['price'])).'",
											comment="'.mysqli_escape_string($ddb,$value['comment']).'",
											export="'.(($value['export']) ? '1' : '0').'"
				');
				echo mysqli_error($ddb);
			}
			$msg = date('d.m.Y H:i:s').' Товары сохранены.';
			break;
	}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
  	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
  	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  	<script src="js/jquery.ui.datepicker-ru.js"></script>
    <style type="text/css">
				* {font-family: Tahoma; font-size: 12px;}
        table{border-collapse: collapse;}
        table th {background: #f0f0f0; font-weight: normal;}
        /*table td, table th {border: 1px solid #bbb;padding:3px;overflow:hidden;white-space: nowrap;}*/
        input[type=text] {min-width: 20px;border:1px solid #DDD;padding:3px;/* float:left; */}
        input[readonly] {background:none;color: #555; border: 1px solid white;}
        img.edit {float: right; margin:3px;width: 16px;height: 16px;cursor: pointer;opacity: 0.9;}
        .link {float: right; margin:3px;width: 16px;height: 16px;cursor: pointer;opacity: 0.9;}
        :hover.edit, :hover.link {opacity:1;}
        .w75 {width:65%}
        .w50 {width: 40px !important;}
        .bold {font-weight: bold;}
        
				tr.head td {
        	background-color: #333;
        	color: #FFF;
        	font-size: 14px;
        	font-weight: bold;
        	padding: 4px;
        }

				a.button {
					display: inline-block;
					border: none;
					background-color: blue;				
					padding:5px 10px;
					color: white;
					font-weight: bold;
					text-decoration: none;
					-moz-border-radius: 4px; 
					-webkit-border-radius: 4px; 
					border-radius: 4px;
				}
				
				a.red {
					background-color: red;
				}
				
				a.blue {
					background-color: blue;
				}

				
				input[type=submit] {
					border: none;
					min-width: 20px;
					background-color: blue;				
					padding:5px 10px;
					color: white;
					font-weight: bold;
					-moz-border-radius: 4px; 
					-webkit-border-radius: 4px; 
					border-radius: 4px;
				}
				
				input[type=submit].red, a.button {
					background-color: red;				
				}
				
				input[type=submit].green {
					background-color: green;				
				}
				
				div.add {
					width: 400px;
					padding: 15px;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color: #F3F3F3;
				}
				
				div.msg {
					width: 99%;
					border: none;
					-moz-border-radius: 5px; 
					-webkit-border-radius: 5px; 
					border-radius: 5px;
					background-color: #666;
					color: #FFF;
					padding: 8px;
					text-align: center;
					margin: 10px 0;
				}
				
				tr.do td { background-color: #B0FFB0; }
				tr.cancel td { background-color: #FFBBBB; }
				tr.new td { background-color: #FFFFB9; }
				
				
        
    </style>
	</head>
	<body>
		<?php if (isset($msg) && $msg != '') echo '<div class="msg"><b>'.date('H:i:s').'</b> '.$msg.'</div>'; ?>
		<form method="post" action="product_apteka.php?product_id=<?php echo  $product_id?>&rand=<?php echo  rand(0,1000000);?>">
			<input type="hidden" name="act" value="save" />		
			<table width="100%" border="1" cellpadding="3">
				<tr class="head">
					<td width="2%" align="center" title="Номер по порядку">№</td>
					<td width="2%" align="center" title="Удалить">X</td>
					<td width="2%" align="center" title="Экспортировать">E</td>
					<td width="5%" align="center" title="Код ЕГК">ЕГК</td>
					<td width="20%" align="center" title="Наименование препарата">Наименование</td>
					<td width="10%" align="center" title="Фирма производитель">Фирма</td>
					<td width="10%" align="center" title="Страна производитель">Страна</td>
					<td width="10%" align="center" title="Формула расчета цены, исходя из цены на товар">ФЦ</td>
					<td width="6%" align="center" title="Точная цена (имеет приоритет перед формулой)">Цена</td>
					<td width="25%" align="center" title="Комментарий для администратора (не экспортируется)">Комментарий</td>
				</tr>
<?php
	$qu = 'SELECT * FROM product_apteka WHERE product_id="'.$product_id.'" ORDER BY id';
	$re = @mysqli_query($ddb, $qu);
	echo @mysqli_error($ddb);
	$i = 1;
	while ($ro = @mysqli_fetch_array($re)) {
		echo '
				<tr>
					<td align="center">'.$i.'</td>
					<td align="center"><input type="checkbox" name="items['.$i.'][del]" title="Удалить" /></td>
					<td align="center"><input type="checkbox" name="items['.$i.'][export]" title="Экспортировать"'.(($ro['export']) ? ' checked' : '').' /></td>
					<td align="center"><input type="text" size="5" maxlength="10" name="items['.$i.'][egk]" value="'.htmlspecialchars($ro['egk']).'" title="Код ЕГК" /></td>
					<td align="center"><input type="text" size="50" maxlength="128" name="items['.$i.'][name]" value="'.htmlspecialchars($ro['name']).'" title="Наименование препарата (макс. 128 знаков)" /></td>
					<td align="center"><input type="text" size="25" maxlength="64" name="items['.$i.'][firm]" value="'.htmlspecialchars($ro['firm']).'" title="Фирма-производитель препарата (макс. 64 знаков)" /></td>
					<td align="center"><input type="text" size="15" maxlength="15" name="items['.$i.'][country]" value="'.htmlspecialchars($ro['country']).'" title="Страна изготовления препарата (макс. 15 знаков)" /></td>
					<td align="center">Цена * <input type="text" size="2" maxlength="5" name="items['.$i.'][price_coeff]" value="'.htmlspecialchars($ro['price_coeff']).'" title="Коэффициент цены" /> + <input type="text" size="4" maxlength="8" name="items['.$i.'][price_diff]" value="'.htmlspecialchars($ro['price_diff']).'" title="Изменение цены" /></td>
					<td align="center"><input type="text" size="10" maxlength="10" name="items['.$i.'][price]" value="'.htmlspecialchars($ro['price']).'" title="Цена (имеет приоритет над формулой)" /></td>					
					<td align="center"><input type="text" size="100" maxlength="1024" name="items['.$i.'][comment]" value="'.htmlspecialchars($ro['comment']).'" title="Комментарий (не экспортируется)" /></td>
					</td>
				</tr>
		';
		$i++;
	}
	for ($j=0;$j<=5;$j++) {
		echo '
				<tr>
					<td align="center"></td>
					<td align="center"><input type="checkbox" name="items['.$i.'][del]" title="Удалить" checked /></td>
					<td align="center"><input type="checkbox" name="items['.$i.'][export]" title="Экспортировать" /></td>
					<td align="center"><input type="text" size="5" maxlength="10" name="items['.$i.'][egk]" value="" title="Код ЕГК" /></td>
					<td align="center"><input type="text" size="50" maxlength="128" name="items['.$i.'][name]" value="" title="Наименование препарата (макс. 128 знаков)" /></td>
					<td align="center"><input type="text" size="25" maxlength="64" name="items['.$i.'][firm]" value="" title="Фирма-производитель препарата (макс. 64 знаков)" /></td>
					<td align="center"><input type="text" size="15" maxlength="15" name="items['.$i.'][country]" value="" title="Страна изготовления препарата (макс. 15 знаков)" /></td>
					<td align="center">Цена * <input type="text" size="2" maxlength="5" name="items['.$i.'][price_coeff]" value="1" title="Коэффициент цены" /> + <input type="text" size="4" maxlength="8" name="items['.$i.'][price_diff]" value="0" title="Изменение цены" /></td>
					<td align="center"><input type="text" size="10" maxlength="10" name="items['.$i.'][price]" value="" title="Цена (имеет приоритет над формулой)" /></td>					
					<td align="center"><input type="text" size="100" maxlength="1024" name="items['.$i.'][comment]" value="" title="Комментарий (не экспортируется)" /></td>
					</td>
				</tr>
		';
		$i++;
	}
?>				
			</table>
			<br />
			<center><input type="submit" value="Сохранить товары" /></center>
	</body>
</html>