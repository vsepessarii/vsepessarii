<?
	//макросы изменения скидок
	require 'connect.php';

/*	mb_internal_encoding("UTF-8");
	header('Content-Type: text/html; charset=utf-8');
	session_start();
	$ddb = @mysqli_connect('localhost','briefmag_brief','brief2013');
	@mysqli_query($ddb, "set names utf8");
	if (!$ddb) {
		echo "Don't connect to database";
		exit;
	}
	if ($_GET['p'] == '') $_GET['p'] = 1;
	@mysqli_select_db('briefmag_db');
	$ro_user = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT user_group_id FROM user WHERE user_id="'.intval($_SESSION['user_id']).'"'));
*/	
	if ($ro_user['user_group_id'] == 1) {

		//удаление макроса
		if ($_GET['act'] == 'del') {
			$id = intval($_GET['id']);
			@mysqli_query($ddb, 'DELETE FROM macros_discount WHERE ID="'.$id.'"');
			@mysqli_query($ddb, 'DELETE FROM macros_discount_product WHERE MID="'.$id.'"');
			@mysqli_query($ddb, 'DELETE FROM macros_discount_action WHERE MID="'.$id.'"');
			$tpl['msg'] = 'Макрос удален.';
			$_GET['act'] = '';
			$_GET['id'] = '';
		}
		
		switch ($_POST['act']) {
			case 'add':
				@mysqli_query($ddb, '
					INSERT INTO macros_discount 
					SET	TITLE="'.mysqli_escape_string($_POST['title']).'",
							COMMENT="'.mysqli_escape_string($_POST['comment']).'",
							STATUS="'.mysqli_escape_string($_POST['status']).'"
				');
				$_GET['id'] = @mysqli_insert_id($ddb);
				$_GET['act'] = 'edit';
				$tpl['msg'] = 'Макрос " '.$_POST['title'].'" добавлен. Укажите дату и время действий товаров и список товаров со скидками';
				break;
				
			case 'edit':
				$_POST['id'] = intval($_POST['id']);
				@mysqli_query($ddb, '
					UPDATE macros_discount
					SET	TITLE="'.mysqli_escape_string($_POST['title']).'",
							COMMENT="'.mysqli_escape_string($_POST['comment']).'",
							STATUS="'.mysqli_escape_string($_POST['status']).'"
					WHERE ID="'.$_POST['id'].'"
				');
				echo mysqli_error($ddb);
				
				//устанавливаем действия
/*
				@mysqli_query($ddb, 'DELETE FROM macros_special_action WHERE MID="'.$_POST['id'].'"');
				$actions = explode(chr(13).chr(10),$_POST['actions']);
				foreach ($actions as $act) {
					$year = mb_substr($act,0,4);
					$month = mb_substr($act,5,2);
					$day = mb_substr($act,8,2);
					$hour = mb_substr($act,11,2);
					$minute = mb_substr($act,14,2);
					$action = mb_substr($act,17,1);
					if ($year > '0000' && $year < '2999' && $month >= '01' && $month <= '12' && $day >= '01' && $day <= '31' && $hour >= '00' && $hour <= '23' && $minute >= '00' && $minute <= '59' && ($action == '+' || $action == '-')) @mysqli_query($ddb, '
						INSERT INTO macros_special_action
							SET	MID="'.$_POST['id'].'",
							YEAR="'.$year.'",
								MONTH="'.$month.'",
								DAY="'.$day.'",
								HOUR="'.$hour.'",
								MINUTE="'.$minute.'",
								ACTION="'.$action.'"
					');
				}
*/				
				@mysqli_query($ddb, 'DELETE FROM macros_discount_action WHERE MID="'.$_POST['id'].'"');
				for ($i=0;$i<200;$i++) if ($_POST['action'.$i] != '') {
					$year = mb_substr($_POST['date'.$i],0,4);
					$month = mb_substr($_POST['date'.$i],5,2);
					$day = mb_substr($_POST['date'.$i],8,2);
					$weekday = $_POST['weekday'.$i];
					$hour = $_POST['hour'.$i];
					$minute = $_POST['minute'.$i];
					$action = $_POST['action'.$i];
					@mysqli_query($ddb, '
						INSERT INTO macros_discount_action
							SET	MID="'.$_POST['id'].'",
							YEAR="'.$year.'",
								MONTH="'.$month.'",
								DAY="'.$day.'",
								WEEKDAY="'.$weekday.'",
								HOUR="'.$hour.'",
								MINUTE="'.$minute.'",
								ACTION="'.$action.'"
					');
				}
				
				//устанавливаем товары для обработки и цены на них
/*
				@mysqli_query($ddb, 'DELETE FROM macros_special_product WHERE MID="'.$_POST['id'].'"');
				$products = explode(chr(13).chr(10),$_POST['products']);
				foreach ($products as $product) {
					$tmp = explode('-',$product);
					$product_id = trim(intval($tmp[0]));
					$price = trim(floatval($tmp[1]));
					if ($product_id > 0 && $price > 0) @mysqli_query($ddb, '
						INSERT INTO macros_special_product
						SET	MID="'.$_POST['id'].'",
								PID="'.$product_id.'",
								PRICE="'.$price.'"
					');
				}
*/				
				@mysqli_query($ddb, 'DELETE FROM macros_discount_product WHERE MID="'.$_POST['id'].'"');
				for ($i=0;$i<200;$i++) if (floatval($_POST['price'.$i]) > 0 || floatval($_POST['discount'.$i]) > 0) @mysqli_query($ddb, '
					INSERT INTO macros_discount_product
					SET	MID="'.$_POST['id'].'",
							PID="'.$_POST['pid'.$i].'",
							PRICE="'.floatval($_POST['price'.$i]).'",
							PERCENT="'.floatval($_POST['discount'.$i]).'"
				'); 
				
				//var_dump($_POST);
				
				$_GET['act'] = 'edit';
				$_GET['id'] = $_POST['id'];
				$tpl['msg'] = 'Макрос "'.$_POST['title'].'" изменен.';
				break;
		}

		
		$tpl['body_content'] = '
			<style>
				div.msg {
					width:100%;
					-moz-border-radius: 10px; 
					-webkit-border-radius: 10px; 
					border-radius: 10px;
					background-color:#eee;
					border:1px solid #666;
					text-align:center;
					color: red;
					font-size:14px;
					font-weight: bold;
					pagging: 10px;
					margin: 10px 0;	
				}
				
				a {
					font-weight:bold;
					text-decoration:none !important;	
				}
				
				td {
					padding: 5px !important;	
				}
				
				.gray {
					border-color: #BBB !important;
					color: #BBB;	
				}
				
			</style>
			<script language="javascript">
				function conf(txt,url) {
					if (confirm(txt)) {
						parent.location=url;
					} else {
					}
				}
			</script>
			
			
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="25% align="left"><a href="'.$_SERVER['PHP_SELF'].'?act=add" class="button">Новый макрос</a></td>
					<td width="75%" align="right"><a href="/index.php?route=common/home&token='.$_SESSION['token'].'">Вернуться в панель управления</a>
					</td>
				</tr>
			</table>
			<br>
			'.(($tpl['msg'] != '') ? '<div class="msg">'.$tpl['msg'].'</div>' : '').'
			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="">
				<tr>
					<td valign="top" width="50%">
						<table class="list">
							<thead>
								<tr>
									<td width="5%"></td>
									<td width="25%">Название</td>
									<td width="55%">Комментарий</td>
									<td width="15%">Работает</td>
								</tr>
							</thead>
		';
		$qu = 'SELECT * FROM macros_discount ORDER BY ID';
		$re = @mysqli_query($ddb, $qu);
		while ($ro = @mysqli_fetch_array($re)) {
			$tpl['body_content'] .= '
							<tbody>
								<tr>
									<td align="center"><a href="#" onClick="javascript:conf(\'Вы уверены? Удаление  НЕЛЬЗЯ отменить!\',\'?act=del&page='.$_GET['page'].'&id='.$ro['ID'].'\');"><img src="img/del.png" border="0" width="13" height="13" valign="middle"></a></td>
									<td><a href="?act=edit&id='.$ro['ID'].'" style="'.(($ro['ID'] == $_GET['id']) ? 'color:red; ' : '').'">'.$ro['TITLE'].'</a></td>
									<td><a href="?act=edit&id='.$ro['ID'].'" style="'.(($ro['ID'] == $_GET['id']) ? 'color:red; ' : '').'">'.mb_substr($ro['COMMENT'],0,35).'</a></td>
									<td><a href="?act=edit&id='.$ro['ID'].'" style="'.(($ro['ID'] == $_GET['id']) ? 'color:red; ' : '').'">'.(($ro['STATUS']) ? 'Да' : 'Нет').'</a></td>
								</tr>
							</tbody>
			';
		}

		$tpl['body_content'] .= '
						</table>
					</td>
					<td class="right" valign="top" width="50%">
		';
		if ($_GET['act'] == 'add' || $_GET['act'] == 'edit') {
			if ($_GET['act'] == 'add') {
				$tpl['meta_title'] = 'Добавить макрос скидок';
				$tpl['body_title'] = 'Добавить макрос скидок';
			} elseif ($_GET['act'] == 'edit') {
				$tpl['meta_title'] = 'Обновить макрос скидок';
				$tpl['body_title'] = 'Обновить макрос скидок';
				$ro = @mysqli_fetch_array(@mysqli_query($ddb, 'SELECT * FROM macros_discount WHERE ID="'.intval($_GET['id']).'"'));
			} else {
				$tpl['meta_title'] = 'Макросы скидок';
				$tpl['body_title'] = 'Макросы скидок';
			}
			$tpl['body_content'] .= '
				<script language="javascript">
					function Validate(form) {
						var br = String.fromCharCode(13) + String.fromCharCode(10);
						var msg = "";
						if (form.title.value == "") msg = msg + br + " - не заполнено название;";
						if (msg == "") {
							form.submit();
					 	} else {
					 		alert("Вы допустили следующие ошибки:" + msg);
				 		}
					}
				</script>
				<form action="'.$_SERVER['PHP_SELF'].'" method="post" onSubmit="Validate(this); return false;">
				<input type="hidden" name="act" value="'.$_GET['act'].'">
				<input type="hidden" name="id" value="'.$_GET['id'].'">
				<table width="100%" class="" style="border:0 important;">
					<tr>
						<td width="15%" class="txt" align="right">Название:</td>
						<td width="85%" class="txt" align="left"><input name="title" id="title" title="Название макроса" class="txt" style="width:200px;" value="'.$ro['TITLE'].'"></td>
					</tr>
					<tr>
						<td class="txt" align="right" valign="top">Комментарий:</td>
						<td class="txt" align="left"><textarea name="comment" title="Комментарий к макросу" class="txt" style="width:400px; height:70px;">'.$ro['COMMENT'].'</textarea></td>
					</tr>
			';
			if ($_GET['act'] == 'edit') {
				/*
				unset($actions);
				$qu_tmp = 'SELECT * FROM  macros_special_action WHERE MID="'.$ro['ID'].'" ORDER BY YEAR, MONTH, DAY, HOUR, MINUTE';
				$re_tmp = @mysqli_query($ddb, $qu_tmp);
				//echo $qu_tmp;
				while ($ro_tmp = @mysqli_fetch_array($re_tmp)) $actions[] = $ro_tmp['YEAR'].'-'.$ro_tmp['MONTH'].'-'.$ro_tmp['DAY'].' '.$ro_tmp['HOUR'].':'.$ro_tmp['MINUTE'].' '.$ro_tmp['ACTION'];
				unset($products);
				$qu_tmp = 'SELECT * FROM  macros_special_product WHERE MID="'.$ro['ID'].'" ORDER BY ID';
				$re_tmp = @mysqli_query($ddb, $qu_tmp);
				while ($ro_tmp = @mysqli_fetch_array($re_tmp)) $products[] = $ro_tmp['PID'].' - '.$ro_tmp['PRICE'];
				*/ 
				$tpl['body_content'] .= '
					<!--tr>
						<td class="txt" align="right">Действия:</td>
						<td class="txt" align="left"><textarea name="actions" title="Действия в формате 2013-02-08 15:30 +" class="txt" style="width:200px; height:100px;">'.@implode(chr(13).chr(10),$actions).'</textarea></td>
					</tr-->	
					<!--tr>
						<td class="txt" align="right">Товары:</td>
						<td class="txt" align="left"><textarea name="products" title="Товары в формате 1234 - 755.10" class="txt" style="width:200px; height:100px;">'.@implode(chr(13).chr(10),$products).'</textarea></td>
					</tr-->	
				';
				$tpl['body_content'] .= '
						<tr>
							<td colspan="2">
								<span style="display:inline-block; width:75px; text-align:center;"><strong>Дата</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span style="display:inline-block; width:45px; text-align:center;"><strong>День</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span style="display:inline-block; width:92px; text-align:center;"><strong>Время</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<span style="display:inline-block; width:100px;"><strong>Действие</strong></span>
								<br>
				';
				$qu_tmp = 'SELECT * FROM  macros_discount_action WHERE MID="'.$ro['ID'].'" ORDER BY YEAR, MONTH, DAY, HOUR, MINUTE';
				$re_tmp = @mysqli_query($ddb, $qu_tmp);
				for ($i=0;$i<mysqli_num_rows($re_tmp)+5;$i++) {
					$ro_tmp = @mysqli_fetch_array($re_tmp);
					$tpl['body_content'] .= '
  							<input id="date'.$i.'" name="date'.$i.'" title="Дата выполения макроса" style="width:75px; text-align:center;" class="txt" value="'.(($ro_tmp['YEAR'] > 0) ? $ro_tmp['YEAR'].'-'.$ro_tmp['MONTH'].'-'.$ro_tmp['DAY'] : '').'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  							<select name="weekday'.$i.'" title="День недели" style="width:45px;" class="txt">
  								<option value=""></option>
  								<option value="Mon"'.(($ro_tmp['WEEKDAY'] == 'Mon') ? ' selected' : '').'>Пн</option>
  								<option value="Tue"'.(($ro_tmp['WEEKDAY'] == 'Tue') ? ' selected' : '').'>Вт</option>
  								<option value="Wed"'.(($ro_tmp['WEEKDAY'] == 'Wed') ? ' selected' : '').'>Ср</option>
  								<option value="Thu"'.(($ro_tmp['WEEKDAY'] == 'Thu') ? ' selected' : '').'>Чт</option>
  								<option value="Fri"'.(($ro_tmp['WEEKDAY'] == 'Fri') ? ' selected' : '').'>Пт</option>
  								<option value="Sat"'.(($ro_tmp['WEEKDAY'] == 'Sat') ? ' selected' : '').'>Сб</option>
  								<option value="Sun"'.(($ro_tmp['WEEKDAY'] == 'Sun') ? ' selected' : '').'>Вс</option>
  							</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<select name="hour'.$i.'" style="width:44px;">
									<option value=""></option>
					';
					for ($j=0;$j<=23;$j++) $tpl['body_content'] .= '<option value="'.sprintf('%02d',$j).'"'.(($ro_tmp['HOUR'] == sprintf('%02d',$j)) ? ' selected' : '').'>'.sprintf('%02d',$j).'</option>';
					$tpl['body_content'] .= '
								</select>
								:
								<select name="minute'.$i.'" style="width:44px;">
									<option value=""></option>
					';
					for ($j=0;$j<=59;$j++) $tpl['body_content'] .= '<option value="'.sprintf('%02d',$j).'"'.(($ro_tmp['MINUTE'] == sprintf('%02d',$j)) ? ' selected' : '').'>'.sprintf('%02d',$j).'</option>';
					$tpl['body_content'] .= '
								</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  							<select id="action'.$i.'" name="action'.$i.'" title="(+) установка акционной цены, (-) сброс акционной цены, ( ) удаление действия" class="txt">
  								<option value=""></option>
  								<option value="+"'.(($ro_tmp['ACTION'] == '+') ? ' selected' : '').'>+</option>
  								<option value="-"'.(($ro_tmp['ACTION'] == '-') ? ' selected' : '').'>-</option>
  							<select>
  							<script> $(function() { $("#date'.$i.'" ).datepicker(); }); </script>
  							<br>

					';
				}
				$tpl['body_content'] .= '								
								
								
								<script>
									$(function() {
				';
				$qu_tmp = 'SELECT * FROM  macros_discount_product WHERE MID="'.$ro['ID'].'" ORDER BY ID';
				$re_tmp = @mysqli_query($ddb, $qu_tmp);
				for ($i=0;$i<mysqli_num_rows($re_tmp)+5;$i++) $tpl['body_content'] .= '
										$("#product'.$i.'").autocomplete({
											source: "ajax/product.php",
											minLength: 2,
											select: function( event, ui ) {
												$("#pid'.$i.'").val(ui.item.id);
												$("#price_original'.$i.'").val(ui.item.price);
											}
										});
				';
				$tpl['body_content'] .= '
									});
									</script>						
								<!--div class="ui-widget"-->
									<span style="display:inline-block; width:40px; text-align:center;"><strong>ID</strong></span>
									<span style="display:inline-block; width:500px; text-align:left;"><strong>Товар</strong></span>
									<span style="display:inline-block; width:60px; text-align:center;"><strong>Цена</strong></span>
									<span style="display:inline-block; width:80px; text-align:center;" title="Цена со скидкой"><strong>Цена (ск)</strong></span>
									<span style="display:inline-block; width:80px; text-align:center;" title="Процент скидки"><strong>Процент (ск)</strong></span>
									<br>
				';
				$re_tmp = @mysqli_query($ddb, $qu_tmp);
				for ($i=0;$i<mysqli_num_rows($re_tmp)+5;$i++) {
					$ro_tmp = @mysqli_fetch_array($re_tmp);
					$qu_product = '
						SELECT	product_description.product_id as id,
										product_description.name as name,
										manufacturer.name as manufacturer,
										product.model as model,
										product.price as price
						FROM		product_description,
										product LEFT JOIN manufacturer ON product.manufacturer_id = manufacturer.manufacturer_id
						WHERE		product.product_id = product_description.product_id &&
										product.product_id = "'.$ro_tmp['PID'].'"
					';
					//echo $qu_product.'<br>';
					$re_product = @mysqli_query($ddb, $qu_product);
					//echo mysqli_error($ddb);
					$ro_product = @mysqli_fetch_array($re_product);
					$tpl['body_content'] .= '
	  							<input id="pid'.$i.'" name="pid'.$i.'" readonly="readonly" title="id" style="width:40px; text-align:center;" class="txt gray" value="'.$ro_product['id'].'" />
	  							<input id="product'.$i.'" name="product'.$i.'" title="Товар" style="width:500px;" class="txt" value="'.$ro_product['name'].' '.$ro_product['manufacturer'].' '.$ro_product['model'].'" />
	  							<!--input id="price'.$i.'" name="price'.$i.'" readonly="readonly" style="width:60px; text-align:center;" title="Цена" class="txt gray" value="'.((floatval($ro_product['price']) > 0) ? floatval($ro_product['price']) : '').'" /-->
	  							<input id="price_original'.$i.'" name="price_original'.$i.'" readonly="readonly" style="width:60px; text-align:center;" title="Цена" class="txt gray" value="'.((floatval($ro_product['price']) > 0) ? floatval($ro_product['price']) : '').'" />
	  							<input id="price'.$i.'" name="price'.$i.'" style="width:80px; text-align:center;" title="Цена со скидкой" class="txt" value="'.(($ro_tmp['PRICE'] > 0) ? $ro_tmp['PRICE'] : '').'" />
	  							<input id="percent'.$i.'" name="percent'.$i.'" style="width:80px; text-align:center;" title="Процент скидки (имеет приоритет)" class="txt" value="'.(($ro_tmp['PERCENT'] > 0) ? $ro_tmp['PERCENT'] : '').'" />
	  							<br>
					';
				}			
				
				$tpl['body_content'] .= '
								<!--/div-->
							</td>
						</tr>
				';
				
			}
			$tpl['body_content'] .= '					
					<tr>
						<td class="txt" align="right">Работает:</td>
						<td class="txt" align="left">
							<select name="status">
								<option value="0"'.(($ro['STATUS'] == '0') ? ' selected' : '').'>нет</option>
								<option value="1"'.(($ro['STATUS'] == '1') ? ' selected' : '').'>да</option>
							</select>
						</td>
					</tr>
										<tr>
						<td></td>
						<td><input type="submit" class="txt" value="'.(($_GET['act'] == 'add') ? 'Добавить' : 'Обновить').' макрос скидок"></td>
					</tr>
			';
			$tpl['body_content'] .= '
				</table>			
				</form>
			';
		}
		require 'template.php';	
	} else {
		header('Location: /');
	}
?>