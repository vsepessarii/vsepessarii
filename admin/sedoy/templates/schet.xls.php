<?php
	require '../connect.php';
	require '../functions.php';
	error_reporting(0);
	mb_internal_encoding('utf-8');
	ini_set('display_errors', FALSE);
	ini_set('display_startup_errors', FALSE);
	
	if ($ro_user['user_group_id'] != 1) die('Access denied');

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
	date_default_timezone_set('Europe/London');

	/** PHPExcel_IOFactory */
	require_once '../class/PHPExcel/IOFactory.php';

	//echo date('H:i:s') , " Load from Excel5 template" , EOL;

	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("schet.xls");

//	die('123');




	//$objPHPExcel->setActiveSheetIndex(0);

	//$objPHPExcel->getActiveSheet()->setCellValue('BC40', '77.20');
	
	$ro_order = Result($ddb, 'SELECT * FROM `order` WHERE order_id="'.intval($_GET['id']).'"');

	$ro_shipping = Result($ddb, 'SELECT * FROM order_total WHERE order_id='.$ro_order['order_id'].' && code="shipping"');
	
	$qu_product = 'SELECT * FROM order_product WHERE order_id="'.$ro_order['order_id'].'"';
	$nu_product = @mysqli_num_rows(@mysqli_query($ddb, $qu_product));
	$re_product = @mysqli_query($ddb, $qu_product);

	
	$order_date = substr($ro_order['date_added'],0,10);
	$order_date = Date2Str($order_date);

	$client = htmlspecialchars_decode($ro_order['doc_firm']).', '.', ИНН: '.$ro_order['doc_inn'].', КПП: '.$ro_order['doc_kpp'].', '.htmlspecialchars_decode($ro_order['doc_address']);

	$objPHPExcel->getActiveSheet()->setCellValue('A7', 'Счет №'.$ro_order['order_id'].' от '.$order_date);	

	$objPHPExcel->getActiveSheet()->setCellValue('F11', $client);

	$objPHPExcel->getActiveSheet()->setCellValue('Y3', ' '.GetSetting('config_firm_bank_account').' ');
	
	/*
	$objPHPExcel->getActiveSheet()->setCellValue('AY21', $ro_order['order_id']);
	$objPHPExcel->getActiveSheet()->setCellValue('AY22', $order_date);
	$objPHPExcel->getActiveSheet()->setCellValue('AL28', $order_date);
	$objPHPExcel->getActiveSheet()->setCellValue('I12', $client);
	$objPHPExcel->getActiveSheet()->setCellValue('I18', 'тот же');
	*/

	//увеличиваем количество товарных строк, если надо
	if ($nu_product >1) {
		$objPHPExcel->getActiveSheet()->insertNewRowBefore(15,$nu_product-1);
	
		for ($i=14;$i<14+$nu_product;$i++) {
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('C'.$i.':Q'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('R'.$i.':T'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('U'.$i.':W'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('X'.$i.':AB'.$i);
			$objPHPExcel->getActiveSheet()->mergeCells('AC'.$i.':AG'.$i);
			//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle ('BC34'), 'BC'.$i);
			//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle ('B34:BC34'), 'B'.$i.':BC'.$i);
		}
	}
	
	//заполняем товарные строки
	$i = 14;
	while ($ro_product = @mysqli_fetch_array($re_product)) {
		$ro_prod = Result($ddb, 'SELECT * FROM product WHERE product_id = "'.$ro_product['product_id'].'"');
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $i-13); //номер по порядку
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, ProductTitle($ro_product['product_id'])); //наименование товара
		$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, 'шт'); //наименование ЕИ
		$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $ro_product['quantity']); //кол-во
		$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $ro_product['price']); //цена
		$objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, '=U'.$i.'*X'.$i); //сумма без НДС
		$i++;
	}

	//доставка
	$objPHPExcel->getActiveSheet()->setCellValue('A'.(15+$nu_product), $ro_shipping['title']); //наименование
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.(15+$nu_product), round($ro_shipping['value'])); //сумма

	//считаем суммы
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.(14+$nu_product), '=SUM(AC14:AC'.(13+$nu_product).')');
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.(16+$nu_product), '=SUM(AC'.(14+$nu_product).':AC'.(15+$nu_product).')');

	//сумма прописью
	$objPHPExcel->getActiveSheet()->setCellValue('A'.(18+$nu_product), mb_ucfirst(Summ2Str($ro_order['total']))); //сумма c НДС

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="schet_'.$ro_order['order_id'].'.xls"');
	header('Cache-Control: max-age=0');
	//echo date('H:i:s') , " Write to Excel5 format" , EOL;
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('tmp.xls');
	echo file_get_contents('tmp.xls');
	unlink('tmp.xls');
	/*
	$objWriter->save('torg12_'.date('H-i-s').'.xls');
	echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
	echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
	echo date('H:i:s') , " Done writing file" , EOL;
	echo 'File has been created in ' , getcwd() , EOL;
	*/

