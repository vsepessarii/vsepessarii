<?php
$host = $_SERVER['HTTP_HOST'];

// HTTP
//define('HTTP_SERVER', $host.'admin/');
define('HTTP_SERVER', 'http://'.$host.'/');
define('HTTP_CATALOG', $host.'');
define('HTTP_IMAGE', $host.'/image/');

// HTTPS
define('HTTPS_SERVER', $host.'/admin/');
define('HTTPS_CATALOG', $host.'/');
define('HTTPS_IMAGE', $host.'/image/');

// DIR
//$dir = dirname(__FILE__);
$dir = '/home/vagrant/www/admin.vsepessarii.test';
define('DIR_APPLICATION', $dir.'/admin/');
define('DIR_SYSTEM', $dir.'/system/');
define('DIR_DATABASE', $dir.'/system/database/');
define('DIR_LANGUAGE', $dir.'/admin/language/');
define('DIR_TEMPLATE', $dir.'/admin/view/template/');
define('DIR_CONFIG', $dir.'/system/config/');
define('DIR_SAVE_IMAGE', $dir.'/admin/image/');
define('DIR_IMAGE', '/home/vagrant/www/vsepessarii.dev/public/image/');
define('DIR_CACHE', $dir.'/system/cache/');
define('DIR_DOWNLOAD', $dir.'/download/');
define('DIR_LOGS', $dir.'/system/logs/');
define('DIR_CATALOG', $dir.'/catalog/');

// DB
define('DB_DRIVER', 'mysqliz');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'homestead');
define('DB_PASSWORD', 'secret');
	define('DB_DATABASE', 'vsepessarii');
define('DB_PREFIX', '');
